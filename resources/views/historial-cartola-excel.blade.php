<html>
<style type="text/css">
@media screen {
  @font-face {
  font-family: 'Avenir';
  font-style: normal;
  font-weight: 400;
  src: local('Avenir Book'), local('Avenir-Book'), url(https://elinous.cl/swconstructora/app/AvenirLTStd-Book.9334084d0d28fc558b10.woff2) format('woff2');
}

  @font-face {
  font-family: 'Avenir';
  font-style: normal;
  font-weight: 500;
  src: local('Avenir Heavy'), local('Avenir-Heavy'), url(https://elinous.cl/swconstructora/app/AvenirLTStd-Heavy.a30d5d1a2ef6734a676c.woff2) format('woff2');
  }
}
  body {
    font-family: "Avenir", Tahoma, Sans-Serif;
    font-weight: 400;
  }
</style>
<table xmlns="http://www.w3.org/1999/xhtml" border="0" cellspacing="0" cellpadding="0" >
  <colgroup>
    <col width="180" />
    <col width="299" />
    <col width="100" />
    <col width="226" />
    <col width="226" />
    <col width="176" />
    <col width="163" />
    <col width="149" />
    <col width="78" />
  </colgroup>
  <tr>
    <td style="padding: 10px;">
      <!-- imagen de los dioses -->
      <img src="../resources/img/powered-budgeter.png">
    </td>
  </tr>
  <tr style="background: #bfbfbf;">
    <td style="text-align:left;width:41.15mm;font-weight: 500; background: #bfbfbf;  border-width: 0.0261cm;  border-color: #000000;border-style: solid;">
      <p>N° Cartola</p>
    </td>
    <td style="text-align:left;width:68.32mm;font-weight: 500; background: #bfbfbf;  border-width: 0.0261cm;  border-color: #000000;border-style: solid;">
      <p>Razón Social</p>
    </td>
    <td style="text-align:left;width:35.84mm;font-weight: 500; background: #bfbfbf;  border-width: 0.0261cm;  border-color: #000000;border-style: solid;" >
      <p>Tipo</p>
    </td>
    <td style="text-align:left;width:51.79mm; font-weight: 500;background: #bfbfbf;  border-width: 0.0261cm;  border-color: #000000;border-style: solid;">
      <p>RUT</p>
    </td>
    <td style="text-align:left;width:51.79mm;font-weight: 500; background: #bfbfbf;  border-width: 0.0261cm;  border-color: #000000;border-style: solid;">
      <p>Total neto Facturado</p>
    </td>
    <td style="text-align:left;width:40.32mm;font-weight: 500; background: #bfbfbf;  border-width: 0.0261cm;  border-color: #000000;border-style: solid;">
      <p>IVA total</p>
    </td>
    <td style="text-align:left;width:37.24mm; font-weight: 500;background: #bfbfbf; border-width: 0.0261cm;   border-color: #000000;border-style: solid;">
      <p>Total final</p>
    </td>
  </tr>
  <tr >
    <td style="text-align:left;width:41.15mm;  border-width: 0.0261cm;  border-color: #000000;border-style: solid;">{{$nro_cart}}</td>
    <td style="text-align:left;width:68.32mm;   border-width: 0.0261cm;  border-color: #000000;border-style: solid;">{{$razon_so}}</td>
    <td style="text-align:left;width:35.84mm;   border-width: 0.0261cm;  border-color: #000000;border-style: solid;" >{{$tipo_emp}}</td>
    <td style="text-align:left;width:51.79mm;   border-width: 0.0261cm;  border-color: #000000;border-style: solid;" >{{$rut_empr}}</td>
    <td style="text-align:left;width:51.79mm;   border-width: 0.0261cm;  border-color: #000000;border-style: solid;" >{{$tot_ne_f}}</td>
    <td style="text-align:left;width:40.32mm;   border-width: 0.0261cm;  border-color: #000000;border-style: solid;" >{{$tota_iva}}</td>
    <td style="text-align:left;width:37.24mm;   border-width: 0.0261cm;  border-color: #000000;border-style: solid;" >{{$tot_fina}}</td>
  </tr>
  <tr>
    <td style="text-align:left;width:41.15mm;font-weight: 500; background: #bfbfbf;  border-width: 0.0261cm;  border-color: #000000;border-style: solid;" >
      <p>N° Factura</p>
    </td>
    <td style="text-align:left;width:68.32mm;font-weight: 500; background: #bfbfbf;  border-width: 0.0261cm;  border-color: #000000;border-style: solid;" >
      <p>Neto factura</p>
    </td>
    <td style="text-align:left;width:35.84mm;font-weight: 500; background: #bfbfbf;  border-width: 0.0261cm;  border-color: #000000;border-style: solid;" >
      <p>Neto Nota de Crédito</p>
    </td>
    <td style="text-align:left;width:51.79mm;font-weight: 500; background: #bfbfbf; border-width: 0.0261cm;   border-color: #000000;border-style: solid;" >
      <p>Neto aprobado</p>
    </td>
    <td style="text-align:left;width:51.79mm;font-weight: 500; background: #bfbfbf;  border-width: 0.0261cm;  border-color: #000000;border-style: solid;" >
      <p>IVA</p>
    </td>
    <td style="text-align:left;width:40.32mm;font-weight: 500; background: #bfbfbf;  border-width: 0.0261cm;  border-color: #000000;border-style: solid;" >
      <p>Subtotal a pagar</p>
    </td>
    <td style="text-align:left;width:37.24mm;font-weight: 500; background: #bfbfbf;  border-width: 0.0261cm;  border-color: #000000;border-style: solid;" >
      <p>Vencimiento</p>
    </td>
  </tr>
  @foreach($detalles as $dato)
    <tr>
      <td style="text-align:left;width:41.15mm;   border-width: 0.0261cm;  border-color: #000000;border-style: solid;" >{{$dato->nro_fact}}</td>
      <td style="text-align:left;width:68.32mm;   border-width: 0.0261cm;  border-color: #000000;border-style: solid;" >{{$dato->net_fact}}</td>
      <td style="text-align:left;width:35.84mm;   border-width: 0.0261cm;  border-color: #000000;border-style: solid;" >{{$dato->net_no_c}}</td>
      <td style="text-align:left;width:51.79mm;   border-width: 0.0261cm;  border-color: #000000;border-style: solid;" >{{$dato->net_apro}}</td>
      <td style="text-align:left;width:51.79mm;   border-width: 0.0261cm;  border-color: #000000;border-style: solid;" >{{$dato->iva_deta}}</td>
      <td style="text-align:left;width:40.32mm;   border-width: 0.0261cm;  border-color: #000000;border-style: solid;" >{{$dato->tota_iva}}</td>
      <td style="text-align:left;width:37.24mm;   border-width: 0.0261cm;  border-color: #000000;border-style: solid;" >{{$dato->vencimie}}</td>
    </tr>
  @endforeach
</table>
</html>
