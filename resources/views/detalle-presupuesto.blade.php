<html>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <style type="text/css">
  @media screen {
    @font-face {
    font-family: 'Avenir';
    font-style: normal;
    font-weight: 400;
    src: local('Avenir Book'), local('Avenir-Book'), url(https://elinous.cl/swconstructora/app/AvenirLTStd-Book.9334084d0d28fc558b10.woff2) format('woff2');
  }

    @font-face {
    font-family: 'Avenir';
    font-style: normal;
    font-weight: 500;
    src: local('Avenir Heavy'), local('Avenir-Heavy'), url(https://elinous.cl/swconstructora/app/AvenirLTStd-Heavy.a30d5d1a2ef6734a676c.woff2) format('woff2');
    }
  }
    body {
      font-family: "Avenir", Tahoma, Sans-Serif;
      font-weight: 400;
    }
  </style>

  <style>
    table { border-collapse: collapse; border: 1px solid black; }
    thead { display: table-header-group; }
    tr { border-collapse: collapse; border: 1px solid black; }
    td { border-collapse: collapse; border: 1px solid black; }
  </style>

  <table xmlns="http://www.w3.org/1999/xhtml" border="1" cellpadding="5" class="ta2">
    <colgroup>
      <col width="251" />
      <col width="161" />
      <col width="143" />
      <col width="91" />
    </colgroup>
    <thead>
      <tr class="ro1">
        <td style="text-align:center;width:57.41mm; font-weight: 500;" class="ce12"><p>Categoría</p></td>
        <td style="text-align:center;width:36.95mm; font-weight: 500;" class="ce12"><p>Subcategoria</p></td>
        <td style="text-align:center;width:32.76mm; font-weight: 500;" class="ce12"><p>Presupuesto</p></td>
      </tr>
    </thead>
    <tbody>
      @foreach($categorias as $categoria)
      <tr class="ro1">
        @if ($categoria->cabecera)
          <td rowspan="{{$categoria->montoC}}" style="text-align:center; width:57.41mm; " class="ce5"><p>{{$categoria->nombreC}}</p></td>
        @endif
        <td style="text-align:left;width:36.95mm; " class="ce7"><p>{{$categoria->nombre}}</p></td>
        <td style="text-align:right;width:32.76mm; " class="ce7"><p>${{$categoria->monto}}</p></td>
      </tr>
      @endforeach
    </tbody>
  </table>
</html>
