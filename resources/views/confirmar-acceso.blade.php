<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Confirmar acceso</title>
    <style type="text/css">
    @media screen {
      @font-face {
      font-family: 'Avenir';
      font-style: normal;
      font-weight: 400;
      src: local('Avenir Book'), local('Avenir-Book'), url(https://elinous.cl/swconstructora/app/AvenirLTStd-Book.9334084d0d28fc558b10.woff2) format('woff2');
    }

      @font-face {
      font-family: 'Avenir';
      font-style: normal;
      font-weight: 500;
      src: local('Avenir Heavy'), local('Avenir-Heavy'), url(https://elinous.cl/swconstructora/app/AvenirLTStd-Heavy.a30d5d1a2ef6734a676c.woff2) format('woff2');
      }
    }
      body {
        font-family: "Avenir", Tahoma, Sans-Serif;
        font-weight: 400;
      }
    </style>
  </head>
  <body>
<!-- tabla conntenedora 1 -->
  <table width="100%" cellspacing="0" cellpadding="0" border="0" style="background-color:#fff;" >
    <tr>
      <td>
        <!--tabla de 600 px-->
        <table width="600" cellspacin="0" cellpadding="0" border="0"  style="margin:0 auto; padding:15px 0;" >
          <tr>
            <td style="padding:15px 50px !important;">
                <a href="https://budgeter.cl" target="new"><img  src="https://elinous.cl/swconstructora/email-assets/logo-budgeter.png" alt=""></a>
            </td>
          </tr>
          <tr>
            <td style="padding: 0 50px;  color:#9f9eac !important; text-align:left !important;  font-size: 30px !important; padding-top:24px !important;">
                <span style="color:#43425d !important;">Hola {{$nombre}}</span>, hemos recibido tu pago con éxito
              </td>
          </tr>
          <tr>
            <td  style=" padding-top:34px !important; line-height: 1.4em !important;padding: 0 50px; padding-bottom: 30px !important; color:#9f9eac !important;  font-size: 18px !important;">
              Ya puedes comenzar a trabajar con Budgeter, si aún no has confirmado tus datos de acceso, ingresa aquí:
              <a href="{{$link}}" style="color:#f9aa33 !important; display:block !important;margin-top:18px !important; font-weight:500 !important;">Confirmar acceso</a>
            </td>
          </tr>
        </table>
        <!--fin tabla de 600 px-->
      </td>
    </tr>
  </table>
  <!-- fin contenedora 1 -->
  <!-- tabla contenedora 3 -->
  <table width="100%" cellspacing="0" cellpadding="0" border="0" style="background-color:#fff;" >
    <tr>
      <td>
        <!--tabla de 600 px-->
        <table width="600" cellspacin="0" cellpadding="0" border="0"  style="margin:0 auto; padding:15px 0;" >
          <tr>
              <td style="padding: 0 50px; color:#9f9eac !important;  font-size: 16px !important;line-height: 1.6em !important;">
                Si tienes dudas o dificultades para confirmar tu acceso, recuerda que siempre puedes comunicarte con nosotros al correo soporte@budgeter.cl
              </td>
              </td>
          </tr>
          <tr>
              <td style="padding: 0 50px; padding-bottom: 30px !important; color:#858E9F !important; font-size: 16px !important;line-height: 1.6em !important;">
              <span style=" color:#43425d;  margin-top:25px !important;  display:block !important;">Equipo Budgeter</span>
              </td>
          </tr>
        </table>
        <!--fin tabla de 600 px-->
      </td>
    </tr>
  </table>
  <!-- fin contenedora 3  -->
  <!-- tabla footer -->
  <table width="100%" cellspacing="0" cellpadding="0" border="0" style="background:#43425d;">
    <tr>
      <td style="padding: 44px 0 !important;">
        <table width="600" cellspacin="0" cellpadding="0" border="0"  style="margin:0 auto; padding: 0 50px;">
          <tr style="text-align:left !important;  font-size: 18px !important;" >
              <td>
                  <div style="font-size: 16px; color:#9f9eac !important; font-weight:500!important;">Encuéntranos en</div>
                  <a  href="https://budgeter.cl" target="new" style="text-decoration: none !important;font-size: 18px;color:#fff !important; display:block !important;">www.budgeter.cl</a>
                  <div style="font-size: 14px; color:#fff !important; margin:14px 0 !important;">La mejor gestión para tu negocio</div>
                  <br>
                  <!-- <hr style="margin-top:34px !important;">
                  <div style="font-size: 12px;color:#858E9F !important;">¿Quieres dejar de recibir estos correos? No hay problema, haz click aquí.</div> -->
              </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <!-- fin tabla footer-->
  </body>
</html>
