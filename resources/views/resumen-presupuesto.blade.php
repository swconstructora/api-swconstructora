<html>
  <style type="text/css">
  @media screen {
    @font-face {
    font-family: 'Avenir';
    font-style: normal;
    font-weight: 400;
    src: local('Avenir Book'), local('Avenir-Book'), url(https://elinous.cl/swconstructora/app/AvenirLTStd-Book.9334084d0d28fc558b10.woff2) format('woff2');
  }

    @font-face {
    font-family: 'Avenir';
    font-style: normal;
    font-weight: 500;
    src: local('Avenir Heavy'), local('Avenir-Heavy'), url(https://elinous.cl/swconstructora/app/AvenirLTStd-Heavy.a30d5d1a2ef6734a676c.woff2) format('woff2');
    }
  }
    body {
      font-family: "Avenir", Tahoma, Sans-Serif;
      font-weight: 400;
    }
  </style>
  <style>
    /* table { border-collapse: collapse; border: 1px solid black; } */
    tr {page-break-after: always;}
    .tr-bord { border-collapse: collapse; border: 1px solid black;}
    .td-bord { border-collapse: collapse; border: 1px solid black; }
  </style>

  <table xmlns="http://www.w3.org/1999/xhtml" border="0" cellspacing="0" cellpadding="0" class="ta1">
    <colgroup>
      <col width="251" />
      <col width="104" />
      <col width="161" />
      <col width="78" />
    </colgroup>
    <tr class="ro1">
      <td style="text-align:left;width:57.41mm;" class="ce1"><img src="../resources/img/powered-budgeter.png"></td>
    </tr>
    <tr class="ro1">
      <td style="text-align:left;width:57.41mm; " class="ce2 td-bord"><p>Fecha de creación presupuesto</p></td>
      <td style="text-align:left;width:57.41mm; " colspan="3" class="ce2 td-bord"><p>{{$fech_crea}}</p></td>
    </tr>
    <tr class="ro1">
      <td rowspan="6" style="text-align:left;width:57.41mm; " class="ce3 td-bord"><p>Observaciones de presupuesto:</p></td>
      <td colspan="3" rowspan="6" style="text-align:left;width:23.79mm; " class="ce8 td-bord"><p>{{$obse_pres}}</p></td>
    </tr>
    <tr class="ro1"></tr>
    <tr class="ro1"></tr>
    <tr class="ro1"></tr>
    <tr class="ro1"></tr>
    <tr class="ro1"></tr>
    <tr class="ro1">
      <td style="text-align:left;width:57.41mm; " class="ce2 td-bord"><p>Asignaciones presupuestarias</p></td>
      <td style="text-align:left;width:23.79mm; " class="ce1"> </td>
      <td style="text-align:left;width:36.95mm; " class="ce1"> </td>
      <td style="text-align:left;width:17.9mm; " class="ce1"> </td>
    </tr>
    <tr class="ro1">
      <td style="text-align:left;width:57.41mm; " class="ce2 td-bord"><p>Categoría</p></td>
      <td style="text-align:left;width:23.79mm; " class="ce2 td-bord"><p>Presupuesto</p></td>
      <td style="text-align:left;width:36.95mm; " class="ce1"> </td>
      <td style="text-align:left;width:17.9mm; " class="ce1"> </td>
    </tr>
    <tr class="ro1">
      <td style="text-align:left;width:57.41mm; " class="ce4 td-bord"><p>Materiales</p></td>
      <td style="text-align:left;width:23.79mm; " class="ce9 td-bord"><p>${{$mont_mate}}</p></td>
      <td style="text-align:left;width:36.95mm; " class="ce1"> </td>
      <td style="text-align:left;width:17.9mm; " class="ce1"> </td>
    </tr>
    <tr class="ro1">
      <td style="text-align:left;width:57.41mm; " class="ce5 td-bord"><p>Subcontratistas</p></td>
      <td style="text-align:left;width:23.79mm; " class="ce10 td-bord"><p>${{$mont_subc}}</p></td>
      <td style="text-align:left;width:36.95mm; " class="ce1"> </td>
      <td style="text-align:left;width:17.9mm; " class="ce1"> </td>
    </tr>
    <tr class="ro1">
      <td style="text-align:left;width:57.41mm; " class="ce5 td-bord"><p>Mano de Obra</p></td>
      <td style="text-align:left;width:23.79mm; " class="ce10 td-bord"><p>${{$mont_mobr}}</p></td>
      <td style="text-align:left;width:36.95mm; " class="ce1"> </td>
      <td style="text-align:left;width:17.9mm; " class="ce1"> </td>
    </tr>
    <tr class="ro1">
      <td style="text-align:left;width:57.41mm; " class="ce5 td-bord"><p>Gastos Generales</p></td>
      <td style="text-align:left;width:23.79mm; " class="ce10 td-bord"><p>${{$mont_ggnr}}</p></td>
      <td style="text-align:left;width:36.95mm; " class="ce1"> </td>
      <td style="text-align:left;width:17.9mm; " class="ce1"> </td>
    </tr>
    <tr class="ro1">
      <td style="text-align:left;width:57.41mm; " class="ce5 td-bord"><p>Arriendos</p></td>
      <td style="text-align:left;width:23.79mm; " class="ce10 td-bord"><p>${{$mont_arri}}</p></td>
      <td style="text-align:left;width:36.95mm; " class="ce1"> </td>
      <td style="text-align:left;width:17.9mm; " class="ce1"> </td>
    </tr>
    <tr class="ro1">
      <td style="text-align:left;width:57.41mm; " class="ce1"> </td>
      <td style="text-align:left;width:23.79mm; " class="ce1"> </td>
      <td style="text-align:left;width:36.95mm; " class="ce1"> </td>
      <td style="text-align:left;width:17.9mm; " class="ce1"> </td>
    </tr>
    <tr class="ro1">
      <td style="text-align:left;width:57.41mm; " class="ce1"> </td>
      <td style="text-align:left;width:23.79mm; " class="ce1"> </td>
      <td style="text-align:left;width:36.95mm; " class="ce1"> </td>
      <td style="text-align:left;width:17.9mm; " class="ce1"> </td>
    </tr>
    <tr class="ro1">
      <td colspan="2" style="text-align:left;width:57.41mm; " class="ce6 td-bord"><p>Presupuesto total proyecto</p></td>
      <td style="text-align:left;width:36.95mm; " class="ce9 td-bord"><p>${{$mont_ptot}}</p></td>
      <td style="text-align:left;width:17.9mm; " class="ce1"> </td>
    </tr>
    <tr class="ro1">
      <td colspan="2" style="text-align:left;width:57.41mm; " class="ce6 td-bord"><p>Total presupuesto asignado</p></td>
      <td style="text-align:left;width:36.95mm; " class="ce10 td-bord"><p>${{$mont_pasg}}</p></td>
      <td style="text-align:left;width:17.9mm; " class="ce1"> </td>
    </tr>
    <tr class="ro1">
      <td colspan="2" style="text-align:left;width:57.41mm; " class="ce6 td-bord"><p>Total presupuesto sin asignar (disponible)</p></td>
      <td style="text-align:left;width:36.95mm; " class="ce10 td-bord"><p>${{$mont_sasg}}</p></td>
      <td style="text-align:left;width:17.9mm; " class="ce1"> </td>
    </tr>
    <tr class="ro1">
      <td style="text-align:left;width:57.41mm; " class="ce1"> </td>
      <td style="text-align:left;width:23.79mm; " class="ce1"> </td>
      <td style="text-align:left;width:36.95mm; " class="ce1"> </td>
      <td style="text-align:left;width:17.9mm; " class="ce1"> </td>
    </tr>
    <tr class="ro1">
      <td style="text-align:left;width:57.41mm; " class="ce1"> </td>
      <td style="text-align:left;width:23.79mm; " class="ce1"> </td>
      <td style="text-align:left;width:36.95mm; " class="ce1"> </td>
      <td style="text-align:left;width:17.9mm; " class="ce1"> </td>
    </tr>
  </table>
</html>
