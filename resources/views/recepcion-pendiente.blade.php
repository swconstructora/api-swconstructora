<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Orden de compra</title>
    <style type="text/css">
    @media screen {
      @font-face {
      font-family: 'Avenir';
      font-style: normal;
      font-weight: 400;
      src: local('Avenir Book'), local('Avenir-Book'), url(https://elinous.cl/swconstructora/app/AvenirLTStd-Book.9334084d0d28fc558b10.woff2) format('woff2');
    }

      @font-face {
      font-family: 'Avenir';
      font-style: normal;
      font-weight: 500;
      src: local('Avenir Heavy'), local('Avenir-Heavy'), url(https://elinous.cl/swconstructora/app/AvenirLTStd-Heavy.a30d5d1a2ef6734a676c.woff2) format('woff2');
      }
    }
      body {
        font-family: "Avenir", Tahoma, Sans-Serif;
        font-weight: 400;
      }
    </style>
  </head>
  <body>
<!-- tabla conntenedora 1 -->
  <table width="100%" cellspacing="0" cellpadding="0" border="0" style="background-color:#fff;" >
    <tr>
      <td>
        <!--tabla de 600 px-->
        <table width="600" cellspacin="0" cellpadding="0" border="0"  style="margin:0 auto; padding:15px 0;" >
          <tr>
            <td style="padding:15px 50px !important;">
                <a href="https://budgeter.cl" target="new"><img  src="https://elinous.cl/swconstructora/email-assets/logo-budgeter.png" alt=""></a>
            </td>
          </tr>
        </table>
        <!-- Detalle -->
        <table width="700" cellspacin="0" cellpadding="0" border="0"  style="margin:10px auto; border-collapse: collapse; color:#fff!important;" >
          @if ($es_oc)
            <tr>
              <td width="50%" style="padding: 7px 16px;  background: #43425d; line-height: 1.4em !important;">
                Nombre Proveedor: {{$nom_prov}}
              </td>
            </tr>
            <tr>
              <td width="50%" style="padding: 7px 16px;  background: #43425d; line-height: 1.4em !important;">
                RUT Proveedor: {{$rut_prov}}
              </td>
            </tr>
            <tr>
              <td width="50%" style="padding: 7px 16px;  background: #43425d; line-height: 1.4em !important;">
                Obra mandante: {{$obra_man}}
              </td>
            </tr>
          @else
            <tr>
              <td width="50%" style="padding: 7px 16px;  background: #43425d; line-height: 1.4em !important;">
                Proyecto Emisor: {{$nom_proy}}
              </td>
            </tr>
            <tr>
              <td width="50%" style="padding: 7px 16px;  background: #43425d; line-height: 1.4em !important;">
                Proyecto Receptor: {{$nom_rece}}
              </td>
            </tr>
          @endif
          <tr>
            <td width="50%" style="padding: 7px 16px;   background: #43425d; line-height: 1.4em !important; ">
              Dirección: {{$dire_des}}
            </td>
          </tr>
        </table>
        <!--fin tabla detalle -->
        <!-- Tabla materiales -->
        <table width="700" cellspacin="0" cellpadding="0" border="1"  style="margin:20px auto; border:0; border-collapse: collapse; border-color:#ccc; color:#43425d !important;" >
          <tr>
            <td width="10%" style="padding: 7px 16px;  line-height: 1.4em !important;">
              <b style="font-weight: 500 !important;">Fecha recepción</b>
            </td>
            <td width="30%" style="padding: 7px 16px;  line-height: 1.4em !important;">
              <b style="font-weight: 500 !important;">Material</b>
            </td>
            <td width="15%" style="padding: 7px 16px;   line-height: 1.4em !important;">
              <b style="font-weight: 500 !important;">Unidad</b>
            </td>
            <td width="10%" style="padding: 7px 16px;   line-height: 1.4em !important;">
              <b style="font-weight: 500 !important;">Cantidad</b>
            </td>
            <td width="10%" style="padding: 7px 16px;  line-height: 1.4em !important;">
              <b style="font-weight: 500 !important;">Recibido</b>
            </td>
          </tr>
          @foreach ($detalles as $deta)
            <tr>
              <td  style="padding: 7px 16px; line-height: 1.4em !important;">
                {{$deta->fecha}}
              </td>
              <td  style="padding: 7px 16px; line-height: 1.4em !important;">
                {{$deta->material}}
              </td>
              <td  style="padding: 7px 16px;   line-height: 1.4em !important;">
                {{$deta->unidad}}
              </td>
              <td  style="padding: 7px 16px;  line-height: 1.4em !important;">
                {{$deta->cantidad}}
              </td>
              <td  style="padding: 7px 16px;   line-height: 1.4em !important;">

              </td>
            </tr>
          @endforeach
        </table>
          <!-- Tabla materiales -->
      </td>
    </tr>
  </table>
  <!-- fin tabla footer-->
  </body>
</html>
