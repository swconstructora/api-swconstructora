<html>
  <style type="text/css">
  @media screen {
    @font-face {
    font-family: 'Avenir';
    font-style: normal;
    font-weight: 400;
    src: local('Avenir Book'), local('Avenir-Book'), url(https://elinous.cl/swconstructora/app/AvenirLTStd-Book.9334084d0d28fc558b10.woff2) format('woff2');
  }

    @font-face {
    font-family: 'Avenir';
    font-style: normal;
    font-weight: 500;
    src: local('Avenir Heavy'), local('Avenir-Heavy'), url(https://elinous.cl/swconstructora/app/AvenirLTStd-Heavy.a30d5d1a2ef6734a676c.woff2) format('woff2');
    }
  }
    body {
      font-family: "Avenir", Tahoma, Sans-Serif;
      font-weight: 400;
    }
  </style>
  <table>
    <tr>
      <td style="padding: 10px;">
      <img src="../resources/img/powered-budgeter.png">
      </td>
    </tr>
  </table>
@foreach($datos as $dato)
  <table xmlns="http://www.w3.org/1999/xhtml" border="0" cellspacing="0" cellpadding="0" class="ta2">
    <colgroup>
      <col width="192" />
      <col width="127" />
      <col width="157" />
      <col width="226" />
      <col width="226" />
      <col width="176" />
      <col width="148" />
      <col width="149" />
      <col width="91"  />
    </colgroup>
    <tr class="ro1" style="background: #ccc;">
      <td style="text-align:left;width:43.96mm; background: #bfbfbf;font-weight: 500;" class="ce1">
        <p>Razón Social</p>
      </td>
      <td style="text-align:left;width:29.12mm; background: #bfbfbf;font-weight: 500;" class="ce5">
        <p>Tipo</p>
      </td>
      <td style="text-align:left;width:35.84mm; background: #bfbfbf;font-weight: 500;" class="ce5">
        <p>RUT</p>
      </td>
      <td style="text-align:left;width:51.79mm; background: #bfbfbf;font-weight: 500;" class="ce5">
        <p>Total neto Facturado</p>
      </td>
      <td style="text-align:left;width:51.79mm; background: #bfbfbf;font-weight: 500;" class="ce5">
        <p>IVA total</p>
      </td>
      <td style="text-align:left;width:40.32mm; background: #bfbfbf;font-weight: 500;" class="ce5">
        <p>Total final</p>
      </td>
      <td style="text-align:left;width:33.88mm; "> </td>
      <td style="text-align:left;width:34.15mm; "> </td>
    </tr>
    <tr class="ro1">
      <td style="text-align:left;width:43.96mm; " class="ce2"> {{$dato->razon_so}} </td>
      <td style="text-align:left;width:29.12mm; " class="ce2"> {{$dato->tipo_emp}} </td>
      <td style="text-align:left;width:35.84mm; " class="ce4"> {{$dato->rut_empr}} </td>
      <td style="text-align:left;width:51.79mm; " class="ce4"> {{$dato->tot_ne_f}} </td>
      <td style="text-align:left;width:51.79mm; " class="ce4"> {{$dato->tota_iva}} </td>
      <td style="text-align:left;width:40.32mm; " class="ce4"> {{$dato->tot_fina}} </td>
      <td style="text-align:left;width:33.88mm; " > </td>
      <td style="text-align:left;width:34.15mm; " > </td>
    </tr>
    <tr class="ro1">
      <!-- <td style="text-align:left;width:43.96mm; background: #bfbfbf;font-weight: 500;" class="ce7">
        <p>N° OC</p>
      </td> -->
      <td style="text-align:left;width:29.12mm; background: #bfbfbf;font-weight: 500;" class="ce3">
        <p>N° Factura</p>
      </td>
      <td style="text-align:left;width:35.84mm; background: #bfbfbf;font-weight: 500;" class="ce3">
        <p>Neto factura</p>
      </td>
      <td style="text-align:left;width:51.79mm; background: #bfbfbf;font-weight: 500;" class="ce3">
        <p>Neto Nota de Crédito</p>
      </td>
      <td style="text-align:left;width:51.79mm; background: #bfbfbf;font-weight: 500;" class="ce3">
        <p>Neto aprobado</p>
      </td>
      <td style="text-align:left;width:40.32mm; background: #bfbfbf;font-weight: 500;" class="ce3">
        <p>IVA</p>
      </td>
      <td style="text-align:left;width:33.88mm; background: #bfbfbf;font-weight: 500;" class="ce6">
        <p>Subtotal a pagar</p>
      </td>
      <td style="text-align:left;width:34.15mm; background: #bfbfbf;font-weight: 500;" class="ce7">
        <p>Vencimiento</p>
      </td>
    </tr>
    @foreach($dato->detalles as $dat)
      <tr class="ro1">
        <!-- <td style="text-align:left;width:43.96mm; " class="ce4"> {{$dat->nro_ordc}} </td> -->
        <td style="text-align:left;width:29.12mm; " class="ce4"> {{$dat->nro_fact}} </td>
        <td style="text-align:left;width:35.84mm; " class="ce4"> {{$dat->net_fact}} </td>
        <td style="text-align:left;width:51.79mm; " class="ce4"> {{$dat->net_no_c}} </td>
        <td style="text-align:left;width:51.79mm; " class="ce4"> {{$dat->net_apro}} </td>
        <td style="text-align:left;width:40.32mm; " class="ce4"> {{$dat->iva_deta}} </td>
        <td style="text-align:left;width:33.88mm; " class="ce4"> {{$dat->tota_iva}} </td>
        <td style="text-align:left;width:34.15mm; " class="ce4"> {{$dat->vencimie}} </td>
      </tr>
    @endforeach
    </table>
@endforeach
</html>
