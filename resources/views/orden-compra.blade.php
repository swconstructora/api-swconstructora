<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Orden de compra</title>
    <style type="text/css">
    @media screen {
      @font-face {
      font-family: 'Avenir';
      font-style: normal;
      font-weight: 400;
      src: local('Avenir Book'), local('Avenir-Book'), url(https://elinous.cl/swconstructora/app/AvenirLTStd-Book.9334084d0d28fc558b10.woff2) format('woff2');
    }

      @font-face {
      font-family: 'Avenir';
      font-style: normal;
      font-weight: 500;
      src: local('Avenir Heavy'), local('Avenir-Heavy'), url(https://elinous.cl/swconstructora/app/AvenirLTStd-Heavy.a30d5d1a2ef6734a676c.woff2) format('woff2');
      }
    }
      body {
        font-family: "Avenir", Tahoma, Sans-Serif;
        font-weight: 400;
      }
    </style>
  </head>
  <body>
<!-- tabla conntenedora 1 -->
  <table width="100%" cellspacing="0" cellpadding="0" border="0" style="background-color:#fff;" >
    <tr>
      <td>
        <!--tabla de 600 px-->
        <table width="600" cellspacin="0" cellpadding="0" border="0"  style="margin:0 auto; padding:15px 0;" >
          <tr>
            <td style="padding:15px 50px !important;">
                <a href="https://budgeter.cl" target="new"><img  src="https://elinous.cl/swconstructora/email-assets/logo-budgeter.png" alt=""></a>
            </td>
          </tr>
          <tr>
            <td style="padding: 0 50px;  color:#9f9eac !important; text-align:left !important;  font-size: 30px !important; padding-top:24px !important;">
              Nueva <span style="color:#43425d !important;">orden de compra (#{{$NRO_ORDEN}}) </span>  para {{$NOM_PRY}}
              </td>
          </tr>
        </table>
        <!-- Detalle -->
        <table width="700" cellspacin="0" cellpadding="0" border="0"  style="margin:10px auto; border-collapse: collapse; color:#fff!important;" >
          <tr>
            <td width="50%" style="padding: 7px 16px; border-top-left-radius:3px;   background: #43425d; line-height: 1.4em !important;">
              <b style="font-weight: 500 !important;">Mandante</b>
            </td>
            <td width="50%" style="padding: 7px 16px; border-top-right-radius:3px; background: #f9aa33; line-height: 1.4em !important;">
              <b style="font-weight: 500 !important;">Proveedor</b>
            </td>
          </tr>
          <tr>
              <td width="50%" style="padding: 7px 16px;  background: #43425d; line-height: 1.4em !important;">
                Mandante: {{$RAZ_PRY}}
              </td>
              <td width="50%" style="padding: 7px 16px;   background: #f9aa33; line-height: 1.4em !important;">
                  {{$RAZ_PRO}}
              </td>
          </tr>
          <tr>
              <td width="50%" style="padding: 7px 16px;  background: #43425d; line-height: 1.4em !important;">
                RUT: {{$RUT_PRY}}
              </td>
              <td width="50%" style="padding: 7px 16px;   background: #f9aa33; line-height: 1.4em !important;">
                  {{$RUT_PRO}}
              </td>
          </tr>
          <tr>
              <td width="50%" style="padding: 7px 16px;  background: #43425d; line-height: 1.4em !important;">
                  Proyecto: {{$NOM_PRY}}
              </td>
              <td width="50%" style="padding: 7px 16px;  background: #f9aa33; line-height: 1.4em !important;">
                  <b style="font-weight: 500 !important;">Fecha de pago:</b> {{$FEC_PAG}}
              </td>
          </tr>
          <tr>
              <td width="50%" style="padding: 7px 16px;   background: #43425d; line-height: 1.4em !important; ">
                  <a style="color:#fff !important;">Dirección: {{$DIR_DES}}</a>
              </td>
              <td width="50%" style="padding: 7px 16px;  background: #f9aa33; line-height: 1.4em !important;">
                <b style="font-weight: 500 !important;">Fecha de entrega:</b> {{$FEC_ENT}}
              </td>
          </tr>
          <tr>
              <td width="50%" style="padding: 7px 16px;  background: #43425d; line-height: 1.4em !important;">
                  {{$COR_USU}}
              </td>
              <td width="50%" style="padding: 7px 16px; background: #f9aa33; line-height: 1.4em !important;">
                  <!-- <b style="font-weight: 500 !important;">Obra mandante:</b> {{$NOM_PRY}} -->
              </td>
          </tr>
          <tr>
            <td width="50%" style="padding: 7px 16px; border-bottom-left-radius:3px;   background: #43425d; line-height: 1.4em !important;">
              <b style="font-weight: 500 !important;">Fecha de emisión:</b> {{$FEC_EMI}}
            </td>
            <td width="50%" style="padding: 7px 16px; border-bottom-right-radius:3px;  background: #f9aa33; line-height: 1.4em !important;">
              <!-- <b style="font-weight: 500 !important;">Dirección despacho:</b> {{$DIR_CONS}} -->
            </td>
          </tr>
        </table>
        <!--fin tabla detalle -->
        <!-- Tabla materiales -->
        <table width="700" cellspacin="0" cellpadding="0" border="1"  style="margin:20px auto; border:0; border-collapse: collapse; border-color:#ccc; color:#43425d !important;" >
          <tr>
            <td width="15%" style="padding: 7px 16px;  line-height: 1.4em !important;">
              <b style="font-weight: 500 !important;">Fecha recepción</b>
            </td>
            <td width="30%" style="padding: 7px 16px;  line-height: 1.4em !important;">
              <b style="font-weight: 500 !important;">Material</b>
            </td>
            <td width="15%" style="padding: 7px 16px;   line-height: 1.4em !important;">
              <b style="font-weight: 500 !important;">Unidad</b>
            </td>
            <td width="10%" style="padding: 7px 16px;   line-height: 1.4em !important;">
              <b style="font-weight: 500 !important;">Cantidad</b>
            </td>
            <td width="20%" style="padding: 7px 16px;  line-height: 1.4em !important;">
              <b style="font-weight: 500 !important;">Precio unitario</b>
            </td>
            <td width="20%" style="padding: 7px 16px; line-height: 1.4em !important;">
              <b style="font-weight: 500 !important;">Precio total</b>
            </td>
          </tr>
          @foreach ($DETALLE as $deta)
            <tr>
              <td  style="padding: 7px 16px; line-height: 1.4em !important;">
                {{$deta->FECH_ENT}}
              </td>
              <td  style="padding: 7px 16px; line-height: 1.4em !important;">
                {{$deta->NOM_MATE}}
              </td>
              <td  style="padding: 7px 16px;   line-height: 1.4em !important;">
                {{$deta->UNI_MATE}}
              </td>
              <td  style="padding: 7px 16px;  line-height: 1.4em !important;">
                {{$deta->CAN_MATE}}
              </td>
              <td  style="padding: 7px 16px;   line-height: 1.4em !important;">
                ${{$deta->PRE_UNIT}}
              </td>
              <td  style="padding: 7px 16px;   line-height: 1.4em !important;">
                ${{$deta->PRE_TOTL}}
              </td>
            </tr>
          @endforeach
          <!-- <tr>
            <td  style="padding: 7px 16px;  line-height: 1.4em !important;">
              Perfiles 3x2
            </td>
            <td  style="padding: 7px 16px;  line-height: 1.4em !important;">
              Milimetros
            </td>
            <td  style="padding: 7px 16px;  line-height: 1.4em !important;">
              2
            </td>
            <td  style="padding: 7px 16px;  line-height: 1.4em !important;">
              $10.000
            </td>
            <td  style="padding: 7px 16px; line-height: 1.4em !important;">
              $20.000
            </td>
          </tr> -->
          <tr>
            <td colspan="5" style="padding: 7px 16px;  line-height: 1.4em !important;">
                <b style="font-weight: 500 !important; float:right;">Subtotal</b>
            </td>
            <td  style="padding: 7px 16px;  line-height: 1.4em !important;">
              ${{$SUB_TOT}}
            </td>
          </tr>
          <tr>
            <td colspan="5" style="padding: 7px 16px;  line-height: 1.4em !important;">
              <b style="font-weight: 500 !important; float:right;">IVA (19%)</b>
            </td>
            <td  style="padding: 7px 16px;  line-height: 1.4em !important;">
              ${{$IVA_INP}}
            </td>
          </tr>
          <tr>
            <td colspan="5" style="padding: 7px 16px;  line-height: 1.4em !important;">
                <b style="font-weight: 500 !important; float:right;">Total</b>
            </td>
            <td  style="padding: 7px 16px;  line-height: 1.4em !important;">
              ${{$TOT_ORD}}
            </td>
          </tr>
        </table>
          <!-- Tabla materiales -->
      </td>
    </tr>
  </table>
  <!-- fin contenedora 1 -->
  <!-- tabla contenedora 2 -->
  <table width="100%" cellspacing="0" cellpadding="0" border="0" style="background-color:#fff;" >

    <tr>
      <td>
        <table width="600" cellspacin="0" cellpadding="0" border="0"  style="margin:0 auto;" >
          <tr>
            <td  style="line-height: 1.4em !important;padding: 0 50px; color:#9f9eac !important;  font-size: 16px !important;">
              <p style="color:#43425d; font-weight:500 !important;">Aprobación de compra:</p>
              @foreach ($CAR_AUT as $carg)
                <p style="margin: 2px 0;">{{$carg}}</p>
              @endforeach
            </td>
          </tr>
        </table>
        <!--tabla de 600 px-->
        <table width="600" cellspacin="0" cellpadding="0" border="0"  style="margin:0 auto; padding:15px 0;" >
          <tr>
              <td style="padding: 0 50px; color:#43425d !important;  font-size: 16px !important;line-height: 1.6em !important;">
                <p style="color:#43425d; font-weight:500 !important;">Importante:</p>
               Para gestionar el pago de la presente Orden de Compra, es indispensable que el proveedor remita la factura correspondiente equivalente al total expuesto, al correo electrónico
    {{$CORR_AS}} indicando en el asunto N° de la Orden de Compra y nombre de la obra mandante. No se podrá gestionar pago alguno sin el envío oportuno de los documentos tributarios correspondientes.

              </td>
          </tr>
          <tr>
              <td style="padding: 0 50px; padding-bottom: 30px !important; font-size: 16px !important;line-height: 1.6em !important;">
                <span style=" color:#43425d;  margin-top:25px !important;  display:block !important;">Orden de Compra enviada automáticamente.</span>
              </td>
          </tr>
        </table>
        <!--fin tabla de 600 px-->
      </td>
    </tr>
  </table>
  <!-- fin contenedora 2  -->
  <!-- tabla footer -->
  <table width="100%" cellspacing="0" cellpadding="0" border="0" style="background:#43425d;">
    <tr>
      <td style="padding: 44px 0 !important;">
        <table width="600" cellspacin="0" cellpadding="0" border="0"  style="margin:0 auto; padding: 0 50px;">
          <tr style="text-align:left !important;  font-size: 18px !important;" >
              <td>
                  <div style="font-size: 16px; color:#9f9eac !important; font-weight:500!important;">Encuéntranos en</div>
                  <a  href="https://budgeter.cl" target="new" style="text-decoration: none !important;font-size: 18px;color:#fff !important; display:block !important;">www.budgeter.cl</a>
                  <div style="font-size: 14px; color:#fff !important; margin:14px 0 !important;">La mejor gestión para tu negocio</div>
                  <br>
                  <!-- <hr style="margin-top:34px !important;">
                  <div style="font-size: 12px;color:#858E9F !important;">¿Quieres dejar de recibir estos correos? No hay problema, haz click aquí.</div> -->
              </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <!-- fin tabla footer-->
  </body>
</html>
