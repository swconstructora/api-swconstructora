<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Content-Type, Content-Length, Origin, Accept, X-Auth-Token, Client, Authorization, X-Requested-With,XSRF-TOKEN, x-xsrf-token, x_csrftoken, X-CSRF-Token');

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});

// USUARIO CONTROLLER
Route::post('usuario/login','UsuarioController@login');
Route::get('retornar-usuario','UsuarioController@obtenerAutorizacion');
Route::post('usuario/cambiar-contrasena','UsuarioController@cambiarPass');
Route::post('usuario/crear-empleado','UsuarioController@agregarEmpleado');
Route::get('usuario/{id_constructora}/gerentes','UsuarioController@cargarGerentes');
Route::get('usuario/{id_constructora}/administradores-empleados','UsuarioController@cargarEmpleadosAdministradores');
Route::get('usuario/{id_constructora}/jefes-empleados','UsuarioController@cargarEmpleadosjefes');
Route::delete('usuario/{id_empleado}/eliminar','UsuarioController@eliminarEmpleado');
Route::post('usuario/reenviar-contrasena','UsuarioController@reenviarContrasena');
Route::post('usuario/olvido-contrasena','UsuarioController@olvidoContrasena');
Route::post('usuario/solicitar-ayuda','UsuarioController@solicitarAyuda');
Route::post('usuario/editar-cuenta','UsuarioController@editarCuenta');
Route::get('avatares/{id_cons}/usuario/{id_usu}/{nombre_adjunto}','UsuarioController@cargarAvatar');
Route::post('usuario/editar-organizacion','UsuarioController@editarOrganizacion');
Route::get('constructora/{RUTA_CONS}','UsuarioController@obtenerOrganizacion');
Route::post('usuario/registrar-usuario-potencial','UsuarioController@crearUsuarioPotencial');
Route::post('usuario/crear-demo','UsuarioController@crearDemo');
Route::post('usuario/generar-bienvenida','UsuarioController@bienvenido');
Route::post('usuario/contactar-ventas','UsuarioController@contactarVentas');

//PROYECTO CONTROLLER
Route::resource('proyecto','ProyectoController');
Route::post('proyecto/crear-proyecto','ProyectoController@crearProyecto');
Route::post('proyecto/{id_constructora}/proyectos','ProyectoController@cargarProyectos');
Route::delete('proyecto/{id_proyecto}/eliminar','ProyectoController@eliminarProyecto');
Route::post('proyecto/tareas-pendientes','ProyectoController@cargarTareasPendientes');
// Route::get('proyecto/{id_proyecto}','ProyectoController@getProyecto');
Route::get('proyecto/{id_proyecto}/cargar-empleados/{ID_CARG}','ProyectoController@cargarEmpleados');
Route::post('proyecto/guardar-administradores','ProyectoController@guardarEmpleadosAdmin');
Route::post('proyecto/empleados-asignados','ProyectoController@cargarEmpleadosProyecto');
Route::post('proyecto/detalle-proyecto','ProyectoController@detalleProyecto');
Route::post('proyecto/{id_proy}/cambiar-estado','ProyectoController@cambiarEstadoProyecto');

//PROYECTO PRESUPUESTO
Route::get('proyecto/{id_proyecto}/areas','ProyectoPresupuestoController@cargarAreasProyecto');
Route::post('proyecto/areas','ProyectoPresupuestoController@agregarAreas');
Route::post('proyecto/presupuesto','ProyectoPresupuestoController@cargarPresupuesto');
Route::post('proyecto/guardar-presupuesto','ProyectoPresupuestoController@guardarPresupuesto');
Route::post('proyecto/totales-presupuesto','ProyectoPresupuestoController@cargarTotalesPresupuesto');
Route::post('proyecto/presupuesto-comparar','ProyectoPresupuestoController@cargarPresupuestoComparar');
Route::get('proyecto/{id_proyecto}/historial-presupuesto','ProyectoPresupuestoController@cargarHistorialPresupuesto');
Route::post('proyecto/otros-costos','ProyectoPresupuestoController@cargarOtrosCostos');
Route::post('proyecto/guardar-costos','ProyectoPresupuestoController@guardarOtrosCostos');
Route::post('proyecto/ejecucion-presupuestaria','ProyectoPresupuestoController@cargarEjecucionPresupuestaria');
Route::get('proyecto/{id_subcate}/monto-presupuesto','ProyectoPresupuestoController@obtenerValorPresuArriendo');
Route::get('proyecto/presupuesto-pdf/{id_proy}/{id_pres}','ProyectoPresupuestoController@generarPDFPresupuesto');
Route::get('proyecto/presupuesto-excel/{id_proy}/{id_pres}','ProyectoPresupuestoController@generarExcelPresupuesto');

//PROYECTO PEDIDO
Route::post('proyecto/nota-pedido','ProyectoPedidoController@crearNotaPedido');
Route::post('proyecto/{id_proyecto}/nota-pedido','ProyectoPedidoController@cargarNotasPedidos');
Route::post('proyecto/{id_proyecto}/buscar-nota-pedido','ProyectoPedidoController@buscarNotaPedido');
Route::get('proyecto/{id_proyecto}/nota-pedido/espera','ProyectoPedidoController@cargarNotasPedidosEspera');
Route::post('proyecto/{id_nota_pedido}/estado-aprobacion','ProyectoPedidoController@aprobarRechazarNotaPedido');
Route::get('proyecto/{id_proyecto}/nota-pedido/aprobadas','ProyectoPedidoController@cargarNotasPedidosAprobadas');
Route::get('proyecto/{id_nota_pedido}/nota-pedido/get','ProyectoPedidoController@cargarNotaPedido');
Route::get('proyecto/usuarios-creadores/{ID_CONS}/{ID_PROY}','ProyectoPedidoController@cargarEmpleadosNotaPedido');

//PROYECTO ORDEN
Route::post('proyecto/nota-pedido/seleccionadas','ProyectoOrdenController@cargarNotasSeleccionadas');
Route::post('proyecto/cotizacion/guardar','ProyectoOrdenController@guardarCotizacion');
Route::post('proyecto/cotizacion/historial','ProyectoOrdenController@historialCotizaciones');
Route::get('proyecto/{id_proyecto}/elegir-cotizacion','ProyectoOrdenController@cargarCotizacionesPreOrden');
Route::post('proyecto/cotizacion/proveedor','ProyectoOrdenController@verificarProvedorContizacion');
Route::get('proyecto/cotizacion/{id_coti}','ProyectoOrdenController@cargarDatosCotizados');
Route::post('proyecto/order-compra/generar','ProyectoOrdenController@generarOrdenDeCompra');
Route::post('proyecto/cotizacion/editar','ProyectoOrdenController@editarCotizacion');
Route::get('proyecto/{ID_PROY}/orden-compra/historial','ProyectoOrdenController@historialOrdenCompra');
Route::get('proyecto/{ID_PROY}/get-ordenes-espera','ProyectoOrdenController@getOrdenesCompraEspera');
Route::post('proyecto/{ID_PROY}/orden-compra/busqueda','ProyectoOrdenController@buscarHistorial');
Route::get('proyecto/orden-compra/{ID_ORDEN}/get-detalle','ProyectoOrdenController@getDetalleOrdenCompraEspera');
Route::post('proyecto/orden-compra/{ID_ORDEN}/aprobar-rechazar','ProyectoOrdenController@aprobarRechazarOrdenCompra');
Route::get('proyecto/orden-compra/{ID_ORDEN}/get-dato','ProyectoOrdenController@cargarDatosOCAprobadas');
Route::get('proyecto/cotizacion/{ID_COTI}/eliminar','ProyectoOrdenController@eliminarCotizacion');
Route::post('proyecto/orden-compra/solicitar-aporobacion','ProyectoOrdenController@solicitarAprobacionOC');
Route::get('proyecto/orden-compra/get-recepcion-detalle/{ID_RECE_MAT}','ProyectoOrdenController@verDetalleRecepcion');
Route::post('proyecto/orden-compra/anular-oc','ProyectoOrdenController@anularOC');
Route::get('proyecto/orden-compra/cerrar-oc/{id_orden}','ProyectoOrdenController@cerrarOC');
Route::get('proyecto/orden-compra/abrir-oc/{id_orden}','ProyectoOrdenController@abrirOC');
Route::get('proyecto/orden-compra/facturas-oc/{id_orden}','ProyectoOrdenController@facturasOC');

// PROYECTO PAGO
Route::post('proyecto/{ID_PROY}/pago/filtrar-ordenes','ProyectoPagoController@cargarHistorialOrdenesCompraFactura');
Route::get('proyecto/pago/orden-compra/{ID_ORDENC}','ProyectoPagoController@detalleOC');
Route::post('proyecto/{ID_PROY}/pago-empresa','ProyectoPagoController@cargarPagosEmpresa');
Route::post('proyecto/{ID_PROY}/generar-cartola','ProyectoPagoController@generarCartola');
Route::get('proyecto/cartola/{ID_CARTO}','ProyectoPagoController@cargarCartola');
Route::get('proyecto/cartola/{ID_CARTO}/detalle-empresa/{ID_EMPR}','ProyectoPagoController@cargarDetalleCartolaEmpresa');
Route::post('proyecto/cartola/{ID_CARTO}/consolidar-pago','ProyectoPagoController@consolidarPago');
Route::get('pago/{id_pago}/adjunto/{nombre_adjunto}','ProyectoPagoController@cargarAdjuntoConsolidado');
Route::post('proyecto/{ID_PROY}/historial-cartolas','ProyectoPagoController@cargarHistorialCartola');
Route::post('proyecto/{ID_PROY}/detalle-empresas','ProyectoPagoController@cargarEmpresasCartola');
Route::post('proyecto/empresa/{id_empresa}/detalle-pago','ProyectoPagoController@detallePagoEmpresa');
Route::post('proyecto/pago/guardar-documento','ProyectoPagoController@guardarDocumentoPago');
Route::get('orden/{id_ordenc}/pago/{id_pago}/{nombre_adjunto}','ProyectoPagoController@cargarUrlOrdenPago');
Route::post('proyecto/pago/agregar-notacredito','ProyectoPagoController@agregarNotaCreditoOrdenPago');
Route::get('orden/{id_ordenc}/pago/{id_pago}/nota-credito/{id_nota_cred}/{nombre_adjunto}','ProyectoPagoController@cargarUrlOrdenNotaCredito');
Route::get('proyecto/orden/{id_ordenc}/flujo-orden','ProyectoPagoController@obtenerFlujoOrden');
Route::get('proyecto/cartola/generar-excel/{ID_PROY}/{ID_CARTO}/{ID_EMPR}/{tipo}','ProyectoPagoController@convertirExcel');
Route::get('proyecto/cartola/generar-pdf/{ID_PROY}/{ID_CARTO}/{ID_EMPR}/{tipo}','ProyectoPagoController@generarPDF');
Route::post('proyecto/documento-tributario/filtrar-proveedor','ProyectoPagoController@proveedoresPendientesPago');
Route::get('proyecto/documento-tributario/{id_proy}/detalle-orden/{id_empr}','ProyectoPagoController@ordenesCompraPorProveedor');
Route::post('proyecto/documento-tributario/validar-factura','ProyectoPagoController@validarFactura');
Route::post('proyecto/documento-tributario/guardar-factura','ProyectoPagoController@guardarFactura');
Route::get('proyecto/{id_proy}/documento-tributario/{id_cons}/{nombre}','ProyectoPagoController@descargarFactura');
Route::get('proyecto/{id_cons}/{id_proy}/nota-credito-descargar/{nombre}','ProyectoPagoController@cargarUrlFacturaNotaCredito');

//ADJUNTOS
Route::get('cotizacion/{id_cons}/{id_proy}/adjunto/{id_coti}/{nombre_adjunto}','ProyectoOrdenController@cargarAdjunto');
Route::get('subcontrato/{id_subcon}/{nombre_adjunto}','ProyectoSubcontrataController@cargarUrlSubcontrato');
Route::get('subcontrato/{id_subcon}/pago/{id_pago}/{nombre_adjunto}','ProyectoSubcontrataController@cargarUrlSubcontratoPago');
Route::get('subcontrato/{id_subcon}/pago/{id_pago}/nota-credito/{id_nota_cred}/{nombre_adjunto}','ProyectoSubcontrataController@cargarUrlNotaCredito');
// SUBCONTRATAR
Route::post('proyecto/subcontratar/totales-presupuesto','ProyectoSubcontrataController@cargarTotalesPptoSubcontrata');
Route::post('proyecto/subcontratar/buscar-subcontratista','ProyectoSubcontrataController@buscarSubcontratista');
Route::post('proyecto/subcontratar/guardar','ProyectoSubcontrataController@guardarSubcontratar');
Route::post('proyecto/subcontratar/buscar','ProyectoSubcontrataController@buscarSubcontratos');
Route::get('proyecto/subcontratar/{id_subcon}','ProyectoSubcontrataController@cargarSubcontrato');
Route::post('proyecto/subcontratar/finalizar','ProyectoSubcontrataController@finalizarSubcontrato');
Route::post('proyecto/subcontratar/nuevo-pago','ProyectoSubcontrataController@nuevoPagoSubcontrato');
Route::post('proyecto/subcontratar/eliminar-pago','ProyectoSubcontrataController@eliminarPagoSubcontrato');
Route::post('proyecto/subcontratar/agregar-notacredito','ProyectoSubcontrataController@agregarNotaCreditoPagoSubcontrato');
Route::post('proyecto/subcontratar/eliminar-notacredito','ProyectoSubcontrataController@eliminarNotaCredito');
Route::post('proyecto/subcontratar/guardar-arriendo','ProyectoSubcontrataController@guardarSubcontratarArriendo');
Route::post('proyecto/subcontratar/{id_arriendo}/actualizar-arriendo','ProyectoSubcontrataController@gestionArriedo');
//CATEGORIA CONTROLLER
Route::resource('categoria','CategoriaController');
Route::post('categoria/subcategorias','CategoriaController@cargarSubcategorias');
Route::post('categoria/lista-por-tipo','CategoriaController@cargarPorTipo');
Route::post('categoria/buscar','CategoriaController@buscarCategorias');
//EMPRESA CONTROLLER
Route::post('empresa/{id_constructora}/subcontratista','EmpresaController@cargarSubcontratistas');
Route::post('empresa/{id_constructora}/filtrar-subcontratista','EmpresaController@filtrarSubcontratista');
Route::delete('empresa/subcontratista/{id_empresa}/eliminar','EmpresaController@eliminarSubcontratista');
Route::get('empresa/subcontratista/{id_empresa}','EmpresaController@getSubcontratista');
Route::get('empresa/bancos','EmpresaController@cargarBancos');
Route::get('empresa/tipo-cuentas','EmpresaController@cargarTipoCuentas');
Route::post('empresa/crear-empresa','EmpresaController@crearEmpresa');
Route::post('empresa/{id_constructora}/proveedores','EmpresaController@cargarProveedores');
Route::get('empresa/{id_constructora}/proveedores','EmpresaController@getProveedores');
Route::post('empresa/{id_constructora}/filtrar-proveedor','EmpresaController@filtrarProveedor');
Route::delete('empresa/proveedor/{id_empresa}/eliminar','EmpresaController@eliminarProveedor');
Route::get('empresa/proveedor/{id_proveedor}','EmpresaController@getProveedor');
Route::post('empresa/subcontratista-arriendos','EmpresaController@subcontratistaArriendo');

//MATERIAL CONTROLLER
Route::resource('material','MaterialController');
Route::post('material/unidad-medida/obtener','MaterialController@cargarUnidadMedida');
Route::post('material/guardar','MaterialController@crearMaterial');
Route::post('material/{id_constructora}/materiales','MaterialController@cargarMateriales');
Route::get('material/{id_constructora}/materiales','MaterialController@getMateriales');
Route::post('material/{id_constructora}buscar-materiales','MaterialController@buscarMateriales');
Route::delete('material/{id_material}/eliminar','MaterialController@eliminarMaterial');

//ARRIENDO CONTROLLER
Route::resource('arriendo','ArriendoController');
Route::post('arriendo/guardar','ArriendoController@crearArriendo');
Route::post('arriendo/{id_constructora}/arriendos','ArriendoController@cargarArriendos');
Route::post('arriendo/{id_constructora}buscar-arriendos','ArriendoController@buscarArriendos');
Route::delete('arriendo/{id_arriendo}/eliminar','ArriendoController@eliminarArriendo');
Route::get('arriendo/{id_constructora}/arriendos','ArriendoController@getArriendo');

//BODEGA CONTROLLER
Route::get('proyecto/{ID_PROY}/get-ordenes-aprobadas','BodegaController@cargarOCAprobadas');
Route::post('bodega/recepcion-compra/guardar-posponer','BodegaController@guardarRecepcionCompra');
Route::get('bodega/{ID_PROY}/listar-materiales','BodegaController@listarMateriales');
Route::get('bodega/{ID_PROY}/get-recepciones','BodegaController@historialRecepcion');
Route::get('bodega/{ID_PROY}/listar-subareas','BodegaController@listarSubAreas');
Route::post('bodega/{ID_PROY}/generar-entrega','BodegaController@generarEntrega');
Route::post('bodega/{ID_PROY}/historial-entrega','BodegaController@historialEntrega');
Route::post('bodega/{ID_PROY}/recepcion/filtrar-historial','BodegaController@filtarHistorialRecepcion');
Route::post('bodega/dui/emitir','BodegaController@emitirDUI');
Route::post('bodega/dui/listar-recepcion/{ID_PROY}','BodegaController@verDUIsRecibidas');
Route::post('bodega/dui/listar-emision/{ID_PROY}','BodegaController@verDUIsEmitidas');
Route::get('bodega/dui/get-detalle/{ID_DUI}','BodegaController@getDetalleDUI');
Route::post('bodega/dui/aceptar-rechazar','BodegaController@aceptarRechazarDUI');
Route::post('bodega/guia-despacho/get-guias/{ID_PROY}','BodegaController@listarGuiasDespacho');
Route::get('bodega/guia-despacho/get-detalle/{ID_GUIA}','BodegaController@detallleGuiaDespacho');
Route::post('bodega/guia-despacho/guardar-archivos/{ID_GUIA}','BodegaController@guardarGuiaDespacho');
Route::get('bodega/guia-despacho/descargar/{ID_GUIA}/{NOMBRE}','BodegaController@descargarGuia');
Route::get('bodega/dui/get-finalizadas/{ID_PROY}','BodegaController@getDUIGuiasDespachoFinalizadas');
Route::get('bodega/dui/get-lista-recepcion/{ID_PROY}','BodegaController@getDUIRecepcion');
Route::get('bodega/dui/get-detalle-recepcion/{ID_DUI}','BodegaController@detalleRececpionDUI');
Route::post('bodega/dui/guardar-recepcion/{ID_DUI}','BodegaController@recepcionarDUI');
Route::get('bodega/orden-compra/{ID_ORDENC}/get-orden-devolucion','BodegaController@getOrdenCompraDevolucion');
Route::post('bodega/orden-compra/generar-devolucion','BodegaController@solicitarAprobacionDevolucion');
Route::post('bodega/orden-compra/listar-historial/{ID_PROY}','BodegaController@getHistorialDevolucionProveedor');
Route::post('bodega/orden-compra/listar-historial-sin-paginacion/{ID_PROY}','BodegaController@obtenerHistorialDevolucionProv');
Route::post('bodega/orden-compra/aceptar-rechazar-devolucion','BodegaController@aceptarRechazarDevolucionProveedor');
Route::get('bodega/orden-compra/{ID_DEVO_PROV}/get-detalle-devolucion','BodegaController@getDetalleDevolucion');
Route::post('bodega/merma/generar-merma/{ID_PROY}','BodegaController@generarMerma');
Route::post('bodega/merma/historial-merma/{ID_PROY}','BodegaController@historialMermas');
Route::post('bodega/merma/aprobar-rechazar','BodegaController@aprobarRechazarMerma');
Route::get('bodega/inventario/valoracion/{ID_PROY}','BodegaController@inventarioValoracion');
Route::post('bodega/stock-materiales/{ID_PROY}','BodegaController@getStockInventario');
Route::post('bodega/stock-materiales/actualizar/limite','BodegaController@definirStockCritico');
Route::post('bodega/traspasar-inventario','BodegaController@traspasarInventario');
Route::get('bodega/inventario/valor-total/{ID_PROY}','BodegaController@calcularPromInventario');
Route::post('bodega/inventario-extra','BodegaController@guardarInventarioExtra');
Route::post('bodega/inventario-extra/{ID_PROY}','BodegaController@historialInventarioExtra');
Route::post('bodega/inventario-extra/{id_inventario}','BodegaController@editarHistorialExtra');
Route::delete('bodega/inventario-extra/{id_inventario}/eliminar','BodegaController@eliminarHistorialExtra');
Route::get('bodega/recepcion/descargar-pdf/{ID_ORD}', 'BodegaController@generarPDFRecepcion');
Route::get('bodega/recepcion/descargar-pdf-dui/{ID_DUI}', 'BodegaController@generarPDFDui');

// CARGOS
Route::post('cargo/{ID_CONST}/crear-cargo','CargoController@crearCargo');
Route::post('cargo/{ID_CARG}/asignar-subordinados','CargoController@asignarAprobacion');
Route::get('cargo/{ID_CONST}/{ID_CARG}/get-cargos','CargoController@getCargos');
Route::get('cargo/{ID_CONST}/{ID_CARG}/listar-cargos','CargoController@listarCargos');
Route::get('cargo/{ID_CARG}/ver-cargo','CargoController@verCargo');
Route::get('cargo/{ID_CARG}/cargar-modulos','CargoController@listaModulos');
Route::post('cargo/{ID_CARG}/asignar-modulos','CargoController@asignarModulos');
Route::get('cargo/{ID_CONST}/{ID_CARG}/listar-cargos-empleado','CargoController@listarCargosEmpleado');
Route::delete('cargo/{ID_CARG}/eliminar','CargoController@eliminarCargo');

//PAGO SOFTWARE
Route::get('pago-soft/get-cobro-uf','PagoSoftController@getCobroUF');
Route::get('pago-soft/get-num-uf','PagoSoftController@getNumUF');
Route::post('pago-soft/comenzar-webpay','PagoSoftController@comenzarWebpay');
Route::get('pago-soft/generar-pago-webpay/{id_pago}','PagoSoftController@pagarPorWebpay');
Route::post('pago-soft/respuesta-webpay/{orden}','PagoSoftController@respuestaWebpay');
Route::post('pago-soft/fin-pago-webpay/{orden}','PagoSoftController@finPagoWebpay');
Route::post('pago-soft/pendientes','PagoSoftController@obtenerPendientes');
Route::post('pago-soft/historial','PagoSoftController@obtenerHistorial');
Route::post('pago-soft/actualizar-fact','PagoSoftController@actualizarFacturacion');

// RUTAS PARA PRUEBAS


Route::get('/eliminar-demos', function () {
  app('App\Console\Commands\EliminarDemos')->handle();
  return "Demos eliminados";
});
