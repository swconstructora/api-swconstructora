<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cartola extends Model
{
    //referenciar tabla con la clase
    protected $table = 'CARTOLA';
    protected $primaryKey = 'ID_CARTO';
    // desactiva el metodo de la fecha de creacion de laravel
    public $timestamps = false;

    public function DetalleCartola(){
        return $this->belongsToMany(Pago::class,'DETALLE_CARTOLA','CARTOLA_ID_CARTO','PAGO_ID_PAGO')
                                  ->withPivot('TOTAL_FACTURADO_IVA')->withPivot('FECHA_VENCIMIENTO')
                                  ->withPivot('ESTADO_CARTO')->withPivot('TOTAL_NETO')
                                  ->withPivot('ID_EMPR');
    }

    public function consolidado() {
      return $this->hasOne(Consolidado::class, 'ID_CARTO', 'ID_CARTO');
    }
}
