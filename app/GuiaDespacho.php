<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GuiaDespacho extends Model
{
  //referenciar tabla con la clase
  protected $table = 'GUIA_DESPACHO';
  protected $primaryKey = 'ID_GUIA';
  // desactiva el metodo de la fecha de creacion de laravel
  public $timestamps = false;

  public function dui() {
    return $this->hasOne(DUI::class, 'ID_DUI', 'DUI_ID_DUI')
      ->with(['proyectoE','proyectoR']);
  }

  public function duiDetalle() {
    return $this->hasOne(DUI::class, 'ID_DUI', 'DUI_ID_DUI')
      ->with(['proyectoE','proyectoR','detalle']);
  }

  public function dap() {
    return $this->hasOne(DevolucionProveedor::class, 'ID_DEVO_PROV', 'DEVOLUCION_PROVEEDOR_ID_DEVO_PROV')
      ->with(['ocProveedor','proyecto']);
  }

  public function dapDetalle() {
    return $this->hasOne(DevolucionProveedor::class, 'ID_DEVO_PROV', 'DEVOLUCION_PROVEEDOR_ID_DEVO_PROV')
      ->with(['ocProveedor','proyecto','detalle_devolucion','EmpleadoC','DuenoC']);
  }
}
