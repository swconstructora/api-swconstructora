<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    //referenciar tabla con la clase
    protected $table = 'EMPRESA';
    protected $primaryKey = 'ID_EMPR';
    // desactiva el metodo de la fecha de creacion de laravel
    public $timestamps = false;

    public function contactos(){
        return $this->hasMany(Contacto::class,'EMPRESA_ID_EMPR');
    }

    public function cotizaciones() {
      return $this->hasMany(Cotizacion::class ,'EMPRESA_ID_EMPR', 'ID_EMPR');
    }

    public function pagos() {
      return $this->hasMany(Pago::class ,'EMPRESA_ID_EMPR', 'ID_EMPR');
    }

    public function ordenesPendientes() {
      return $this->hasMany(OrdenCompra::class, 'ID_EMPR', 'ID_EMPR')->whereIn('ESTADO_ORDEN',['A','RP']);
    }

    public function DetalleSubcategoria(){
        return $this->belongsToMany(Subcategoria::class,'DETALLE_SCON_SUBC','EMPRESA_ID_EMPR','SUBCATEGORIA_ID_SUBC');
    }

}
