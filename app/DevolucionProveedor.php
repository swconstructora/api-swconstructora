<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DevolucionProveedor extends Model
{
  //referenciar tabla con la clase
  protected $table = 'DEVOLUCION_PROVEEDOR';
  protected $primaryKey = 'ID_DEVO_PROV';
  // desactiva el metodo de la fecha de creacion de laravel
  public $timestamps = false;

  public function detalle_devolucion() {
    return $this->belongsToMany(DetalleCotizacion::class, 'DETALLE_DEVOLUCION','DEVOLUCION_PROVEEDOR_ID_DEVO_PROV','DETALLE_COTIZACION_ID_DETA_COTI')
      ->withPivot('CANT_DEVOLVER','PREC_NETO_DEVO')->with(['Material']);
  }

  public function ordenCompra() {
    return $this->hasOne(OrdenCompra::class, 'ID_ORDENC', 'ID_ORDENC')->with('Flujo');
  }

  public function ocProveedor() {
    return $this->hasOne(OrdenCompra::class, 'ID_ORDENC', 'ID_ORDENC')->with('empresa');
  }

  public function proyecto() {
    return $this->hasOne(Proyecto::class, 'ID_PROY', 'ID_PROY');
  }

  public function guiaDespacho() {
    return $this->hasOne(GuiaDespacho::class, 'DEVOLUCION_PROVEEDOR_ID_DEVO_PROV', 'ID_DEVO_PROV');
  }

  public function EmpleadoC() {
    return $this->belongsToMany(Empleado::class, 'USUARIO','ID_USU','ID_USU','ID_USU_C','USUARIO_ID_USU')
      ->with('cargo');
  }

  public function DuenoC() {
    return $this->belongsToMany(Dueno::class, 'USUARIO','ID_USU','ID_USU','ID_USU_C','USUARIO_ID_USU')
      ->with('cargo');
  }

  public function EmpleadoR() {
    return $this->belongsToMany(Empleado::class, 'USUARIO','ID_USU','ID_USU','ID_USU_R','USUARIO_ID_USU')
      ->with('cargo');
  }

  public function DuenoR() {
    return $this->belongsToMany(Dueno::class, 'USUARIO','ID_USU','ID_USU','ID_USU_R','USUARIO_ID_USU')
      ->with('cargo');
  }

}
