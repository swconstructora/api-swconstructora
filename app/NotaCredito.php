<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotaCredito extends Model
{
    //referenciar tabla con la clase
    protected $table = 'NOTA_CREDITO';
    protected $primaryKey = 'ID_NOTA_CRED';
    // desactiva el metodo de la fecha de creacion de laravel
    public $timestamps = false;

    protected $casts = [
      'NOTA_ASIGNADA' => 'boolean'
    ];
}
