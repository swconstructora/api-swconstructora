<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModeloNegocio extends Model
{
  //referenciar tabla con la clase
  protected $table = 'MODELO_NEGOCIO';
  protected $primaryKey = 'ID_MODE_NEGO';
  // desactiva el metodo de la fecha de creacion de laravel
  public $timestamps = false;
}
