<?php

namespace App;

class Entorno
{
// API
  // Amazon produccion
  // private $route_api = "http://api-sportvita.us-east-2.elasticbeanstalk.com";
  // Hosting compartido produccion
  public $route_api = "https://api-swconstructora.elinous.cl/";
  // Local desarrollo
  // private $route_api = "http://127.0.0.1:8000";

// WEB
  // Hosting compartido produccion
  public $route_web = "https://budgeter.cl/";
  // Local desarrollo
  // public $route_web = "http://localhost:4200/#/";

// Almacenamiento de imagenes y archivos de $usuarios
  // Amazon produccion
  // public $origen_storage= "s3";
  // Hosting compartido produccion
  public $origen_storage= "local";


}
