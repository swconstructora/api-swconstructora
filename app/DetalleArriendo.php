<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleArriendo extends Model
{
    //referenciar tabla con la clase
    protected $table = 'DETALLE_ARRIENDO';
    protected $primaryKey = 'ID_DET_ARRIEN';
    // desactiva el metodo de la fecha de creacion de laravel
    public $timestamps = false;

    public function arriendo() {
      return $this->hasOne(Arriendo::class, 'ID_ARRIEN', 'ID_ARRIEN');
    }

}
