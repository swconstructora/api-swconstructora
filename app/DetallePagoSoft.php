<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetallePagoSoft extends Model
{
    //referenciar tabla con la clase
    protected $table = 'DETALLE_PAGO_SOFT';
    protected $primaryKey = 'ID_DETA_PAGO_SOFT';
    // desactiva el metodo de la fecha de creacion de laravel
    public $timestamps = false;

    public function proyecto() {
      return $this->hasOne(Proyecto::class, 'ID_PROY', 'PROYECTO_ID_PROY');
    }
}
