<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivoProyecto extends Model
{
    //referenciar tabla con la clase
    protected $table = 'ACTIVO_PROYECTO';
    protected $primaryKey = 'ID_ACTIPROY';
    // desactiva el metodo de la fecha de creacion de laravel
    public $timestamps = false;
}
