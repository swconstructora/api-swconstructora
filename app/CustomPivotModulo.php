<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class CustomPivotModulo extends Pivot
{
    protected $casts = [
      'PUEDE_EDITAR' => 'boolean',
      'PUEDE_VER' => 'boolean',
      'RECIBE_FACTURA' => 'boolean'
    ];
}
