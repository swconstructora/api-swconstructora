<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleDevolucion extends Model
{
  //referenciar tabla con la clase
  protected $table = 'DETALLE_DEVOLUCION';
  protected $primaryKey = 'ID_DETA_DEVO';
  // desactiva el metodo de la fecha de creacion de laravel
  public $timestamps = false;
}
