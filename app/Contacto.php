<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contacto extends Model
{
    //referenciar tabla con la clase
    protected $table = 'CONTACTO_EMPRESA';
    protected $primaryKey = 'ID_CONTA';
    // desactiva el metodo de la fecha de creacion de laravel
    public $timestamps = false;
}
