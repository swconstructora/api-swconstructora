<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proyecto extends Model
{
    //referenciar tabla con la clase
    protected $table = 'PROYECTO';
    protected $primaryKey = 'ID_PROY';
    // desactiva el metodo de la fecha de creacion de laravel
    public $timestamps = false;

    public function DetalleEmpleados(){
        return $this->belongsToMany(Empleado::class,'DETALLE_EMP_PROY','PROYECTO_ID_PROY','EMPLEADO_ID_EMP')
          ->using(CustomPivotModulo::class)->withPivot('ID_ASIGNADOR','MONTO_LIMITE','HORAS_LIMITE','MONTO_ILIMITADO','HORA_ILIMITADA','RECIBE_FACTURA');
    }

    public function DetalleSubarea(){
        return $this->belongsToMany(Subarea::class,'DETALLE_SAREA_PROY','PROYECTO_ID_PROY','SUBAREA_ID_SAREA')
          ->withPivot('MONTO_MATERIAL','MONTO_PEDIDO','SUBAREA_ID_SAREA');
    }

    public function constructora() {
      return $this->hasOne(Constructora::class, 'ID_CONS', 'CONSTRUCTORA_ID_CONS');
    }
}
