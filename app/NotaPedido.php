<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotaPedido extends Model
{
    //referenciar tabla con la clase
    protected $table = 'NOTA_PEDIDO';
    protected $primaryKey = 'ID_NOTA_PED';
    // desactiva el metodo de la fecha de creacion de laravel
    public $timestamps = false;

    public function DetalleNotaPedido(){
        return $this->belongsToMany(Subarea::class,'DETALLE_NOTA_PEDIDO','NOTA_PEDIDO_ID_NOTA_PED','SUBAREA_ID_SAREA')
                                    ->withPivot('CANTIDAD_DNP');
    }

    public function DetalleCotizacion() {
      return $this->hasMany(DetalleCotizacion::class, 'NOTA_PEDIDO_ID_NOTA_PED' ,'ID_NOTA_PED')
        ->whereIn('ESTADO_COTIZACION',['E','N'])
        ->with(['Empresa']);
    }

    public function DetalleCotizacionModal() {
      return $this->hasMany(DetalleCotizacion::class, 'NOTA_PEDIDO_ID_NOTA_PED' ,'ID_NOTA_PED')->with(['Cotizacion']);
    }

    public function Material() {
      return $this->hasOne(Material::class, 'ID_MATE', 'MATERIAL_ID_MATE')
        ->with(['Unidad','Subcategoria']);
    }

    public function Flujo() {
      return $this->hasMany(FlujoNotaPedido::class, 'NOTA_PEDIDO_ID_NOTA_PED', 'ID_NOTA_PED')
        ->orderBy('ID_FLUJO_NOTA','ASC')->with(['Empleado','Dueno']);
    }

    public function Creador() {
      return $this->hasOne(FlujoNotaPedido::class, 'NOTA_PEDIDO_ID_NOTA_PED', 'ID_NOTA_PED')
        ->where('TIPO_FLUJO_NOTA','C');
    }
}
