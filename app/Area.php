<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    //referenciar tabla con la clase
    protected $table = 'AREA';
    protected $primaryKey = 'ID_AREA';
    // desactiva el metodo de la fecha de creacion de laravel
    public $timestamps = false;

    public function subareas() {
      return $this->hasMany(Subarea::class, 'AREA_ID_AREA', 'ID_AREA');
    }
}
