<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dueno extends Model
{
    //referenciar tabla con la clase
    protected $table = 'DUENO';
    protected $primaryKey = 'ID_DUE';
    // desactiva el metodo de la fecha de creacion de laravel
    public $timestamps = false;

    public function constructoras(){
        return $this->hasMany(Constructora::class,'DUENO_ID_DUE');
    }

    public function cargo() {
      return $this->hasOne(Cargo::class,'ID_CARG','CARGO_ID_CARG');
    }

    public function usuario() {
      return $this->hasOne(Usuario::class, 'ID_USU', 'USUARIO_ID_USU');
    }

}
