<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnidadMedida extends Model
{
    //referenciar tabla con la clase
    protected $table = 'UNIDAD_MEDIDA';
    protected $primaryKey = 'ID_UNID';
    // desactiva el metodo de la fecha de creacion de laravel
    public $timestamps = false;
}
