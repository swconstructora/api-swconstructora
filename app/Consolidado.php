<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Consolidado extends Model
{
    //referenciar tabla con la clase
    protected $table = 'CONSOLIDADO';
    protected $primaryKey = 'ID_CONSO';
    // desactiva el metodo de la fecha de creacion de laravel
    public $timestamps = false;

    public function DetalleConsolidado(){
        return $this->belongsToMany(Pago::class,'DETALLE_CONSOLIDADO','CONSOLIDADO_ID_CONSO','PAGO_ID_PAGO');
    }
}
