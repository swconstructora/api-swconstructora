<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecepcionMaterial extends Model
{
  //referenciar tabla con la clase
  protected $table = 'RECEPCION_MATERIAL';
  protected $primaryKey = 'ID_RECE_MAT';
  // desactiva el metodo de la fecha de creacion de laravel
  public $timestamps = false;

  public function detalle() {
    return $this->hasMany(DetalleRecepcion::class, 'RECEPCION_MATERIAL_ID_RECE_MAT', 'ID_RECE_MAT')
      ->with(['detalleC','detalleD']);
  }

  public function OC() {
    return $this->hasOne(OrdenCompra::class, 'ID_ORDENC', 'NRO_ID')->with(['flujo','empresa']);
  }

  public function DUI() {
    return $this->hasOne(DUI::class, 'ID_DUI', 'NRO_ID')
      ->with(['proyectoE','entrega','guiaDespacho','flujo']);
  }
}
