<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FlujoDUI extends Model
{
  //referenciar tabla con la clase
  protected $table = 'FLUJO_DUI';
  protected $primaryKey = 'ID_FLUJO_DUI';
  // desactiva el metodo de la fecha de creacion de laravel
  public $timestamps = false;

  public function Empleado() {
    return $this->belongsToMany(Empleado::class, 'USUARIO','ID_USU','ID_USU','USUARIO_ID_USU','USUARIO_ID_USU')
      ->with('cargo');
  }

  public function Dueno() {
    return $this->belongsToMany(Dueno::class, 'USUARIO','ID_USU','ID_USU','USUARIO_ID_USU','USUARIO_ID_USU')
      ->with('cargo');
  }
}
