<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subcategoria extends Model
{
    //referenciar tabla con la clase
    protected $table = 'SUBCATEGORIA';
    protected $primaryKey = 'ID_SUBC';
    // desactiva el metodo de la fecha de creacion de laravel
    public $timestamps = false;

    public function categoria(){
        return $this->hasOne(Categoria::class,'ID_CATE','CATEGORIA_ID_CATE');
    }
}
