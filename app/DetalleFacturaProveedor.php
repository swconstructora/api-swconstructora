<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleFacturaProveedor extends Model
{
  //referenciar tabla con la clase
  protected $table = 'DETALLE_FACTURA_PROVEEDOR';
  protected $primaryKey = 'ID_DETA';
  // desactiva el metodo de la fecha de creacion de laravel
  public $timestamps = false;

  public function factura() {
    return $this->hasOne(FacturaProveedor::class, 'ID_FACT', 'ID_FACT_PROV');
  }

  public function detalle_cotizacion() {
    return $this->hasOne(DetalleCotizacion::class, 'ID_DETA_COTI', 'ID_DETA_COTI');
  }

  public function material() {
    return $this->hasOne(Material::class, 'ID_MATE', 'ID_MATE')->with('Unidad');
  }
}
