<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InventarioExtra extends Model
{
  //referenciar tabla con la clase
  protected $table = 'INVENTARIO_EXTRA';
  protected $primaryKey = 'ID_INVENT_EXT';
  // desactiva el metodo de la fecha de creacion de laravel
  public $timestamps = false;

  public function material() {
    return $this->hasOne(Material::class, 'ID_MATE', 'MATERIAL_ID_MATE')->with(['Unidad','Subcategoria']);
  }
}
