<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Potencial extends Model
{
  //referenciar tabla con la clase
  protected $table = 'POTENCIAL';
  protected $primaryKey = 'ID_POTN';
  // desactiva el metodo de la fecha de creacion de laravel
  public $timestamps = false;
}
