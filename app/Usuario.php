<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Usuario extends Authenticatable
{
    //referenciar tabla con la clase
    protected $table = 'USUARIO';
    protected $primaryKey = 'ID_USU';
        protected $hidden = [
            'CONTRASENA_USU'
        ];
    // desactiva el metodo de la fecha de creacion de laravel
    public $timestamps = false;
    //
    public function dueno(){
        return $this->hasOne(Dueno::class,'USUARIO_ID_USU');
    }
    public function empleado(){
        return $this->hasOne(Empleado::class,'USUARIO_ID_USU')->with('constructora');
    }

    //
    // public function DetalleNotificacion(){
    //     return $this->belongsToMany(ConfigNotificacion::class,'DETALLE_CONF_NOTI','USUARIO_ID_USU','CONFIG_NOTIFICACION_ID_CONF')
    //     ->withPivot('CORREO_CONF')->withPivot('PUSH_CONF');
    // }

}
