<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Presupuesto extends Model
{
    //referenciar tabla con la clase
    protected $table = 'PRESUPUESTO';
    protected $primaryKey = 'ID_PRESU';
    // desactiva el metodo de la fecha de creacion de laravel
    public $timestamps = false;

    public function DetallePresupuesto(){
        return $this->belongsToMany(Subcategoria::class,'DETALLE_PRESUPUESTO','PRESUPUESTO_ID_PRESU','SUBCATEGORIA_ID_SUBC')->withPivot('MONTO_PRESU');
    }

}
