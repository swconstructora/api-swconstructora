<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoBanco extends Model
{
    //referenciar tabla con la clase
    protected $table = 'TIPO_CUENTA_BANCO';
    protected $primaryKey = 'ID_TIPO_CUENTA';
    // desactiva el metodo de la fecha de creacion de laravel
    public $timestamps = false;

}
