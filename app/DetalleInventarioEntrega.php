<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleInventarioEntrega extends Model
{
  protected $table = 'DETALLE_INVENTARIO_ENTREGA';
  // protected $primaryKey = 'ID_DETA_COTI';
  public $timestamps = false;

}
