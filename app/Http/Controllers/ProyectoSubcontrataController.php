<?php

namespace App\Http\Controllers;

use App\Proyecto;
use App\Categoria;
use App\Subcategoria;
use App\Presupuesto;
use App\Empresa;
use App\Subcontratar;
use App\Contacto;
use App\Pago;
use App\NotaCredito;
use App\FlujoPago;
use App\DetalleArriendo;
use App\DetallePagoArriendo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use DB;
class ProyectoSubcontrataController extends Controller
{

  public function cargarTotalesPptoSubcontrata(Request $request){
    $ID_PROY = $request->input('ID_PROY');
    $ID_CATE = $request->input('ID_CATE');
    $ID_SUBC = $request->input('ID_SUBC');

    $totales = (object) array(
    'total_cat_comprometido' => 0,
    'total_cat_asignado' => 0,
    'total_cat_libre' => 0,
    'porcentaje_cat_comprometido' => 0,
    'porcentaje_cat_libre' => 100,
    'total_subcat_comprometido' => 0,
    'total_subcat_asignado' => 0,
    'total_subcat_libre' => 0,
    'porcentaje_subcat_comprometido' => 0,
    'porcentaje_subcat_libre' => 100,
    );

      // return $totales;
    if($ID_PROY && $ID_CATE && $ID_SUBC){
      // ->where('PRESUPUESTO.ID_PRESU', $ID_PRESU )
      // $total_cat_comprometido = Presupuesto::join('DETALLE_PRESUPUESTO','PRESUPUESTO_ID_PRESU','PRESUPUESTO.ID_PRESU')
      //                                 ->where('PRESUPUESTO.PROYECTO_ID_PROY',$ID_PROY )
      //                                 ->orderBy('ID_PRESU', 'desc');
      //                                 ->join('SUBCATEGORIA','SUBCATEGORIA.ID_SUBC','DETALLE_PRESUPUESTO.SUBCATEGORIA_ID_SUBC')
      //                                 ->join('CATEGORIA','CATEGORIA.ID_CATE','SUBCATEGORIA.CATEGORIA_ID_CATE')
      //                                 ->where('CATEGORIA.ID_CATE',$ID_CATE)
      //                                 ->where('CATEGORIA.TIPO_CATE','SC')->sum('DETALLE_PRESUPUESTO.MONTO_PRESU');
      $totales->total_cat_comprometido = Subcontratar::join('SUBCATEGORIA','SUBCATEGORIA.ID_SUBC','SUBCONTRATAR.SUBCATEGORIA_ID_SUBC')
                               ->join('CATEGORIA','CATEGORIA.ID_CATE','SUBCATEGORIA.CATEGORIA_ID_CATE')
                               ->where('CATEGORIA.ID_CATE',$ID_CATE)->sum("MONTO_PPTO_SUBCON");


      $totales->total_cat_asignado = Presupuesto::join('DETALLE_PRESUPUESTO','PRESUPUESTO_ID_PRESU','PRESUPUESTO.ID_PRESU')
                              ->where('PRESUPUESTO.PROYECTO_ID_PROY',$ID_PROY )
                              ->join('SUBCATEGORIA','SUBCATEGORIA.ID_SUBC','DETALLE_PRESUPUESTO.SUBCATEGORIA_ID_SUBC')
                              ->join('CATEGORIA','CATEGORIA.ID_CATE','SUBCATEGORIA.CATEGORIA_ID_CATE')
                              ->where('CATEGORIA.ID_CATE',$ID_CATE)
                              ->whereRaw('ID_PRESU = (select MAX(ID_PRESU) from PRESUPUESTO WHERE '.$ID_PROY.' = PROYECTO_ID_PROY)')
                              ->sum('DETALLE_PRESUPUESTO.MONTO_PRESU');

      $totales->total_cat_libre = intval($totales->total_cat_asignado) - intval($totales->total_cat_comprometido);

      if($totales->total_cat_comprometido != 0 && $totales->total_cat_asignado != 0){
        $totales->porcentaje_cat_comprometido = intval($totales->total_cat_comprometido) * 100 /intval($totales->total_cat_asignado);
        $totales->porcentaje_cat_libre = 100-$totales->porcentaje_cat_comprometido;
      }


      $totales->total_subcat_comprometido = Subcontratar::where('SUBCATEGORIA_ID_SUBC',$ID_SUBC)->sum("MONTO_PPTO_SUBCON");

      $totales->total_subcat_asignado = Presupuesto::join('DETALLE_PRESUPUESTO','PRESUPUESTO_ID_PRESU','PRESUPUESTO.ID_PRESU')
                              ->where('PRESUPUESTO.PROYECTO_ID_PROY',$ID_PROY )
                              ->join('SUBCATEGORIA','SUBCATEGORIA.ID_SUBC','DETALLE_PRESUPUESTO.SUBCATEGORIA_ID_SUBC')
                              ->where('SUBCATEGORIA.ID_SUBC',$ID_SUBC)
                              ->whereRaw('ID_PRESU = (select MAX(ID_PRESU) from PRESUPUESTO WHERE '.$ID_PROY.' = PROYECTO_ID_PROY)')
                              ->sum('DETALLE_PRESUPUESTO.MONTO_PRESU');

      $totales->total_subcat_libre = intval($totales->total_subcat_asignado) - intval($totales->total_subcat_comprometido);
        if($totales->total_subcat_comprometido != 0 && $totales->total_subcat_asignado != 0){
          $totales->porcentaje_subcat_comprometido = intval($totales->total_subcat_comprometido) * 100 /intval($totales->total_subcat_asignado);
          $totales->porcentaje_subcat_libre = 100-$totales->porcentaje_subcat_comprometido;
      }
      }

      return response()->json(['code' => 200, 'message' => 'Totales encontrados','data'=>$totales],200);
  }

  public function buscarSubcontratista(Request $request){
      $busqueda = $request->input('texto_busqueda');
      $ID_CONS = $request->input('ID_CONS');
      $ID_SUBC = $request->input('ID_SUBC');

      $resultados = Empresa::where('TIPO_EMPR','S')
                            ->where('CONSTRUCTORA_ID_CONS',$ID_CONS)
                            ->join('DETALLE_SCON_SUBC','DETALLE_SCON_SUBC.EMPRESA_ID_EMPR','ID_EMPR')
                            ->with(['contactos'])
                            ->where('DETALLE_SCON_SUBC.SUBCATEGORIA_ID_SUBC','=',$ID_SUBC)
                            ->where(function ($query) use ($busqueda){
                              $query->orWhereRaw("RAZON_SOCIAL_EMPR like '%$busqueda%'")
                              ->orWhereRaw("NOMBRE_EMPR like '%$busqueda%'");
                            })->get();



      return $resultados;
  }

  public function guardarSubcontratar(Request $request){

    $subcontratar = new Subcontratar();
    date_default_timezone_set('America/Santiago');
    $now = new \DateTime();

    $subcontratar->FECHA_CREACION_SUBCON = $now;
    $subcontratar->FECHA_INICIO_SUBCON = $request->input('FECHA_INICIO_SUBCON');
    $subcontratar->FECHA_TERMINO_SUBCON = $request->input('FECHA_TERMINO_SUBCON');
    $subcontratar->MONTO_PPTO_SUBCON = $request->input('MONTO_PPTO_SUBCON');
    $subcontratar->OBSERVACION_SUBCON = $request->input('OBSERVACION_SUBCON');
    $subcontratar->ESTADO_SUBCON = 'P';
    $subcontratar->EMPRESA_ID_EMPR = $request->input('ID_EMPR');
    $subcontratar->PROYECTO_ID_PROY = $request->input('ID_PROY');
    $subcontratar->SUBCATEGORIA_ID_SUBC = $request->input('ID_SUBC');
    $subcontratar->save();


    $arch64 = $request->input('ARCHIVO');
    $arch64_nombre = $request->input('NOMBRE_ARCH');
    $arch64_tipo = $request->input('TIPO_ARCH');
    $ID_CONS = Proyecto::find($subcontratar->PROYECTO_ID_PROY)->value("CONSTRUCTORA_ID_CONS");
    // Procesar base 64
    $archivo = base64_decode(preg_replace('#^data:'.$arch64_tipo.';base64,#i', '', $arch64));
    $subcontratarEdit = Subcontratar::find($subcontratar->ID_SUBCON);
    file_put_contents($arch64_nombre , $archivo);
    $file = fopen($arch64_nombre,'r+');
    Storage::disk(app('App\Entorno')->origen_storage)->put('archivos/cons/'.$ID_CONS.'/proyecto/'.$subcontratar->PROYECTO_ID_PROY.'/subcontratos/'.$subcontratar->ID_SUBCON.'/'.$arch64_nombre,  $file);
    ftruncate($file,0);
    fclose($file);

    // La ruta usa el cargarUrlSubcontrato
    $subcontratarEdit->CONTRATO_URL= app('App\Entorno')->route_api.'subcontrato/'.$subcontratar->ID_SUBCON.'/'.$arch64_nombre;
    $subcontratarEdit->NOMBRE_CONTRATO = $arch64_nombre;
    $subcontratarEdit->save();

    return response()->json(['code' => 200, 'message' => 'Subcontrato generado con éxito'],200);
  }

  public function cargarUrlSubcontrato($ID_SUBCON,$NOMBRE_ARCH){
      $NOMBRE= trim($NOMBRE_ARCH);
      $subcontratar = Subcontratar::find($ID_SUBCON);
      $ID_CONS = Proyecto::find($subcontratar->PROYECTO_ID_PROY)->value("CONSTRUCTORA_ID_CONS");
      $file =storage_path().'/app/archivos/cons/'.$ID_CONS.'/proyecto/'.$subcontratar->PROYECTO_ID_PROY.'/subcontratos/'.$ID_SUBCON.'/'.$NOMBRE;
      return response()->download($file,$NOMBRE);
  }

  public function buscarSubcontratos(Request $request){

    $busqueda_nombre = $request->input('busqueda_nombre');
    $busqueda_rut = $request->input('busqueda_rut');
    $ID_CONS = $request->input('ID_CONS');
    $ID_SUBC = $request->input('ID_SUBC');
    $ID_PROY = $request->input('ID_PROY');
    $por_pagina = $request->input('POR_PAGINA');

      $resultados = Empresa::where('TIPO_EMPR','S')
                            ->where('CONSTRUCTORA_ID_CONS',$ID_CONS)
                            ->join('SUBCONTRATAR','SUBCONTRATAR.EMPRESA_ID_EMPR','ID_EMPR')
                            ->with(['contactos'])
                            ->where('SUBCONTRATAR.PROYECTO_ID_PROY', $ID_PROY)
                            ->where(function ($query) use ($ID_SUBC, $busqueda_nombre,$busqueda_rut){
                              if($ID_SUBC != null || $ID_SUBC != ''){
                                if($busqueda_nombre != null || $busqueda_nombre != ''){
                                    $query->where('SUBCONTRATAR.SUBCATEGORIA_ID_SUBC', $ID_SUBC)
                                    ->whereRaw("EMPRESA.NOMBRE_EMPR like '%$busqueda_nombre%'")
                                    ->orWhereRaw("EMPRESA.RAZON_SOCIAL_EMPR like '%$busqueda_nombre%'");
                                } else{
                                  $query->where('SUBCONTRATAR.SUBCATEGORIA_ID_SUBC', $ID_SUBC);
                                }
                              }
                              if($busqueda_nombre != null || $busqueda_nombre != ''){
                                $query->orWhereRaw("EMPRESA.RAZON_SOCIAL_EMPR like '%$busqueda_nombre%'")
                                ->orWhereRaw("EMPRESA.NOMBRE_EMPR like '%$busqueda_nombre%'");
                              }
                              if($busqueda_rut != null || $busqueda_rut != ''){
                                $query->orWhereRaw("EMPRESA.RUT_EMPR like '%$busqueda_rut%'");
                              }

                            })
                            ->orderBy('ID_SUBCON','desc')
                            ->paginate($por_pagina);



      return $resultados;
  }

  public function cargarSubcontrato($ID_SUBCON){
      $subcontrato = Subcontratar::where('ID_SUBCON', $ID_SUBCON)
                                  ->join('EMPRESA','EMPRESA.ID_EMPR','EMPRESA_ID_EMPR')
                                  ->with(['pagos' =>function($query){
                                    $query->selectRaw('PAGO.ID_PAGO,PAGO.SUBCONTRATAR_ID_SUBCON, PAGO.NUM_FACTURA,PAGO.MONTO_NETO,
                                    PAGO.FECHA_CREACION_PAGO,  PAGO.APLICA_IVA, PAGO.FACTURA_URL,PAGO.FECHA_PAGADO,PAGO.NUM_TRANSACCION,PAGO.ESTADO_PAGO,
                                    IFNULL(SUM(NOTA_CREDITO.MONTO_NETO), 0)as MONTO_NETO_NCRED,
                                    (PAGO.MONTO_NETO - IFNULL(SUM(NOTA_CREDITO.MONTO_NETO), 0)) as MONTO_APROBADO_NETO,
                                    IF(PAGO.APLICA_IVA = "S", (PAGO.MONTO_TOTAL - IFNULL(SUM(NOTA_CREDITO.MONTO_TOTAL), 0)), "EXENTA") as MONTO_APROBADO_IVA')
                                    ->leftJoin('NOTA_CREDITO','NOTA_CREDITO.PAGO_ID_PAGO','PAGO.ID_PAGO')
                                    ->groupBy('PAGO.ID_PAGO');
                                    // code...
                                  }])->first();

      $subcontrato->contactos = Contacto::where('EMPRESA_ID_EMPR',$subcontrato->ID_EMPR)->get();
      if($subcontrato->TIPO_SUBCON == 'A'){
        $detalle_subcate = Subcategoria::join('DETALLE_ARRIENDO','DETALLE_ARRIENDO.ID_SUBC','=','SUBCATEGORIA.ID_SUBC')
                                      ->where('SUBCONTRATAR_ID_SUBCON',$subcontrato->ID_SUBCON)
                                      ->groupBy('DETALLE_ARRIENDO.ID_SUBC')
                                      ->selectRaw('SUBCATEGORIA.*,DETALLE_ARRIENDO.SUBCONTRATAR_ID_SUBCON')
                                      ->get();

        foreach ($detalle_subcate as $key => $value) {
          $value->monto = 0;
          $valor = DB::table('DETALLE_PRESUPUESTO')
                       ->where('SUBCATEGORIA_ID_SUBC',$value->ID_SUBC)
                       ->orderBy('PRESUPUESTO_ID_PRESU','desc')->value('MONTO_PRESU');
          if($valor){$value->monto = $valor;}
          $valorDesc = DetallePagoArriendo::where('ID_SUBC',$value->ID_SUBC)->sum('MONTO_PAGADO');
          if($valorDesc){$value->monto = $value->monto - $valorDesc ;}

          $value->DETALLE_ARRIENDO = DetalleArriendo::where('ID_SUBC',$value->ID_SUBC)
                                          ->where('SUBCONTRATAR_ID_SUBCON',$value->SUBCONTRATAR_ID_SUBCON)
                                          ->join('ARRIENDO','ARRIENDO.ID_ARRIEN','=','DETALLE_ARRIENDO.ID_ARRIEN')
                                          ->get();
            foreach ($value->DETALLE_ARRIENDO as $key => $val) {
              $val->seleccionado=false;
              $val->MONTO_PAGADO = 0;
              $monto = DetallePagoArriendo::where('DETALLE_ARRIENDO_ID_DET_ARRIEN',$val->ID_DET_ARRIEN)
                                          ->sum('MONTO_PAGADO');
              if($monto)$val->MONTO_PAGADO = $monto;
            }
        }
        $subcontrato->detalle = $detalle_subcate;
      }
      return response()->json(['code' => 200, 'message' => 'Subcontrato encontrado', 'data' =>$subcontrato],200);

    }

  public function finalizarSubcontrato(Request $request){
      $ID_SUBCON = $request->input('ID_SUBCON');
      $subcontratar = Subcontratar::find($ID_SUBCON);
      $subcontratar->ESTADO_SUBCON = $request->input('ESTADO_SUBCON');
      $subcontratar->save();
      return response()->json(['code' => 200, 'message' => 'Subcontrato finalizado', 'data' =>$subcontratar],200);
    }

  public function nuevoPagoSubcontrato(Request $request){
    $APLICA_IVA = $request->input('APLICA_IVA');
    $TIPO_SUBCON = $request->input('TIPO_SUBCON');
    $lista = $request->input('LISTA');
    $pago = new Pago();
      date_default_timezone_set('America/Santiago');
    $now = new \DateTime();
    $pago->FECHA_CREACION_PAGO = $now;
    $pago->ESTADO_PAGO = 'A';
    $pago->MONTO_NETO = $request->input('MONTO_NETO');
    $pago->OBSERVACION = $request->input('OBSERVACION');
    $pago->MONTO_TOTAL = $request->input('MONTO_TOTAL');
    $pago->APLICA_IVA = 'N';

    if($APLICA_IVA){
        $pago->APLICA_IVA = 'S';
        $pago->MONTO_IVA = $request->input('MONTO_IVA');
    }

    $pago->SUBCONTRATAR_ID_SUBCON = $request->input('ID_SUBCON');
    $pago->NUM_FACTURA  = $request->input('NUM_FACTURA');
    $pago->save();

    $arch64 = $request->input('ARCHIVO');
    $arch64_nombre = $request->input('NOMBRE_ARCH');
    $arch64_tipo = $request->input('TIPO_ARCH');

    // Procesar base 64
    $archivo = base64_decode(preg_replace('#^data:'.$arch64_tipo.';base64,#i', '', $arch64));
    $pagoEdit = Pago::find($pago->ID_PAGO);
    $subcontratar = Subcontratar::find($pagoEdit->SUBCONTRATAR_ID_SUBCON);
    $ID_CONS = Proyecto::find($subcontratar->PROYECTO_ID_PROY)->value("CONSTRUCTORA_ID_CONS");

    file_put_contents($arch64_nombre , $archivo);
    $file = fopen($arch64_nombre,'r+');
    Storage::disk(app('App\Entorno')->origen_storage)->put('archivos/cons/'.$ID_CONS.'/proyecto/'.$subcontratar->PROYECTO_ID_PROY.'/subcontratos/'.$subcontratar->ID_SUBCON.'/pagos/'.$pago->ID_PAGO.'/'.$arch64_nombre,  $file);
    ftruncate($file,0);
    fclose($file);

    // La ruta usa el cargarUrlSubcontratoPago
    $pagoEdit->FACTURA_URL= app('App\Entorno')->route_api.'subcontrato/'.$subcontratar->ID_SUBCON.'/pago/'.$pago->ID_PAGO.'/'.$arch64_nombre;
    $pagoEdit->NOMBRE_FACTURA = $arch64_nombre;
    $pagoEdit->save();


    $flujoPago = new FlujoPago();
    $flujoPago->FECHA_FLUJO_PAGO= $now;
    $flujoPago->TIPO_FLUJO_PAGO = 'A';
    $flujoPago->OBSERVACION_FLUJO_PAGO = $request->input('OBSERVACION');
    $flujoPago->PAGO_ID_PAGO = $pago->ID_PAGO;
    $flujoPago->USUARIO_ID_USU = $request->input('ID_EMP');
    $flujoPago->save();

    if($TIPO_SUBCON == 'A'){
      foreach ($lista as $key => $value) {
        $detalle_pago_arriendo = new DetallePagoArriendo();
        $detalle_pago_arriendo->ID_SUBC = $value['ID_SUBC'];
        $detalle_pago_arriendo->ID_SUBCON = $value['SUBCONTRATAR_ID_SUBCON'];
        $detalle_pago_arriendo->MONTO_PAGADO = preg_replace('/\./', '', $value['MONTO_PAGO']);
        $detalle_pago_arriendo->PAGO_ID_PAGO = $pago->ID_PAGO;
        $detalle_pago_arriendo->DETALLE_ARRIENDO_ID_DET_ARRIEN = $value['ID_DET_ARRIEN'];
        $detalle_pago_arriendo->FECHA_PAGO_ARRIEN = $now;
        $detalle_pago_arriendo->save();
      }

    }
    return response()->json(['code' => 200, 'message' => 'Pago creado con exito'],200);

  }

  public function cargarUrlSubcontratoPago($ID_SUBCON,$ID_PAGO,$NOMBRE_ARCH){
      $NOMBRE= trim($NOMBRE_ARCH);
      $subcontratar = Subcontratar::find($ID_SUBCON);
      $ID_CONS = Proyecto::find($subcontratar->PROYECTO_ID_PROY)->value("CONSTRUCTORA_ID_CONS");
      $file =storage_path().'/app/archivos/cons/'.$ID_CONS.'/proyecto/'.$subcontratar->PROYECTO_ID_PROY.'/subcontratos/'.$ID_SUBCON.'/pagos/'.$ID_PAGO.'/'.$NOMBRE;
      return response()->download($file,$NOMBRE);
  }

  public function eliminarPagoSubcontrato(Request $request){
    $ID_PAGO = $request->input('ID_PAGO');
    $pago = Pago::find($ID_PAGO);
    $subcontratar = Subcontratar::find($pago->SUBCONTRATAR_ID_SUBCON);
    $ID_CONS = Proyecto::find($subcontratar->PROYECTO_ID_PROY)->value("CONSTRUCTORA_ID_CONS");
    $NOMBRE= trim($pago->NOMBRE_FACTURA);
    if(Storage::disk(app('App\Entorno')->origen_storage)->exists('archivos/cons/'.$ID_CONS.'/proyecto/'.$subcontratar->PROYECTO_ID_PROY.'/subcontratos/'.$subcontratar->ID_SUBCON.'/pagos/'.$pago->ID_PAGO.'/'.$NOMBRE)){
         Storage::disk(app('App\Entorno')->origen_storage)->deleteDirectory('archivos/cons/'.$ID_CONS.'/proyecto/'.$subcontratar->PROYECTO_ID_PROY.'/subcontratos/'.$subcontratar->ID_SUBCON.'/pagos/'.$pago->ID_PAGO);
    }
    $flujos = FlujoPago::where('PAGO_ID_PAGO',$ID_PAGO)->delete();
    $pago->delete();

    return response()->json(['code' => 200, 'message' => 'Pago eliminado con exito'],200);
  }

  public function agregarNotaCreditoPagoSubcontrato(Request $request){

    $nc = new NotaCredito();
    date_default_timezone_set('America/Santiago');
    $now = new \DateTime();
    $nc->FECHA_CREACION_NOTA_CRED = $now;
    $nc->OBSERVACION = $request->input('OBSERVACION');
    $nc->MONTO_NETO = $request->input('MONTO_NETO');
    $nc->MONTO_TOTAL = $request->input('MONTO_TOTAL');
    $nc->APLICA_IVA = 'N';
    $APLICA_IVA = $request->input('APLICA_IVA');
    if($APLICA_IVA){
        $nc->APLICA_IVA = 'S';
        $nc->MONTO_IVA = $request->input('MONTO_IVA');
    }

    $nc->PAGO_ID_PAGO = $request->input('ID_PAGO');
    $nc->NUM_DOCUMENTO  = $request->input('NUM_DOCUMENTO');
    $nc->save();

    $arch64 = $request->input('ARCHIVO');
    $arch64_nombre = $request->input('NOMBRE_ARCH');
    $arch64_tipo = $request->input('TIPO_ARCH');

    // Procesar base 64
    $archivo = base64_decode(preg_replace('#^data:'.$arch64_tipo.';base64,#i', '', $arch64));
    $ncEdit = NotaCredito::find($nc->ID_NOTA_CRED);
    $pago = Pago::find($ncEdit->PAGO_ID_PAGO);
    $subcontratar = Subcontratar::find($pago->SUBCONTRATAR_ID_SUBCON);
    $ID_CONS = Proyecto::find($subcontratar->PROYECTO_ID_PROY)->value("CONSTRUCTORA_ID_CONS");

    file_put_contents($arch64_nombre , $archivo);
    $file = fopen($arch64_nombre,'r+');
    Storage::disk(app('App\Entorno')->origen_storage)->put('archivos/cons/'.$ID_CONS.'/proyecto/'.$subcontratar->PROYECTO_ID_PROY.'/subcontratos/'.$subcontratar->ID_SUBCON.'/pagos/'.$pago->ID_PAGO.'/notas-credito/'.$nc->ID_NOTA_CRED.'/'.$arch64_nombre,  $file);
    ftruncate($file,0);
    fclose($file);

    // La ruta usa el cargarUrlSubcontratoPago
    $ncEdit->NOTA_CRED_URL= app('App\Entorno')->route_api.'subcontrato/'.$subcontratar->ID_SUBCON.'/pago/'.$pago->ID_PAGO.'/nota-credito/'.$nc->ID_NOTA_CRED.'/'.$arch64_nombre;
    $ncEdit->NOMBRE_NOTA_CRED = $arch64_nombre;
    $ncEdit->save();

    return response()->json(['code' => 200, 'message' => 'Nota de crédito agregada con exito'],200);

  }

  public function cargarUrlNotaCredito($ID_SUBCON,$ID_PAGO,$ID_NOTA_CRED,$NOMBRE_ARCH){
      $NOMBRE= trim($NOMBRE_ARCH);
      $subcontratar = Subcontratar::find($ID_SUBCON);
      $ID_CONS = Proyecto::find($subcontratar->PROYECTO_ID_PROY)->value("CONSTRUCTORA_ID_CONS");
      $file =storage_path().'/app/archivos/cons/'.$ID_CONS.'/proyecto/'.$subcontratar->PROYECTO_ID_PROY.'/subcontratos/'.$ID_SUBCON.'/pagos/'.$ID_PAGO.'/notas-credito/'.$ID_NOTA_CRED.'/'.$NOMBRE;
      return response()->download($file,$NOMBRE);
  }

  public function eliminarNotaCredito(Request $request){
    $ID_NOTA_CRED = $request->input('ID_NOTA_CRED');
    $nota_cred = NotaCredito::find($ID_NOTA_CRED);
    $pago = Pago::find($nota_cred->PAGO_ID_PAGO);
    $subcontratar = Subcontratar::find($pago->SUBCONTRATAR_ID_SUBCON);
    $ID_CONS = Proyecto::find($subcontratar->PROYECTO_ID_PROY)->value("CONSTRUCTORA_ID_CONS");
    $NOMBRE= trim($pago->NOMBRE_FACTURA);
    if(Storage::disk(app('App\Entorno')->origen_storage)->exists('archivos/cons/'.$ID_CONS.'/proyecto/'.$subcontratar->PROYECTO_ID_PROY.'/subcontratos/'.$subcontratar->ID_SUBCON.'/pagos/'.$pago->ID_PAGO.'/notas-credito/'.$ID_NOTA_CRED.'/',$NOMBRE)){
         Storage::disk(app('App\Entorno')->origen_storage)->deleteDirectory('archivos/cons/'.$ID_CONS.'/proyecto/'.$subcontratar->PROYECTO_ID_PROY.'/subcontratos/'.$subcontratar->ID_SUBCON.'/pagos/'.$pago->ID_PAGO.'/notas-credito/'.$ID_NOTA_CRED);
    }
    $nota_cred->delete();

    return response()->json(['code' => 200, 'message' => 'Pago eliminado con exito'],200);
  }

  public function guardarSubcontratarArriendo(Request $request){
    $detalle_arriendo = $request->input('ARRIENDOS');
    $ID_PROY = $request->input('ID_PROY');
    $ID_EMPR = $request->input('ID_EMPR');
    $OBSERVACION = $request->input('OBSERVACION');
    $arch64 = $request->input('ARCHIVO');
    $arch64_nombre = $request->input('NOMBRE_ARCH');
    $arch64_tipo = $request->input('TIPO_ARCH');

      date_default_timezone_set('America/Santiago');
    $now = new \DateTime();

    $subcontratar = new  Subcontratar();
    $subcontratar->FECHA_CREACION_SUBCON = $now;
    $subcontratar->MONTO_PPTO_SUBCON = 0;
    $subcontratar->EMPRESA_ID_EMPR = $ID_EMPR;
    $subcontratar->PROYECTO_ID_PROY = $ID_PROY;
    $subcontratar->TIPO_SUBCON = 'A';
    $subcontratar->OBSERVACION_SUBCON = $OBSERVACION;
    $subcontratar->save();

    $ID_CONS = Proyecto::find($ID_PROY)->value("CONSTRUCTORA_ID_CONS");
    // Procesar base 64
    $archivo = base64_decode(preg_replace('#^data:'.$arch64_tipo.';base64,#i', '', $arch64));
    $subcontratarEdit = Subcontratar::find($subcontratar->ID_SUBCON);
    file_put_contents($arch64_nombre , $archivo);
    $file = fopen($arch64_nombre,'r+');
    Storage::disk(app('App\Entorno')->origen_storage)->put('archivos/cons/'.$ID_CONS.'/proyecto/'.$subcontratar->PROYECTO_ID_PROY.'/subcontratos/'.$subcontratar->ID_SUBCON.'/'.$arch64_nombre,  $file);
    ftruncate($file,0);
    fclose($file);

    // La ruta usa el cargarUrlSubcontrato
    $subcontratarEdit->CONTRATO_URL= app('App\Entorno')->route_api.'subcontrato/'.$subcontratar->ID_SUBCON.'/'.$arch64_nombre;
    $subcontratarEdit->NOMBRE_CONTRATO = $arch64_nombre;
    $subcontratarEdit->save();

    foreach ($detalle_arriendo as $key => $value) {
      foreach ($value['DETALLE'] as $key => $val) {
        for ($i=0; $i < (int)$val['CANTIDAD']; $i++) {
          $det = new DetalleArriendo();
          $det->SUBCONTRATAR_ID_SUBCON = $subcontratar->ID_SUBCON;
          $det->ID_SUBC = $value['ID_SUBC'];
          $det->ID_ARRIEN = $val['ID_ARRIEN'];
          $det->ID_UNID =  $val['ID_UNID'];
          $det->FECHA_INICIO =  $val['FECHA'];
          $det->HORA_INICIO =  $val['HORA'];
          $det->PRECIO =  $val['VALOR'];
          $det->CANTIDAD =  $val['CANTIDAD'];
          $det->save();
        }
      }
    }
    return response()->json(['code' => 200, 'message' => 'Arriendo guardado con exito'],200);
  }

  public function gestionArriedo(Request $request,$ID_ARRIEN){
    $FECHA_ENTREGA = $request->input('FECHA_ENTREGA');
    $HORA_ENTREGA = $request->input('HORA_ENTREGA');
    $TIPO = $request->input('TIPO');

    $det = DetalleArriendo::find($ID_ARRIEN);
    if($TIPO=='A'){
      $det->FECHA_ENTREGA = $FECHA_ENTREGA;
      $det->HORA_ENTREGA  = $HORA_ENTREGA;
      $det->ESTADO = 'F';
      $det->save();

      return response()->json(['code' => 200, 'message' => 'Arriendo finalizado con exito'],200);
    }else{
      $det->FECHA_ENTREGA = $FECHA_ENTREGA;
      $det->HORA_ENTREGA  = $HORA_ENTREGA;
      $det->ESTADO = 'A';
      $det->save();

      return response()->json(['code' => 200, 'message' => 'Arriendo activado con exito'],200);
    }

  }
}
