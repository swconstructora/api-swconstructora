<?php

namespace App\Http\Controllers;

use App\Arriendo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ArriendoController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     //crear materiales para la constructora
    public function store(Request $request){}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){}

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $ID_USU){}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){}
    // crear arriendo
    public function crearArriendo(Request $request){
      $NOMBRE = $request->input('NOMBRE');
      $MATERIAL = $request->input('MATERIAL');
      $CONSTRUCTORA = $request->input('CONSTRUCTORA');

      $arrien = new Arriendo();
      $arrien->NOMBRE_ARRIEN = $NOMBRE;
      $arrien->SUBCATEGORIA_ID_SUBC = $MATERIAL;
      $arrien->CONSTRUCTORA_ID_CONS = $CONSTRUCTORA;
      $arrien->save();
      return response()->json(['code' => 200, 'message' => 'Arriendo guardado correctamente'],200);
    }

    // cargar arriendos de la constructora con paginacion
    public function cargarArriendos($ID_CONS,Request $request){
      $por_pagina = $request->input('POR_PAGINA');
      $arrien = Arriendo::join('SUBCATEGORIA','SUBCATEGORIA.ID_SUBC','SUBCATEGORIA_ID_SUBC')
                      ->join('CATEGORIA','CATEGORIA.ID_CATE','CATEGORIA_ID_CATE')
                      ->where('CONSTRUCTORA_ID_CONS',$ID_CONS)->paginate($por_pagina);
      return $arrien;
    }

    //cargar arriendos de la constructora sin paginacion
    public function getArriendo($ID_CONS){
      $arrien = Arriendo::join('SUBCATEGORIA','SUBCATEGORIA.ID_SUBC','SUBCATEGORIA_ID_SUBC')
                      ->join('CATEGORIA','CATEGORIA.ID_CATE','CATEGORIA_ID_CATE')
                      ->where('CONSTRUCTORA_ID_CONS',$ID_CONS)->get();
      return $arrien;
    }

    public function buscarArriendos(Request $request,$ID_CONS){
      $palabra = $request->input('palabra');
      $por_pagina = $request->input('POR_PAGINA');
      $arrien = Arriendo::join('SUBCATEGORIA','SUBCATEGORIA.ID_SUBC','SUBCATEGORIA_ID_SUBC')
                      ->join('CATEGORIA','CATEGORIA.ID_CATE','CATEGORIA_ID_CATE')
                      ->where('CONSTRUCTORA_ID_CONS',$ID_CONS)
                      ->where('NOMBRE_ARRIEN','like','%'.$palabra.'%')->paginate($por_pagina);
      return $arrien;
    }

    public function eliminarArriendo($ID_ARRIEN){
      $arrien = Arriendo::where('ID_ARRIEN',$ID_ARRIEN)
                            ->delete();
      return response()->json(['code' => 200, 'message' => 'Arriendo eliminado con exito'],200);
      // $estado = false;
      // $nota = NotaPedido::where('MATERIAL_ID_MATE',$ID_MATE)->first();
      // if($nota <> null){
      //   $estado = true;
      // }
      // $invetario = InventarioProyecto::where('MATERIAL_ID_MATE',$ID_MATE)->first();
      // if($invetario <> null){
      //   $estado = true;
      // }
      // if($estado == false){
      //   $material->delete();
      //   return response()->json(['code' => 200, 'message' => 'Material eliminado con exito'],200);
      // }else{
      //   return response()->json(['code' => 200, 'message' => 'Material está asociado a un proyecto'],200);
      // }
    }


}
