<?php

namespace App\Http\Controllers;

use App\Empleado;
use App\Dueno;
use App\Area;
use App\Subarea;
use App\Categoria;
use App\Subcategoria;
use App\NotaPedido;
use App\FlujoNotaPedido;
use App\FlujoOrdenCompra;
use App\Cotizacion;
use App\DetalleCotizacion;
use App\Usuario;
use App\Subcontratar;
use App\Presupuesto;
use App\Cargo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class ProyectoPedidoController extends Controller
{

    // crear nota de pedido con detalle de flujo
    public function crearNotaPedido(Request $request){
      $CANTIDAD= $request->input('CANTIDAD');
      $OBSER = $request->input('OBSER');
      $ID_PROY = $request->input('ID_PROY');
      $ID_USU = $request->input('ID_USU');
      $ID_MATE = $request->input('ID_MATE');
      $AREAS = $request->input('AREAS');
      $nota = new NotaPedido();
      $nota->CANTIDAD_NOTA_PED = $CANTIDAD;
      $nota->OBSERVACION_NOTA_PED = $OBSER;
      date_default_timezone_set('America/Santiago');
              $now = new \DateTime();
      $nota->FECHA_CREACION_NOTA_PED = $now;
      $nota->ESTADO_NOTA_PED = 'E';
      $nota->PROYECTO_ID_PROY = $ID_PROY;
      $nota->ID_USU_NOTA_PED = $ID_USU;
      $nota->MATERIAL_ID_MATE = $ID_MATE;
      $nota->save();
      foreach ($AREAS as $key => $value) {
        foreach ($value['subareas'] as $key => $val) {
          if(+$val['CANT_MATERIAL'] <> 0){
            $nota->DetalleNotaPedido()->attach($val['ID_SAREA'],['CANTIDAD_DNP' => +$val['CANT_MATERIAL']]);
          }
        }
      }
      $usr = Usuario::find($ID_USU);
      $flujoNota = new FlujoNotaPedido();
      $flujoNota->FECHA_FLUJO_NOTA = $now;
      $flujoNota->TIPO_FLUJO_NOTA = 'C';
      $flujoNota->OBSERVACION_FLUJO_NOTA = $OBSER;
      $flujoNota->NOTA_PEDIDO_ID_NOTA_PED = $nota->ID_NOTA_PED ;
      $flujoNota->USUARIO_ID_USU = $ID_USU;
      $flujoNota->NOMBRE_USU = $usr->dueno?($usr->dueno->NOMBRE_DUE.' '.$usr->dueno->APELLIDO_DUE):($usr->empleado->NOMBRE_EMP.' '.$usr->empleado->APELLIDO_EMP);
      $flujoNota->CARGO_USU = $usr->dueno?($usr->dueno->cargo->NOMBRE_CARG):($usr->empleado->cargo->NOMBRE_CARG);
      $flujoNota->save();
      if($usr->TIPO_USU == 'D') {
        $nota->ESTADO_NOTA_PED = 'A';
        $nota->save();
        $flujoNota = new FlujoNotaPedido();
        $flujoNota->FECHA_FLUJO_NOTA = $now;
        $flujoNota->TIPO_FLUJO_NOTA = 'A';
        $flujoNota->OBSERVACION_FLUJO_NOTA = 'Aprobado automáticamente';
        $flujoNota->NOTA_PEDIDO_ID_NOTA_PED = $nota->ID_NOTA_PED ;
        $flujoNota->USUARIO_ID_USU = $ID_USU;
        $flujoNota->NOMBRE_USU = $usr->dueno->NOMBRE_DUE.' '.$usr->dueno->APELLIDO_DUE;
        $flujoNota->CARGO_USU = $usr->dueno->cargo->NOMBRE_CARG;
        $flujoNota->save();
      }
      else {
        $mod = $usr->empleado->cargo->modulosP()->where('ID_MOD',23)
          ->first()->hijosC()->wherePivot('CARGO_ID_CARG',$usr->empleado->CARGO_ID_CARG)
          ->wherePivot('PUEDE_EDITAR',true)->where('ID_MOD',25)->first();
        if($mod) {
          $nota->ESTADO_NOTA_PED = 'A';
          $nota->save();
          $flujoNota = new FlujoNotaPedido();
          $flujoNota->FECHA_FLUJO_NOTA = $now;
          $flujoNota->TIPO_FLUJO_NOTA = 'A';
          $flujoNota->OBSERVACION_FLUJO_NOTA = 'Aprobado automáticamente';
          $flujoNota->NOTA_PEDIDO_ID_NOTA_PED = $nota->ID_NOTA_PED ;
          $flujoNota->USUARIO_ID_USU = $ID_USU;
          $flujoNota->NOMBRE_USU = $usr->empleado->NOMBRE_EMP.' '.$usr->empleado->APELLIDO_EMP;
          $flujoNota->CARGO_USU = $usr->empleado->cargo->NOMBRE_CARG;
          $flujoNota->save();
        }
      }
      return response()->json(['code' => 200, 'message' => 'Nota de pedido creada con exito'],200);
    }

    public function cargarEmpleadosNotaPedido($ID_CONS, $ID_PROY) {
      $idsCargos = Cargo::join('DETALLE_MODULO_PADRE', 'CARGO_ID_CARG', 'ID_CARG')
        ->where('CONSTRUCTORA_ID_CONS',$ID_CONS)->where('MODULO_HIJO_ID',24)
        ->where('PUEDE_EDITAR',1)->pluck('ID_CARG');
      $empleados = Empleado::join('DETALLE_EMP_PROY','EMPLEADO_ID_EMP','ID_EMP')
        ->whereIn('CARGO_ID_CARG',$idsCargos)->where('PROYECTO_ID_PROY',$ID_PROY)
        ->selectRaw("concat(NOMBRE_EMP,' ',APELLIDO_EMP) as NOMBRE_USU, USUARIO_ID_USU")->get();
      $dueno = Dueno::whereIn('CARGO_ID_CARG',$idsCargos)
        ->selectRaw("concat(NOMBRE_DUE,' ',APELLIDO_DUE) as NOMBRE_USU, USUARIO_ID_USU")->get();
      $merge = array_merge($empleados->all(),$dueno->all());
      return response()->json(['msg'=>'Creadores','data'=>$merge]);
    }

    // cargar notas de pedidos
    public function cargarNotasPedidos($ID_PROY,Request $request){
      $por_pagina = $request->input('POR_PAGINA');
      $notas = NotaPedido::where('PROYECTO_ID_PROY',$ID_PROY)
                        ->join('MATERIAL','MATERIAL.ID_MATE','MATERIAL_ID_MATE')
                        ->join('UNIDAD_MEDIDA','UNIDAD_MEDIDA.ID_UNID','MATERIAL.UNIDAD_MEDIDA_ID_UNID')
                        ->with(['DetalleNotaPedido','Creador'])
                        ->selectRaw('NOTA_PEDIDO.*,MATERIAL.NOMBRE_MATE,UNIDAD_MEDIDA.NOMBRE_UNID')
                        ->orderBy('FECHA_CREACION_NOTA_PED','desc')
                        ->paginate($por_pagina);
      return $notas;
    }
    // cargar notas de pedido en estado de espera con detalle de flujo
    public function cargarNotasPedidosEspera($ID_PROY) {
      $notas = NotaPedido::where('PROYECTO_ID_PROY',$ID_PROY)
                        ->join('MATERIAL','MATERIAL.ID_MATE','MATERIAL_ID_MATE')
                        ->join('SUBCATEGORIA','SUBCATEGORIA.ID_SUBC','MATERIAL.SUBCATEGORIA_ID_SUBC')
                        ->join('CATEGORIA','CATEGORIA.ID_CATE','SUBCATEGORIA.CATEGORIA_ID_CATE')
                        ->join('UNIDAD_MEDIDA','UNIDAD_MEDIDA.ID_UNID','MATERIAL.UNIDAD_MEDIDA_ID_UNID')
                        ->selectRaw('NOTA_PEDIDO.*,MATERIAL.NOMBRE_MATE,UNIDAD_MEDIDA.NOMBRE_UNID,SUBCATEGORIA.NOMBRE_SUBC,SUBCATEGORIA.ID_SUBC,
                                     SUBCATEGORIA.NOMBRE_SUBC,CATEGORIA.*')
                        ->where('ESTADO_NOTA_PED','E')
                        ->orderBy('FECHA_CREACION_NOTA_PED','desc')
                        ->get();
      foreach ($notas as $key => $value) {
        $totales = (object) array(

        'total_subcat_comprometido' => 0,
        'total_subcat_asignado' => 0,
        'total_subcat_libre' => 0,
        'porcentaje_subcat_comprometido' => 0,
        'porcentaje_subcat_libre' => 100,
        );


        $totales->total_subcat_comprometido =  app('App\Http\Controllers\BodegaController')->getPromOC($value->ID_SUBC,$ID_PROY);

        $totales->total_subcat_asignado = Presupuesto::join('DETALLE_PRESUPUESTO','PRESUPUESTO_ID_PRESU','PRESUPUESTO.ID_PRESU')
                                ->where('PRESUPUESTO.PROYECTO_ID_PROY',$ID_PROY )
                                ->join('SUBCATEGORIA','SUBCATEGORIA.ID_SUBC','DETALLE_PRESUPUESTO.SUBCATEGORIA_ID_SUBC')
                                ->where('SUBCATEGORIA.ID_SUBC',$value->ID_SUBC)
                                ->whereRaw('ID_PRESU = (select MAX(ID_PRESU) from PRESUPUESTO WHERE '.$ID_PROY.' = PROYECTO_ID_PROY)')
                                ->sum('DETALLE_PRESUPUESTO.MONTO_PRESU');

        $totales->total_subcat_libre = intval($totales->total_subcat_asignado) - intval($totales->total_subcat_comprometido);
          if($totales->total_subcat_comprometido != 0 && $totales->total_subcat_asignado != 0){
            $totales->porcentaje_subcat_comprometido = intval($totales->total_subcat_comprometido) * 100 /intval($totales->total_subcat_asignado);
            $totales->porcentaje_subcat_libre = 100-$totales->porcentaje_subcat_comprometido;
        }
        $value->totales = $totales;

        $value->DETALLE_FLUJO = FlujoNotaPedido::where('NOTA_PEDIDO_ID_NOTA_PED',$value->ID_NOTA_PED)
                                                    ->leftjoin('USUARIO','USUARIO.ID_USU','USUARIO_ID_USU')
                                                    ->selectRaw('FLUJO_NOTA_PEDIDO.*,USUARIO.TIPO_USU')->get();
              foreach ($value->DETALLE_FLUJO as $key => $val) {
                if($val->TIPO_USU == 'E'){
                  $val->empleado = Empleado::where('USUARIO_ID_USU',$val->USUARIO_ID_USU)
                                            ->join('CARGO','CARGO.ID_CARG','CARGO_ID_CARG')->first();
                }else{
                  $val->dueno = Dueno::where('USUARIO_ID_USU',$val->USUARIO_ID_USU)
                                      ->join('CARGO','CARGO.ID_CARG','CARGO_ID_CARG')->first();

                }
              }

              $value->area = Area::join('SUBAREA','SUBAREA.AREA_ID_AREA','ID_AREA')
                          ->join("DETALLE_SAREA_PROY",'DETALLE_SAREA_PROY.SUBAREA_ID_SAREA','SUBAREA.ID_SAREA')
                          ->select('AREA.*')->groupBy('AREA.ID_AREA')
                          ->where("DETALLE_SAREA_PROY.PROYECTO_ID_PROY",$value->PROYECTO_ID_PROY)->get();
              foreach ($value->area as $key => $ar) {
                $ar->CANTIDAD_TOTAL;
                $ar->subarea = Subarea::where('AREA_ID_AREA','=',$ar->ID_AREA)
                                       ->get();
                           foreach ($ar->subarea as $key => $sub) {
                             $CANTIDAD_DNP = $value->DetalleNotaPedido()->where('SUBAREA_ID_SAREA',$sub->ID_SAREA)->value('CANTIDAD_DNP');
                             // return $detalle;
                             $sub->cantidad = $CANTIDAD_DNP;
                             $ar->CANTIDAD_TOTAL = $ar->CANTIDAD_TOTAL + $sub->cantidad;
                           }
              }

      }      return $notas;
    }


    // buscar nota de pedido por nombre de material , estado o por rango de fecha
    public function buscarNotaPedido(Request $request,$ID_PROY){
      $material = null;
      $estado = null;
      $fechaInicio = null;
      $fechaFin = null;
      $usrCrea = $request->input('usrCrea');
      $por_pagina = $request->input('POR_PAGINA');
      if($request->input('material')){$material = $request->input('material');}
      if($request->input('estado')){$estado = $request->input('estado');}
      if($request->input('fechaInicio')){$fechaInicio = $request->input('fechaInicio');}
      if($request->input('fechaFin')){$fechaFin = $request->input('fechaFin');}


      $notas = NotaPedido::where('PROYECTO_ID_PROY',$ID_PROY)
                        ->join('MATERIAL','MATERIAL.ID_MATE','MATERIAL_ID_MATE')
                        ->join('UNIDAD_MEDIDA','UNIDAD_MEDIDA.ID_UNID','MATERIAL.UNIDAD_MEDIDA_ID_UNID')
                        ->join('FLUJO_NOTA_PEDIDO','FLUJO_NOTA_PEDIDO.NOTA_PEDIDO_ID_NOTA_PED','NOTA_PEDIDO.ID_NOTA_PED')
                        ->with(['DetalleNotaPedido','Creador'])
                        ->selectRaw('NOTA_PEDIDO.*,MATERIAL.NOMBRE_MATE,UNIDAD_MEDIDA.NOMBRE_UNID')
                        ->distinct()
                        ->where(function ($query) use ($material){
                          if($material != null){
                            $query->where('MATERIAL.NOMBRE_MATE','like','%'.$material.'%');
                          }
                        })
                        ->where(function ($query) use ($estado){
                          if($estado != null){
                            $query->where('ESTADO_NOTA_PED',$estado);
                          }
                        })
                        ->where(function ($query) use ($fechaInicio,$fechaFin){
                          if($fechaInicio != null && $fechaFin != null){
                              $query->whereBetween('FECHA_CREACION_NOTA_PED', [$fechaInicio, $fechaFin]);
                          }
                        })->where(function ($query) use ($usrCrea) {
                          if($usrCrea){
                            $query->where('FLUJO_NOTA_PEDIDO.USUARIO_ID_USU',$usrCrea)
                              ->where('FLUJO_NOTA_PEDIDO.TIPO_FLUJO_NOTA','C');
                          }
                        })->orderBy('FECHA_CREACION_NOTA_PED','desc')->paginate($por_pagina);
      return $notas;
    }

    public function aprobarRechazarNotaPedido(Request $request,$ID_NOTA_PED){
      $estado = $request->input('ESTADO');
      $OBSERVACION = $request->input('OBSERVACION');
      $id_user = $request->input('USUARIO');
      $usr = Usuario::find($id_user);
      $nota = NotaPedido::find($ID_NOTA_PED);
      if($nota->ESTADO_NOTA_PED == 'A'){ return response()->json(['code' => 201, 'message' => 'Otro usuario ha aprobado esta nota de pedido antes'],200); }
      if($nota->ESTADO_NOTA_PED == 'R'){ return response()->json(['code' => 201, 'message' => 'Otro usuario ha rechazado esta nota de pedido antes'],200); }
      $nota->ESTADO_NOTA_PED = $estado;
      $nota->save();

      $flujoNota = new FlujoNotaPedido();
      date_default_timezone_set('America/Santiago');
              $now = new \DateTime();
      $flujoNota->FECHA_FLUJO_NOTA = $now;
      $flujoNota->TIPO_FLUJO_NOTA = $estado;
      $flujoNota->OBSERVACION_FLUJO_NOTA = $OBSERVACION;
      $flujoNota->NOTA_PEDIDO_ID_NOTA_PED = $ID_NOTA_PED ;
      $flujoNota->USUARIO_ID_USU = $id_user;
      $flujoNota->NOMBRE_USU = $usr->dueno?($usr->dueno->NOMBRE_DUE.' '.$usr->dueno->APELLIDO_DUE):($usr->empleado->NOMBRE_EMP.' '.$usr->empleado->APELLIDO_EMP);
      $flujoNota->CARGO_USU = $usr->dueno?($usr->dueno->cargo->NOMBRE_CARG):($usr->empleado->cargo->NOMBRE_CARG);
      $flujoNota->save();

      if($estado == 'A'){
        return response()->json(['code' => 200, 'message' => 'Nota de pedido aprobada'],200);
      }else{
        return response()->json(['code' => 200, 'message' => 'Nota de pedido rechazada'],200);
      }
    }

    // cargar notas de pedido en estado de aprobacion con detalle de flujo
    public function cargarNotasPedidosAprobadas($ID_PROY){
      $notas = NotaPedido::where('PROYECTO_ID_PROY',$ID_PROY)
                        ->join('MATERIAL','MATERIAL.ID_MATE','MATERIAL_ID_MATE')
                        ->join('SUBCATEGORIA','SUBCATEGORIA.ID_SUBC','MATERIAL.SUBCATEGORIA_ID_SUBC')
                        ->join('UNIDAD_MEDIDA','UNIDAD_MEDIDA.ID_UNID','MATERIAL.UNIDAD_MEDIDA_ID_UNID')
                        ->selectRaw('NOTA_PEDIDO.*,MATERIAL.NOMBRE_MATE,UNIDAD_MEDIDA.NOMBRE_UNID,SUBCATEGORIA.NOMBRE_SUBC')
                        ->where('ESTADO_NOTA_PED','A')
                        ->orderBy('FECHA_CREACION_NOTA_PED','desc')
                        ->get();
      foreach ($notas as $key => $value) {
        $value->CONT_DET_COTI = 0;
        $cont = DetalleCotizacion::where('NOTA_PEDIDO_ID_NOTA_PED',$value->ID_NOTA_PED)
                                  ->count('NOTA_PEDIDO_ID_NOTA_PED');
        $value->CONT_DET_COTI =  $cont;
        $value->DETALLE_FLUJO = FlujoNotaPedido::where('NOTA_PEDIDO_ID_NOTA_PED',$value->ID_NOTA_PED)
                                                    ->join('USUARIO','USUARIO.ID_USU','USUARIO_ID_USU')
                                                    ->selectRaw('FLUJO_NOTA_PEDIDO.*,USUARIO.TIPO_USU')->get();
              foreach ($value->DETALLE_FLUJO as $key => $val) {
                if($val->TIPO_USU == 'E'){
                  $val->empleado = Empleado::where('USUARIO_ID_USU',$val->USUARIO_ID_USU)
                                            ->join('CARGO','CARGO.ID_CARG','CARGO_ID_CARG')->first();
                }else{
                  $val->dueno = Dueno::where('USUARIO_ID_USU',$val->USUARIO_ID_USU)
                                      ->join('CARGO','CARGO.ID_CARG','CARGO_ID_CARG')->first();
                }
              }

              $value->area = Area::join('SUBAREA','SUBAREA.AREA_ID_AREA','ID_AREA')
                          ->join("DETALLE_SAREA_PROY",'DETALLE_SAREA_PROY.SUBAREA_ID_SAREA','SUBAREA.ID_SAREA')
                          ->select('AREA.*')->groupBy('AREA.ID_AREA')
                          ->where("DETALLE_SAREA_PROY.PROYECTO_ID_PROY",$value->PROYECTO_ID_PROY)->get();
              foreach ($value->area as $key => $ar) {
                $ar->CANTIDAD_TOTAL;
                $ar->subarea = Subarea::where('AREA_ID_AREA','=',$ar->ID_AREA)
                                       ->get();
                           foreach ($ar->subarea as $key => $sub) {
                             $CANTIDAD_DNP = $value->DetalleNotaPedido()->where('SUBAREA_ID_SAREA',$sub->ID_SAREA)->value('CANTIDAD_DNP');
                             // return $detalle;
                             $sub->cantidad = $CANTIDAD_DNP;
                             $ar->CANTIDAD_TOTAL = $ar->CANTIDAD_TOTAL + $sub->cantidad;
                           }
              }

      }      return $notas;
    }
    // cargar datos de nota de pedido seguimineto
    public function cargarNotaPedido($ID_NOTA_PED){
      $nota = NotaPedido::where('ID_NOTA_PED',$ID_NOTA_PED)
                          ->join('MATERIAL','MATERIAL.ID_MATE','NOTA_PEDIDO.MATERIAL_ID_MATE')
                          ->join('UNIDAD_MEDIDA','UNIDAD_MEDIDA.ID_UNID','MATERIAL.UNIDAD_MEDIDA_ID_UNID')
                          ->leftJoin('DETALLE_COTIZACION', 'DETALLE_COTIZACION.NOTA_PEDIDO_ID_NOTA_PED', '=', 'NOTA_PEDIDO.ID_NOTA_PED')
                          ->leftJoin('COTIZACION', 'COTIZACION.ID_COTI', '=', 'DETALLE_COTIZACION.COTIZACION_ID_COTI')
                          ->leftJoin('DETALLE_ORDEN_COTIZACION','DETALLE_ORDEN_COTIZACION.DETALLE_COTIZACION_ID_DETA_COTI','=','DETALLE_COTIZACION.ID_DETA_COTI')
                          ->leftJoin('ORDEN_COMPRA','ORDEN_COMPRA.ID_ORDENC','=','DETALLE_ORDEN_COTIZACION.ORDEN_COMPRA_ID_ORDENC')
                          ->leftJoin('EMPRESA','EMPRESA.ID_EMPR','=','ORDEN_COMPRA.ID_EMPR')
                          ->leftJoin('FLUJO_ORDEN','FLUJO_ORDEN.ORDEN_COMPRA_ID_ORDENC','=','ORDEN_COMPRA.ID_ORDENC')
                          ->selectRaw('NOTA_PEDIDO.*,MATERIAL.NOMBRE_MATE,UNIDAD_MEDIDA.NOMBRE_UNID,
                                       ORDEN_COMPRA.ID_ORDENC,EMPRESA.NOMBRE_EMPR,DETALLE_COTIZACION.FECHA_RECEPCION_DETCO,
                                       FLUJO_ORDEN.ID_FLUJO,DETALLE_COTIZACION.ESTADO_COTIZACION, COTIZACION.ESTADO_COTI,
                                       ORDEN_COMPRA.ESTADO_ORDEN')
                          ->first();
      if($nota->ID_ORDENC <> null){
        $nota->FLUJO_ORDEN = FlujoOrdenCompra::where('ORDEN_COMPRA_ID_ORDENC',$nota->ID_ORDENC)
                                              ->join('USUARIO','USUARIO.ID_USU','USUARIO_ID_USU')
                                              ->selectRaw('FLUJO_ORDEN.*,USUARIO.TIPO_USU')
                                              ->get();
      }
      if($nota->ID_ORDENC <> null){
        foreach ($nota->FLUJO_ORDEN as $key => $val) {
          if($val->TIPO_USU == 'E'){
            $val->empleado = Empleado::where('USUARIO_ID_USU',$val->USUARIO_ID_USU)
                                      ->join('CARGO','CARGO.ID_CARG','CARGO_ID_CARG')->first();
          }else{
            $val->dueno = Dueno::where('USUARIO_ID_USU',$val->USUARIO_ID_USU)
                                ->join('CARGO','CARGO.ID_CARG','CARGO_ID_CARG')->first();
          }
        }
      }


      if($nota->ID_FLUJO <> null){
        $nota->FECHA_BODEGA = FlujoOrdenCompra::where('ORDEN_COMPRA_ID_ORDENC',$nota->ID_ORDENC)
                                                ->where('TIPO_FLUJO','RC')
                                                ->max('FECHA_FLUJO');
      }
      $nota->DETALLE_FLUJO = FlujoNotaPedido::where('NOTA_PEDIDO_ID_NOTA_PED',$ID_NOTA_PED)
                                                  ->join('USUARIO','USUARIO.ID_USU','USUARIO_ID_USU')
                                                  ->selectRaw('FLUJO_NOTA_PEDIDO.*,USUARIO.TIPO_USU')->get();
            foreach ($nota->DETALLE_FLUJO as $key => $val) {
              if($val->TIPO_USU == 'E'){
                $val->empleado = Empleado::where('USUARIO_ID_USU',$val->USUARIO_ID_USU)
                                          ->join('CARGO','CARGO.ID_CARG','CARGO_ID_CARG')->first();
              }else{
                $val->dueno = Dueno::where('USUARIO_ID_USU',$val->USUARIO_ID_USU)
                                          ->join('CARGO','CARGO.ID_CARG','CARGO_ID_CARG')->first();
              }
            }

            $nota->area = Area::join('SUBAREA','SUBAREA.AREA_ID_AREA','ID_AREA')
                        ->join("DETALLE_SAREA_PROY",'DETALLE_SAREA_PROY.SUBAREA_ID_SAREA','SUBAREA.ID_SAREA')
                        ->select('AREA.*')->groupBy('AREA.ID_AREA')
                        ->where("DETALLE_SAREA_PROY.PROYECTO_ID_PROY",$nota->PROYECTO_ID_PROY)->get();

            foreach ($nota->area as $key => $ar) {
              $ar->CANTIDAD_TOTAL = 0;
              $ar->subarea = Subarea::where('AREA_ID_AREA','=',$ar->ID_AREA)
                                     ->get();
                         foreach ($ar->subarea as $key => $sub) {
                           $CANTIDAD_DNP = $nota->DetalleNotaPedido()->where('SUBAREA_ID_SAREA',$sub->ID_SAREA)->value('CANTIDAD_DNP');
                           // return $detalle;
                           $sub->cantidad = $CANTIDAD_DNP;
                           $ar->CANTIDAD_TOTAL = $ar->CANTIDAD_TOTAL + $sub->cantidad;
                         }
            }
            return $nota;
    }
}
