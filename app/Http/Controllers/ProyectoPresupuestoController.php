<?php

namespace App\Http\Controllers;

use App\Proyecto;
use App\Area;
use App\Subarea;
use App\Categoria;
use App\Subcategoria;
use App\Presupuesto;
use App\Subcontratar;
use App\OrdenCompra;
use App\Gasto;
use App\DetallePagoArriendo;
use App\Exports\MultipleSheetExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use DB;
use PDF;
use PDFMerger;
use View;

class ProyectoPresupuestoController extends Controller
{



    public function cargarAreasProyecto($ID_PROY){
      $proyecto = Proyecto::find($ID_PROY);
      $areas = Area::join('SUBAREA','SUBAREA.AREA_ID_AREA','ID_AREA')
                  ->join("DETALLE_SAREA_PROY",'DETALLE_SAREA_PROY.SUBAREA_ID_SAREA','SUBAREA.ID_SAREA')
                  ->select('AREA.*')->groupBy('AREA.ID_AREA')
                  ->where("DETALLE_SAREA_PROY.PROYECTO_ID_PROY",$ID_PROY)->get();
      foreach ($areas as $key => $ar) {
        $ar->MATERIAL_TOTAL = 0;
        $ar->PEDIDO_TOTAL = 0;
        $ar->CANTIDAD_TOTAL = 0;
        $ar->ASOCIADO = false;
        $ar->SE_QUITO = false;
        foreach ($ar->subareas as $key => &$sub) {
          $sub->MONTO_PEDIDO = 0;
          $sub->MONTO_MATERIAL = 0;
          $sub->ASOCIADO = false;
          $detalle = $proyecto->DetalleSubarea()->wherePivot('SUBAREA_ID_SAREA',$sub->ID_SAREA)->first();
          $sub->MONTO_PEDIDO = $detalle->pivot->MONTO_PEDIDO;
          $sub->MONTO_MATERIAL = $detalle->pivot->MONTO_MATERIAL;
          $ar->MATERIAL_TOTAL = $ar->MATERIAL_TOTAL + $sub->MONTO_MATERIAL;
          $ar->PEDIDO_TOTAL = $ar->PEDIDO_TOTAL + $sub->MONTO_PEDIDO;
          $sub->CANT_MATERIAL = 0;
          if($ar->MATERIAL_TOTAL > 0 || $ar->PEDIDO_TOTAL > 0){ $ar->ASOCIADO = true; }
          if($sub->countEntregas() > 0 || $sub->countNotas() > 0){ $ar->ASOCIADO = true; $sub->ASOCIADO = true; }
        }
      }
      return $areas;
    }

    public function agregarAreas(Request $request){
      $ID_PROY = $request->input('ID_PROY');
      $proyecto = Proyecto::find($ID_PROY);
      $Aquitados = $request->input('Aquitados');
      $areas = $request->input('areas');

      foreach ($areas as $key => $area) {
        if($area['ID_AREA'] == 0){
          $areaNueva = new Area();
          $areaNueva->NOMBRE_AREA = $area['NOMBRE_AREA'];
          $areaNueva->save();
          $area['ID_AREA'] = $areaNueva->ID_AREA;
        }
        foreach ($area['subareas'] as $key => $subarea) {
          $subArea = new Subarea();
          if($subarea['ID_SAREA'] != 0) { $subArea = Subarea::find($subarea['ID_SAREA']); }
          $subArea->NOMBRE_SAREA = $subarea['NOMBRE_SAREA'];
          $subArea->AREA_ID_AREA = $area['ID_AREA'];
          $subArea->save();
          if($subarea['ID_SAREA'] == 0) { $proyecto->DetalleSubarea()->attach($subArea->ID_SAREA); }
        }
      }

      foreach ($Aquitados as $key => $area) {
        if($area['SE_QUITO']) { Area::find($area['ID_AREA'])->delete(); }
        else {
          foreach ($area['subareas'] as $key => $subarea) {
            $proyecto->DetalleSubarea()->detach($subarea);
            Subarea::find($subarea)->delete();
          }
          if(Area::find($area['ID_AREA'])->subareas()->count() == 0) {
            Area::find($area['ID_AREA'])->delete();
          }
        }
      }
      return response()->json(['code' => 200, 'message' => 'Areas registradas correctamente','areas'=>$areas],200);
    }

    public function cargarPresupuesto(Request $request){
      $ID_PROY = $request->input('ID_PROY');
      $tipo = $request->input('TIPO_CATE');
      if($tipo == 'TODAS'){

        $data = (object) array(
          'materiales' => array(),
          'subcontratista' => array(),
          'manoobra' => array(),
          'gastos' => array(),
          'arriendos' => array()
        );

        $categorias = Categoria::with(['subcategorias'])->orderBy('NOMBRE_CATE')->get();
        $idpresu = 0;
        $presu = null;
        // return $categorias;
        foreach ($categorias as $key => $cat) {
          // $subcategorias = Subcategoria::where('CATEGORIA_ID_CATE','=',$cat->ID_CATE)->get();
          // $cat->subcategorias = $subcategorias;
          $cat->MONTO_TOTAL_CAT = 0;
            $presupuesto = Presupuesto::with('DetallePresupuesto')
                            ->join('DETALLE_PRESUPUESTO','PRESUPUESTO_ID_PRESU','PRESUPUESTO.ID_PRESU')
                            ->where('PRESUPUESTO.PROYECTO_ID_PROY',$ID_PROY )
                            ->whereIn('DETALLE_PRESUPUESTO.SUBCATEGORIA_ID_SUBC',$cat->subcategorias->pluck('ID_SUBC'))
                            ->orderBy('ID_PRESU', 'desc')->first();
                            if($presupuesto){
                                $idpresu = $presupuesto->ID_PRESU;
                                $presu = $presupuesto;
                            }


                    foreach ($cat->subcategorias as $keys => $sub) {
                        $sub->MONTO_PRESU = null;
                        $sub->EDITABLE = false;
                        if($presupuesto){
                          foreach ($presupuesto->DetallePresupuesto as $keydt => $dtpresu) {

                              if($dtpresu->ID_SUBC == $sub->ID_SUBC){
                                $cat->MONTO_TOTAL_CAT = $cat->MONTO_TOTAL_CAT + intval($dtpresu->pivot->MONTO_PRESU);
                                $sub->MONTO_PRESU = $dtpresu->pivot->MONTO_PRESU;
                              }
                          }
                    }
                  }
                  if($cat->TIPO_CATE == 'M'){
                        array_push($data->materiales, $cat);
                  }
                  if($cat->TIPO_CATE == 'SC'){
                        array_push($data->subcontratista, $cat);
                  }
                  if($cat->TIPO_CATE == 'MO'){
                        array_push($data->manoobra, $cat);
                  }
                  if($cat->TIPO_CATE == 'G'){
                        array_push($data->gastos, $cat);
                  }
                  if($cat->TIPO_CATE == 'A'){
                        array_push($data->arriendos, $cat);
                  }
          }


              return response()->json(['code' => 200, 'message' => 'Categorias encontradas',
              'data'=>$data, 'ppto'=>$presu],200);

      }

    }

    public function guardarPresupuesto(Request $request){
      $ID_PROY = $request->input('ID_PROY');
      $categorias = $request->input('categorias');
      $obs = $request->input('OBS_PRESU');
      if($ID_PROY){
        $presupuesto = new Presupuesto();
        date_default_timezone_set('America/Santiago');
        $now = new \DateTime();
        $presupuesto->FECHA_CREACION = $now;
        $presupuesto->OBS_PRESU = $obs;
        $presupuesto->PROYECTO_ID_PROY = $ID_PROY;
        $presupuesto->save();

        if($categorias){
                foreach ($categorias as $key => $cat) {
                      foreach ($cat['subcategorias'] as $key => $sub) {
                        if($sub['MONTO_PRESU'] != null || $sub['MONTO_PRESU'] != 0 || $sub['MONTO_PRESU'] != ''){
                            $presupuesto->DetallePresupuesto()->attach($sub['ID_SUBC'],['MONTO_PRESU' => $sub['MONTO_PRESU']]);
                        }

                      }
                }
              return response()->json(['code' => 200, 'message' => 'Presupuesto de materiales creado con éxito'],200);
        }
          return response()->json(['code' => 200, 'message' => 'Presupuesto creado sin asignar detalle, revisar'],200);
      }
    }

    public function cargarTotalesPresupuesto(Request $request){
      $ID_PROY = $request->input('ID_PROY');
      $ID_PRESU = $request->input('ID_PRESU');
      $TIPO_SELECT = $request->input('TIPO_CATE');
      $totales = (object) array(
        'total_disponible' => 0,
        'total_materiales' => 0,
        'total_subcontratista' => 0,
        'total_manoobra' => 0,
        'total_gastos' => 0,
        'total_arriendos' => 0,
      );

      if($TIPO_SELECT != 'TODAS'){
        $monto_total = Presupuesto::join('DETALLE_PRESUPUESTO','PRESUPUESTO_ID_PRESU','PRESUPUESTO.ID_PRESU')
                        ->where('PRESUPUESTO.ID_PRESU', $ID_PRESU )
                        ->where('PRESUPUESTO.PROYECTO_ID_PROY',$ID_PROY )
                        ->join('SUBCATEGORIA','SUBCATEGORIA.ID_SUBC','DETALLE_PRESUPUESTO.SUBCATEGORIA_ID_SUBC')
                        ->join('CATEGORIA','CATEGORIA.ID_CATE','SUBCATEGORIA.CATEGORIA_ID_CATE')
                        ->where('CATEGORIA.TIPO_CATE',$TIPO_SELECT)->sum('DETALLE_PRESUPUESTO.MONTO_PRESU');

        if($TIPO_SELECT == 'M'){
          $totales->total_materiales = $monto_total;
        }
        if($TIPO_SELECT == 'SC'){
          $totales->total_subcontratista = $monto_total;
        }
        if($TIPO_SELECT == 'MO'){
          $totales->total_manoobra = $monto_total;
        }
        if($TIPO_SELECT == 'G'){
          $totales->total_gastos = $monto_total;
        }
        if($TIPO_SELECT == 'A'){
          $totales->total_arriendos = $monto_total;
        }

      } else{
        $ppto_proyecto = Proyecto::where('ID_PROY', $ID_PROY)->value('PRESUPUESTO_TOTAL_PROY');
        if($ID_PRESU == 0){
                $totales->total_disponible = $ppto_proyecto;
        } else{
          $total_usado = Presupuesto::join('DETALLE_PRESUPUESTO','PRESUPUESTO_ID_PRESU','PRESUPUESTO.ID_PRESU')
                          ->where('PRESUPUESTO.ID_PRESU', $ID_PRESU )
                          ->where('PRESUPUESTO.PROYECTO_ID_PROY',$ID_PROY )
                          ->sum('DETALLE_PRESUPUESTO.MONTO_PRESU');
              if($total_usado){
                  $totales->total_disponible = intval($ppto_proyecto) - intval($total_usado);
              }


          $totales->total_materiales = Presupuesto::join('DETALLE_PRESUPUESTO','PRESUPUESTO_ID_PRESU','PRESUPUESTO.ID_PRESU')
                          ->where('PRESUPUESTO.ID_PRESU', $ID_PRESU )
                          ->where('PRESUPUESTO.PROYECTO_ID_PROY',$ID_PROY )
                          ->join('SUBCATEGORIA','SUBCATEGORIA.ID_SUBC','DETALLE_PRESUPUESTO.SUBCATEGORIA_ID_SUBC')
                          ->join('CATEGORIA','CATEGORIA.ID_CATE','SUBCATEGORIA.CATEGORIA_ID_CATE')
                          ->where('CATEGORIA.TIPO_CATE','M')->sum('DETALLE_PRESUPUESTO.MONTO_PRESU');

          $totales->total_subcontratista = Presupuesto::join('DETALLE_PRESUPUESTO','PRESUPUESTO_ID_PRESU','PRESUPUESTO.ID_PRESU')
                                          ->where('PRESUPUESTO.ID_PRESU', $ID_PRESU )
                                          ->where('PRESUPUESTO.PROYECTO_ID_PROY',$ID_PROY )
                                          ->join('SUBCATEGORIA','SUBCATEGORIA.ID_SUBC','DETALLE_PRESUPUESTO.SUBCATEGORIA_ID_SUBC')
                                          ->join('CATEGORIA','CATEGORIA.ID_CATE','SUBCATEGORIA.CATEGORIA_ID_CATE')
                                          ->where('CATEGORIA.TIPO_CATE','SC')->sum('DETALLE_PRESUPUESTO.MONTO_PRESU');

          $totales->total_manoobra = Presupuesto::join('DETALLE_PRESUPUESTO','PRESUPUESTO_ID_PRESU','PRESUPUESTO.ID_PRESU')
                                        ->where('PRESUPUESTO.ID_PRESU', $ID_PRESU )
                                        ->where('PRESUPUESTO.PROYECTO_ID_PROY',$ID_PROY )
                                        ->join('SUBCATEGORIA','SUBCATEGORIA.ID_SUBC','DETALLE_PRESUPUESTO.SUBCATEGORIA_ID_SUBC')
                                        ->join('CATEGORIA','CATEGORIA.ID_CATE','SUBCATEGORIA.CATEGORIA_ID_CATE')
                                        ->where('CATEGORIA.TIPO_CATE','MO')->sum('DETALLE_PRESUPUESTO.MONTO_PRESU');

          $totales->total_manoobra = Presupuesto::join('DETALLE_PRESUPUESTO','PRESUPUESTO_ID_PRESU','PRESUPUESTO.ID_PRESU')
                                        ->where('PRESUPUESTO.ID_PRESU', $ID_PRESU )
                                        ->where('PRESUPUESTO.PROYECTO_ID_PROY',$ID_PROY )
                                        ->join('SUBCATEGORIA','SUBCATEGORIA.ID_SUBC','DETALLE_PRESUPUESTO.SUBCATEGORIA_ID_SUBC')
                                        ->join('CATEGORIA','CATEGORIA.ID_CATE','SUBCATEGORIA.CATEGORIA_ID_CATE')
                                        ->where('CATEGORIA.TIPO_CATE','MO')->sum('DETALLE_PRESUPUESTO.MONTO_PRESU');

         $totales->total_gastos = Presupuesto::join('DETALLE_PRESUPUESTO','PRESUPUESTO_ID_PRESU','PRESUPUESTO.ID_PRESU')
                                       ->where('PRESUPUESTO.ID_PRESU', $ID_PRESU )
                                       ->where('PRESUPUESTO.PROYECTO_ID_PROY',$ID_PROY )
                                       ->join('SUBCATEGORIA','SUBCATEGORIA.ID_SUBC','DETALLE_PRESUPUESTO.SUBCATEGORIA_ID_SUBC')
                                       ->join('CATEGORIA','CATEGORIA.ID_CATE','SUBCATEGORIA.CATEGORIA_ID_CATE')
                                       ->where('CATEGORIA.TIPO_CATE','G')->sum('DETALLE_PRESUPUESTO.MONTO_PRESU');

         $totales->total_arriendos = Presupuesto::join('DETALLE_PRESUPUESTO','PRESUPUESTO_ID_PRESU','PRESUPUESTO.ID_PRESU')
                                       ->where('PRESUPUESTO.ID_PRESU', $ID_PRESU )
                                       ->where('PRESUPUESTO.PROYECTO_ID_PROY',$ID_PROY )
                                       ->join('SUBCATEGORIA','SUBCATEGORIA.ID_SUBC','DETALLE_PRESUPUESTO.SUBCATEGORIA_ID_SUBC')
                                       ->join('CATEGORIA','CATEGORIA.ID_CATE','SUBCATEGORIA.CATEGORIA_ID_CATE')
                                       ->where('CATEGORIA.TIPO_CATE','A')->sum('DETALLE_PRESUPUESTO.MONTO_PRESU');
                    }

      return response()->json(['code' => 200, 'message' => 'Totales enviados','TIPO_SELECT'=>$TIPO_SELECT, 'data'=>$totales],200);
    }
}

public function cargarPresupuestoComparar(Request $request) {
  $ID_PROY = $request->input('ID_PROY');
  $ID_PRESU_1 = $request->input('ID_PRESU_1');
  $ID_PRESU_2 = $request->input('ID_PRESU_2');
  $presupuestos = [];
  $tipos = Categoria::all()->unique('TIPO_CATE')->pluck('TIPO_CATE');
  foreach ($tipos as $key => $tipo) {
    $presupuestos[$tipo] = $this->cargarPresupuestoPorTipo($ID_PROY,$tipo,$ID_PRESU_1,$ID_PRESU_2);
  }
  return response()->json($presupuestos);
}

public function cargarPresupuestoPorTipo($ID_PROY,$tipo,$ID_PRESU_1,$ID_PRESU_2){
  // $ID_PROY = $request->input('ID_PROY');
  // $tipo = $request->input('TIPO_CATE');
  // $ID_PRESU_1 = $request->input('ID_PRESU_1');
  // $ID_PRESU_2 = $request->input('ID_PRESU_2');

  $categorias = Categoria::where('TIPO_CATE',$tipo)->orderBy('NOMBRE_CATE')->with(['subcategorias'])->get();
  $ppto1 = null;
  $ppto2 = null;
    foreach ($categorias as $key => $cat) {
      $cat->MONTO_TOTAL_CAT_1 = 0;
      $cat->MONTO_TOTAL_CAT_2 = 0;

        $ppto1 = Presupuesto::with('DetallePresupuesto')
                        ->join('DETALLE_PRESUPUESTO','PRESUPUESTO_ID_PRESU','PRESUPUESTO.ID_PRESU')
                        ->where('PRESUPUESTO.PROYECTO_ID_PROY',$ID_PROY )
                        ->where('PRESUPUESTO.ID_PRESU',$ID_PRESU_1 )
                        ->whereIn('DETALLE_PRESUPUESTO.SUBCATEGORIA_ID_SUBC',$cat->subcategorias->pluck('ID_SUBC'))->first();
        // return $ppto1;
        $ppto2 = Presupuesto::with('DetallePresupuesto')
                        ->join('DETALLE_PRESUPUESTO','PRESUPUESTO_ID_PRESU','PRESUPUESTO.ID_PRESU')
                        ->where('PRESUPUESTO.PROYECTO_ID_PROY',$ID_PROY )
                        ->where('PRESUPUESTO.ID_PRESU',$ID_PRESU_2 )
                        ->whereIn('DETALLE_PRESUPUESTO.SUBCATEGORIA_ID_SUBC',$cat->subcategorias->pluck('ID_SUBC'))->first();

                  foreach ($cat->subcategorias as $keys => $sub) {
                      $sub->MONTO_PRESU_1 = 0;
                      $sub->MONTO_PRESU_2 = 0;

                      if($ppto1){
                        foreach ($ppto1->DetallePresupuesto as $keydt => $dtpresu) {
                            if($dtpresu->ID_SUBC == $sub->ID_SUBC){
                              $cat->MONTO_TOTAL_CAT_1 = $cat->MONTO_TOTAL_CAT_1 + intval($dtpresu->pivot->MONTO_PRESU);
                              $sub->MONTO_PRESU_1 = $dtpresu->pivot->MONTO_PRESU;
                            }
                        }
                      }
                      if($ppto2){
                        foreach ($ppto2->DetallePresupuesto as $keydt2 => $dtpresu2) {
                            if($dtpresu2->ID_SUBC == $sub->ID_SUBC){
                              $cat->MONTO_TOTAL_CAT_2 = $cat->MONTO_TOTAL_CAT_2 + intval($dtpresu2->pivot->MONTO_PRESU);
                              $sub->MONTO_PRESU_2 = $dtpresu2->pivot->MONTO_PRESU;
                            }
                          }
                      }
        }
}

              // Obtener total por presupuesto 1
              $ppto1_total_tipo = Presupuesto::join('DETALLE_PRESUPUESTO','PRESUPUESTO_ID_PRESU','PRESUPUESTO.ID_PRESU')
                              ->where('PRESUPUESTO.ID_PRESU', $ID_PRESU_1 )
                              ->where('PRESUPUESTO.PROYECTO_ID_PROY',$ID_PROY )
                              ->join('SUBCATEGORIA','SUBCATEGORIA.ID_SUBC','DETALLE_PRESUPUESTO.SUBCATEGORIA_ID_SUBC')
                              ->join('CATEGORIA','CATEGORIA.ID_CATE','SUBCATEGORIA.CATEGORIA_ID_CATE')
                              ->where('CATEGORIA.TIPO_CATE',$tipo)->sum('DETALLE_PRESUPUESTO.MONTO_PRESU');

              // Obtener total por presupuesto 2
              $ppto2_total_tipo = Presupuesto::join('DETALLE_PRESUPUESTO','PRESUPUESTO_ID_PRESU','PRESUPUESTO.ID_PRESU')
                              ->where('PRESUPUESTO.ID_PRESU', $ID_PRESU_2 )
                              ->where('PRESUPUESTO.PROYECTO_ID_PROY',$ID_PROY )
                              ->join('SUBCATEGORIA','SUBCATEGORIA.ID_SUBC','DETALLE_PRESUPUESTO.SUBCATEGORIA_ID_SUBC')
                              ->join('CATEGORIA','CATEGORIA.ID_CATE','SUBCATEGORIA.CATEGORIA_ID_CATE')
                              ->where('CATEGORIA.TIPO_CATE',$tipo)->sum('DETALLE_PRESUPUESTO.MONTO_PRESU');

            $objeto = (object)array(
              'data' => $categorias,
              'ppto1' => $ID_PRESU_1,
              'ppto2' => $ID_PRESU_2,
              'ppto1_total_tipo' => $ppto1_total_tipo,
              'ppto2_total_tipo' => $ppto2_total_tipo
            );
            return $objeto;
        // return response()->json(['code' => 200, 'message' => 'Categorias encontradas comparar',
        // 'data'=>$categorias, 'ppto1'=>$ID_PRESU_1,'ppto2'=>$ID_PRESU_2,'ppto1_total_tipo'=>$ppto1_total_tipo,'ppto2_total_tipo'=>$ppto2_total_tipo,],200);
      }



    public function cargarHistorialPresupuesto($ID_PROY){
      if($ID_PROY){
          $presupuestos = Presupuesto::where('PROYECTO_ID_PROY',$ID_PROY)->orderBy('FECHA_CREACION','desc')->get();
          foreach ($presupuestos as $key => $value) {
            $value->TOTAL_USADO = 0;
            $value->seleccionado = false;
            $value->desactivar = false;
            $total_usado = Presupuesto::join('DETALLE_PRESUPUESTO','PRESUPUESTO_ID_PRESU','PRESUPUESTO.ID_PRESU')
                            ->where('PRESUPUESTO.ID_PRESU', $value->ID_PRESU )
                            ->where('PRESUPUESTO.PROYECTO_ID_PROY',$ID_PROY )
                            ->sum('DETALLE_PRESUPUESTO.MONTO_PRESU');
            if($total_usado){
              $value->TOTAL_USADO = $total_usado;
            }
          }
          return response()->json(['code' => 200, 'message' => 'Presupuestos encontrados', 'data'=>$presupuestos],200);
      }
      return response()->json(['code' => 200, 'message' => 'No hay presupuestos', 'data'=>[]],200);

    }


    public function cargarOtrosCostos(Request $request){
      $ID_PROY = $request->input('ID_PROY');
      // Mano de obra y gastos
      $categorias = Categoria::WhereIn('TIPO_CATE',['MO','G'])->with(['subcategorias'])->get();
      $mes_ano = $request->input('MES_ANO');
      $mes = date('m', strtotime($mes_ano));
      $ano = date('Y', strtotime($mes_ano));

      $data = (object) array(
        'manoobra' => array(),
        'gastos' => array(),
      );

      foreach ($categorias as $key => $cat) {
          $cat->MONTO_GASTO_TOTAL = 0;
          // $cat->subcategorias = Subcategoria::where('CATEGORIA_ID_CATE','=',$cat->ID_CATE)->get();

            foreach ($cat->subcategorias as $keys => $sub) {
                $sub->MONTO_GASTO = null;
                $sub->EDITABLE = false;
                // $sub->FECHA_PAGO = null;
                $sub->FECHA_MODIFICACION = null;
                $sub->ID_GASTO = null;

                $gasto = Gasto::where('PROYECTO_ID_PROY', $ID_PROY)
                                ->whereMonth('MES_ANO_ASIGNADO','=',$mes)
                                ->whereYear('MES_ANO_ASIGNADO',$ano)
                                ->where('SUBCATEGORIA_ID_SUBC', $sub->ID_SUBC)->first();

                if($gasto){
                  $sub->ID_GASTO = $gasto->ID_GASTO;
                  $sub->MONTO_GASTO = $gasto->MONTO_GASTO;
                  // $sub->FECHA_PAGO = $gasto->FECHA_PAGO.'T00:00:00';
                  $sub->FECHA_MODIFICACION = $gasto->FECHA_MODIFICACION;
                  $cat->MONTO_GASTO_TOTAL = $cat->MONTO_GASTO_TOTAL + intval($sub->MONTO_GASTO);
                }
            }
            $ultima_modificacion = Gasto::where('PROYECTO_ID_PROY', $ID_PROY)
                            ->whereMonth('MES_ANO_ASIGNADO','=',$mes)
                            ->whereYear('MES_ANO_ASIGNADO',$ano)
                            ->join('SUBCATEGORIA','SUBCATEGORIA.ID_SUBC','GASTO.SUBCATEGORIA_ID_SUBC')
                            ->where('SUBCATEGORIA.CATEGORIA_ID_CATE', $cat->ID_CATE)
                            ->orderBy('FECHA_MODIFICACION', 'desc')->first();
            $cat->ultima_modificacion = null;
            if($ultima_modificacion){
                      $cat->ultima_modificacion = $ultima_modificacion->value('FECHA_MODIFICACION');
            }

           if($cat->TIPO_CATE == 'MO'){
                  array_push($data->manoobra, $cat);
                        }
            if($cat->TIPO_CATE == 'G'){
                  array_push($data->gastos, $cat);
            }
      }

      $total_manoobra = Gasto::where('PROYECTO_ID_PROY', $ID_PROY)
                      ->whereMonth('MES_ANO_ASIGNADO',$mes)
                      ->whereYear('MES_ANO_ASIGNADO',$ano)
                      ->join('SUBCATEGORIA','SUBCATEGORIA.ID_SUBC','GASTO.SUBCATEGORIA_ID_SUBC')
                      ->join('CATEGORIA','CATEGORIA.ID_CATE','SUBCATEGORIA.CATEGORIA_ID_CATE')
                      ->where('CATEGORIA.TIPO_CATE','MO')->sum('GASTO.MONTO_GASTO');

      $total_gastos = Gasto::where('PROYECTO_ID_PROY', $ID_PROY)
                      ->whereMonth('MES_ANO_ASIGNADO',$mes)
                      ->whereYear('MES_ANO_ASIGNADO',$ano)
                      ->join('SUBCATEGORIA','SUBCATEGORIA.ID_SUBC','GASTO.SUBCATEGORIA_ID_SUBC')
                      ->join('CATEGORIA','CATEGORIA.ID_CATE','SUBCATEGORIA.CATEGORIA_ID_CATE')
                      ->where('CATEGORIA.TIPO_CATE','G')->sum('GASTO.MONTO_GASTO');

      return response()->json(['code' => 200, 'message' => 'Categorias encontradas',
      'data'=>$data, 'mes_ano'=>$mes_ano, 'total_mo'=>$total_manoobra, 'total_g'=>$total_gastos],200);

    }

    public function guardarOtrosCostos(Request $request){
      $ID_PROY = $request->input('ID_PROY');
      $categorias = $request->input('categorias');
      $mes_ano = $request->input('MES_ANO');
      if($ID_PROY){

        if($categorias){
                foreach ($categorias as $key => $cat) {
                      foreach ($cat['subcategorias'] as $key => $sub) {
                        if($sub['MONTO_GASTO'] != null || $sub['MONTO_GASTO'] != 0 || $sub['MONTO_GASTO'] != ''){
                          date_default_timezone_set('America/Santiago');
                          $now = new \DateTime();
                          if($sub['ID_GASTO']){
                              $gasto_old = Gasto::find($sub['ID_GASTO']);
                              $gasto_old->FECHA_MODIFICACION = $now;
                              // $gasto_old->SUBCATEGORIA_ID_SUBC =  $sub['ID_SUBC'];
                              $gasto_old->MONTO_GASTO = $sub['MONTO_GASTO'];
                              // $gasto_old->FECHA_PAGO = $sub['FECHA_PAGO'];
                              // $gasto_old->MES_ANO_ASIGNADO = $mes_ano;
                              // $gasto_old->PROYECTO_ID_PROY = $ID_PROY;
                              $gasto_old->save();
                          } else{
                            $gasto = new Gasto();
                            $gasto->FECHA_MODIFICACION = $now;
                            $gasto->SUBCATEGORIA_ID_SUBC =  $sub['ID_SUBC'];
                            $gasto->MONTO_GASTO = $sub['MONTO_GASTO'];
                            // $gasto->FECHA_PAGO = $sub['FECHA_PAGO'];
                            $gasto->MES_ANO_ASIGNADO = $mes_ano;
                            $gasto->PROYECTO_ID_PROY = $ID_PROY;
                            $gasto->save();

                          }

                        }

                      }

                }

              return response()->json(['code' => 200, 'message' => 'Otros costos guardados con éxito'],200);
        }
      }
    }


    // Ejecucion presupuestaria
    public function cargarEjecucionPresupuestaria(Request $request){
      $ID_PROY = $request->input('ID_PROY');

        $data = (object) array(
          'materiales' => array(),
          'subcontratista' => array(),
          'manoobra' => array(),
          'gastos' => array(),
          'arriendos' => array(),
          'MATERIALES_TOTAL_ASIGNADO' => 0,
          'MATERIALES_TOTAL_OC_APROBADAS' => 0,
          'MATERIALES_TOTAL_OC_RECEPCIONADO' => 0,
          'MATERIALES_TOTAL_OC_UTILIZADO' => 0,
          'MATERIALES_TOTAL_DISPONIBLE' => 0,
          'MATERIALES_TOTAL_DEVENGADO' => 0,
          'MATERIALES_TOTAL_PAGADO' => 0,
          'MATERIALES_PORCENTAJE_EJECUCION' => 0,
          'SUBCONTRATISTA_TOTAL_ASIGNADO' => 0,
          'SUBCONTRATISTA_TOTAL_CONTRATOS' => 0,
          'SUBCONTRATISTA_TOTAL_DISPONIBLE' => 0,
          'SUBCONTRATISTA_TOTAL_DEVENGADO'=> 0,
          'SUBCONTRATISTA_TOTAL_PAGADO' => 0,
          'SUBCONTRATISTA_PORCENTAJE_EJECUCION' => 0,
          'MANOOBRA_TOTAL_ASIGNADO' => 0,
          'MANOOBRA_TOTAL_DISPONIBLE' => 0,
          'MANOOBRA_TOTAL_PAGADO' => 0,
          'MANOOBRA_PORCENTAJE_EJECUCION' => 0,
          'GASTOS_TOTAL_ASIGNADO' => 0,
          'GASTOS_TOTAL_DISPONIBLE' => 0,
          'GASTOS_TOTAL_PAGADO' => 0,
          'GASTOS_PORCENTAJE_EJECUCION' => 0,
          'ARRIENDOS_TOTAL_ASIGNADO' => 0,
          'ARRIENDOS_TOTAL_DISPONIBLE' => 0,
          'ARRIENDOS_TOTAL_DEVENGADO'=> 0,
          'ARRIENDOS_TOTAL_PAGADO' => 0,
        );
        $totales = (object) array(
          'TOTAL_ASIGNADO' => 0,
          'TOTAL_OC_CONTRATOS' => 0,
          'TOTAL_OC_RECEPCIONADO' => 0,
          'TOTAL_OC_UTILIZADO' => 0,
          'TOTAL_DISPONIBLE' => 0,
          'TOTAL_DEVENGADO' => 0,
          'TOTAL_PAGADO' => 0,
          'PORCENTAJE_EJECUCION' => 0,
        );


        $categorias = Categoria::with(['subcategorias'])->get();
        $idpresu = 0;
        $presu = null;
        foreach ($categorias as $key => $cat) {
            $cat->MONTO_ASIGNADO = 0;
            $cat->MONTO_OC_APROBADAS = 0;
            $cat->MONTO_CONTRATOS = 0;
            $cat->MONTO_OC_RECEPCIONADO = 0;
            $cat->MONTO_OC_UTILIZADO = 0;
            $cat->MONTO_DISPONIBLE = 0;
            $cat->MONTO_DEVENGADO = 0;
            $cat->MONTO_PAGADO = 0;
            $cat->PORCENTAJE_EJECUCION = 0;
            $presupuesto = Presupuesto::with('DetallePresupuesto')
                            ->join('DETALLE_PRESUPUESTO','PRESUPUESTO_ID_PRESU','PRESUPUESTO.ID_PRESU')
                            ->where('PRESUPUESTO.PROYECTO_ID_PROY',$ID_PROY )
                            ->whereIn('DETALLE_PRESUPUESTO.SUBCATEGORIA_ID_SUBC',$cat->subcategorias->pluck('ID_SUBC'))
                            ->orderBy('ID_PRESU', 'desc')->first();
                            if($presupuesto){
                                $idpresu = $presupuesto->ID_PRESU;
                                $presu = $presupuesto;
                            }


                    foreach ($cat->subcategorias as $keys => $sub) {
                        $sub->MONTO_ASIGNADO = 0;
                        $sub->MONTO_OC_APROBADAS = 0;
                        $sub->MONTO_CONTRATOS = 0;
                        $sub->MONTO_OC_RECEPCIONADO = 0;
                        $sub->MONTO_OC_UTILIZADO = 0;
                        $sub->MONTO_DISPONIBLE = 0;
                        $sub->MONTO_DEVENGADO = 0;
                        $sub->MONTO_PAGADO = 0;
                        $sub->PORCENTAJE_EJECUCION = 0;
                        if($presupuesto){
                          foreach ($presupuesto->DetallePresupuesto as $keydt => $dtpresu) {
                              if($dtpresu->ID_SUBC == $sub->ID_SUBC){
                                $sub->MONTO_ASIGNADO = intval($dtpresu->pivot->MONTO_PRESU);
                                $cat->MONTO_ASIGNADO = $cat->MONTO_ASIGNADO + $sub->MONTO_ASIGNADO;

                              }
                          }
                        }

                      if($cat->TIPO_CATE == 'M'){
                        // Total Ordenes aprobadas por subcategoria
                        $sub->MONTO_OC_APROBADAS = app('App\Http\Controllers\BodegaController')->getPromOC($sub->ID_SUBC,$ID_PROY);
                        $cat->MONTO_OC_APROBADAS= $cat->MONTO_OC_APROBADAS + $sub->MONTO_OC_APROBADAS;

                        // Total Ordenes recepcionadas por subcategoria
                        $sub->MONTO_OC_RECEPCIONADO = app('App\Http\Controllers\BodegaController')->getSumRecep($sub->ID_SUBC,$ID_PROY)[0];
                        $cat->MONTO_OC_RECEPCIONADO= $cat->MONTO_OC_RECEPCIONADO + $sub->MONTO_OC_RECEPCIONADO;

                        // Total de ordenes utilizadas o entregadas
                        $sub->MONTO_OC_UTILIZADO = app('App\Http\Controllers\BodegaController')->getSumaUtilizado($sub->ID_SUBC,$ID_PROY);
                        $cat->MONTO_OC_UTILIZADO= $cat->MONTO_OC_UTILIZADO + $sub->MONTO_OC_UTILIZADO;

                        // Total presupuesto disponible
                        $sub->MONTO_DISPONIBLE = $sub->MONTO_ASIGNADO - $sub->MONTO_OC_APROBADAS;
                        $cat->MONTO_DISPONIBLE= $cat->MONTO_DISPONIBLE + $sub->MONTO_DISPONIBLE;

                        // Total Ordenes con pagos (Devengado)
                        $sub->MONTO_DEVENGADO = $this->getOcDevengado($sub->ID_SUBC,$ID_PROY);
                        $cat->MONTO_DEVENGADO = $cat->MONTO_DEVENGADO + $sub->MONTO_DEVENGADO;

                        // Total Ordenes con pagos (Consolidados)
                        $sub->MONTO_PAGADO = $this->getOcPagado($sub->ID_SUBC,$ID_PROY);
                        $cat->MONTO_PAGADO = $cat->MONTO_PAGADO + $sub->MONTO_PAGADO;

                        // Porcentaje ejecucion
                        if($sub->MONTO_ASIGNADO > 0 && $sub->MONTO_PAGADO >0){
                          $sub->PORCENTAJE_EJECUCION = intval($sub->MONTO_PAGADO*100/$sub->MONTO_ASIGNADO);
                        }

                      }
                      if($cat->TIPO_CATE == 'SC'){
                        // Total subcontratos
                        $sub->MONTO_CONTRATOS = intval(Subcontratar::where('SUBCATEGORIA_ID_SUBC',$sub->ID_SUBC)->where('PROYECTO_ID_PROY',$ID_PROY)->sum('MONTO_PPTO_SUBCON'));
                        $cat->MONTO_CONTRATOS= $cat->MONTO_CONTRATOS + $sub->MONTO_CONTRATOS;

                        // Total presupuesto disponible
                        $sub->MONTO_DISPONIBLE = $sub->MONTO_ASIGNADO - $sub->MONTO_CONTRATOS;
                        $cat->MONTO_DISPONIBLE= $cat->MONTO_DISPONIBLE + $sub->MONTO_DISPONIBLE;

                        // Total subcontratos con pagos (Devengado)
                        $sub->MONTO_DEVENGADO = $this->getSubconDevengado($sub->ID_SUBC,$ID_PROY);
                        $cat->MONTO_DEVENGADO = $cat->MONTO_DEVENGADO + $sub->MONTO_DEVENGADO;

                        // Total subcontratos con pagos (Consolidados)
                        $sub->MONTO_PAGADO = $this->getSubconPagado($sub->ID_SUBC,$ID_PROY);
                        $cat->MONTO_PAGADO = $cat->MONTO_PAGADO + $sub->MONTO_PAGADO;

                        // Porcentaje ejecucion
                        if($sub->MONTO_ASIGNADO > 0 && $sub->MONTO_PAGADO >0){
                          $sub->PORCENTAJE_EJECUCION = intval($sub->MONTO_PAGADO*100/$sub->MONTO_ASIGNADO);
                        }
                      }
                      if($cat->TIPO_CATE == 'MO'){

                        // Total presupuesto disponible
                        $sub->MONTO_DISPONIBLE = $sub->MONTO_ASIGNADO - $sub->MONTO_CONTRATOS;
                        $cat->MONTO_DISPONIBLE= $cat->MONTO_DISPONIBLE + $sub->MONTO_DISPONIBLE;

                        // Total gastos mano de obra
                        $sub->MONTO_PAGADO = intval(Gasto::where('SUBCATEGORIA_ID_SUBC',$sub->ID_SUBC)
                        ->where('PROYECTO_ID_PROY',$ID_PROY)->orderBy('ID_GASTO', 'desc')->sum('MONTO_GASTO'));
                        $cat->MONTO_PAGADO = $cat->MONTO_PAGADO + $sub->MONTO_PAGADO;

                        // Porcentaje ejecucion
                        if($sub->MONTO_ASIGNADO > 0 && $sub->MONTO_PAGADO >0){
                          $sub->PORCENTAJE_EJECUCION = intval($sub->MONTO_PAGADO*100/$sub->MONTO_ASIGNADO);
                        }
                      }
                      if($cat->TIPO_CATE == 'G'){

                        // Total presupuesto disponible
                        $sub->MONTO_DISPONIBLE = $sub->MONTO_ASIGNADO - $sub->MONTO_CONTRATOS;
                        $cat->MONTO_DISPONIBLE= $cat->MONTO_DISPONIBLE + $sub->MONTO_DISPONIBLE;

                        // Total gastos mano de obra
                        $sub->MONTO_PAGADO = intval(Gasto::where('SUBCATEGORIA_ID_SUBC',$sub->ID_SUBC)
                        ->where('PROYECTO_ID_PROY',$ID_PROY)->orderBy('ID_GASTO', 'desc')->sum('MONTO_GASTO'));
                        $cat->MONTO_PAGADO = $cat->MONTO_PAGADO + $sub->MONTO_PAGADO;

                        // Porcentaje ejecucion
                        if($sub->MONTO_ASIGNADO > 0 && $sub->MONTO_PAGADO >0){
                          $sub->PORCENTAJE_EJECUCION = intval($sub->MONTO_PAGADO*100/$sub->MONTO_ASIGNADO);
                        }
                      }

                      if($cat->TIPO_CATE == 'A'){
                          // Total detalles arriendos con pagos (Devengado)
                        $sub->MONTO_DEVENGADO = $this->getSubconArriendoDevengado($sub->ID_SUBC,$ID_PROY);
                        $cat->MONTO_DEVENGADO = $cat->MONTO_DEVENGADO + $sub->MONTO_DEVENGADO;

                        // Total presupuesto disponible
                        $sub->MONTO_DISPONIBLE = $sub->MONTO_ASIGNADO - $sub->MONTO_DEVENGADO;
                        $cat->MONTO_DISPONIBLE= $cat->MONTO_DISPONIBLE + $sub->MONTO_DISPONIBLE;

                        // Total arriendos con pagos (Consolidados)
                        $sub->MONTO_PAGADO = $this->getSubconArriendoPagado($sub->ID_SUBC,$ID_PROY);
                        $cat->MONTO_PAGADO = $cat->MONTO_PAGADO + $sub->MONTO_PAGADO;

                        // Porcentaje ejecucion
                        if($sub->MONTO_ASIGNADO > 0 && $sub->MONTO_PAGADO >0){
                          $sub->PORCENTAJE_EJECUCION = intval($sub->MONTO_PAGADO*100/$sub->MONTO_ASIGNADO);
                        }
                      }

                  }

                  if($cat->TIPO_CATE == 'M'){
                      // Porcentaje ejecucion cat
                      if($cat->MONTO_ASIGNADO > 0 && $cat->MONTO_PAGADO >0){
                          $cat->PORCENTAJE_EJECUCION = intval($cat->MONTO_PAGADO*100/$cat->MONTO_ASIGNADO);
                      }
                        array_push($data->materiales, $cat);
                        $data->MATERIALES_TOTAL_ASIGNADO = $data->MATERIALES_TOTAL_ASIGNADO + $cat->MONTO_ASIGNADO;
                        $data->MATERIALES_TOTAL_OC_APROBADAS = $data->MATERIALES_TOTAL_OC_APROBADAS + $cat->MONTO_OC_APROBADAS;
                        $data->MATERIALES_TOTAL_OC_RECEPCIONADO = $data->MATERIALES_TOTAL_OC_RECEPCIONADO + $cat->MONTO_OC_RECEPCIONADO;
                        $data->MATERIALES_TOTAL_OC_UTILIZADO = $data->MATERIALES_TOTAL_OC_UTILIZADO + $cat->MONTO_OC_UTILIZADO;
                        $data->MATERIALES_TOTAL_DISPONIBLE = $data->MATERIALES_TOTAL_DISPONIBLE + $cat->MONTO_DISPONIBLE;
                        $data->MATERIALES_TOTAL_DEVENGADO = $data->MATERIALES_TOTAL_DEVENGADO + $cat->MONTO_DEVENGADO;
                        $data->MATERIALES_TOTAL_PAGADO = $data->MATERIALES_TOTAL_PAGADO + $cat->MONTO_PAGADO;
                        if($data->MATERIALES_TOTAL_PAGADO > 0){
                        $data->MATERIALES_PORCENTAJE_EJECUCION = intval($data->MATERIALES_TOTAL_PAGADO*100/$data->MATERIALES_TOTAL_ASIGNADO);
                        }



                  }

                  if($cat->TIPO_CATE == 'SC'){
                      // Porcentaje ejecucion cat
                      if($cat->MONTO_ASIGNADO > 0 && $cat->MONTO_PAGADO >0){
                          $cat->PORCENTAJE_EJECUCION = intval($cat->MONTO_PAGADO*100/$cat->MONTO_ASIGNADO);
                      }
                        array_push($data->subcontratista, $cat);
                        $data->SUBCONTRATISTA_TOTAL_ASIGNADO = $data->SUBCONTRATISTA_TOTAL_ASIGNADO + $cat->MONTO_ASIGNADO;
                        $data->SUBCONTRATISTA_TOTAL_CONTRATOS = $data->SUBCONTRATISTA_TOTAL_CONTRATOS + $cat->MONTO_CONTRATOS;
                        $data->SUBCONTRATISTA_TOTAL_DISPONIBLE = $data->SUBCONTRATISTA_TOTAL_DISPONIBLE + $cat->MONTO_DISPONIBLE;
                        $data->SUBCONTRATISTA_TOTAL_DEVENGADO = $data->SUBCONTRATISTA_TOTAL_DEVENGADO + $cat->MONTO_DEVENGADO;
                        $data->SUBCONTRATISTA_TOTAL_PAGADO = $data->SUBCONTRATISTA_TOTAL_PAGADO + $cat->MONTO_PAGADO;
                        if($data->SUBCONTRATISTA_TOTAL_ASIGNADO > 0){
                            $data->SUBCONTRATISTA_PORCENTAJE_EJECUCION = intval($data->SUBCONTRATISTA_TOTAL_PAGADO*100/$data->SUBCONTRATISTA_TOTAL_ASIGNADO);
                        }



                  }

                  if($cat->TIPO_CATE == 'MO'){
                    // Porcentaje ejecucion cat
                    if($cat->MONTO_ASIGNADO > 0 && $cat->MONTO_PAGADO >0){
                        $cat->PORCENTAJE_EJECUCION = intval($cat->MONTO_PAGADO*100/$cat->MONTO_ASIGNADO);
                    }
                      array_push($data->manoobra, $cat);
                      $data->MANOOBRA_TOTAL_ASIGNADO = $data->MANOOBRA_TOTAL_ASIGNADO + $cat->MONTO_ASIGNADO;
                      $data->MANOOBRA_TOTAL_DISPONIBLE = $data->MANOOBRA_TOTAL_DISPONIBLE + $cat->MONTO_DISPONIBLE;
                      $data->MANOOBRA_TOTAL_PAGADO = $data->MANOOBRA_TOTAL_PAGADO + $cat->MONTO_PAGADO;
                      if($data->MANOOBRA_TOTAL_ASIGNADO > 0){
                        $data->MANOOBRA_PORCENTAJE_EJECUCION = intval($data->MANOOBRA_TOTAL_PAGADO*100/$data->MANOOBRA_TOTAL_ASIGNADO);
                      }



                  }
                  if($cat->TIPO_CATE == 'G'){
                    // Porcentaje ejecucion cat
                    if($cat->MONTO_ASIGNADO > 0 && $cat->MONTO_PAGADO >0){
                        $cat->PORCENTAJE_EJECUCION = intval($cat->MONTO_PAGADO*100/$cat->MONTO_ASIGNADO);
                    }

                      array_push($data->gastos, $cat);
                      $data->GASTOS_TOTAL_ASIGNADO = $data->GASTOS_TOTAL_ASIGNADO + $cat->MONTO_ASIGNADO;
                      $data->GASTOS_TOTAL_DISPONIBLE = $data->GASTOS_TOTAL_DISPONIBLE + $cat->MONTO_DISPONIBLE;
                      $data->GASTOS_TOTAL_PAGADO = $data->GASTOS_TOTAL_PAGADO + $cat->MONTO_PAGADO;
                      if($data->GASTOS_TOTAL_ASIGNADO > 0){
                        $data->GASTOS_PORCENTAJE_EJECUCION = intval($data->GASTOS_TOTAL_PAGADO*100/$data->GASTOS_TOTAL_ASIGNADO);
                      }



                  }

                  if($cat->TIPO_CATE == 'A'){
                    // Porcentaje ejecucion cat
                    if($cat->MONTO_ASIGNADO > 0 && $cat->MONTO_PAGADO >0){
                        $cat->PORCENTAJE_EJECUCION = intval($cat->MONTO_PAGADO*100/$cat->MONTO_ASIGNADO);
                    }
                      array_push($data->arriendos, $cat);
                      $data->ARRIENDOS_TOTAL_ASIGNADO = $data->ARRIENDOS_TOTAL_ASIGNADO + $cat->MONTO_ASIGNADO;
                      $data->ARRIENDOS_TOTAL_DISPONIBLE = $data->ARRIENDOS_TOTAL_DISPONIBLE + $cat->MONTO_DISPONIBLE;
                      $data->ARRIENDOS_TOTAL_DEVENGADO = $data->ARRIENDOS_TOTAL_DEVENGADO + $cat->MONTO_DEVENGADO;
                      $data->ARRIENDOS_TOTAL_PAGADO = $data->ARRIENDOS_TOTAL_PAGADO + $cat->MONTO_PAGADO;
                      if($data->ARRIENDOS_TOTAL_ASIGNADO > 0){
                        $data->ARRIENDOS_PORCENTAJE_EJECUCION = intval($data->ARRIENDOS_TOTAL_PAGADO*100/$data->ARRIENDOS_TOTAL_ASIGNADO);
                      }



                  }

            }

            $totales->TOTAL_ASIGNADO = $data->MATERIALES_TOTAL_ASIGNADO + $data->SUBCONTRATISTA_TOTAL_ASIGNADO + $data->MANOOBRA_TOTAL_ASIGNADO + $data->GASTOS_TOTAL_ASIGNADO+ $data->ARRIENDOS_TOTAL_ASIGNADO;
            $totales->TOTAL_OC_CONTRATOS = $data->MATERIALES_TOTAL_OC_APROBADAS + $data->SUBCONTRATISTA_TOTAL_CONTRATOS;
            $totales->TOTAL_OC_RECEPCIONADO = $data->MATERIALES_TOTAL_OC_RECEPCIONADO;
            $totales->TOTAL_OC_UTILIZADO = $data->MATERIALES_TOTAL_OC_UTILIZADO;
            $totales->TOTAL_DISPONIBLE = $data->MATERIALES_TOTAL_DISPONIBLE + $data->SUBCONTRATISTA_TOTAL_DISPONIBLE + $data->MANOOBRA_TOTAL_DISPONIBLE + $data->GASTOS_TOTAL_DISPONIBLE+ $data->ARRIENDOS_TOTAL_DISPONIBLE;
            $totales->TOTAL_DEVENGADO = $data->MATERIALES_TOTAL_DEVENGADO + $data->SUBCONTRATISTA_TOTAL_DEVENGADO + $data->ARRIENDOS_TOTAL_DEVENGADO;
            $totales->TOTAL_PAGADO = $data->MATERIALES_TOTAL_PAGADO + $data->SUBCONTRATISTA_TOTAL_PAGADO + $data->MANOOBRA_TOTAL_PAGADO + $data->GASTOS_TOTAL_PAGADO+ $data->ARRIENDOS_TOTAL_PAGADO;
            if($totales->TOTAL_ASIGNADO > 0){
              $totales->PORCENTAJE_EJECUCION = intval($totales->TOTAL_PAGADO *100/$totales->TOTAL_ASIGNADO);
            }

          return response()->json(['code' => 200, 'message' => 'Ejecucion encontrada',
            'data'=>$data, 'totales'=>$totales],200);

      }

      private function getOcDevengado($ID_SUBC,$ID_PROY) {
        $suma = +OrdenCompra::join('DETALLE_ORDEN_COTIZACION','ORDEN_COMPRA_ID_ORDENC','ID_ORDENC')
          ->join('DETALLE_COTIZACION','DETALLE_COTIZACION.ID_DETA_COTI','DETALLE_ORDEN_COTIZACION.DETALLE_COTIZACION_ID_DETA_COTI')
          ->join('NOTA_PEDIDO','NOTA_PEDIDO.ID_NOTA_PED','DETALLE_COTIZACION.NOTA_PEDIDO_ID_NOTA_PED')
          ->join('MATERIAL','MATERIAL.ID_MATE','NOTA_PEDIDO.MATERIAL_ID_MATE')
          ->join('PAGO','PAGO.ORDEN_COMPRA_ID_ORDENC','ORDEN_COMPRA.ID_ORDENC')
          ->where('MATERIAL.SUBCATEGORIA_ID_SUBC',$ID_SUBC)->whereIn('ORDEN_COMPRA.ESTADO_ORDEN',['A','RP'])
          ->where('ORDEN_COMPRA.ID_PROY',$ID_PROY)->select('PAGO.ID_PAGO')->sum('PAGO.MONTO_NETO');

        return $suma;
      }

      private function getOcPagado($ID_SUBC,$ID_PROY) {
        $suma = +OrdenCompra::join('DETALLE_ORDEN_COTIZACION','ORDEN_COMPRA_ID_ORDENC','ID_ORDENC')
          ->join('DETALLE_COTIZACION','DETALLE_COTIZACION.ID_DETA_COTI','DETALLE_ORDEN_COTIZACION.DETALLE_COTIZACION_ID_DETA_COTI')
          ->join('NOTA_PEDIDO','NOTA_PEDIDO.ID_NOTA_PED','DETALLE_COTIZACION.NOTA_PEDIDO_ID_NOTA_PED')
          ->join('MATERIAL','MATERIAL.ID_MATE','NOTA_PEDIDO.MATERIAL_ID_MATE')
          ->join('PAGO','PAGO.ORDEN_COMPRA_ID_ORDENC','ORDEN_COMPRA.ID_ORDENC')
          ->where('MATERIAL.SUBCATEGORIA_ID_SUBC',$ID_SUBC)->whereIn('ORDEN_COMPRA.ESTADO_ORDEN',['A','RP'])
          ->where('ORDEN_COMPRA.ID_PROY',$ID_PROY)
          ->where('PAGO.ESTADO_PAGO','C')
          ->select('PAGO.ID_PAGO','PAGO.ESTADO_PAGO','PAGO.MONTO_NETO')->sum('PAGO.MONTO_NETO');

        return $suma;
      }

      private function getSubconDevengado($ID_SUBC,$ID_PROY) {
        $suma = +Subcontratar::where('SUBCATEGORIA_ID_SUBC',$ID_SUBC)->where('PROYECTO_ID_PROY',$ID_PROY)
              ->join('PAGO','PAGO.SUBCONTRATAR_ID_SUBCON','SUBCONTRATAR.ID_SUBCON')->select('PAGO.ID_PAGO')->sum('PAGO.MONTO_NETO');

        return $suma;
      }

      private function getSubconPagado($ID_SUBC,$ID_PROY) {
        $suma = +Subcontratar::where('SUBCATEGORIA_ID_SUBC',$ID_SUBC)->where('PROYECTO_ID_PROY',$ID_PROY)
              ->join('PAGO','PAGO.SUBCONTRATAR_ID_SUBCON','SUBCONTRATAR.ID_SUBCON')->where('PAGO.ESTADO_PAGO','C')
              ->select('PAGO.ID_PAGO')->sum('PAGO.MONTO_NETO');

        return $suma;
      }

      private function getSubconArriendoDevengado($ID_SUBC,$ID_PROY) {
        $suma = +Subcontratar::join('DETALLE_PAGO_ARRIENDO','DETALLE_PAGO_ARRIENDO.ID_SUBCON','SUBCONTRATAR.ID_SUBCON')
              ->where('PROYECTO_ID_PROY',$ID_PROY)->where('DETALLE_PAGO_ARRIENDO.ID_SUBC',$ID_SUBC)
              ->join('PAGO','PAGO.ID_PAGO','DETALLE_PAGO_ARRIENDO.PAGO_ID_PAGO')
              ->select('PAGO.ID_PAGO')->sum('PAGO.MONTO_NETO');

        return $suma;
      }

      private function getSubconArriendoPagado($ID_SUBC,$ID_PROY) {
        $suma = +Subcontratar::join('DETALLE_PAGO_ARRIENDO','DETALLE_PAGO_ARRIENDO.ID_SUBCON','SUBCONTRATAR.ID_SUBCON')
              ->where('PROYECTO_ID_PROY',$ID_PROY)->where('DETALLE_PAGO_ARRIENDO.ID_SUBC',$ID_SUBC)
              ->join('PAGO','PAGO.ID_PAGO','DETALLE_PAGO_ARRIENDO.PAGO_ID_PAGO')->where('PAGO.ESTADO_PAGO','C')
              ->select('PAGO.ID_PAGO')->sum('PAGO.MONTO_NETO');

        return $suma;
      }

      public function obtenerValorPresuArriendo($ID_SUBC){
        $valor = 0;
        $monto = DB::table('DETALLE_PRESUPUESTO')
                     ->where('SUBCATEGORIA_ID_SUBC',$ID_SUBC)
                     ->orderBy('PRESUPUESTO_ID_PRESU','desc')->value('MONTO_PRESU');
        if($monto)$valor = $monto;
        $valorDesc = DetallePagoArriendo::where('ID_SUBC',$ID_SUBC)->sum('MONTO_PAGADO');
        if($valorDesc){$valor = $valor - $valorDesc ;}

        return $valor;
      }

      public function prepararDatosPresupuesto($ID_PROY, $ID_PRESU) {
        $req = new \Illuminate\Http\Request();
        $req->replace([
          'ID_PROY' =>$ID_PROY,
          'ID_PRESU_1' => $ID_PRESU
        ]);
        $presupuestos = (array)$this->cargarPresupuestoComparar($req)->getData();
        $pres = Presupuesto::find($ID_PRESU);
        $resumen = array(
          'fech_crea' => $pres->FECHA_CREACION,
          'obse_pres' => $pres->OBS_PRESU,
          'mont_mate' => 0,
          'mont_subc' => 0,
          'mont_mobr' => 0,
          'mont_ggnr' => 0,
          'mont_arri' => 0,
          'mont_ptot' => Proyecto::find($ID_PROY)->PRESUPUESTO_TOTAL_PROY,
          'mont_pasg' => 0,
          'mont_sasg' => 0
        );
        $presus = [];
        $datos = [];
        foreach ($presupuestos as $key => $presupuesto) {
          $categorias = [];
          $nombre = '';
          switch ($key) {
            case 'A':
              $nombre = 'ARRIENDOS';
              $resumen['mont_arri'] = $presupuesto->ppto1_total_tipo;
              break;
            case 'SC':
              $nombre = 'SUBCONTRATISTAS';
              $resumen['mont_subc'] = $presupuesto->ppto1_total_tipo;
              break;
            case 'MO':
              $nombre = 'MANO DE OBRA';
              $resumen['mont_mobr'] = $presupuesto->ppto1_total_tipo;
              break;
            case 'G':
              $nombre = 'GASTOS GENERALES';
              $resumen['mont_ggnr'] = $presupuesto->ppto1_total_tipo;
              break;
            case 'M':
              $nombre = 'MATERIALES';
              $resumen['mont_mate'] = $presupuesto->ppto1_total_tipo;
              break;
          }
          $resumen['mont_pasg'] += $presupuesto->ppto1_total_tipo;
          foreach ($presupuesto->data as $categoria) {
            $categorias[] = (object)array(
              'nombreC' => $categoria->NOMBRE_CATE,
              'montoC' => 1,
              'cabecera' => true,
              'nombre' => '',
              'monto' => 0
            );
            $pos = count($categorias)-1;
            $first = true;
            foreach ($categoria->subcategorias as $subcates) {
              if($subcates->MONTO_PRESU_1 > 0) {
                if($first){
                  $categorias[$pos]->nombre = $subcates->NOMBRE_SUBC;
                  $categorias[$pos]->monto = $subcates->MONTO_PRESU_1;
                  $first = false;
                }
                else {
                  $categorias[$pos]->montoC ++;
                  $categorias[] = (object)array(
                    'nombre' => $subcates->NOMBRE_SUBC,
                    'monto' => $subcates->MONTO_PRESU_1,
                    'cabecera' => false
                  );
                }
              }
            }
            if($categorias[$pos]->montoC == 1) { array_splice($categorias,$pos,1); }
          }
          $datos[] = (object)array(
            'nombre' => $nombre,
            'datos' => array('categorias'=>$categorias),
            'vista' => 'detalle-presupuesto'
          );
        }
        $resumen['mont_sasg'] = $resumen['mont_ptot'] - $resumen['mont_pasg'];

        array_unshift($datos,
          (object)array(
            'nombre' => 'RESUMEN',
            'datos' => (array)$resumen,
            'vista' => 'resumen-presupuesto'
          )
        );
        return $datos;
      }

      public function generarExcelPresupuesto($ID_PROY, $ID_PRESU) {
        date_default_timezone_set('America/Santiago');
        $nomPro = Proyecto::find($ID_PROY)->NOMBRE_PROY;
        $datos = (array)$this->prepararDatosPresupuesto($ID_PROY, $ID_PRESU);
        return (new MultipleSheetExport($datos))->download($nomPro.' - Presupuesto N'.$ID_PRESU.' - '.date('d-m-Y').'.xlsx');
      }

      public function generarPDFPresupuesto($ID_PROY, $ID_PRESU) {
        date_default_timezone_set('America/Santiago');
        $nomPro = Proyecto::find($ID_PROY)->NOMBRE_PROY;
        $datos = (array)$this->prepararDatosPresupuesto($ID_PROY, $ID_PRESU);
        $first = true;
        $pdfM = new PDFMerger();
        $page_footer_html = view('footer')->render();
        $options = [
          'encoding'      => 'UTF-8',
          'margin-bottom' => '30mm',
          'footer-html'   => $page_footer_html,
        ];
        foreach ($datos as $key => $value) {
          $pdf = PDF::loadView($value->vista, $value->datos);
          $pdf->setPaper('a4');
          $pdf->setOptions($options);
          $pdf->save(storage_path().'/app/'.$key.'.pdf');
          $pdfM->addPDF(storage_path().'/app/'.$key.'.pdf', 'all');
          Storage::delete(storage_path().'/app/'.$key.'.pdf');
        }
        return $pdfM->merge('download', $nomPro.' - Presupuesto N'.$ID_PRESU.' - '.date('d-m-Y').'.pdf');
      }
}
