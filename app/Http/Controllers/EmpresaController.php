<?php

namespace App\Http\Controllers;

use App\Empresa;
use App\Contacto;
use App\Banco;
use App\TipoBanco;
use App\Subcategoria;
use Illuminate\Http\Request;

class EmpresaController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $proyecto = Proyecto::all();
        return $proyecto;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){}

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $ID_USU){}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){}

    // cargar subcontratista (se puede mejorar)
    public function cargarSubcontratistas(Request $request,$ID_CONS){
      $por_pagina = $request->input('POR_PAGINA');
      $empresa = Empresa::where('TIPO_EMPR','S')
                          ->where('CONSTRUCTORA_ID_CONS',$ID_CONS)
                          ->with(['contactos'])->paginate($por_pagina);
      return $empresa;
    }

    // filtrar subcontratista(razon,rut o por sub-categoria)
    public function filtrarSubcontratista(Request $request,$ID_CONS){
      $tipo = $request->input('tipo');
      $palabra = $request->input('palabra');
      $ID_SUBC = $request->input('id');
      $por_pagina = $request->input('POR_PAGINA');

      if($tipo == 'R'){
        $empresa = Empresa::where('TIPO_EMPR','S')
                            ->where('CONSTRUCTORA_ID_CONS',$ID_CONS)
                            ->with(['contactos'])
                            ->whereRaw("REPLACE(REPLACE(RUT_EMPR,'.',''),'-','') like '%$palabra%'")->paginate($por_pagina);
        return $empresa;
      }
      if($tipo == 'RZ'){
        $empresa = Empresa::where('TIPO_EMPR','S')
                            ->where('CONSTRUCTORA_ID_CONS',$ID_CONS)
                            ->with(['contactos'])
                            ->where('RAZON_SOCIAL_EMPR','like','%'.$palabra.'%')->paginate($por_pagina);
        return $empresa;
      }
      if($tipo == 'SC'){
        $empresa = Empresa::where('TIPO_EMPR','S')
                            ->join('DETALLE_SCON_SUBC','DETALLE_SCON_SUBC.EMPRESA_ID_EMPR','ID_EMPR')
                            ->where('CONSTRUCTORA_ID_CONS',$ID_CONS)
                            ->with(['contactos'])
                            ->where('DETALLE_SCON_SUBC.SUBCATEGORIA_ID_SUBC','=',$ID_SUBC)->paginate($por_pagina);
        return $empresa;
      }
      if($tipo == 'N'){
        $empresa = Empresa::where('TIPO_EMPR','S')
                            ->where('CONSTRUCTORA_ID_CONS',$ID_CONS)
                            ->with(['contactos'])->paginate($por_pagina);;
        return $empresa;
      }
    }

    // eliminar subcontratista
    public function eliminarSubcontratista($ID_EMP){
      $empresa = Empresa::where('ID_EMPR',$ID_EMP)->where('TIPO_EMPR','S')->first();
      if($empresa){
        if(count($empresa->DetalleSubcategoria) <> 0){
          $empresa->DetalleSubcategoria()->where('EMPRESA_ID_EMPR','=',$ID_EMP)->detach();
        }
        Contacto::where('EMPRESA_ID_EMPR','=',$ID_EMP)->delete();
        $empresa->delete();
      }
      return response()->json(['code' => 200, 'message' => 'Subcontratista eliminado correctamente'],200);
    }

    //obtener los datos de un sub-contratista
    public function getSubcontratista($ID_EMPR){
      $empresa = Empresa::where('TIPO_EMPR','S')
                          ->where('ID_EMPR',$ID_EMPR)
                          ->with(['contactos'])->first();

      $subcate = Subcategoria::with(['categoria'])->get();

      $categorias = collect();
      foreach ($empresa->DetalleSubcategoria as $key => $value) {
        foreach ($subcate as $key => $val) {
          if($value->ID_SUBC == $val->ID_SUBC){
            $categorias->push($val);
          }
        }
      }
      return response()->json(['code' => 200, 'message' => 'Subcontratista encontrado correctamente','data'=>$empresa,'scate'=>$categorias],200);
    }

    // cargar bancos
    public function cargarBancos(){
      $bancos = Banco::all();
      return $bancos;
    }
    // cargar tipo de cuentas
    public function cargarTipoCuentas(){
      $tipoCuenta = TipoBanco::all();
      return $tipoCuenta;
    }

    // crear proveedor o subcontratista
    public function crearEmpresa(Request $request){
      $RAZON_SOCIAL_EMPR = $request->input('RAZON_SOCIAL_EMPR');
      $NOMBRE_EMPR = $request->input('NOMBRE_EMPR');
      $RUT_EMPR = $request->input('RUT_EMPR');
      $DIRECCION = $request->input('DIRECCION');
      $EMAIL_EMPR = $request->input('EMAIL_EMPR');
      $TELEFONO_EMPR = $request->input('TELEFONO_EMPR');
      $SITIO_WEB_EMP = $request->input('SITIO_WEB_EMP');
      $ID_CONS = $request->input('ID_CONS');
      $ID_BANC = $request->input('ID_BANC');
      $ID_TIPO_CUEN = $request->input('ID_TIPO_CUEN');
      $NUMERO_CUENTA_EMPR = $request->input('NUMERO_CUENTA_EMPR');
      $TIPO_EMPR = $request->input('TIPO_EMPR');
      $ID_EMPR = $request->input('ID_EMPR');
      $CONTACTOS = $request->input('CONTACTOS');
      $SUBCATE = $request->input('SUBCATE');

      if($TIPO_EMPR <> 'P'){
        if($ID_EMPR == 0){
          if(Empresa::where('RUT_EMPR',$RUT_EMPR)->where('TIPO_EMPR','S')->where('CONSTRUCTORA_ID_CONS',$ID_CONS)->first()) {
            return response()->json(['message'=>'Rut ya registrado','code'=>201]);
          }
          $empresa = new Empresa();
          $empresa->RAZON_SOCIAL_EMPR = $RAZON_SOCIAL_EMPR;
          $empresa->NOMBRE_EMPR = $NOMBRE_EMPR;
          $empresa->RUT_EMPR = $RUT_EMPR;
          $empresa->DIRECCION = $DIRECCION;
          $empresa->EMAIL_EMPR = $EMAIL_EMPR;
          $empresa->TELEFONO_EMPR = $TELEFONO_EMPR;
          $empresa->SITIO_WEB_EMP = $SITIO_WEB_EMP;
          $empresa->CONSTRUCTORA_ID_CONS = $ID_CONS;
          $empresa->TIPO_CUENTA_BANCO_ID_TIPO_CUENTA = $ID_TIPO_CUEN;
          $empresa->BANCO_ID_BANCO = $ID_BANC;
          $empresa->NUMERO_CUENTA_EMPR = $NUMERO_CUENTA_EMPR;
          $empresa->TIPO_EMPR = $TIPO_EMPR;
          $empresa->save();

          if (count($CONTACTOS) <> 0) {
            foreach ($CONTACTOS as $key => $value) {
              $contacto = new Contacto();
              $contacto->EMPRESA_ID_EMPR = $empresa->ID_EMPR;
              $contacto->NOMBRE_CONTA = $value['NOMBRE_CONT'];
              $contacto->TELEFONO_CONTA = $value['TELEFONO_CONT'];
              $contacto->EMAIL_CONTA = $value['EMAIL_CONT'];
              $contacto->save();
            }
          }
          foreach ($SUBCATE as $key => $val) {
            $empresa->DetalleSubcategoria()->attach($val['ID_SUBC']);
          }
          return response()->json(['code' => 200, 'message' => 'Subcontratista registrado correctamente'],200);
        }else{
          if(Empresa::where('RUT_EMPR',$RUT_EMPR)->where('TIPO_EMPR','S')->where('CONSTRUCTORA_ID_CONS',$ID_CONS)->where('ID_EMPR','!=',$ID_EMPR)->first()) {
            return response()->json(['message'=>'Rut ya registrado','code'=>201]);
          }
          $empresa = Empresa::find($ID_EMPR);
          $empresa->RAZON_SOCIAL_EMPR = $RAZON_SOCIAL_EMPR;
          $empresa->NOMBRE_EMPR = $NOMBRE_EMPR;
          $empresa->RUT_EMPR = $RUT_EMPR;
          $empresa->DIRECCION = $DIRECCION;
          $empresa->EMAIL_EMPR = $EMAIL_EMPR;
          $empresa->TELEFONO_EMPR = $TELEFONO_EMPR;
          $empresa->SITIO_WEB_EMP = $SITIO_WEB_EMP;
          $empresa->CONSTRUCTORA_ID_CONS = $ID_CONS;
          $empresa->TIPO_CUENTA_BANCO_ID_TIPO_CUENTA = $ID_TIPO_CUEN;
          $empresa->BANCO_ID_BANCO = $ID_BANC;
          $empresa->NUMERO_CUENTA_EMPR = $NUMERO_CUENTA_EMPR;
          $empresa->TIPO_EMPR = $TIPO_EMPR;
          $empresa->save();
          $empresa->DetalleSubcategoria()->detach();
          Contacto::where('EMPRESA_ID_EMPR','=',$empresa->ID_EMPR)->delete();
          if (count($CONTACTOS) <> 0) {
            foreach ($CONTACTOS as $key => $value) {
              $contacto = new Contacto();
              $contacto->EMPRESA_ID_EMPR = $empresa->ID_EMPR;
              $contacto->NOMBRE_CONTA = $value['NOMBRE_CONT'];
              $contacto->TELEFONO_CONTA = $value['TELEFONO_CONT'];
              $contacto->EMAIL_CONTA = $value['EMAIL_CONT'];
              $contacto->save();
            }
          }
          foreach ($SUBCATE as $key => $val) {
            $empresa->DetalleSubcategoria()->attach($val['ID_SUBC']);
          }
          return response()->json(['code' => 200, 'message' => 'Subcontratista editado correctamente'],200);
        }
      }else{
        if($ID_EMPR == 0){
          if(Empresa::where('RUT_EMPR',$RUT_EMPR)->where('TIPO_EMPR','P')->where('CONSTRUCTORA_ID_CONS',$ID_CONS)->first()) {
            return response()->json(['message'=>'Rut ya registrado','code'=>201]);
          }
          $empresa = new Empresa();
          $empresa->RAZON_SOCIAL_EMPR = $RAZON_SOCIAL_EMPR;
          $empresa->NOMBRE_EMPR = $NOMBRE_EMPR;
          $empresa->RUT_EMPR = $RUT_EMPR;
          $empresa->DIRECCION = $DIRECCION;
          $empresa->EMAIL_EMPR = $EMAIL_EMPR;
          $empresa->TELEFONO_EMPR = $TELEFONO_EMPR;
          $empresa->SITIO_WEB_EMP = $SITIO_WEB_EMP;
          $empresa->CONSTRUCTORA_ID_CONS = $ID_CONS;
          $empresa->TIPO_CUENTA_BANCO_ID_TIPO_CUENTA = $ID_TIPO_CUEN;
          $empresa->BANCO_ID_BANCO = $ID_BANC;
          $empresa->NUMERO_CUENTA_EMPR = $NUMERO_CUENTA_EMPR;
          $empresa->TIPO_EMPR = $TIPO_EMPR;
          $empresa->save();

          if (count($CONTACTOS) <> 0) {
            foreach ($CONTACTOS as $key => $value) {
              $contacto = new Contacto();
              $contacto->EMPRESA_ID_EMPR = $empresa->ID_EMPR;
              $contacto->NOMBRE_CONTA = $value['NOMBRE_CONT'];
              $contacto->TELEFONO_CONTA = $value['TELEFONO_CONT'];
              $contacto->EMAIL_CONTA = $value['EMAIL_CONT'];
              $contacto->save();
            }
          }
          return response()->json(['code' => 200, 'message' => 'Proveedor registrado correctamente'],200);
        }
        else{
          if(Empresa::where('RUT_EMPR',$RUT_EMPR)->where('TIPO_EMPR','P')->where('CONSTRUCTORA_ID_CONS',$ID_CONS)->where('ID_EMPR','!=',$ID_EMPR)->first()) {
            return response()->json(['message'=>'Rut ya registrado','code'=>201]);
          }
          $empresa = Empresa::find($ID_EMPR);
          $empresa->RAZON_SOCIAL_EMPR = $RAZON_SOCIAL_EMPR;
          $empresa->NOMBRE_EMPR = $NOMBRE_EMPR;
          $empresa->RUT_EMPR = $RUT_EMPR;
          $empresa->DIRECCION = $DIRECCION;
          $empresa->EMAIL_EMPR = $EMAIL_EMPR;
          $empresa->TELEFONO_EMPR = $TELEFONO_EMPR;
          $empresa->SITIO_WEB_EMP = $SITIO_WEB_EMP;
          $empresa->CONSTRUCTORA_ID_CONS = $ID_CONS;
          $empresa->TIPO_CUENTA_BANCO_ID_TIPO_CUENTA = $ID_TIPO_CUEN;
          $empresa->BANCO_ID_BANCO = $ID_BANC;
          $empresa->NUMERO_CUENTA_EMPR = $NUMERO_CUENTA_EMPR;
          $empresa->TIPO_EMPR = $TIPO_EMPR;
          $empresa->save();

          Contacto::where('EMPRESA_ID_EMPR','=',$ID_EMPR)->delete();
          if (count($CONTACTOS) <> 0) {
            foreach ($CONTACTOS as $key => $value) {
              $contacto = new Contacto();
              $contacto->EMPRESA_ID_EMPR = $ID_EMPR;
              $contacto->NOMBRE_CONTA = $value['NOMBRE_CONT'];
              $contacto->TELEFONO_CONTA = $value['TELEFONO_CONT'];
              $contacto->EMAIL_CONTA = $value['EMAIL_CONT'];
              $contacto->save();
            }
          }
          return response()->json(['code' => 200, 'message' => 'Proveedor editado correctamente'],200);
        }
      }

    }

    // cargar proveedor con paginador
    public function cargarProveedores($ID_CONS,Request $request){
      $por_pagina = $request->input('POR_PAGINA');
      $empresa = Empresa::where('TIPO_EMPR','P')
                          ->where('CONSTRUCTORA_ID_CONS',$ID_CONS)
                          ->with(['contactos'])->paginate($por_pagina);
      return $empresa;
    }

    // cargar proveedor sin paginador
    public function getProveedores($ID_CONS){
      $empresa = Empresa::where('TIPO_EMPR','P')
                          ->where('CONSTRUCTORA_ID_CONS',$ID_CONS)
                          ->with(['contactos'])->get();
      return $empresa;
    }

    // filtrar proveedor(razon)
    public function filtrarProveedor(Request $request,$ID_CONS){
      $palabra = $request->input('palabra');
      $por_pagina = $request->input('POR_PAGINA');
      $empresa = Empresa::where('TIPO_EMPR','P')
                          ->where('CONSTRUCTORA_ID_CONS',$ID_CONS)
                          ->with(['contactos'])
                          ->where(function ($query) use ($palabra) {
                            if($palabra)$query->whereRaw("concat(RAZON_SOCIAL_EMPR,' ',NOMBRE_EMPR) like '%$palabra%'");
                          })
                          ->paginate($por_pagina);
      return $empresa;
    }

    // eliminar proveedor
    public function eliminarProveedor($ID_EMP){

      $empresa = Empresa::where('ID_EMPR',$ID_EMP)->where('TIPO_EMPR','P')->first();
      if($empresa){
        if($empresa->cotizaciones()->first()){
          return response()->json(['code' => 200, 'data' => false, 'message' => 'No se puede eliminar un proveedor con cotizaciones asociadas'],200);
        }
        else if($empresa->pagos()->first()){
          return response()->json(['code' => 200, 'data' => false, 'message' => 'No se puede eliminar un proveedor con pagos asociados'],200);
        }
        else if($empresa){
            Contacto::where('EMPRESA_ID_EMPR','=',$ID_EMP)->delete();
            $empresa->delete();
            return response()->json(['code' => 200, 'data' => true, 'message' => 'Proveedor eliminado correctamente'],200);
          }

      }
      return response()->json(['code' => 200, 'data' => false, 'message' => 'No se encontro el proveedor'],200);
    }

    //obtener los datos de un proveedor
    public function getProveedor($ID_EMPR){
      $empresa = Empresa::where('TIPO_EMPR','P')
                          ->where('ID_EMPR',$ID_EMPR)
                          ->with(['contactos'])->first();


      return response()->json(['code' => 200, 'message' => 'Proveedor encontrado correctamente','data'=>$empresa],200);
    }

    public function subcontratistaArriendo(Request $request){
      $palabra = $request->input('PALABRA');
      $por_pagina = $request->input('POR_PAGINA');
      $lst_arriendo = null;
      if($request->input('ARRIENDOS'))$lst_arriendo = $request->input('ARRIENDOS');
      $id_cons = $request->input('ID_CONS');

      $lista = [];
      if($lst_arriendo <> null){
        foreach ($lst_arriendo as $key => $val) {
          $lista[] = $val['ID_SUBC'];
        }
      }

      $subcon = Empresa::where('TIPO_EMPR','S')
                ->join('DETALLE_SCON_SUBC','DETALLE_SCON_SUBC.EMPRESA_ID_EMPR','=','ID_EMPR')
                ->join('SUBCATEGORIA','SUBCATEGORIA.ID_SUBC','=','DETALLE_SCON_SUBC.SUBCATEGORIA_ID_SUBC')
                ->join('CATEGORIA','CATEGORIA.ID_CATE','=','SUBCATEGORIA.CATEGORIA_ID_CATE')
                ->where('CATEGORIA.TIPO_CATE','A')
                ->where(function ($query) use ($palabra) {
                  if($palabra)$query->whereRaw("concat(RAZON_SOCIAL_EMPR,' ',NOMBRE_EMPR) like '%$palabra%'");
                })->where(function ($query) use ($lista) {
                  if(count($lista) <> 0)$query->whereIn('SUBCATEGORIA.ID_SUBC',$lista);
                })
                ->where('EMPRESA.CONSTRUCTORA_ID_CONS',$id_cons)
                ->select('EMPRESA.*')
                ->groupBy('EMPRESA.ID_EMPR')
                ->paginate($por_pagina);

      foreach ($subcon as $key => $value) {
        $value->contactos;
        $value->DetalleSubcategoria;
      }

      return $subcon;
    }
}
