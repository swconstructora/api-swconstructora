<?php

namespace App\Http\Controllers;

use App\Cargo;
use App\Modulo;
use App\OrdenCompra;
use Illuminate\Http\Request;

class CargoController extends Controller
{

    public function listarCargos($ID_CONS,$ID_CARG) {
      $cargos = Cargo::where('CONSTRUCTORA_ID_CONS',$ID_CONS)->where('ID_CARG','!=',$ID_CARG)
                     ->select('NOMBRE_CARG','ID_CARG')->orderBy('NIVEL_CARG','ASC')->orderBy('NOMBRE_CARG','ASC')->get();
      return $cargos;
    }

    public function crearCargo(Request $request, $ID_CONS) {
      $cargo = new Cargo();
      $cargo->NOMBRE_CARG = $request->input('NOMBRE_CARG');
      $cargo->NIVEL_CARG = $request->input('NIVEL_CARG');
      $cargo->CARGO_AUTORIZA_ID_CARG = $request->input('ID_CARG_AUTORIZA');
      $cargo->CONSTRUCTORA_ID_CONS = $ID_CONS;
      $cargo->save();
      // $cargo->modulos()->attach(1,
      //   [
      //     'PUEDE_EDITAR'=>false,
      //     'PUEDE_VER'=>true
      //   ]);
      return $cargo;
    }

    public function asignarAprobacion(Request $request,$ID_CARG) {
      $id_aut = $request->input('ID_CARG_AUTORIZA');
      $cargo = Cargo::find($ID_CARG);
      if(!$cargo->ES_DUENO) {
        $circulo = $this->revisarJefes($ID_CARG,$id_aut);
        if($circulo) {
          return response()->json(['msg'=>'se produjo circularidad','data'=>false]);
        }
      }
      $idsEmpl = $cargo->empleados()->pluck('USUARIO_ID_USU');
      $ordenes = OrdenCompra::join('FLUJO_ORDEN','ORDEN_COMPRA_ID_ORDENC','ID_ORDENC')
        ->where('ESTADO_ORDEN','E')->whereIn('USUARIO_ID_USU',$idsEmpl)
        ->where('CARGO_APRUEBA',$cargo->CARGO_AUTORIZA_ID_CARG)
        ->update(['CARGO_APRUEBA'=>$request->input('ID_CARG_AUTORIZA')]);
      $cargo->NOMBRE_CARG = $request->input('NOMBRE_CARG');
      $cargo->NIVEL_CARG = $request->input('NIVEL_CARG');
      $cargo->CARGO_AUTORIZA_ID_CARG = $request->input('ID_CARG_AUTORIZA');
      $cargo->save();
      $cargo->jefe;
      return response()->json(['msg'=>'Cargo actualizado','data'=>$cargo]);
    }

    public function getCargos($ID_CONS,$ID_CARG) {
      $cargos = Cargo::where('CONSTRUCTORA_ID_CONS',$ID_CONS)
                     ->where(function ($query) use ($ID_CARG) {
                       if(!Cargo::find($ID_CARG)->ES_DUENO) {
                         $query->where('ID_CARG','!=',$ID_CARG);
                       }
                     })->withCount('modulos')
                     ->with('jefe')
                     ->orderBy('NOMBRE_CARG','ASC')
                     // ->orderBy('ID_CARG','ASC')
                     ->get();
      return $cargos;
    }

    public function verCargo($ID_CARG) {
      $cargo = Cargo::find($ID_CARG);
      $cargo->jefe;
      // if($cargo->ES_DUENO) {
      //   $cargo->jefes = [];
      //   return $cargo;
      // }
      // $cargo->jefes = $cargo->jefes()->select('NOMBRE_CARG','ID_CARG')->orderBy('NOMBRE_CARG','ASC')->get();
      return $cargo;
    }

    public function listaModulos($ID_CARG) {
      $modulosC = [];
      $modulosR = [];
      if(Cargo::find($ID_CARG)){
        $modulosC = Cargo::find($ID_CARG)->modulosC()->get();
      }
      if(Cargo::find($ID_CARG)){
        $modulosR = Cargo::find($ID_CARG)->modulosR()->get();
      }
      $mod = Modulo::whereNull('MODULO_ID_MOD_PADRE')->get();
      // $modC = Modulo::->where('TIPO_MOD','C')->get();
      // $modP = Modulo::->where('TIPO_MOD','C')->get();
      foreach ($mod as $key => &$modulo) {
        $modulo->chEd = false;
        $modulo->chVe = false;
        // $modulo->hijosR;
        // if(count($modulosC) > 0 && count($modulosR) > 0) {
        if($modulo->ID_MOD == 1) {
          $modulo->chVe = true;
        }
          foreach ($modulosC as $key => &$value) {
            if($value->ID_MOD == $modulo->ID_MOD){
              $modulo->chEd = $value->pivot->PUEDE_EDITAR;
              $modulo->chVe = $value->pivot->PUEDE_VER;
              break;
            }
          }
          foreach ($modulosR as $key => &$value) {
            if($value->ID_MOD == $modulo->ID_MOD){
              $modulo->chEd = $value->pivot->PUEDE_EDITAR;
              $modulo->chVe = $value->pivot->PUEDE_VER;
              break;
            }
          }
        // }
        foreach ($modulo->hijosR as $key => &$h) {
          $h->chEd = false;
          $h->chVe = false;
          if(count($modulosR) > 0) {
            foreach ($modulosR as $key => &$value) {
              foreach ($value->hijosC()->wherePivot('CARGO_ID_CARG',$ID_CARG)->get() as $key => &$hr) {
                // return $hr;
                if($h->ID_MOD == $hr->ID_MOD){
                  $h->chEd = $hr->pivot->PUEDE_EDITAR;
                  $h->chVe = $hr->pivot->PUEDE_VER;
                  break;
                }
              }
            }
          }
        }
      }
      $datos = $mod->groupBy('TIPO_MOD')->toArray();
      return $datos;
    }

    public function asignarModulos(Request $request,$ID_CARG) {
      $cargo = Cargo::find($ID_CARG);
      $idModsC = $request->input('idModsC');
      $idModsP = $request->input('idModsP');
      $cargo->modulos()->sync([]);
      foreach ($idModsC as $key => $mod) {
        $cargo->modulos()->attach($mod['id'],
          [
            'PUEDE_EDITAR'=>$mod['edita'],
            'PUEDE_VER'=>$mod['ve']
          ]);
      }
      foreach ($idModsP as $key => $mod) {
        $cargo->modulos()->attach($mod['id'],
          [
            'PUEDE_EDITAR'=>$mod['edita'],
            'PUEDE_VER'=>$mod['ve']
          ]);
          $cargo->modulos()->where('ID_MOD',$mod['id'])->first()->hijosC()->wherePivot('CARGO_ID_CARG',$ID_CARG)->sync([]);
          foreach ($mod['hijos'] as $key => $value) {
            $cargo->modulos()->where('ID_MOD',$mod['id'])->first()->hijosC()->wherePivot('CARGO_ID_CARG',$ID_CARG)->attach($value['id'],
              [
                'PUEDE_EDITAR'=>$value['edita'],
                'PUEDE_VER'=>$value['ve'],
                'CARGO_ID_CARG'=>$ID_CARG
              ]);
          }
      }
      $cargo->modulosC;
      foreach ($cargo->modulosP as $key => &$mod) {
        $mod->hijos = $mod->hijosC()->wherePivot('CARGO_ID_CARG',$ID_CARG)->get();
      }
      return response()->json(['data'=>$cargo]);
    }

    public function listarCargosEmpleado($ID_CONS,$ID_CARG) {
      // $cargo = Cargo::find($ID_CARG);
      $idsSub = $this->revisarSubordinados($ID_CARG);
      // return $idsSub;
      // $idsSub[] = ;
      $cargos = Cargo::whereIn('ID_CARG',$idsSub)->where('CONSTRUCTORA_ID_CONS',$ID_CONS)
        ->orderBy('ID_CARG')->get();
      return $cargos;
    }

    public function revisarSubordinados($ID_CARG) {
      $idsCargos = [];
      $cargo = Cargo::find($ID_CARG);
      foreach ($cargo->subordinados()->get() as $key => $carg) {
        $idsCargos[] = $carg->ID_CARG;
        if($carg->subordinados()->first()) {
          $idsCargos = array_merge($idsCargos,$this->revisarSubordinados($carg->ID_CARG));
        }
      }
      return $idsCargos;
    }

    public function eliminarCargo($ID_CARG) {
      $cargo = Cargo::find($ID_CARG);
      if(count($cargo->empleados) > 0) {
        return response()->json(['msg'=>'Hay empleados asociados','seelimino'=>false],200);
      }
      foreach ($cargo->modulos()->get() as $key => $value) {
        $value->hijosC()->wherePivot('CARGO_ID_CARG',$ID_CARG)->sync([]);
      }
      $cargo->modulos()->sync([]);
      if($cargo->delete()) {
        return response()->json(['msg'=>'Se elimino el cargo','seelimino'=>true],200);
      }
      return response()->json(['msg'=>'Ocurrio un error','seelimino'=>false],200);
    }

    private function revisarJefes($ID_CARGO,$ID_CARGO_REV) {
      $carg = Cargo::find($ID_CARGO_REV);
      if($carg->ES_DUENO) { return false; }
      // else if($carg->CARGO_AUTORIZA_ID_CARG == null) { return false; }
      else if($carg->CARGO_AUTORIZA_ID_CARG === +$ID_CARGO) { return true; }
      else { return $this->revisarJefes($ID_CARGO,$carg->CARGO_AUTORIZA_ID_CARG); }
    }
}
