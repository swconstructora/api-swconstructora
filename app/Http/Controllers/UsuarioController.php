<?php

namespace App\Http\Controllers;

use App\Usuario;
use App\Cargo;
use App\Modulo;
use App\Empleado;
use App\Dueno;
use App\Constructora;
use App\Proyecto;
use App\FacturacionCons;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Response;
use JWTAuth;
use JWTFactory;
use Mail;
use Tymon\JWTAuth\Exceptions\JWTException;

class UsuarioController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $usuario = Usuario::all();
        return $usuario;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){}

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $ID_USU){

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        //
    }

    // public function obtenerDatos(){
    //   return "hola mundo";
    // }

    public function login(Request $request){
        if(!$request->input('EMAIL_USU') || !$request->input('CONTRASENA_USU')){
          return response()->json(['code' => 422, 'message' => 'No se pudieron procesar los valores'],422);
        }

        $usuario = Usuario::where('EMAIL_USU','=', $request->input('EMAIL_USU'))
        ->where('CONTRASENA_USU','=',md5($request->input('CONTRASENA_USU')))->first();

        if(!$usuario){
          return response()->json(null);
        }
        $token = JWTAuth::fromUser($usuario);
        return response()->json(compact('token'));

    }

    public function obtenerAutorizacion(){

        try {

            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());

        }

        // the token is valid and we have found the user via the sub claim
        // return response()->json(compact('user'));
         $user =  JWTAuth::parseToken()->authenticate();
         if($user->TIPO_USU == 'D'){
            $user->dueno->constructoras;
            $user->primeros_pasos = true;
            foreach ($user->dueno->constructoras as $key => $cons) {
                $proy = Proyecto::where("CONSTRUCTORA_ID_CONS",$cons->ID_CONS)->first();
                if($proy){
                  $user->primeros_pasos = false;
                }
            }
            // validar si no hay proyectos
            $user->dueno->cargo->modulosC;
            $user->dueno->cargo->jefe;

            $mods = $user->dueno->cargo->modulosP()->get();
            foreach ($user->dueno->cargo->modulosP as $key => $mod) {
              $mod->hijos = $mod->hijosC()->wherePivot('CARGO_ID_CARG',$user->dueno->CARGO_ID_CARG)->get();
            }
         }
         elseif($user->TIPO_USU == 'E'){
           $user->primeros_pasos = false;
           $user->empleado->DetalleProyecto;
           $user->empleado->cargo->modulosC;
           $user->empleado->cargo->jefe;
           foreach ($user->empleado->cargo->modulosP as $key => &$mod) {
             $mod->hijos = $mod->hijosC()->wherePivot('CARGO_ID_CARG',$user->empleado->CARGO_ID_CARG)->get();
           }
         }
        // elseif($user->TIPO_USU == 'S'){
        //     $user->Socio;
        // }
           return response()->json(["usuario"=>$user]);
    }

    public function cambiarPass(Request $request) {
      if(!$request){ return response()->json(['message'=>'Error envio de datos'],200); }
      if($request->input('CONTRASENA_ACTUAL')){
        if(!Usuario::where('ID_USU',$request->input('ID_USU'))->where('CONTRASENA_USU',md5($request->input('CONTRASENA_ACTUAL')))->first()){
          return response()->json(['message'=>'Contraseña incorrecta','data'=>false],200);
        }
      }
      Usuario::where('ID_USU',$request->input('ID_USU'))
        ->update(['CONTRASENA_USU'=>md5($request->input('CONTRASENA_USU'))]);

      return response()->json(['message'=>'Contraseña cambiada','data'=>true],200);
    }

    public function agregarEmpleado(Request $request){
      $EMAIL_USU = $request->input('EMAIL_USU');
      $ASIGNADOR = $request->input('ASIGNADOR');
      $ID_CONS = $request->input('ID_CONS');
      $NOMBRE_EMP = $request->input('NOMBRE_EMP');
      $APELLIDO_EMP = $request->input('APELLIDO_EMP');

      $empleado = new Empleado();
      if($request->input('ID_EMP')) {
        $empleado = Empleado::find($request->input('ID_EMP'));
        $usuario = Usuario::where('ID_USU',$empleado->USUARIO_ID_USU)->first();
        $usuario->EMAIL_USU = $EMAIL_USU;
        $usuario->save();
      }
      else {
        $usuarioRegistrado = Usuario::where('EMAIL_USU',$EMAIL_USU)->first();
        if($usuarioRegistrado == null){
          $asig = Usuario::join('EMPLEADO','EMPLEADO.USUARIO_ID_USU','ID_USU')
                             ->where('ID_USU',$ASIGNADOR)->first();
          if($asig <> null){
            $NOMBRE_ASIGNADOR =  $asig->NOMBRE_EMP.' '.$asig->APELLIDO_EMP;
          }else{
            $asig = Usuario::join('DUENO','DUENO.USUARIO_ID_USU','ID_USU')
                               ->where('ID_USU',$ASIGNADOR)->first();
           $NOMBRE_ASIGNADOR =  $asig->NOMBRE_DUE.' '.$asig->APELLIDO_DUE;

          }
          date_default_timezone_set('America/Santiago');
          $now = new \DateTime();
          $usuario = new Usuario();
          $usuario->EMAIL_USU = $EMAIL_USU;
          $usuario->CONTRASENA_USU = md5('0000');
          $usuario->FECHA_REG_USU = $now;
          $usuario->TIPO_USU = 'E';
          $usuario->save();
          $empleado->USUARIO_ID_USU = $usuario->ID_USU;
          $empleado->CONSTRUCTORA_ID_CONS = $ID_CONS;
        }
        else {
          return response()->json(['code' => 200, 'message' => 'Email ya está registrado','estado'=>'false'],200);
        }
      }

      $empleado->NOMBRE_EMP = $NOMBRE_EMP;
      $empleado->APELLIDO_EMP = $APELLIDO_EMP;
      $empleado->CARGO_ID_CARG = $request->input('ID_CARGO');
      $empleado->save();


      if(!$request->input('ID_EMP')) {
        $esReenviar='G';
      // Armar token con link
        date_default_timezone_set('America/Santiago');
        $now = new \DateTime();
        $now->add(new \DateInterval('P30D'));
        $claims = array('usuario'=>$usuario,'exp'=>date_timestamp_get($now),'sub'=>$usuario->ID_USU);
        $user = JWTFactory::make($claims);
        $token = JWTAuth::encode($user);
        $link = app('App\Entorno')->route_web.'app/cuenta/cambiar-contrasena?token='.$token;
        $data = array('nombre'=>$empleado->NOMBRE_EMP,'link'=>$link,'asignador'=>$NOMBRE_ASIGNADOR,'esReenviarC'=>$esReenviar);

        $email = $usuario->EMAIL_USU;
        $nombre = $empleado->NOMBRE_EMP;

        Mail::send('crear-contrasena', $data, function($message) use ($email, $nombre) {
           $message->to($email, $nombre)->subject('Crear contraseña');
           $message->from('no-reply@budgeter.cl','Equipo Budgeter');
        });
      }
      return response()->json(['code' => 200, 'message' => 'Empleado registrado correctamente','estado'=>'true'],200);
    }

    public function cargarGerentes($ID_CONS){
      $gerente = Empleado::where('EMPLEADO.CONSTRUCTORA_ID_CONS','=',$ID_CONS)
                          ->join('CARGO','CARGO.ID_CARG','CARGO_ID_CARG')
                          ->join('USUARIO','USUARIO.ID_USU','USUARIO_ID_USU')
                          // ->where('CARGO.SIGLA_CARG','=','G')
                          ->selectRaw('EMPLEADO.* , USUARIO.*')->with(['cargo'])->get();
      foreach ($gerente as $key => $value) {
        $estado = Empleado::join('DETALLE_EMP_PROY','DETALLE_EMP_PROY.EMPLEADO_ID_EMP','ID_EMP')
                              ->where('ID_EMP','=',$value->ID_EMP)->first();
        if($estado){
          $value->proyecto = 'true';
        }
        else{
          $value->proyecto = "false";
        }
      }

      return $gerente;
    }

    public function cargarEmpleadosAdministradores($ID_CONS){
      $finanzas = Empleado::where('EMPLEADO.CONSTRUCTORA_ID_CONS','=',$ID_CONS)
                          ->join('CARGO','CARGO.ID_CARG','CARGO_ID_CARG')
                          ->join('USUARIO','USUARIO.ID_USU','USUARIO_ID_USU')
                          ->where('CARGO.SIGLA_CARG','=','F')
                          ->selectRaw('EMPLEADO.* , USUARIO.EMAIL_USU, CARGO.*')->get();

      $obras = Empleado::where('EMPLEADO.CONSTRUCTORA_ID_CONS','=',$ID_CONS)
                          ->join('CARGO','CARGO.ID_CARG','CARGO_ID_CARG')
                          ->join('USUARIO','USUARIO.ID_USU','USUARIO_ID_USU')
                          ->where('CARGO.SIGLA_CARG','=','AO')
                          ->selectRaw('EMPLEADO.* , USUARIO.EMAIL_USU, CARGO.*')->get();

      foreach ($finanzas as $key => $value) {
        $estado = Empleado::join('DETALLE_EMP_PROY','DETALLE_EMP_PROY.EMPLEADO_ID_EMP','ID_EMP')
                              ->where('ID_EMP','=',$value->ID_EMP)->first();
        if($estado){$value->proyecto = 'true';}
        else{$value->proyecto = "false";}
      }

      foreach ($obras as $key => $value) {
        $estado = Empleado::join('DETALLE_EMP_PROY','DETALLE_EMP_PROY.EMPLEADO_ID_EMP','ID_EMP')
                              ->where('ID_EMP','=',$value->ID_EMP)->first();
        if($estado){$value->proyecto = 'true';}
        else{$value->proyecto = "false";}
      }

      return response()->json(['code' => 200, 'message' => 'Empleados encontrado correctamente','finanzas'=>$finanzas,'obras'=>$obras],200);
    }

    public function cargarEmpleadosjefes($ID_CONS){
      $bodeguero = Empleado::where('EMPLEADO.CONSTRUCTORA_ID_CONS','=',$ID_CONS)
                          ->join('CARGO','CARGO.ID_CARG','CARGO_ID_CARG')
                          ->join('USUARIO','USUARIO.ID_USU','USUARIO_ID_USU')
                          ->where('CARGO.SIGLA_CARG','=','B')
                          ->selectRaw('EMPLEADO.* , USUARIO.EMAIL_USU, CARGO.*')->get();

      $compras = Empleado::where('EMPLEADO.CONSTRUCTORA_ID_CONS','=',$ID_CONS)
                          ->join('CARGO','CARGO.ID_CARG','CARGO_ID_CARG')
                          ->join('USUARIO','USUARIO.ID_USU','USUARIO_ID_USU')
                          ->where('CARGO.SIGLA_CARG','=','JC')
                          ->selectRaw('EMPLEADO.* , USUARIO.EMAIL_USU, CARGO.*')->get();

      $terreno = Empleado::where('EMPLEADO.CONSTRUCTORA_ID_CONS','=',$ID_CONS)
                          ->join('CARGO','CARGO.ID_CARG','CARGO_ID_CARG')
                          ->join('USUARIO','USUARIO.ID_USU','USUARIO_ID_USU')
                          ->where('CARGO.SIGLA_CARG','=','JT')
                          ->selectRaw('EMPLEADO.* , USUARIO.EMAIL_USU, CARGO.*')->get();

      foreach ($bodeguero as $key => $value) {
        $estado = Empleado::join('DETALLE_EMP_PROY','DETALLE_EMP_PROY.EMPLEADO_ID_EMP','ID_EMP')
                              ->where('ID_EMP','=',$value->ID_EMP)->first();
        if($estado){$value->proyecto = 'true';}
        else{$value->proyecto = "false";}
      }
      foreach ($compras as $key => $value) {
        $estado = Empleado::join('DETALLE_EMP_PROY','DETALLE_EMP_PROY.EMPLEADO_ID_EMP','ID_EMP')
                              ->where('ID_EMP','=',$value->ID_EMP)->first();
        if($estado){$value->proyecto = 'true';}
        else{$value->proyecto = "false";}
      }
      foreach ($terreno as $key => $value) {
        $estado = Empleado::join('DETALLE_EMP_PROY','DETALLE_EMP_PROY.EMPLEADO_ID_EMP','ID_EMP')
                              ->where('ID_EMP','=',$value->ID_EMP)->first();
        if($estado){$value->proyecto = 'true';}
        else{$value->proyecto = "false";}
      }

      return response()->json(['code' => 200, 'message' => 'Empleados encontrado correctamente','bodeguero'=>$bodeguero,'compras'=>$compras,'terreno'=>$terreno],200);
    }
    // eliminar usuario-empleado
    public function eliminarEmpleado($ID_EMP){
        $empleado = Empleado::find($ID_EMP);
        $usuario = Usuario::find($empleado->USUARIO_ID_USU);
        $empleado->delete();
        $usuario->delete();
        return response()->json(['code' => 200, 'message' => 'Empleado eliminado correctamente'],200);
    }
    // reenviar contraseña a un empleado
    public function reenviarContrasena(Request $request){
        $ASIGNADOR = $request->input('ASIGNADOR');
        $asig = Usuario::join('EMPLEADO','EMPLEADO.USUARIO_ID_USU','ID_USU')
                           ->where('ID_USU',$ASIGNADOR)->first();
             if($asig <> null){
               $NOMBRE_ASIGNADOR =  $asig->NOMBRE_EMP.' '.$asig->APELLIDO_EMP;
             }else{
               $asig = Usuario::join('DUENO','DUENO.USUARIO_ID_USU','ID_USU')
                                  ->where('ID_USU',$ASIGNADOR)->first();
              $NOMBRE_ASIGNADOR =  $asig->NOMBRE_DUE.' '.$asig->APELLIDO_DUE;
             }
        $USUARIO = $request->input('USUARIO');
        $usua = Usuario::join('EMPLEADO','EMPLEADO.USUARIO_ID_USU','ID_USU')
                           ->where('ID_USU',$USUARIO)->first();
        $NOMBRE_USUARIO =  $usua->NOMBRE_EMP.' '.$usua->APELLIDO_EMP;
        $email = $usua->EMAIL_USU;
        $esReenviar =  'R';

        // Armar token con link
          date_default_timezone_set('America/Santiago');
          $now = new \DateTime();
          $now->add(new \DateInterval('P30D'));

          $claims = array('usuario'=>$usua,'exp'=>date_timestamp_get($now),'sub'=>$USUARIO);
          $user = JWTFactory::make($claims);
          $token = JWTAuth::encode($user);
          $link = app('App\Entorno')->route_web.'app/cuenta/cambiar-contrasena?token='.$token;
          $data = array('nombre'=>$NOMBRE_USUARIO,'link'=>$link,'asignador'=>$NOMBRE_ASIGNADOR,'esReenviarC'=>$esReenviar);

        Mail::send('crear-contrasena', $data, function($message) use ($email, $NOMBRE_USUARIO) {
           $message->to($email, $NOMBRE_USUARIO)->subject('Cambiar contraseña');
           $message->from('no-reply@budgeter.cl','Equipo Budgeter');
        });

        return response()->json(['code' => 200, 'message' => 'enviado'],200);

    }

    public function olvidoContrasena(Request $request){
      $EMAIL_USU = $request->input('CORREO');

      $usuario = Usuario::join('EMPLEADO','EMPLEADO.USUARIO_ID_USU','ID_USU')
                         ->where('EMAIL_USU',$EMAIL_USU)->first();

     if($usuario <> null){
       $NOMBRE_USUARIO =  $usuario->NOMBRE_EMP.' '.$usuario->APELLIDO_EMP;
     }else{
       $usuario = Usuario::join('DUENO','DUENO.USUARIO_ID_USU','ID_USU')
                          ->where('EMAIL_USU',$EMAIL_USU)->first();

        if($usuario <> null){
          $NOMBRE_USUARIO =  $usuario->NOMBRE_DUE.' '.$usuario->APELLIDO_DUE;
        }

     }

      $estado = false;
      if($usuario<>null){
        $estado = true;
        $esReenviar='O';
        // Armar token con link
          date_default_timezone_set('America/Santiago');
          $now = new \DateTime();
          $now->add(new \DateInterval('P30D'));

          $claims = array('usuario'=>$usuario,'exp'=>date_timestamp_get($now),'sub'=>$usuario->ID_USU);
          $user = JWTFactory::make($claims);
          $token = JWTAuth::encode($user);
          $link = app('App\Entorno')->route_web.'app/cuenta/cambiar-contrasena?token='.$token;
          $data = array('nombre'=>$NOMBRE_USUARIO,'link'=>$link,'esReenviarC'=>$esReenviar);

        $email = $usuario->EMAIL_USU;

        Mail::send('crear-contrasena', $data, function($message) use ($email, $NOMBRE_USUARIO) {
           $message->to($email, $NOMBRE_USUARIO)->subject('Contraseña olvidada');
           $message->from('no-reply@budgeter.cl','Equipo Budgeter');
        });
        return response()->json(['code' => 200, 'message' => 'Se envio correo','estado'=>$estado],200);
      }else{
        return response()->json(['code' => 200, 'message' => 'No existe correo','estado'=>$estado],200);
      }
    }


    // Para crear ruta cons amigable
    function normalizaUTF8($cadena){
        $originales = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
        $modificadas = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr';
        $cadena = utf8_decode($cadena);
        $cadena = strtr($cadena, utf8_decode($originales), $modificadas);
        $cadena = strtolower($cadena);
        return utf8_encode($cadena);
    }

    public function crearUsuarioPotencial(Request $request) {
      date_default_timezone_set('America/Santiago');
      $now = new \DateTime();

      $vaFact = $request->input('vaFact');
      $datosUser = (object)$request->input('datosUser');
      $user = new Usuario();
      if($datosUser->EMAIL_POTN){
          if(!Usuario::where("EMAIL_USU",'=',$datosUser->EMAIL_POTN)->where('ES_DEMO_USU','N')->first()){
            if(Usuario::where("EMAIL_USU",'=',$datosUser->EMAIL_POTN)->where('ES_DEMO_USU','S')->first()) {
              try {
                $usuario = Usuario::where("EMAIL_USU",'=',$datosUser->EMAIL_POTN)->where('ES_DEMO_USU','S')->first();
                // Eliminar dueno
                $dueno = Dueno::where('USUARIO_ID_USU', $usuario->ID_USU)->first();
                $constructoras = Constructora::where('DUENO_ID_DUE', $dueno->ID_DUE)->get();
                  foreach ($constructoras as $key => $cons) {

                    $proyectos = Proyecto::where('CONSTRUCTORA_ID_CONS', $cons->ID_CONS)->get();
                    foreach ($proyectos as $key => $proy) {
                      $activo_proyecto = ActivoProyecto::where('PROYECTO_ID_PROY', $proy->ID_PROY)->delete();
                    }
                    $facturacion_cons = FacturacionCons::where('CONSTRUCTORA_ID_CONS', $cons->ID_CONS)->delete();
                    $cargos = Cargo::where('CONSTRUCTORA_ID_CONS', $cons->ID_CONS)->delete();
                    $proyectos = Proyecto::where('CONSTRUCTORA_ID_CONS', $cons->ID_CONS)->delete();
                }

                $constructoras = Constructora::where('DUENO_ID_DUE', $dueno->ID_DUE)->delete();
                $dueno = Dueno::where('USUARIO_ID_USU', $usuario->ID_USU)->delete();
              } catch (\Exception $e) {

              }
            }
            $user->EMAIL_USU = $datosUser->EMAIL_POTN;
          }
          else{
            return response()->json(['code' => 409, 'message' => 'Email ya inscrito'],200);
          }
      }
      $user->CONTRASENA_USU = md5($datosUser->PASS_USER);
      $user->TIPO_USU = 'D';
      $user->FECHA_REG_USU = $now;
      $user->save();
      $dueno = new Dueno();
      $dueno->USUARIO_ID_USU = $user->ID_USU;
      $dueno->NOMBRE_DUE = $datosUser->NOMBRE_POTN;
      $dueno->APELLIDO_DUE = $datosUser->APELLIDO_POTN;
      $dueno->TELEFONO_DUE = $datosUser->TELEFONO_POTN;
      $dueno->save();
        $constru = new Constructora();
        $constru->NOMBRE_CONS = $datosUser->NOMBRE_CONS;
        $constru->DUENO_ID_DUE = $dueno->ID_DUE;
        $constru->TIPO_CONS = $datosUser->TIPO_CONS['value'];
        $constru->NOMBRE_TIPO_CONS = $datosUser->NOMBRE_TIPO_CONS;
        // Crear url amigable
        $nombreNormalizado = $this->normalizaUTF8($datosUser->NOMBRE_CONS);
        $url = urlencode($nombreNormalizado);
        $urlSinMas = strtolower(str_replace('+', '-', $url));

        $constru->RUTA_CONS= $urlSinMas;
        $constru->save();
        $cargo = new Cargo();
        $cargo->NOMBRE_CARG = 'Usuario Maestro';
        $cargo->NIVEL_CARG = 1;
        $cargo->CONSTRUCTORA_ID_CONS = $constru->ID_CONS;
        $cargo->ES_DUENO = true;
        $cargo->save();
      $dueno->CARGO_ID_CARG = $cargo->ID_CARG;
      $dueno->save();
      if($vaFact) {
        $datosFact = (object)$request->input('datosFact');
        $fact = new FacturacionCons();
        $fact->RAZON_SOCIAL_FACT = $datosFact->RAZON_SOCIAL_FACT;
        $fact->RUT_FACT = $datosFact->RUT_FACT;
        $fact->TELEFONO_FACT = $datosFact->TELEFONO_FACT;
        $fact->DIRECCION_FACT = $datosFact->DIRECCION_FACT;
        $fact->EMAIL_FACT = $datosFact->EMAIL_FACT;
        $fact->COMUNA_FACT = $datosFact->COMUNA_FACT;
        $fact->GIRO_COMERCIAL_FACT = $datosFact->GIRO_COMERCIAL_FACT;
        $fact->CONSTRUCTORA_ID_CONS = $constru->ID_CONS;
        $fact->save();
      } else{

        $fact = new FacturacionCons();
        $fact->CONSTRUCTORA_ID_CONS = $constru->ID_CONS;
        $fact->FECHA_FACT_PAGO = $now;
        $fact->save();
      }
      return response()->json(['code' => 200, 'message' => 'Usuario creado con éxito'],200);
    }

    public function crearDemo(Request $request) {
      date_default_timezone_set('America/Santiago');
      $now = new \DateTime();

      $NOMBRE = $request->input('NOMBRE_DEMO');
      $EMAIL = $request->input('EMAIL_DEMO');
      if($NOMBRE != '' && $NOMBRE !=null && $EMAIL !='' && $EMAIL !=null){
        $preguntar = Usuario::where('EMAIL_USU',$EMAIL)->first();
        if($preguntar){
            return response()->json(['code' => 200, 'message' => 'Usuario demo registrado con anterioridad', 'usuario'=>$preguntar],200);
        }
        $user = new Usuario();
        $user->EMAIL_USU = $EMAIL;
        $user->CONTRASENA_USU = md5('demo');
        $user->TIPO_USU = 'D';
        $user->FECHA_REG_USU = $now;
        $user->ES_DEMO_USU = 'S';
        $user->save();

        $dueno = new Dueno();
        $dueno->USUARIO_ID_USU = $user->ID_USU;
        $dueno->NOMBRE_DUE = $NOMBRE;
        $dueno->APELLIDO_DUE = '';
        $dueno->TELEFONO_DUE = 0;
        $dueno->save();

          $constru = new Constructora();
          $constru->NOMBRE_CONS = "Demo";
          $constru->DUENO_ID_DUE = $dueno->ID_DUE;
          $constru->TIPO_CONS = "C";
          $constru->NOMBRE_TIPO_CONS = "Constructora";
          // Crear url amigable
          $nombreNormalizado = $this->normalizaUTF8($constru->NOMBRE_CONS);
          $url = urlencode($nombreNormalizado);
          $urlSinMas = strtolower(str_replace('+', '-', $url));

          $constru->RUTA_CONS= $urlSinMas;
          $constru->save();

          $cargo = new Cargo();
          $cargo->NOMBRE_CARG = 'Usuario Demo';
          $cargo->NIVEL_CARG = 1;
          $cargo->CONSTRUCTORA_ID_CONS = $constru->ID_CONS;
          $cargo->ES_DUENO = true;
          $cargo->save();

          $dueno->CARGO_ID_CARG = $cargo->ID_CARG;
          $dueno->save();

          $fact = new FacturacionCons();
          $fact->CONSTRUCTORA_ID_CONS = $constru->ID_CONS;
          $fact->FECHA_FACT_PAGO = $now;
          $fact->save();

          // Crear proyecto
          $proyecto = array('NOMBRE_PROY'=>'Edificio Alta Demo','PRESUPUESTO_TOTAL_PROY'=>1000000000,
          'MONTO_MAX_PROY'=>100000000, 'ID_CONS'=> $constru->ID_CONS, 'ID_PROY'=> 0, 'RAZON'=>'Demo Limitada',
          'RUT'=>'0000000-0','DIRECCION'=>'Avenida Demo #114, San demo');

          $reqP = new Request((array)$proyecto);
          $proy = app('App\Http\Controllers\ProyectoController')->crearProyecto($reqP)->getData()->data;


          // Falta añadir modulos al cargo

          $modC = Modulo::where('TIPO_MOD','C')->get();
          $modP = Modulo::where('TIPO_MOD','P')->get();
          $cargo->modulos()->sync([]);

          foreach ($modC as $key => $mod) {
            $cargo->modulos()->attach($mod->ID_MOD,
              [
                'PUEDE_EDITAR'=>true,
                'PUEDE_VER'=>true
              ]);
          }

          foreach ($modP as $key => $mod) {
            $cargo->modulos()->attach($mod->ID_MOD,
              [
                'PUEDE_EDITAR'=>true,
                'PUEDE_VER'=>true
              ]);
            // $carg->modulos()->where('ID_MOD',$mod->ID_MOD)->first()->hijos()->wherePivot('CARGO_ID_CARG',$id_carg)->sync([]);
            foreach ($mod->hijosR()->get() as $key => $hijo) {
              $cargo->modulos()->where('ID_MOD',$mod->ID_MOD)->first()->hijosC()->wherePivot('CARGO_ID_CARG',$cargo->ID_CARG)->attach($hijo->ID_MOD,
                [
                  'PUEDE_EDITAR'=>$hijo->EDITAR_MOD,
                  'PUEDE_VER'=>$hijo->VER_MOD,
                  'CARGO_ID_CARG'=>$cargo->ID_CARG
                ]);
            }
          }

      return response()->json(['code' => 200, 'message' => 'Usuario demo iniciado con éxito', 'usuario'=>$user],200);
      }

      return response()->json(['code' => 200, 'message' => 'Usuario demo no puede iniciar'],200);
    }


    public function solicitarAyuda(Request $request) {
      $EMAIL_USU = $request->input('EMAIL_USU');
      $ruta_app = $request->input('RUTA_APP');
      $opcion = $request->input('OPCION');
      $mensaje = $request->input('MENSAJE');
      $nombre =  "";
      $email = "";
      $cargo = "";
      $id_usu = "";
      $id_carg = "";
      $id_cons = "";
      $constructora = "";
      $usuario = Usuario::join('EMPLEADO','EMPLEADO.USUARIO_ID_USU','ID_USU')
                          ->join('CARGO','CARGO.ID_CARG','CARGO_ID_CARG')
                          ->join('CONSTRUCTORA','CONSTRUCTORA.ID_CONS','CARGO.CONSTRUCTORA_ID_CONS')
                          ->where('EMAIL_USU',$EMAIL_USU)->first();
       if($usuario <> null){
         $nombre =  $usuario->NOMBRE_EMP.' '.$usuario->APELLIDO_EMP;
         $cargo = $usuario->NOMBRE_CARG;
         $constructora = $usuario->NOMBRE_CONS;
         $id_usu = $usuario->ID_USU;
         $id_carg = $usuario->CARGO_ID_CARG;
         $id_cons = $usuario->CONSTRUCTORA_ID_CONS;
       }else{
         $usuario = Usuario::join('DUENO','DUENO.USUARIO_ID_USU','ID_USU')
                   ->join('CARGO','CARGO.ID_CARG','CARGO_ID_CARG')
                   ->join('CONSTRUCTORA','CONSTRUCTORA.ID_CONS','CARGO.CONSTRUCTORA_ID_CONS')
                   ->where('EMAIL_USU',$EMAIL_USU)->first();
         $cargo = $usuario->NOMBRE_CARG;
         $nombre =  $usuario->NOMBRE_DUE.' '.$usuario->APELLIDO_DUE;
         $constructora = $usuario->NOMBRE_CONS;
         $id_usu = $usuario->ID_USU;
         $id_carg = $usuario->CARGO_ID_CARG;
         $id_cons = $usuario->CONSTRUCTORA_ID_CONS;
       }

       $data = array('nombre'=>$nombre,'cargo'=>$cargo,'constructora'=>$constructora, 'id_usu'=>$id_usu,'id_carg'=>$id_carg,'id_cons'=>$id_cons,
       'ruta_app'=>$ruta_app,'opcion'=>$opcion, 'mensaje'=>$mensaje);

        Mail::send('solicitud-ayuda', $data, function($message) use ($EMAIL_USU, $nombre) {
           $message->to('contacto@elinous.cl','Equipo Budgeter')->subject('Solicitud de ayuda o sugerencia');
           $message->from($EMAIL_USU,$nombre);
        });

        return response()->json(['code' => 200, 'message' => 'Solicitud enviada con éxito'],200);

    }


    public function contactarVentas(Request $request) {

      $nombre = $request->input('NOMBRE');
      $email = $request->input('EMAIL');
      $telefono = $request->input('TELEFONO');
      $motivo = $request->input('MOTIVO');

      if($nombre != null && $nombre != '' && $email != null && $email != ''){

         $data = array('nombre'=>$nombre,'email'=>$email,'telefono'=>$telefono,  'motivo'=>$motivo);
          Mail::send('contactar-ventas', $data, function($message) use ($email, $nombre) {
             $message->to('administracion@elinous.cl','Alexis Reyes')->subject('Contacto por Venta');
             $message->bcc('contacto@elinous.cl', 'Equipo Budgeter');
             $message->from('no-reply@budgeter.cl',$nombre);
          });

          return response()->json(['code' => 200, 'message' => 'Formulario enviado con éxito', 'error'=>Mail:: failures()],200);

      }
      return response()->json(['code' => 200, 'message' => 'Faltan datos'],200);

    }



      public function editarCuenta(Request $request) {
        $ID_USU = $request->input('ID_USU');
        $empleado = Empleado::join('USUARIO','USUARIO.ID_USU','USUARIO_ID_USU')
                            ->where('USUARIO.ID_USU',$ID_USU)->first();
        $dueno = null;
        // Avatar
        $arch64 = $request->input('ARCHIVO');
        $arch64_nombre = $request->input('NOMBRE_ARCH');
        $arch64_tipo = $request->input('TIPO_ARCH');
        $ID_CONS = 1;
        // Cuando es empleado
         if($empleado <> null){
           $empleado->NOMBRE_EMP = $request->input('NOMBRE');
           $empleado->APELLIDO_EMP = $request->input('APELLIDO');
           $empleado->TELEFONO_EMP = $request->input('TELEFONO');
           $empleado->save();
           $ID_CONS = $empleado->CONSTRUCTORA_ID_CONS;

         }else{
           // Cuando es dueno
           $dueno = Dueno::join('USUARIO','USUARIO.ID_USU','USUARIO_ID_USU')
                     ->join('CARGO','CARGO.ID_CARG','CARGO_ID_CARG')
                     ->where('USUARIO.ID_USU',$ID_USU)->first();
           $dueno->NOMBRE_DUE = $request->input('NOMBRE');
           $dueno->APELLIDO_DUE = $request->input('APELLIDO');
           $dueno->TELEFONO_DUE = $request->input('TELEFONO');
           $dueno->save();
           $ID_CONS = $dueno->CONSTRUCTORA_ID_CONS;
         }

         $usuario = Usuario::find($ID_USU);
         // Guardar avatar
        if($arch64 != null){

            $archivo = base64_decode(preg_replace('#^data:'.$arch64_tipo.';base64,#i', '', $arch64));
            file_put_contents($arch64_nombre , $archivo);
            $file = fopen($arch64_nombre,'r+');
            Storage::disk(app('App\Entorno')->origen_storage)->put('archivos/cons/'.$ID_CONS.'/avatares/'.$ID_USU.'/'.$arch64_nombre,  $file);
            ftruncate($file,0);
            fclose($file);

            // La ruta usa el cargarImgAdi()
            $usuario->AVATAR_URL = app('App\Entorno')->route_api.'avatares/'.$ID_CONS.'/usuario/'.$ID_USU.'/'.$arch64_nombre;
            $usuario->AVATAR_NOMBRE = $arch64_nombre;
          }
          $email = $request->input('EMAIL');
          if($email!=null){   $usuario->EMAIL_USU = $email;  }

          $usuario->save();

          return response()->json(['code' => 200, 'message' => 'Cambios realizados a usuario', 'usuario'=>$usuario,
           'empleado'=>$empleado,'dueno'=>$dueno],200);
      }

      public function cargarAvatar($ID_CONS,$ID_USU,$NOMBRE_ARCH){
          $NOMBRE= trim($NOMBRE_ARCH);
          // $file =storage_path().'/app/archivos/cons/'.$ID_CONS.'/avatares/'.$ID_USU.'/'.$NOMBRE;
          $file = Storage::disk(app('App\Entorno')->origen_storage)->get('archivos/cons/'.$ID_CONS.'/avatares/'.$ID_USU.'/'.$NOMBRE);
          return (new Response($file, 200))
          ->header('Content-Type', 'image/jpg');
          // return response()->download($file,$NOMBRE);
      }

      public function editarOrganizacion(Request $request) {
        $ID_CONS = $request->input('ID_CONS');
        $constructora = Constructora::find($ID_CONS);
        $constructora->NOMBRE_CONS = $request->input('NOMBRE_CONS');
        $constructora->TIPO_CONS = $request->input('TIPO_CONS');
        $constructora->NOMBRE_TIPO_CONS = $request->input('NOMBRE_TIPO_CONS');
        $constructora->HEXA_FONDO_CONS = $request->input('HEXA_FONDO_CONS');
        $constructora->save();

        return response()->json(['code' => 200, 'message' => 'Constructora actualizada con éxito', 'cons'=>$constructora],200);

      }

      public function obtenerOrganizacion($RUTA_CONS) {
        $constructora = Constructora::where('RUTA_CONS','=',$RUTA_CONS)->first();
        if($constructora){
            return response()->json(['code' => 200, 'message' => 'Constructora encontrada', 'cons'=>$constructora],200);
        }
          return response()->json(['code' => 200, 'message' => 'Constructora no encontrada', 'cons'=>$constructora],200);
      }

      public function bienvenido(Request $request) {
        $id_carg = $request->input('ID_CARG');
        $reqP = new Request((array)$request->input('proyecto'));
        $cargos = (array)$request->input('cargos');
        $empleados = (array)$request->input('empleados');
        $id_const = $reqP->input('ID_CONS');
        $id_proy = app('App\Http\Controllers\ProyectoController')->crearProyecto($reqP)->getData()->data;

        foreach ($cargos as $key => &$cargo) {
          $reqC = new Request((array)$cargo);
          $cargo = (object)$cargo;
          $cargo->ID_CARG = app('App\Http\Controllers\CargoController')->crearCargo($reqC,$id_const)->ID_CARG;
        }
        foreach ($empleados as $key => &$empleado) {
          $empleado = (object)$empleado;
          $empleado->ID_CARGO = ((object)$cargos[array_search($empleado->CARGO_EMPL,array_column($cargos,'INDEX'))])->ID_CARG;
          $reqE = new Request((array)$empleado);
          $this->agregarEmpleado($reqE);
        }
        $carg = Cargo::find($id_carg);
        $modC = Modulo::where('TIPO_MOD','C')->get();
        $modP = Modulo::where('TIPO_MOD','P')->get();
        $carg->modulos()->sync([]);
        foreach ($modC as $key => $mod) {
          $carg->modulos()->attach($mod->ID_MOD,
            [
              'PUEDE_EDITAR'=>true,
              'PUEDE_VER'=>true
            ]);
        }
        foreach ($modP as $key => $mod) {
          $carg->modulos()->attach($mod->ID_MOD,
            [
              'PUEDE_EDITAR'=>true,
              'PUEDE_VER'=>true
            ]);
          // $carg->modulos()->where('ID_MOD',$mod->ID_MOD)->first()->hijos()->wherePivot('CARGO_ID_CARG',$id_carg)->sync([]);
          foreach ($mod->hijosR()->get() as $key => $hijo) {
            $carg->modulos()->where('ID_MOD',$mod->ID_MOD)->first()->hijosC()->wherePivot('CARGO_ID_CARG',$id_carg)->attach($hijo->ID_MOD,
              [
                'PUEDE_EDITAR'=>$hijo->EDITAR_MOD,
                'PUEDE_VER'=>$hijo->VER_MOD,
                'CARGO_ID_CARG'=>$id_carg
              ]);
          }
        }
        $carg->modulosC;
        foreach ($carg->modulosP as $key => &$mod) {
          $mod->hijos = $mod->hijosC()->wherePivot('CARGO_ID_CARG',$id_carg)->get();
        }
        return response()->json(['msg'=>'bienvenida','cargo'=>$carg]);
      }

}
