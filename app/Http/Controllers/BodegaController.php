<?php

namespace App\Http\Controllers;

use App\OrdenCompra;
use App\DetalleCotizacion;
use App\DetalleRecepcion;
use App\FlujoOrdenCompra;
use App\InventarioProyecto;
use App\Material;
use App\Subarea;
use App\Proyecto;
use App\EntregaMaterial;
use App\DetalleInventarioEntrega;
use App\DUI;
use App\DetalleDUI;
use App\FlujoDUI;
use App\GuiaDespacho;
use App\RecepcionMaterial;
use App\DevolucionProveedor;
use App\DetalleDevolucion;
use App\Merma;
use App\Categoria;
use App\InventarioExtra;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use DB;
use PDF;

class BodegaController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){}

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $ID_USU){}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){}

    public function cargarOCAprobadas($ID_PROY){
      $oc = OrdenCompra::where('ID_PROY',$ID_PROY)
                       ->join('DETALLE_ORDEN_COTIZACION','DETALLE_ORDEN_COTIZACION.ORDEN_COMPRA_ID_ORDENC','ID_ORDENC')
                       ->join('DETALLE_COTIZACION','DETALLE_COTIZACION.ID_DETA_COTI','DETALLE_ORDEN_COTIZACION.DETALLE_COTIZACION_ID_DETA_COTI')
                       ->where('DETALLE_COTIZACION.ESTADO_COTIZACION','O')
                       ->where('ORDEN_COMPRA.ESTADO_ORDEN','A')
                       ->join('EMPRESA','EMPRESA.ID_EMPR','ORDEN_COMPRA.ID_EMPR')
                       ->orderBy('ORDEN_COMPRA.FECHA_EMISION_ORDENC','desc')
                       ->selectRaw('ORDEN_COMPRA.*,EMPRESA.NOMBRE_EMPR')
                       ->with('detalleFecha')
                       // ->ordenBy('FECHA_RECEPCION_DETCO','desc')
                       ->get();


        foreach ($oc as $key => $value) {
          $value->estadoPospuesto = false;
          $detaCoti = DB::table('DETALLE_ORDEN_COTIZACION')
                          ->join('DETALLE_COTIZACION','DETALLE_COTIZACION.ID_DETA_COTI','DETALLE_ORDEN_COTIZACION.DETALLE_COTIZACION_ID_DETA_COTI')
                          ->where('ORDEN_COMPRA_ID_ORDENC',$value->ID_ORDENC)
                          ->selectRaw('DETALLE_COTIZACION.*')
                          ->get();
              foreach ($detaCoti as $key => $val) {
                if($val->CANTIDAD_RECEPCION <> null){
                  $value->estadoPospuesto = true;
                }
              }

        }
       return $oc;
    }

    public function filtarHistorialRecepcion(Request $request,$ID_PROY){
      $tipo = null;
      $criterio = null;
      $fechaIni = null;
      $fechaFin= null;
      $por_pagina = $request->input('POR_PAGINA');

      if($request->input('TIPO')){
        $tipo = $request->input('TIPO');
      }
      if($request->input('ID_PROV')){
        $criterio = $request->input('ID_PROV');
      }
      if($request->input('fechaIni')){
        $fechaIni = $request->input('fechaIni');
      }
      if($request->input('fechaFin')){
        $fechaFin = $request->input('fechaFin');
      }

      $historial = RecepcionMaterial::leftjoin('EMPRESA','EMPRESA.ID_EMPR','RECEPCION_MATERIAL.EMPRESA_ID_EMPR')
        ->leftjoin('PROYECTO','PROYECTO.ID_PROY','RECEPCION_MATERIAL.PROYECTO_ID_PROY_DUI')
        ->select('RECEPCION_MATERIAL.*','EMPRESA.NOMBRE_EMPR','PROYECTO.NOMBRE_PROY')
        ->where('RECEPCION_MATERIAL.PROYECTO_ID_PROY',$ID_PROY)
        ->where(function ($query) use ($tipo) {
          if($tipo) $query->where('TIPO_RECE',$tipo);
        })->where(function ($query) use ($criterio,$tipo) {
          if($tipo && $criterio) {
            if($tipo == 'O')$query->whereRaw("concat(EMPRESA.NOMBRE_EMPR,' ',EMPRESA.RAZON_SOCIAL_EMPR) like '%$criterio%'");
            else $query->whereRaw("PROYECTO.NOMBRE_PROY like '%$criterio%'");
          }
        })->where(function ($query) use ($fechaIni,$fechaFin) {
          if($fechaIni && $fechaFin) $query->whereBetween('FECHA_RECE',[$fechaIni,$fechaFin]);
        })
        ->orderBy('FECHA_RECE','desc')
        ->paginate($por_pagina);

      return $historial;
    }

    public function guardarRecepcionCompra(Request $request){
      $detalle = $request->input('DETALLE');
      $tipo = $request->input('TIPO');
      $observa = $request->input('OBSERV');
      $id_orden = $request->input('ID_ORDEN');
      $id_empleado = $request->input('ID_EMP');
      $id_proyecto = $request->input('ID_PROY');

      $oc = OrdenCompra::find($id_orden);

      $recepcion = RecepcionMaterial::where('NRO_ID',$id_orden)->where('TIPO_RECE','O')->first();
      if(!$recepcion){
        $recepcion = new RecepcionMaterial();
        $recepcion->NRO_ID = $id_orden;
        $recepcion->TIPO_RECE = 'O';
        $recepcion->EMPRESA_ID_EMPR = $oc->ID_EMPR;
        $recepcion->PROYECTO_ID_PROY = $oc->ID_PROY;
      }
      $recepcion->ESTADO_RECE = $tipo;
      $recepcion->FECHA_RECE = $oc->detalleFecha()->first()->FECHA_RECEPCION_DETCO;
      $recepcion->save();

        date_default_timezone_set('America/Santiago');
        $now = new \DateTime();

      foreach ($detalle as $key => $det) {
        if($det['CANTIDAD_RECIBIR'] <> 0 ){
          $recepDet = new DetalleRecepcion();
          $recepDet->ID_DETA_COTI = $det['ID_DETA_COTI'];
          $recepDet->CANTIDAD_RECEPCION = $det['CANTIDAD_RECIBIR'];
          $recepDet->FECHA_EMITIDA = $now;
          $recepDet->ESTADO = $tipo;
          $recepDet->RECEPCION_MATERIAL_ID_RECE_MAT = $recepcion->ID_RECE_MAT;
          $recepDet->save();
          }

          $detalle_coti = DetalleCotizacion::find($det['ID_DETA_COTI']);
          $detalle_coti->CANTIDAD_RECEPCION = DetalleRecepcion::where('ID_DETA_COTI',$det['ID_DETA_COTI'])
                                                                  ->sum('CANTIDAD_RECEPCION');

          $inventario = InventarioProyecto::where('MATERIAL_ID_MATE',$det['ID_MATE'])
            ->where('PROYECTO_ID_PROY',$id_proyecto)->first();

          if($inventario <> null){
            $inventario->STOCK_INVENT = $inventario->STOCK_INVENT + $det['CANTIDAD_RECIBIR'];
            $inventario->FECHA_ULTIMO_INGRESO = $now;
            $inventario->save();
          }
          else{
            $inventario = new InventarioProyecto();
            $inventario->STOCK_INVENT = $det['CANTIDAD_RECIBIR'];
            $inventario->FECHA_ULTIMO_INGRESO = $now;
            $inventario->MATERIAL_ID_MATE = $det['ID_MATE'];
            $inventario->PROYECTO_ID_PROY = $id_proyecto;
            $inventario->save();
          }
          if(!$inventario->detalleOC()->where('ID_DETA_COTI',$det['ID_DETA_COTI'])->first()){
            $inventario->detalleOC()->attach($det['ID_DETA_COTI']);
          }

          if($tipo=='F'){
            $detalle_coti->ESTADO_COTIZACION = 'RP';
          }

          $detalle_coti->save();

      }
      // if($tipo=='F'){
        // $oc = OrdenCompra::where('ID_ORDENC',$id_orden)->first();
        // $oc->ESTADO_ORDEN = 'RP';
        // $oc->save();
      // }

      $flujo = new FlujoOrdenCompra();
      $flujo->TIPO_FLUJO = 'RP';
      $flujo->OBSERVACION_FLUJO = $observa;
      $flujo->FECHA_FLUJO = $now;
      $flujo->ORDEN_COMPRA_ID_ORDENC = $id_orden;
      $flujo->USUARIO_ID_USU = $id_empleado;
      $flujo->save();

      return response()->json(['code' => 200, 'message' => 'Material Recepcionado correctamente'],200);

  }

    public function listarMateriales($ID_PROY) {
      $materiales = Material::join('INVENTARIO_PROY', 'MATERIAL_ID_MATE', 'ID_MATE')
                            ->join('UNIDAD_MEDIDA','ID_UNID','UNIDAD_MEDIDA_ID_UNID')
                            ->join('SUBCATEGORIA','ID_SUBC','SUBCATEGORIA_ID_SUBC')
                            ->join('CATEGORIA','ID_CATE','CATEGORIA_ID_CATE')
                            ->where('PROYECTO_ID_PROY',$ID_PROY)
                            ->where('STOCK_INVENT', '>', 0)
                            ->select('ID_MATE','ID_INVENT_PROY','NOMBRE_MATE','STOCK_INVENT','NOMBRE_UNID','NOMBRE_SUBC','NOMBRE_CATE',DB::raw('0 as CANT_REQU'))
                            ->get();
      foreach ($materiales as $key => $material) {
        $promedios = InventarioProyecto::find($material->ID_INVENT_PROY)->detalleOC()->selectRaw("avg(PRECIO_NETO_DETCO) as promPrecio, avg(CANTIDAD_ENTREGAR_DETCO) as promCant")->first();
        $material->promedio = $promedios->promCant>0?round($promedios->promPrecio/$promedios->promCant):0;
      }
      return $materiales;
    }

    public function listarSubAreas($ID_PROY) {
      $subareas = Subarea::join('DETALLE_SAREA_PROY','SUBAREA_ID_SAREA','ID_SAREA')
                         ->join('AREA','AREA_ID_AREA','ID_AREA')
                         ->where('PROYECTO_ID_PROY',$ID_PROY)
                         ->select('ID_SAREA','NOMBRE_SAREA','NOMBRE_AREA')
                         ->get();
      return $subareas;
    }

    public function historialRecepcion($ID_PROY){



      $oc = OrdenCompra::where('ID_PROY',$ID_PROY)
                       // ->where('ESTADO_ORDEN','A')
                       ->join('DETALLE_ORDEN_COTIZACION','DETALLE_ORDEN_COTIZACION.ORDEN_COMPRA_ID_ORDENC','ID_ORDENC')
                       ->join('DETALLE_COTIZACION','DETALLE_COTIZACION.ID_DETA_COTI','DETALLE_ORDEN_COTIZACION.DETALLE_COTIZACION_ID_DETA_COTI')
                       ->where('DETALLE_COTIZACION.ESTADO_COTIZACION','RP')
                       ->join('EMPRESA','EMPRESA.ID_EMPR','ORDEN_COMPRA.ID_EMPR')
                       ->selectRaw('ORDEN_COMPRA.*,EMPRESA.NOMBRE_EMPR')
                       ->orderBy('ORDEN_COMPRA.FECHA_EMISION_ORDENC','asc')
                       ->with('detalleFecha')
                       ->groupBy('EMPRESA.NOMBRE_EMPR')
                       ->get();
       return $oc;
    }

    public function generarEntrega(Request $request, $ID_PROY) {
      date_default_timezone_set('America/Santiago');
      $now = new \DateTime();
      $entrega = new EntregaMaterial();
      $entrega->NOMBRE_ENTRE = $request->input('nombre');
      $entrega->CARGO_ENTRE = $request->input('cargo');
      $entrega->NOMBRE_AUTORIZA = $request->input('nombre_autoriza');
      $entrega->FECHA_ENTREGA = $now;
      $entrega->OBSERVACION_ENTRE = $request->input('obs');
      $entrega->SUBAREA_ID_SAREA = $request->input('idsarea');
      $entrega->DUI_ID_DUI = $request->input('id_dui');
      $entrega->DEVOLUCION_PROVEEDOR_ID_DEVO_PROV = $request->input('id_dap');
      $entrega->save();
      $idsInv = $request->input('idsInv');
      foreach ($idsInv as $key => $value) {
        InventarioProyecto::where('ID_INVENT_PROY',$value['ID_INVENT_PROY'])->decrement('STOCK_INVENT',$value['CANT_REQU']);
        InventarioProyecto::where('ID_INVENT_PROY',$value['ID_INVENT_PROY'])->update(['FECHA_ULTIMO_RETIRO'=>$now]);
        $entrega->detalle()->attach($value['ID_INVENT_PROY'],['CANT_ENTREGADA'=>$value['CANT_REQU']]);
        if(!$entrega->DUI_ID_DUI && !$entrega->DEVOLUCION_PROVEEDOR_ID_DEVO_PROV) {
          $this->asignarMontoSubarea($ID_PROY,$value['ID_INVENT_PROY'],$entrega->SUBAREA_ID_SAREA,$value['CANT_REQU']);
          // $promedios = InventarioProyecto::find($value['ID_INVENT_PROY'])->detalleOC()->selectRaw("avg(PRECIO_NETO_DETCO) as promPrecio, avg(CANTIDAD_ENTREGAR_DETCO) as promCant")->first();
          // $p = $promedios->promCant>0?round($promedios->promPrecio/$promedios->promCant)*$value['CANT_REQU']:0;
          // $detalle = $proyecto->DetalleSubarea()->where('SUBAREA_ID_SAREA',$entrega->SUBAREA_ID_SAREA)->first();
          // $detalle->pivot->MONTO_MATERIAL += $p;
          // $detalle->pivot->save();

        }
      }
      if($request->input('id_dui')){
        DUI::where('ID_DUI',$request->input('id_dui'))->update(['ESTADO_DUI'=>'EN']);
        $flujo = new FlujoDUI();
        $flujo->TIPO_FLUJO_DUI = 'E';
        $flujo->FECHA_FLUJO_DUI = date('Y-m-d H:i:s');
        $flujo->OBSERVACION_FLUJO_DUI = $request->input('obs');
        $flujo->USUARIO_ID_USU = $request->input('ID_USU');
        $flujo->DUI_ID_DUI = $request->input('id_dui');
        $flujo->save();
      }
      else if($request->input('id_dap')) {
        DevolucionProveedor::where('ID_DEVO_PROV',$request->input('id_dap'))->update(['ESTADO'=>'D']);
        $orden = OrdenCompra::find(DevolucionProveedor::find($request->input('id_dap'))->ID_ORDENC);
        $orden->detalle_valor()->update(['ESTADO_COTIZACION'=>'DE']);
        $orden->ESTADO_ORDEN = 'DE';
        $orden->save();
      }
      return response()->json('Entrega generada');
    }

    private function asignarMontoSubarea($id_proy,$id_inv,$id_sare,$cant) {
      $detalle = Proyecto::find($id_proy)->DetalleSubarea()->wherePivot('SUBAREA_ID_SAREA',$id_sare)->first();
      $detalleExtra = InventarioProyecto::find($id_inv)->detalleInvExt()
        ->wherePivot('INVENT_EXT_USADO',0)->first();

      if($detalleExtra){
        $cantidad_restante = 0;
        $cantidad_restante_inventario = $detalleExtra->STOCK_INVENT - $detalleExtra->pivot->CANTIDAD_USADA;
        if($cant > $cantidad_restante_inventario) {
          $detalle->pivot->MONTO_MATERIAL += $detalleExtra->VALOR_UNITARIO*$cantidad_restante_inventario;
        }
        else { $detalle->pivot->MONTO_MATERIAL += $detalleExtra->VALOR_UNITARIO*$cant; }
        $cantidad_restante = $cant - $cantidad_restante_inventario;
        $detalle->pivot->save();

        if($cantidad_restante >= 0) {
          $detalleExtra->pivot->CANTIDAD_USADA += $cantidad_restante_inventario;
          // if($detalleInv->ESTADO_COTIZACION == 'RP') { $detalleInv->pivot->COTIZACION_USADA = true; }
          if($cantidad_restante == 0) { $detalleExtra->pivot->INVENT_EXT_USADO = true; }
          $detalleExtra->pivot->save();
          if($cantidad_restante > 0){ return $this->asignarMontoSubarea($id_proy,$id_inv,$id_sare,$cantidad_restante); }
        }
        else {
          $detalleExtra->pivot->CANTIDAD_USADA += $cant;
          $detalleExtra->pivot->save();
        }
      }
      else {
        $detalleDUI = InventarioProyecto::find($id_inv)->detalleDUI()
          ->join('DUI','ID_DUI','DUI_ID_DUI')->wherePivot('DUI_UTILIZADA',0)->orderBy('DUI.FECHA_R','asc')
          ->first();

        if($detalleDUI) {
          $cantidad_restante = 0;
          $cantidad_restante_inventario = $detalleDUI->CANT_RECEPCIONADA - $detalleDUI->pivot->CANTIDAD_USADA;
          if($cant > $cantidad_restante_inventario) {
            $detalle->pivot->MONTO_MATERIAL += $detalleDUI->PREC_PROM_COTI*$cantidad_restante_inventario;
          }
          else { $detalle->pivot->MONTO_MATERIAL += $detalleDUI->PREC_PROM_COTI*$cant; }
          $cantidad_restante = $cant - $cantidad_restante_inventario;
          $detalle->pivot->save();

          if($cantidad_restante >= 0) {
            $detalleDUI->pivot->CANTIDAD_USADA += $cantidad_restante_inventario;
            // if($detalleInv->ESTADO_COTIZACION == 'RP') { $detalleInv->pivot->COTIZACION_USADA = true; }
            if($cantidad_restante == 0) { $detalleDUI->pivot->DUI_UTILIZADA = true; }
            $detalleInv->pivot->save();
            if($cantidad_restante > 0){ return $this->asignarMontoSubarea($id_proy,$id_inv,$id_sare,$cantidad_restante); }
          }
          else {
            $detalleDUI->pivot->CANTIDAD_USADA += $cant;
            $detalleDUI->pivot->save();
          }
        }
        else {
          $detalleInv = InventarioProyecto::find($id_inv)->detalleOCEntrega()
            ->join('DETALLE_RECEPCION','DETALLE_RECEPCION.ID_DETA_COTI','DETALLE_COTIZACION.ID_DETA_COTI')
            ->where('ESTADO_COTIZACION','RP')->orderBy('FECHA_EMITIDA','ASC')->select('DETALLE_COTIZACION.*')->first();

          $cantidad_restante = 0;
          $cantidad_restante_inventario = $detalleInv->CANTIDAD_RECEPCION - $detalleInv->pivot->CANTIDAD_USADA;
          if($cant > $cantidad_restante_inventario) {
            $detalle->pivot->MONTO_MATERIAL += $detalleInv->PRECIO_NETO_UNIT*$cantidad_restante_inventario;
          }
          else { $detalle->pivot->MONTO_MATERIAL += $detalleInv->PRECIO_NETO_UNIT*$cant; }
          $cantidad_restante = $cant - $cantidad_restante_inventario;
          $detalle->pivot->save();

          if($cantidad_restante >= 0) {
            $detalleInv->pivot->CANTIDAD_USADA += $cantidad_restante_inventario;
            // if($detalleInv->ESTADO_COTIZACION == 'RP') { $detalleInv->pivot->COTIZACION_USADA = true; }
            if($cantidad_restante == 0) { $detalleInv->pivot->COTIZACION_USADA = true; }
            $detalleInv->pivot->save();
            if($cantidad_restante > 0){ return $this->asignarMontoSubarea($id_proy,$id_inv,$id_sare,$cantidad_restante); }
          }
          else {
            $detalleInv->pivot->CANTIDAD_USADA += $cant;
            $detalleInv->pivot->save();
          }
        }
      }
      return true;
    }

    public function historialEntrega(Request $request, $ID_PROY) {
      $fechIn = $request->input('fechIn');
      $fechFn = $request->input('fechFn');
      $usuRet = $request->input('usuRet');
      $mateNm = $request->input('mateNm');
      $por_pagina = $request->input('POR_PAGINA');
      $hist = DetalleInventarioEntrega::join('ENTREGA_MATERIAL','ID_ENTRE','ENTREGA_MATERIAL_ID_ENTRE')
                                      ->join('INVENTARIO_PROY','ID_INVENT_PROY','INVENTARIO_PROY_ID_INVENT_PROY')
                                      ->join('MATERIAL','ID_MATE','MATERIAL_ID_MATE')
                                      ->join('UNIDAD_MEDIDA','ID_UNID','UNIDAD_MEDIDA_ID_UNID')
                                      ->join('SUBCATEGORIA','ID_SUBC','SUBCATEGORIA_ID_SUBC')
                                      ->join('SUBAREA','ID_SAREA','SUBAREA_ID_SAREA')
                                      ->join('AREA','ID_AREA','AREA_ID_AREA')
                                      ->select('NOMBRE_SUBC','NOMBRE_MATE','CANT_ENTREGADA','NOMBRE_UNID','FECHA_ENTREGA','NOMBRE_ENTRE','CARGO_ENTRE','NOMBRE_SAREA','NOMBRE_AREA','NOMBRE_AUTORIZA','OBSERVACION_ENTRE')
                                      ->where('PROYECTO_ID_PROY',$ID_PROY)
                                      ->where(function ($query) use ($fechIn,$fechFn) {
                                        if($fechIn && $fechFn){ $query->whereBetween('FECHA_ENTREGA',[(new \DateTime($fechIn))->format('Y-m-d 00:00:00'),(new \DateTime($fechFn))->format('Y-m-d 23:59:59')]); }
                                        else if($fechIn) { $query->whereRaw("FECHA_ENTREGA BETWEEN $fechIn and now()"); }
                                        else if($fechFn) {
                                          $query->whereRaw("FECHA_ENTREGA BETWEEN (select min(FECHA_ENTREGA) from ENTREGA_MATERIAL where PROYECTO_ID_PROY = $ID_PROY) and $fechFn");
                                        }
                                      })->where(function ($query) use ($usuRet) {
                                        if($usuRet) { $query->whereRaw("concat(NOMBRE_ENTRE,' ',CARGO_ENTRE) like '%$usuRet%'"); }
                                      })->where(function ($query) use ($mateNm) {
                                        if($mateNm) { $query->whereRaw("NOMBRE_MATE like '%$mateNm%'"); }
                                      })->orderby('FECHA_ENTREGA','desc')
                                      ->paginate($por_pagina);
      return $hist;
    }

    public function emitirDUI(Request $request) {
      date_default_timezone_set('America/Santiago');
      $dui = new DUI();
      $dui->PROYECTO_ID_PROY_E = $request->input('ID_PROY_E');
      $dui->PROYECTO_ID_PROY_R = $request->input('ID_PROY_R');
      $dui->OBSERVACION_E = $request->input('OBSERVACION');
      $dui->FECHA_E = date('Y-m-d H:i:s');
      $dui->ESTADO_DUI = 'ES';
      $dui->TIPO_ENVIO = $request->input('TIPO_ENVIO');
      $dui->save();
      $detalles = $request->input('detalles');
      foreach ($detalles as $key => $detalle) {
        $dui->detalle()->attach($detalle['id'], [
          'CANT_SOLICITADA' => $detalle['cant'],
          'PREC_PROM_COTI' => $detalle['prec']
        ]);
      }
      $flujo = new FlujoDUI();
      $flujo->TIPO_FLUJO_DUI = 'C';
      $flujo->FECHA_FLUJO_DUI = date('Y-m-d H:i:s');
      $flujo->OBSERVACION_FLUJO_DUI = $request->input('OBSERVACION');
      $flujo->USUARIO_ID_USU = $request->input('ID_USU');
      $flujo->DUI_ID_DUI = $dui->ID_DUI;
      $flujo->save();

      $emp = Proyecto::find($dui->PROYECTO_ID_PROY_R)->DetalleEmpleados()
        ->where('USUARIO_ID_USU',$flujo->USUARIO_ID_USU)->first();
      if($emp){
        $automatico = $emp->cargo->modulosP()->where('ID_MOD',44)->first()->hijosC()
          ->wherePivot('CARGO_ID_CARG',$emp->CARGO_ID_CARG)->where('ID_MOD',47)->first()->pivot->PUEDE_EDITAR;
        if($automatico) {
          $req = new \Illuminate\Http\Request();
          $req->replace([
            'ID_DUI' => $dui->ID_DUI,
            'estado' => 'AC',
            'obs' => 'DUI aprobada automaticamente',
            'ID_USU' => $request->input('ID_USU')
          ]);
          $this->aceptarRechazarDUI($req);
          return response()->json(['msg'=>'DUI generada y aprobada automáticamente!']);
        }
      }
      return response()->json(['msg'=>'DUI generada!']);
    }

    public function verDUIsRecibidas(Request $request, $ID_PROY) {
      $proy = $request->input('nmProy');
      $esta = $request->input('estado');
      $fIni = $request->input('fechIn');
      $fTer = $request->input('fechTe');
      return DUI::where('PROYECTO_ID_PROY_R',$ID_PROY)
                ->join('PROYECTO','PROYECTO.ID_PROY','PROYECTO_ID_PROY_E')
                ->where(function ($query) use ($proy) {
                  if($proy) { $query->whereRaw("PROYECTO.NOMBRE_PROY like '%$proy%'"); }
                })->where(function ($query) use ($esta) {
                  if($esta) { $query->where('ESTADO_DUI',$esta); }
                })->where(function ($query) use ($fIni,$fTer) {
                  if($fIni && $fTer) { $query->whereBetween('FECHA_R',[$fIni,$fTer]); }
                })->with('proyectoE')->get();
    }

    public function verDUIsEmitidas(Request $request, $ID_PROY) {
      $proy = $request->input('nmProy');
      $esta = $request->input('estado');
      $fIni = $request->input('fechIn');
      $fTer = $request->input('fechTe');
      return DUI::where('PROYECTO_ID_PROY_E',$ID_PROY)
                ->join('PROYECTO','PROYECTO.ID_PROY','PROYECTO_ID_PROY_R')
                ->where(function ($query) use ($proy) {
                  if($proy) { $query->whereRaw("PROYECTO.NOMBRE_PROY like '%$proy%'"); }
                })->where(function ($query) use ($esta) {
                  if($esta) { $query->where('ESTADO_DUI',$esta); }
                })->where(function ($query) use ($fIni,$fTer) {
                  if($fIni && $fTer) { $query->whereBetween('FECHA_E',[$fIni,$fTer]); }
                })->with('proyectoR')->get();
    }

    public function getDetalleDUI($ID_DUI) {
      return DUI::where('ID_DUI',$ID_DUI)->with(['proyectoE','proyectoR','detalle','guiaDespacho'])->first();
    }

    public function aceptarRechazarDUI(Request $request) {
      date_default_timezone_set('America/Santiago');
      DUI::where('ID_DUI',$request->input('ID_DUI'))->update([
        'ESTADO_DUI'=>$request->input('estado'),
        'OBSERVACION_R'=>$request->input('obs'),
        'FECHA_R'=>date('Y-m-d H:i:s')
      ]);
      $flujo = new FlujoDUI();
      $flujo->TIPO_FLUJO_DUI = $request->input('estado')==='AC'?'A':'R';
      $flujo->FECHA_FLUJO_DUI = date('Y-m-d H:i:s');
      $flujo->OBSERVACION_FLUJO_DUI = $request->input('obs');
      $flujo->USUARIO_ID_USU = $request->input('ID_USU');
      $flujo->DUI_ID_DUI = $request->input('ID_DUI');
      $flujo->save();
      if($request->input('estado') === 'AC') {
        $guia = new GuiaDespacho();
        $guia->TIPO_GUIA = 'DU';
        $guia->ESTADO_GUIA = 'P';
        $guia->DUI_ID_DUI = $request->input('ID_DUI');
        $guia->PROYECTO_ID_PROY = DUI::find($request->input('ID_DUI'))->PROYECTO_ID_PROY_E;
        $guia->save();
      }
      return response()->json('Dui Actualizada');
    }

    public function listarGuiasDespacho(Request $request,$ID_PROY) {
      $tipo=null;
      if($request->input('tipo'))$tipo = $request->input('tipo');
      $estado=null;
      if($request->input('estado'))$estado = $request->input('estado');
      $por_pagina = $request->input('POR_PAGINA');
      $guias = GuiaDespacho::where('PROYECTO_ID_PROY',$ID_PROY)
                           ->where(function ($query) use ($tipo){
                             if($tipo){ $query->where('TIPO_GUIA',$tipo); }
                           })->where(function ($query) use ($estado){
                             if($estado){ $query->where('ESTADO_GUIA',$estado); }
                           })
                           ->with(['dui','dap'])->paginate($por_pagina);
      return $guias;
    }

    public function detallleGuiaDespacho($ID_GUIA) {
      return GuiaDespacho::where('ID_GUIA',$ID_GUIA)
                         ->with(['duiDetalle','dapDetalle'])->first();
    }

    public function guardarGuiaDespacho(Request $request, $ID_GUIA) {
      date_default_timezone_set('America/Santiago');
      $guia = GuiaDespacho::find($ID_GUIA);
      $ID_CONS = Proyecto::find($guia->PROYECTO_ID_PROY)->CONSTRUCTORA_ID_CONS;
      $guia->NRO_GUIA_DESPACHO = $request->input('NRO_GUIA');
      $datos = [];
      $cambios = false;
      if($request->input('GUIA')){
        $cambios = true;
        $datos[] = array(
          'ARCHIVO' => $request->input('GUIA'),
          'NOMBRE_ARCH' => $request->input('NOMBRE_GUIA'),
          'TIPO_ARCH' => $request->input('TIPO_GUIA')
        );
        $guia->URL_GUIA_DESPACHO = app('App\Entorno')->route_api.'bodega/guia-despacho/descargar/'.$ID_GUIA.'/'.$request->input('NOMBRE_GUIA');
        $guia->NOMB_GUIA_DESPACHO = $request->input('NOMBRE_GUIA');
      }
      $guia->NRO_FACTURA = $request->input('NRO_FACTURA');
      if($request->input('FACTURA')) {
        $cambios = true;
        $datos[] = array(
          'ARCHIVO' => $request->input('FACTURA'),
          'NOMBRE_ARCH' => $request->input('NOMBRE_FACTURA'),
          'TIPO_ARCH' => $request->input('TIPO_FACTURA')
        );
        $guia->URL_FACTURA = app('App\Entorno')->route_api.'bodega/guia-despacho/descargar/'.$ID_GUIA.'/'.$request->input('NOMBRE_FACTURA');
        $guia->NOMB_FACTURA = $request->input('NOMBRE_FACTURA');
      }
      if($cambios) {
        foreach ($datos as $key => $value) {
          $archivo = base64_decode(preg_replace('#^data:'.$value['TIPO_ARCH'].';base64,#i', '', $value['ARCHIVO']));
          file_put_contents($value['NOMBRE_ARCH'] , $archivo);
          $file = fopen($value['NOMBRE_ARCH'],'r+');
          Storage::disk(app('App\Entorno')->origen_storage)->put('archivos/cons/'.$ID_CONS.'/proyecto/'.$guia->PROYECTO_ID_PROY.'/guias/'.$guia->ID_GUIA.'/'.$value['NOMBRE_ARCH'],  $file);
          ftruncate($file,0);
          fclose($file);
        }
        $flujo = new FlujoDUI();
        $flujo->TIPO_FLUJO_DUI = 'GE';
        $flujo->FECHA_FLUJO_DUI = date('Y-m-d H:i:s');
        $flujo->OBSERVACION_FLUJO_DUI = 'Se realizaron cambios en la guía de despacho';
        $flujo->USUARIO_ID_USU = $request->input('ID_USU');
        $flujo->DUI_ID_DUI = $guia->DUI_ID_DUI;
        $flujo->save();
      }
      if($guia->TIPO_GUIA === 'DU' && $guia->ESTADO_GUIA != 'F') {
        $flujo = new FlujoDUI();
        $flujo->TIPO_FLUJO_DUI = 'G';
        $flujo->FECHA_FLUJO_DUI = date('Y-m-d H:i:s');
        $flujo->OBSERVACION_FLUJO_DUI = 'Guía de despacho adjuntada';
        $flujo->USUARIO_ID_USU = $request->input('ID_USU');
        $flujo->DUI_ID_DUI = $guia->DUI_ID_DUI;
        $flujo->save();
      }
      $guia->ESTADO_GUIA = 'F';
      $guia->save();

      return response()->json('Guia almacenada');
    }

    public function descargarGuia($ID_GUIA,$NOMBRE_ARCH) {
      $NOMBRE= trim($NOMBRE_ARCH);
      $guia = GuiaDespacho::find($ID_GUIA);
      $ID_CONS = Proyecto::find($guia->PROYECTO_ID_PROY)->CONSTRUCTORA_ID_CONS;
      $file = storage_path().'/app/archivos/cons/'.$ID_CONS.'/proyecto/'.$guia->PROYECTO_ID_PROY.'/guias/'.$guia->ID_GUIA.'/'.$NOMBRE;
      return response()->download($file,$NOMBRE);
    }

    public function getDUIGuiasDespachoFinalizadas($ID_PROY) {
      return DUI::join('GUIA_DESPACHO','DUI_ID_DUI', 'ID_DUI')
                ->select('DUI.*','ESTADO_GUIA')
                ->where('PROYECTO_ID_PROY_E',$ID_PROY)
                ->where('ESTADO_GUIA','F')
                ->where('ESTADO_DUI','AC')
                ->with('proyectoR')
                ->get();
    }

    public function getDUIRecepcion($ID_PROY) {
      return DUI::join('GUIA_DESPACHO','DUI_ID_DUI', 'ID_DUI')
                ->select('DUI.*','ESTADO_GUIA')
                ->where('PROYECTO_ID_PROY_R',$ID_PROY)
                ->where('ESTADO_GUIA','F')
                ->where('ESTADO_DUI','EN')
                ->with('proyectoE')
                ->get();
    }

    public function detalleRececpionDUI($ID_DUI) {
      return DUI::where('ID_DUI',$ID_DUI)->with(['entrega','detalle','recepcion','proyectoE','guiaDespacho'])->first();
    }

    public function recepcionarDUI(Request $request, $ID_DUI) {
      date_default_timezone_set('America/Santiago');
      $now = new \DateTime();
      $dui = DUI::find($ID_DUI);
      $estado = $request->input('ESTADO_RECE');
      $recepcion = RecepcionMaterial::where('NRO_ID',$ID_DUI)->where('TIPO_RECE','D')->first();
      if(!$recepcion){
        $recepcion = new RecepcionMaterial();
        $recepcion->NRO_ID = $ID_DUI;
        $recepcion->TIPO_RECE = 'D';
        $recepcion->PROYECTO_ID_PROY_DUI = $dui->PROYECTO_ID_PROY_E;
        $recepcion->PROYECTO_ID_PROY = $dui->PROYECTO_ID_PROY_R;
        $recepcion->FECHA_RECE = $now;
      }
      $recepcion->ESTADO_RECE = $estado;
      $recepcion->save();
      $detalle = $request->input('detalle');

      foreach ($detalle as $key => $det) {
        if($det['CANTIDAD_RECIBIR'] <> 0 ){
          $recepDet = new DetalleRecepcion();
          $recepDet->DETALLE_DUI_ID_DETA_DUI = $det['ID_DETA_DUI'];
          $recepDet->CANTIDAD_RECEPCION = $det['CANTIDAD_RECIBIR'];
          $recepDet->FECHA_EMITIDA = $now;
          $recepDet->ESTADO = $estado;
          $recepDet->RECEPCION_MATERIAL_ID_RECE_MAT = $recepcion->ID_RECE_MAT;
          $recepDet->save();
        }
        $CANTIDAD_RECEPCION = DetalleRecepcion::where('DETALLE_DUI_ID_DETA_DUI',$det['ID_DETA_DUI'])
                                                              ->sum('CANTIDAD_RECEPCION');
        DetalleDUI::where('ID_DETA_DUI',$det['ID_DETA_DUI'])->update(['CANT_RECEPCIONADA'=>$CANTIDAD_RECEPCION]);

        $inventario = InventarioProyecto::where('MATERIAL_ID_MATE',$det['ID_MATE'])
          ->where('PROYECTO_ID_PROY',$dui->PROYECTO_ID_PROY_R)->first();
        if($inventario <> null){
          $inventario->STOCK_INVENT = $inventario->STOCK_INVENT + $det['CANTIDAD_RECIBIR'];
          $inventario->FECHA_ULTIMO_INGRESO = $now;
          $inventario->save();
        }
        else{
          $inventario = new InventarioProyecto();
          $inventario->STOCK_INVENT = $det['CANTIDAD_RECIBIR'];
          $inventario->FECHA_ULTIMO_INGRESO = $now;
          $inventario->MATERIAL_ID_MATE = $det['ID_MATE'];
          $inventario->PROYECTO_ID_PROY = $dui->PROYECTO_ID_PROY_R;
          $inventario->save();
        }
        if(!$inventario->detalleDUI()->where('ID_DETA_DUI',$det['ID_DETA_DUI'])->first()){
          $inventario->detalleDUI()->attach($det['ID_DETA_DUI']);
        }
      }
      if($estado == 'F') {
        $dui->ESTADO_DUI = 'RC';
        $dui->save();
      }
      $flujo = new FlujoDUI();
      $flujo->TIPO_FLUJO_DUI = $estado=='F'?'RF':'RP';
      $flujo->FECHA_FLUJO_DUI = date('Y-m-d H:i:s');
      $flujo->OBSERVACION_FLUJO_DUI = $request->input('obs');
      $flujo->USUARIO_ID_USU = $request->input('ID_USU');
      $flujo->DUI_ID_DUI = $ID_DUI;
      $flujo->save();

      return response()->json('Recepcion generada');
    }

    public function getOrdenCompraDevolucion($ID_ORDENC) {
      return OrdenCompra::where('ID_ORDENC',$ID_ORDENC)
        ->with(['detalle_devolucion','empresa'])->first();
    }

    public function solicitarAprobacionDevolucion(Request $request) {
      date_default_timezone_set('America/Santiago');
      $now = new \DateTime();
      $devo = new DevolucionProveedor();
      $devo->ID_PROY = $request->input('ID_PROY');
      $devo->ID_ORDENC = $request->input('ID_ORDENC');
      $devo->ESTADO = 'P';
      $devo->OBSERVACION_CREACION = $request->input('OBSERVACION');
      $devo->FECHA_CREACION = $now;
      $devo->ID_USU_C = $request->input('ID_USU');
      $devo->save();
      $detalles = $request->input('detalles');
      foreach ($detalles as $key => $detalle) {
        $devo->detalle_devolucion()->attach($detalle['ID'],[
          'CANT_DEVOLVER' => $detalle['CANT_DEVOLVER'],
          'PREC_NETO_DEVO' => $detalle['PREC_NETO_DEVO']
        ]);
      }
      return response()->json('Devolucion generada');
    }

    public function getHistorialDevolucionProveedor(Request $request, $ID_PROY) {
      $nomProv = $request->input('nomProv');
      $idOrdnC = $request->input('idOrdnC');
      $esEntrega = $request->input('entrega');
      $por_pagina = $request->input('POR_PAGINA');
      $devo = DevolucionProveedor::where('DEVOLUCION_PROVEEDOR.ID_PROY',$ID_PROY)
        ->join('ORDEN_COMPRA','ORDEN_COMPRA.ID_ORDENC','DEVOLUCION_PROVEEDOR.ID_ORDENC')
        ->join('EMPRESA','EMPRESA.ID_EMPR','ORDEN_COMPRA.ID_EMPR')
        ->where(function ($query) use ($esEntrega) {
          if($esEntrega) $query->where('ESTADO','A');
        })
        ->where(function ($query) use ($nomProv) {
          if($nomProv){
            $query->whereRaw("concat(EMPRESA.NOMBRE_EMPR,' ',EMPRESA.RAZON_SOCIAL_EMPR) like '%$nomProv%'");
          }
        })->where(function ($query) use ($idOrdnC) {
          if($idOrdnC){ $query->where('DEVOLUCION_PROVEEDOR.ID_ORDENC',$idOrdnC); }
        })->select('DEVOLUCION_PROVEEDOR.*','EMPRESA.NOMBRE_EMPR','EMPRESA.RAZON_SOCIAL_EMPR')
        ->with(['detalle_devolucion','ordenCompra','guiaDespacho','DuenoC','EmpleadoC','EmpleadoR','DuenoR'])->paginate($por_pagina);
      return $devo;
    }

    public function obtenerHistorialDevolucionProv(Request $request, $ID_PROY) {
      $nomProv = $request->input('nomProv');
      $idOrdnC = $request->input('idOrdnC');
      $esEntrega = $request->input('entrega');
      $devo = DevolucionProveedor::where('DEVOLUCION_PROVEEDOR.ID_PROY',$ID_PROY)
        ->join('ORDEN_COMPRA','ORDEN_COMPRA.ID_ORDENC','DEVOLUCION_PROVEEDOR.ID_ORDENC')
        ->join('EMPRESA','EMPRESA.ID_EMPR','ORDEN_COMPRA.ID_EMPR')
        ->where(function ($query) use ($esEntrega) {
          if($esEntrega) $query->where('ESTADO','A');
        })
        ->where(function ($query) use ($nomProv) {
          if($nomProv){
            $query->whereRaw("concat(EMPRESA.NOMBRE_EMPR,' ',EMPRESA.RAZON_SOCIAL_EMPR) like '%$nomProv%'");
          }
        })->where(function ($query) use ($idOrdnC) {
          if($idOrdnC){ $query->where('DEVOLUCION_PROVEEDOR.ID_ORDENC',$idOrdnC); }
        })->select('DEVOLUCION_PROVEEDOR.*','EMPRESA.NOMBRE_EMPR')
        ->with(['detalle_devolucion','ordenCompra','guiaDespacho','DuenoC','EmpleadoC','EmpleadoR','DuenoR'])->get();
      return $devo;
    }

    public function aceptarRechazarDevolucionProveedor(Request $request) {
      date_default_timezone_set('America/Santiago');
      $now = new \DateTime();
      if($request->input('esEntrega')){
        DevolucionProveedor::where('ID_DEVO_PROV',$request->input('ID_DEVO_PROV'))
          ->update([
            'ESTADO' => $request->input('ESTADO'),
            'ID_USU_RE' => $request->input('ID_USU'),
            'FECHA_RECHAZO' => $now,
            'OBSERVACION_RECHAZO' => $request->input('OBS')
          ]);
      }
      else {
        DevolucionProveedor::where('ID_DEVO_PROV',$request->input('ID_DEVO_PROV'))
          ->update([
            'ESTADO' => $request->input('ESTADO'),
            'ID_USU_R' => $request->input('ID_USU'),
            'FECHA_REVISION' => $now,
            'OBSERVACION_REVISION' => $request->input('OBS')
          ]);
      }

      $devo = DevolucionProveedor::where('ID_DEVO_PROV',$request->input('ID_DEVO_PROV'))
        ->join('ORDEN_COMPRA','ORDEN_COMPRA.ID_ORDENC','DEVOLUCION_PROVEEDOR.ID_ORDENC')
        ->join('EMPRESA','EMPRESA.ID_EMPR','ORDEN_COMPRA.ID_EMPR')
        ->select('DEVOLUCION_PROVEEDOR.*','EMPRESA.NOMBRE_EMPR')
        ->with(['detalle_devolucion','ordenCompra','guiaDespacho','DuenoC','EmpleadoC','EmpleadoR','DuenoR'])->first();
      if($request->input('ESTADO') == 'A'){
        $guia = new GuiaDespacho();
        $guia->TIPO_GUIA = 'DP';
        $guia->ESTADO_GUIA = 'P';
        $guia->DEVOLUCION_PROVEEDOR_ID_DEVO_PROV = $devo->ID_DEVO_PROV;
        $guia->PROYECTO_ID_PROY = $devo->ID_PROY;
        $guia->save();
      }
      return response()->json(['msg'=>'Devolucion a proveedor actualizada','data'=>$devo]);
    }

    public function getDetalleDevolucion($ID_DEVO_PROV) {
      $devo = DevolucionProveedor::where('ID_DEVO_PROV',$ID_DEVO_PROV)
        ->join('ORDEN_COMPRA','ORDEN_COMPRA.ID_ORDENC','DEVOLUCION_PROVEEDOR.ID_ORDENC')
        ->join('EMPRESA','EMPRESA.ID_EMPR','ORDEN_COMPRA.ID_EMPR')
        ->select('DEVOLUCION_PROVEEDOR.*','EMPRESA.NOMBRE_EMPR')
        ->with(['detalle_devolucion','DuenoC','EmpleadoC','EmpleadoR','DuenoR'])
        ->first();
      return $devo;
    }

    public function generarMerma(Request $request, $ID_PROY) {
      date_default_timezone_set('America/Santiago');
      $now = new \DateTime();
      $merma = new Merma();
      $merma->PROYECTO_ID_PROY = $ID_PROY;
      $merma->INVENTARIO_PROY_ID_INVENT_PROY = $request->input('id_inv');
      $merma->CANT_MERMA = $request->input('cantidad');
      $merma->VALOR_MERMA = $request->input('valor');
      $merma->FECHA_MERMA = $now;
      $merma->OBSERVACION = $request->input('obs');
      $merma->ESTADO_MERMA = 'P';
      $merma->ID_USU_C = $request->input('id_usu');
      $merma->save();
      if(filter_var($request->input('aprueba'), FILTER_VALIDATE_BOOLEAN)) {
        $req = new \Illuminate\Http\Request();
        $req->replace([
          'estado' => 'A',
          'ID_MERMA' => $merma->ID_MERMA,
          'id_usu' => $request->input('id_usu')
        ]);
        $this->aprobarRechazarMerma($req);
      }
      return response()->json('merma generada');
    }

    public function historialMermas(Request $request, $ID_PROY) {
      $estado = $request->input('estado');
      $material = $request->input('material');
      $por_pagina = $request->input('POR_PAGINA');
      return Merma::join('INVENTARIO_PROY','INVENTARIO_PROY.ID_INVENT_PROY','MERMA.INVENTARIO_PROY_ID_INVENT_PROY')
        ->join('MATERIAL','MATERIAL.ID_MATE','INVENTARIO_PROY.MATERIAL_ID_MATE')
        ->select('MERMA.*','MATERIAL.NOMBRE_MATE')
        ->where('MERMA.PROYECTO_ID_PROY',$ID_PROY)
        ->where(function ($query) use ($estado) {
          if($estado) { $query->where('ESTADO_MERMA',$estado); }
        })->where(function ($query) use ($material) {
          if($material) { $query->whereRaw("MATERIAL.NOMBRE_MATE like '%$material%'"); }
        })->with('articulo')->orderby('FECHA_MERMA','desc')->paginate($por_pagina);
    }

    public function aprobarRechazarMerma(Request $request) {
      Merma::where('ID_MERMA',$request->input('ID_MERMA'))
        ->update([
          'ESTADO_MERMA' => $request->input('estado'),
          'ID_USU_R' => $request->input('id_usu')
        ]);
      $merm = Merma::find($request->input('ID_MERMA'));
      InventarioProyecto::where('ID_INVENT_PROY',$merm->INVENTARIO_PROY_ID_INVENT_PROY)
        ->decrement('STOCK_INVENT',$merm->CANT_MERMA);
      return response()->json('Merma actualizada!');
    }

    public function inventarioValoracion($ID_PROY) {
      $categorias = Categoria::select('CATEGORIA.ID_CATE','CATEGORIA.NOMBRE_CATE')
        ->where('TIPO_CATE','M')->get();
      // $categorias = Categoria::join('SUBCATEGORIA','CATEGORIA.ID_CATE','SUBCATEGORIA.CATEGORIA_ID_CATE')
        // ->join('MATERIAL','SUBCATEGORIA.ID_SUBC','MATERIAL.SUBCATEGORIA_ID_SUBC')
        // ->select('CATEGORIA.ID_CATE','SUBCATEGORIA.ID_SUBC','SUBCATEGORIA.NOMBRE_SUBC')->get();
      // $categorias = array_values(json_decode(json_encode($categorias->keyBy('ID_SUBC')->groupBy('ID_CATE')),true));
      foreach ($categorias as $key => &$categoria) {
        $total_stock = 0;
        $total_oc = 0;
        $total_re = 0;
        $total_du = 0;
        $total_ie = 0;
        $total_en = 0;
        $total_me = 0;
        $total_de = 0;
        $total_ds = 0;
        $subc = [];
        foreach ($categoria->subcategorias()->select('ID_SUBC','NOMBRE_SUBC')->get() as $key => &$value) {
          // stock inventario
          $value->stock = $this->getPormInv($value->ID_SUBC,$ID_PROY);
          $total_stock += $value->stock;
          // ordenes de compra
          $value->oc = $this->getPromOC($value->ID_SUBC,$ID_PROY);
          $total_oc += $value->oc;
          // recepciones
          $sumas = $this->getSumRecep($value->ID_SUBC,$ID_PROY);
            // recepcion de materiales
            $value->recepciones = $sumas[1];
            $total_re += $value->recepciones;
            // despacho unico interno (DUI)
            $value->dui = $sumas[2];
            $total_du += $value->dui;
          // inventario extra
          $value->inv_extra = $this->getSumaInventarioExtra($value->ID_SUBC,$ID_PROY);
          $total_ie += $value->inv_extra;
          // entregas
          $value->entregas = $this->getSumaUtilizado($value->ID_SUBC,$ID_PROY);
          $total_en += $value->entregas;
          // mermas
          $value->mermas = $this->getSumaMermas($value->ID_SUBC,$ID_PROY);
          $total_me += $value->mermas;
          // devoluciones
          $value->devoluciones = $this->getSumaDevo($value->ID_SUBC,$ID_PROY);
          $total_de += $value->devoluciones;
          // despacho
          $value->despacho = $this->getSumaDespacho($value->ID_SUBC,$ID_PROY);
          $total_ds += $value->despacho;

          $subc[] = $value;
        }
        $categoria->subc = $subc;
        $categoria->total_stock = $total_stock;
        $categoria->total_oc = $total_oc;
        $categoria->total_re = $total_re;
        $categoria->total_du = $total_du;
        $categoria->total_ie = $total_ie;
        $categoria->total_en = $total_en;
        $categoria->total_me = $total_me;
        $categoria->total_de = $total_de;
        $categoria->total_ds = $total_ds;
        // $categoria['nombre'] = Categoria::find($categoria[0]['ID_CATE'])->NOMBRE_CATE;
      }
      $valorizaciones = (object)array(
        'categorias' => $categorias,
        'total_stock' => $categorias->sum('total_stock'),
        'total_oc' => $categorias->sum('total_oc'),
        'total_re' => $categorias->sum('total_re'),
        'total_du' => $categorias->sum('total_du'),
        'total_ie' => $categorias->sum('total_ie'),
        'total_en' => $categorias->sum('total_en'),
        'total_me' => $categorias->sum('total_me'),
        'total_de' => $categorias->sum('total_de'),
        'total_ds' => $categorias->sum('total_ds'),
      );
      return response()->json($valorizaciones);
    }

    private function getPormInv($ID_SUBC,$ID_PROY) {
      $inv = InventarioProyecto::join('MATERIAL','INVENTARIO_PROY.MATERIAL_ID_MATE','MATERIAL.ID_MATE')
        ->where('PROYECTO_ID_PROY',$ID_PROY)
        ->where('MATERIAL.SUBCATEGORIA_ID_SUBC',$ID_SUBC)->select('INVENTARIO_PROY.ID_INVENT_PROY','STOCK_INVENT')->get();
      $p = 0;
      foreach ($inv as $key => $prom) {
        $promedio = InventarioProyecto::join('DETALLE_INVENTARIO_OC','INVENTARIO_PROY_ID_INVENT_PROY','ID_INVENT_PROY')
          ->join('DETALLE_COTIZACION','ID_DETA_COTI','DETALLE_COTIZACION_ID_DETA_COTI')
          ->where('ID_INVENT_PROY',$prom->ID_INVENT_PROY)->where('COTIZACION_USADA',0)
          ->selectRaw("(avg(PRECIO_NETO_UNIT)*sum(CANTIDAD_RECEPCION-CANTIDAD_USADA)) as montoPorm")->first();

        $promedioExtra = InventarioProyecto::join('DETALLE_INVENTARIO_EXTRA','INVENTARIO_PROY_ID_INVENT_PROY','ID_INVENT_PROY')
          ->join('INVENTARIO_EXTRA','ID_INVENT_EXT','INVENTARIO_EXTRA_ID_INVENT_EXT')
          ->where('ID_INVENT_PROY',$prom->ID_INVENT_PROY)->where('INVENT_EXT_USADO',0)
          ->selectRaw("(avg(VALOR_UNITARIO)*sum(INVENTARIO_EXTRA.STOCK_INVENT-CANTIDAD_USADA)) as montoPorm")->first();

        $promedioDui = InventarioProyecto::join('DETALLE_INVENTARIO_DUI','INVENTARIO_ID_INVENT_PROY','ID_INVENT_PROY')
          ->join('DETALLE_DUI','ID_DETA_DUI','DETALLE_DUI_ID_DETA_DUI')
          ->where('ID_INVENT_PROY',$prom->ID_INVENT_PROY)->where('DUI_UTILIZADA',0)
          ->selectRaw("(avg(PREC_PROM_COTI)*sum(CANT_RECEPCIONADA-CANTIDAD_USADA)) as montoPorm")->first();
        $p += ($promedio->montoPorm+$promedioExtra->montoPorm+$promedioDui->montoPorm);
      }
      return $p;
    }

    public function getPromOC($ID_SUBC,$ID_PROY) {
      $suma = +OrdenCompra::join('DETALLE_ORDEN_COTIZACION','ORDEN_COMPRA_ID_ORDENC','ID_ORDENC')
        ->join('DETALLE_COTIZACION','DETALLE_COTIZACION.ID_DETA_COTI','DETALLE_ORDEN_COTIZACION.DETALLE_COTIZACION_ID_DETA_COTI')
        ->join('NOTA_PEDIDO','NOTA_PEDIDO.ID_NOTA_PED','DETALLE_COTIZACION.NOTA_PEDIDO_ID_NOTA_PED')
        ->join('MATERIAL','MATERIAL.ID_MATE','NOTA_PEDIDO.MATERIAL_ID_MATE')
        ->where('MATERIAL.SUBCATEGORIA_ID_SUBC',$ID_SUBC)->whereIn('ORDEN_COMPRA.ESTADO_ORDEN',['A','RP'])
        ->where('ORDEN_COMPRA.ID_PROY',$ID_PROY)->select('ORDEN_COMPRA.ID_ORDENC')->sum('ORDEN_COMPRA.MONTO_TOTAL');
      // $suma = 0;
      // foreach ($ordenes as $key => $orden) {
      //   $suma += OrdenCompra::find($orden->ID_ORDENC)->detalle_valor()->sum('PRECIO_NETO_DETCO');
      // }
      return $suma;
    }

    public function getSumRecep($ID_SUBC,$ID_PROY) {
      $suma = +OrdenCompra::join('DETALLE_ORDEN_COTIZACION','ORDEN_COMPRA_ID_ORDENC','ID_ORDENC')
        ->join('DETALLE_COTIZACION','DETALLE_COTIZACION.ID_DETA_COTI','DETALLE_ORDEN_COTIZACION.DETALLE_COTIZACION_ID_DETA_COTI')
        ->join('NOTA_PEDIDO','NOTA_PEDIDO.ID_NOTA_PED','DETALLE_COTIZACION.NOTA_PEDIDO_ID_NOTA_PED')
        ->join('MATERIAL','MATERIAL.ID_MATE','NOTA_PEDIDO.MATERIAL_ID_MATE')
        ->where('MATERIAL.SUBCATEGORIA_ID_SUBC',$ID_SUBC)->where('ORDEN_COMPRA.ESTADO_ORDEN','RP')
        ->where('ORDEN_COMPRA.ID_PROY',$ID_PROY)->select('ORDEN_COMPRA.ID_ORDENC')->sum('ORDEN_COMPRA.MONTO_TOTAL');
      // $suma = 0;
      // foreach ($ordenes as $key => $orden) {
      //   $suma += OrdenCompra::find($orden->ID_ORDENC)->detalle_valor()->sum('PRECIO_NETO_DETCO');
      // }
      $duis = RecepcionMaterial::join('DETALLE_RECEPCION','DETALLE_RECEPCION.RECEPCION_MATERIAL_ID_RECE_MAT','ID_RECE_MAT')
        ->join('DETALLE_DUI','DETALLE_DUI.ID_DETA_DUI','DETALLE_DUI_ID_DETA_DUI')
        ->join('INVENTARIO_PROY','INVENTARIO_PROY.ID_INVENT_PROY','DETALLE_DUI.INVENTARIO_PROY_ID_INVENT_PROY')
        ->join('MATERIAL','INVENTARIO_PROY.MATERIAL_ID_MATE','MATERIAL.ID_MATE')
        ->where('RECEPCION_MATERIAL.PROYECTO_ID_PROY',$ID_PROY)->where('RECEPCION_MATERIAL.TIPO_RECE','D')
        ->where('RECEPCION_MATERIAL.ESTADO_RECE','F')->where('MATERIAL.SUBCATEGORIA_ID_SUBC',$ID_SUBC)
        ->where('DETALLE_RECEPCION.ESTADO','P')
        ->sum(DB::raw('DETALLE_DUI.PREC_PROM_COTI*DETALLE_DUI.CANT_RECEPCIONADA'));
      // $suma += DUI::join()
      return [$suma+$duis,$suma,$duis];
      // return $suma;
    }

    public function getSumaInventarioExtra($ID_SUBC,$ID_PROY) {
      $inv = InventarioProyecto::join('MATERIAL','INVENTARIO_PROY.MATERIAL_ID_MATE','MATERIAL.ID_MATE')
        ->where('PROYECTO_ID_PROY',$ID_PROY)
        ->where('MATERIAL.SUBCATEGORIA_ID_SUBC',$ID_SUBC)->select('INVENTARIO_PROY.ID_INVENT_PROY','STOCK_INVENT')->get();
      $p = 0;
      foreach ($inv as $key => $prom) {
        $promedios = InventarioProyecto::find($prom->ID_INVENT_PROY)->detalleInvExt()->selectRaw("avg(VALOR_TOTAL) as promPrecio, avg(STOCK_INVENT) as promCant")->first();
        $p += ($promedios->promCant>0?round($promedios->promPrecio/$promedios->promCant):0)*$prom->STOCK_INVENT;
      }
      return $p;
    }

    public function getSumaUtilizado($ID_SUBC,$ID_PROY) {
      $suma = 0;
      $entregas = DetalleInventarioEntrega::join('ENTREGA_MATERIAL','ID_ENTRE','ENTREGA_MATERIAL_ID_ENTRE')
        ->join('INVENTARIO_PROY','ID_INVENT_PROY','INVENTARIO_PROY_ID_INVENT_PROY')
        ->join('MATERIAL','ID_MATE','MATERIAL_ID_MATE')->where('PROYECTO_ID_PROY',$ID_PROY)
        ->whereNotNull('ENTREGA_MATERIAL.SUBAREA_ID_SAREA')
        ->where('SUBCATEGORIA_ID_SUBC',$ID_SUBC)->select('INVENTARIO_PROY_ID_INVENT_PROY','CANT_ENTREGADA')->get();
      foreach ($entregas as $key => $entrega) {
        $promedios = InventarioProyecto::find($entrega->INVENTARIO_PROY_ID_INVENT_PROY)->detalleOC()->selectRaw("avg(PRECIO_NETO_DETCO) as promPrecio, avg(CANTIDAD_ENTREGAR_DETCO) as promCant")->first();
        $suma += ($promedios->promCant>0?round($promedios->promPrecio/$promedios->promCant):0)*$entrega->CANT_ENTREGADA;
      }
      return $suma;
    }

    public function getSumaMermas($ID_SUBC,$ID_PROY) {
      return Merma::join('INVENTARIO_PROY','ID_INVENT_PROY','INVENTARIO_PROY_ID_INVENT_PROY')
        ->join('MATERIAL','ID_MATE','MATERIAL_ID_MATE')->where('MERMA.ESTADO_MERMA','A')
        ->where('MERMA.PROYECTO_ID_PROY',$ID_PROY)->where('SUBCATEGORIA_ID_SUBC',$ID_SUBC)
        ->sum(DB::raw('VALOR_MERMA * CANT_MERMA'));
    }

    public function getSumaDevo($ID_SUBC,$ID_PROY) {
      return +OrdenCompra::join('DETALLE_ORDEN_COTIZACION','ORDEN_COMPRA_ID_ORDENC','ID_ORDENC')
        ->join('DETALLE_COTIZACION','DETALLE_COTIZACION.ID_DETA_COTI','DETALLE_ORDEN_COTIZACION.DETALLE_COTIZACION_ID_DETA_COTI')
        ->join('NOTA_PEDIDO','NOTA_PEDIDO.ID_NOTA_PED','DETALLE_COTIZACION.NOTA_PEDIDO_ID_NOTA_PED')
        ->join('MATERIAL','MATERIAL.ID_MATE','NOTA_PEDIDO.MATERIAL_ID_MATE')
        ->where('MATERIAL.SUBCATEGORIA_ID_SUBC',$ID_SUBC)->where('ORDEN_COMPRA.ESTADO_ORDEN','DE')
        ->where('ORDEN_COMPRA.ID_PROY',$ID_PROY)->select('ORDEN_COMPRA.ID_ORDENC')->sum('ORDEN_COMPRA.MONTO_TOTAL');
    }

    public function getSumaDespacho($ID_SUBC,$ID_PROY) {
      $suma = 0;
      $entregas = DetalleInventarioEntrega::join('ENTREGA_MATERIAL','ID_ENTRE','ENTREGA_MATERIAL_ID_ENTRE')
        ->join('INVENTARIO_PROY','ID_INVENT_PROY','INVENTARIO_PROY_ID_INVENT_PROY')
        ->join('MATERIAL','ID_MATE','MATERIAL_ID_MATE')->where('PROYECTO_ID_PROY',$ID_PROY)
        ->whereNotNull('ENTREGA_MATERIAL.DUI_ID_DUI')
        ->where('SUBCATEGORIA_ID_SUBC',$ID_SUBC)->select('INVENTARIO_PROY_ID_INVENT_PROY','CANT_ENTREGADA')->get();
      foreach ($entregas as $key => $entrega) {
        $promedios = InventarioProyecto::find($entrega->INVENTARIO_PROY_ID_INVENT_PROY)->detalleOC()->selectRaw("avg(PRECIO_NETO_DETCO) as promPrecio, avg(CANTIDAD_ENTREGAR_DETCO) as promCant")->first();
        $suma += ($promedios->promCant>0?round($promedios->promPrecio/$promedios->promCant):0)*$entrega->CANT_ENTREGADA;
      }
      return $suma;
    }

    public function getStockInventario(Request $request, $ID_PROY) {
      $nomMate = $request->input('nomMate');
      $id_subc = $request->input('id_subc');
      $id_unid = $request->input('id_unid');

      $materiales = Material::where(function ($query) use ($nomMate) {
          if($nomMate) $query->whereRaw("NOMBRE_MATE like '%$nomMate%'");
        })->where(function ($query) use ($id_subc) {
          if($id_subc) $query->where('SUBCATEGORIA_ID_SUBC',$id_subc);
        })->where(function ($query) use ($id_unid) {
          if($id_unid) $query->where('UNIDAD_MEDIDA_ID_UNID',$id_unid);
        })->with(['Unidad','Subcategoria'])->get();

      foreach ($materiales as $key => $material) {
        $material->materiales_oc = $this->materialesOC($material->ID_MATE,$ID_PROY);
        $material->materiales_rp = $this->materialesRecepcionados($material->ID_MATE,$ID_PROY);
        $material->materiales_en = $this->materialesUtilizados($material->ID_MATE,$ID_PROY);
        $material->materiales_mr = $this->materialesMerma($material->ID_MATE,$ID_PROY);
        $material->materiales_de = $this->materialesDevolucion($material->ID_MATE,$ID_PROY);
        $inv = InventarioProyecto::where('MATERIAL_ID_MATE',$material->ID_MATE)->where('PROYECTO_ID_PROY',$ID_PROY)->first();
        $material->materiales_sk = $inv?+$inv->STOCK_INVENT:0;
        $material->stock_critico = $inv?+$inv->LIMITE_CRITICO?$inv->LIMITE_CRITICO:null:null;
      }
      return $materiales;
    }

    private function materialesOC($idmate,$ID_PROY) {
      $matesAC = +OrdenCompra::join('DETALLE_ORDEN_COTIZACION','ORDEN_COMPRA_ID_ORDENC','ID_ORDENC')
        ->join('DETALLE_COTIZACION','DETALLE_COTIZACION.ID_DETA_COTI','DETALLE_ORDEN_COTIZACION.DETALLE_COTIZACION_ID_DETA_COTI')
        ->join('NOTA_PEDIDO','NOTA_PEDIDO.ID_NOTA_PED','DETALLE_COTIZACION.NOTA_PEDIDO_ID_NOTA_PED')
        ->where('NOTA_PEDIDO.MATERIAL_ID_MATE',$idmate)->where('ORDEN_COMPRA.ESTADO_ORDEN','A')
        ->where('ORDEN_COMPRA.ID_PROY',$ID_PROY)->select('ORDEN_COMPRA.ID_ORDENC')->sum('DETALLE_COTIZACION.CANTIDAD_ENTREGAR_DETCO');

      $matesRP = +OrdenCompra::join('DETALLE_ORDEN_COTIZACION','ORDEN_COMPRA_ID_ORDENC','ID_ORDENC')
        ->join('DETALLE_COTIZACION','DETALLE_COTIZACION.ID_DETA_COTI','DETALLE_ORDEN_COTIZACION.DETALLE_COTIZACION_ID_DETA_COTI')
        ->join('NOTA_PEDIDO','NOTA_PEDIDO.ID_NOTA_PED','DETALLE_COTIZACION.NOTA_PEDIDO_ID_NOTA_PED')
        ->where('NOTA_PEDIDO.MATERIAL_ID_MATE',$idmate)->where('ORDEN_COMPRA.ESTADO_ORDEN','RP')
        ->where('ORDEN_COMPRA.ID_PROY',$ID_PROY)->select('ORDEN_COMPRA.ID_ORDENC')->sum('DETALLE_COTIZACION.CANTIDAD_RECEPCION');
      return $matesAC+$matesRP;
    }

    private function materialesRecepcionados($idmate,$ID_PROY) {
      return +OrdenCompra::join('DETALLE_ORDEN_COTIZACION','ORDEN_COMPRA_ID_ORDENC','ID_ORDENC')
        ->join('DETALLE_COTIZACION','DETALLE_COTIZACION.ID_DETA_COTI','DETALLE_ORDEN_COTIZACION.DETALLE_COTIZACION_ID_DETA_COTI')
        ->join('NOTA_PEDIDO','NOTA_PEDIDO.ID_NOTA_PED','DETALLE_COTIZACION.NOTA_PEDIDO_ID_NOTA_PED')
        ->where('NOTA_PEDIDO.MATERIAL_ID_MATE',$idmate)->where('ORDEN_COMPRA.ESTADO_ORDEN','RP')
        ->where('ORDEN_COMPRA.ID_PROY',$ID_PROY)->select('ORDEN_COMPRA.ID_ORDENC')->sum('DETALLE_COTIZACION.CANTIDAD_RECEPCION');
    }

    private function materialesUtilizados($idmate,$ID_PROY) {
      return DetalleInventarioEntrega::join('ENTREGA_MATERIAL','ID_ENTRE','ENTREGA_MATERIAL_ID_ENTRE')
        ->join('INVENTARIO_PROY','ID_INVENT_PROY','INVENTARIO_PROY_ID_INVENT_PROY')
        ->where('PROYECTO_ID_PROY',$ID_PROY)->whereNotNull('ENTREGA_MATERIAL.SUBAREA_ID_SAREA')
        ->where('MATERIAL_ID_MATE',$idmate)->select('INVENTARIO_PROY_ID_INVENT_PROY')->sum('CANT_ENTREGADA');
    }

    private function materialesMerma($idmate,$ID_PROY) {
      return +Merma::join('INVENTARIO_PROY','ID_INVENT_PROY','INVENTARIO_PROY_ID_INVENT_PROY')
        ->join('MATERIAL','ID_MATE','MATERIAL_ID_MATE')->where('MERMA.ESTADO_MERMA','A')
        ->where('MERMA.PROYECTO_ID_PROY',$ID_PROY)->where('MATERIAL_ID_MATE',$idmate)
        ->sum('CANT_MERMA');
    }

    private function materialesDevolucion($idmate,$ID_PROY) {
      return +OrdenCompra::join('DETALLE_ORDEN_COTIZACION','ORDEN_COMPRA_ID_ORDENC','ID_ORDENC')
        ->join('DETALLE_COTIZACION','DETALLE_COTIZACION.ID_DETA_COTI','DETALLE_ORDEN_COTIZACION.DETALLE_COTIZACION_ID_DETA_COTI')
        ->join('NOTA_PEDIDO','NOTA_PEDIDO.ID_NOTA_PED','DETALLE_COTIZACION.NOTA_PEDIDO_ID_NOTA_PED')
        ->where('NOTA_PEDIDO.MATERIAL_ID_MATE',$idmate)->where('ORDEN_COMPRA.ESTADO_ORDEN','DE')
        ->where('ORDEN_COMPRA.ID_PROY',$ID_PROY)->select('ORDEN_COMPRA.ID_ORDENC')->sum('DETALLE_COTIZACION.CANTIDAD_ENTREGAR_DETCO');
    }

    public function definirStockCritico(Request $request) {
      $inv = InventarioProyecto::where('MATERIAL_ID_MATE',$request->input('ID_MATE'))
        ->where('PROYECTO_ID_PROY',$request->input('ID_PROY'))->first();
      if(!$inv) {
        $inv = new InventarioProyecto();
        $inv->MATERIAL_ID_MATE = $request->input('ID_MATE');
        $inv->PROYECTO_ID_PROY = $request->input('ID_PROY');
      }
      $inv->LIMITE_CRITICO = $request->input('stock_critico');
      $inv->save();
      return response()->json('Limite actualizado');
    }

    public function traspasarInventario(Request $request) {
      $ID_PROY_E = $request->input('id_proy_e');
      $ID_PROY_R = $request->input('id_proy_r');
      $PORC_CAST = $request->input('porc_cast');
      $TIPO_ENVI = $request->input('tipo_envi');
      date_default_timezone_set('America/Santiago');
      $dui = new DUI();
      $dui->PROYECTO_ID_PROY_E = $ID_PROY_E;
      $dui->PROYECTO_ID_PROY_R = $ID_PROY_R;
      $dui->OBSERVACION_E = 'Traspaso de inventario';
      $dui->OBSERVACION_R = 'Traspaso de inventario';
      $dui->FECHA_E = date('Y-m-d H:i:s');
      $dui->FECHA_R = date('Y-m-d H:i:s');
      $dui->ESTADO_DUI = 'AC';
      $dui->TIPO_ENVIO = $TIPO_ENVI;
      $dui->save();
      $detalles = InventarioProyecto::where('PROYECTO_ID_PROY',$ID_PROY_E)->get();
      foreach ($detalles as $key => $detalle) {
        $promedios = $detalle->detalleOC()->selectRaw("avg(PRECIO_NETO_DETCO) as promPrecio, avg(CANTIDAD_ENTREGAR_DETCO) as promCant")->first();
        $suma = ($promedios->promCant>0?round($promedios->promPrecio/$promedios->promCant):0);
        $suma = $suma * ($PORC_CAST/100);
        $dui->detalle()->attach($detalle->ID_INVENT_PROY, [
          'CANT_SOLICITADA' => $detalle->STOCK_INVENT,
          'PREC_PROM_COTI' => $suma
        ]);
      }
      $guia = new GuiaDespacho();
      $guia->TIPO_GUIA = 'DU';
      $guia->ESTADO_GUIA = 'P';
      $guia->DUI_ID_DUI = $dui->ID_DUI;
      $guia->PROYECTO_ID_PROY = $ID_PROY_E;
      $guia->save();
      return response()->json("Traspaso generado");
    }

    public function calcularPromInventario($ID_PROY) {
      $detalles = InventarioProyecto::where('PROYECTO_ID_PROY',$ID_PROY)->get();
      $suma = 0;
      foreach ($detalles as $key => $detalle) {
        $promedios = $detalle->detalleOC()->selectRaw("avg(PRECIO_NETO_DETCO) as promPrecio, avg(CANTIDAD_ENTREGAR_DETCO) as promCant")->first();
        $suma += ($promedios->promCant>0?round($promedios->promPrecio/$promedios->promCant):0)*$detalle->STOCK_INVENT;
      }
      return round($suma);
    }

    public function guardarInventarioExtra(Request $request) {
      $lista_materiales = $request->input('MATERIALES');
        date_default_timezone_set('America/Santiago');
        $now = new \DateTime();
      foreach ($lista_materiales as $key => $value) {
        $extra = new InventarioExtra();
        $extra->STOCK_INVENT = $value['STOCK_INVENT'];
        $extra->MATERIAL_ID_MATE = $value['MATERIAL_ID_MATE'];
        $extra->PROYECTO_ID_PROY = $value['PROYECTO_ID_PROY'];
        $extra->VALOR_UNITARIO = $value['VALOR_UNITARIO'];
        $extra->FECHA_CREACION= $now;
        $extra->VALOR_TOTAL = $value['VALOR_TOTAL'];
        $extra->save();

        $inventario = InventarioProyecto::where('PROYECTO_ID_PROY',$value['PROYECTO_ID_PROY'])
                                          ->where('MATERIAL_ID_MATE',$value['MATERIAL_ID_MATE'])->first();
        if($inventario != null) {
          $inventario->STOCK_INVENT = $inventario->STOCK_INVENT + $value['STOCK_INVENT'];
          $inventario->save();
        }
        else {
          $inventario = new InventarioProyecto();
          $inventario->STOCK_INVENT = $value['STOCK_INVENT'];
          $inventario->FECHA_ULTIMO_INGRESO = $now;
          $inventario->MATERIAL_ID_MATE = $value['MATERIAL_ID_MATE'];
          $inventario->PROYECTO_ID_PROY = $value['PROYECTO_ID_PROY'];
          $inventario->save();
        }
        $inventario->detalleInvExt()->attach($extra->ID_INVENT_EXT);
      }
      return response()->json(['code' => 200, 'message' => 'Inventario extra agregado correctamente'],200);
    }

    public function historialInventarioExtra($ID_PROY,Request $request){
      $material = $request->input('MATERIAL');
      $inicio = $request->input('FECHA_INI');
      $fin = $request->input('FECHA_FIN');

      $extra = InventarioExtra::where('PROYECTO_ID_PROY',$ID_PROY)
                          ->join('MATERIAL','MATERIAL.ID_MATE','=','MATERIAL_ID_MATE')
                          ->join('UNIDAD_MEDIDA','UNIDAD_MEDIDA.ID_UNID','=','UNIDAD_MEDIDA_ID_UNID')
                          ->join('SUBCATEGORIA','SUBCATEGORIA.ID_SUBC','=','MATERIAL.SUBCATEGORIA_ID_SUBC')
                          ->join('CATEGORIA','CATEGORIA.ID_CATE','=','SUBCATEGORIA.CATEGORIA_ID_CATE')
                          ->where(function ($query) use ($material) {
                            if($material) { $query->whereRaw("MATERIAL.NOMBRE_MATE like '%$material%'"); }
                          })
                          ->where(function ($query) use ($inicio,$fin) {
                            if($inicio && $fin) $query->whereBetween('INVENTARIO_EXTRA.FECHA_CREACION',[$inicio,$fin]);
                          })
                          ->selectRaw('INVENTARIO_EXTRA.* , MATERIAL.*,UNIDAD_MEDIDA.NOMBRE_UNID,CATEGORIA.NOMBRE_CATE')
                          ->get();
      foreach ($extra as $key => $value) {
        $value->EDITABLE = false;
        $value->VALOR_UNITARIO_EDIT = $value->VALOR_UNITARIO;
        $value->STOCK_INVENT_EDIT = $value->STOCK_INVENT;
      }
      return $extra;
    }

    public function editarHistorialExtra($ID_INVENT_EXT,Request $request){
      $cant = $request->input('CANTIDAD');
      $uni = $request->input('UNIDAD_VALOR');

      date_default_timezone_set('America/Santiago');
      $now = new \DateTime();

      $extra = InventarioExtra::find($ID_INVENT_EXT);
      $cantidadResul = 0;
      if($extra->STOCK_INVENT > $cant){
        $cantidadResul = $extra->STOCK_INVENT - $cant;
        $inventario = InventarioProyecto::where('PROYECTO_ID_PROY',$extra->PROYECTO_ID_PROY)
                                          ->where('MATERIAL_ID_MATE',$extra->MATERIAL_ID_MATE)->first();
        if($inventario != null){
          $inventario->STOCK_INVENT = $inventario->STOCK_INVENT - $cantidadResul;
          $inventario->FECHA_ULTIMO_RETIRO = $now;
          $inventario->save();
        }
      }else{
        $cantidadResul = $cant -$extra->STOCK_INVENT;
        $inventario = InventarioProyecto::where('PROYECTO_ID_PROY',$extra->PROYECTO_ID_PROY)
                                          ->where('MATERIAL_ID_MATE',$extra->MATERIAL_ID_MATE)->first();
        if($inventario != null){
          $inventario->STOCK_INVENT = $inventario->STOCK_INVENT + $cantidadResul;
          $inventario->FECHA_ULTIMO_INGRESO = $now;
          $inventario->save();
        }
      }

      $extra->VALOR_UNITARIO = $uni;
      $extra->STOCK_INVENT = $cant;
      $extra->VALOR_TOTAL = (+$cant) * (+$uni);
      $extra->save();

      return response()->json(['code' => 200, 'message' => 'Inventario extra editado correctamente','total'=>$extra->VALOR_TOTAL],200);
    }

    public function eliminarHistorialExtra($ID_INVENT_EXT){
      $extra = InventarioExtra::where('ID_INVENT_EXT',$ID_INVENT_EXT)->first();
      $inventario = InventarioProyecto::where('PROYECTO_ID_PROY',$extra->PROYECTO_ID_PROY)
                                        ->where('MATERIAL_ID_MATE',$extra->MATERIAL_ID_MATE)->first();
      if($inventario != null){
        $inventario->STOCK_INVENT = $inventario->STOCK_INVENT - $extra->STOCK_INVENT;
        $inventario->save();
      }
      $extra->delete();
      return response()->json(['code' => 200, 'message' => 'Material eliminado correctamente'],200);
    }

    public function generarPDFRecepcion($ID_ORDENC) {
      $datos = app('App\Http\Controllers\ProyectoOrdenController')->cargarDatosOCAprobadas($ID_ORDENC)->getData();
      $params = $this->prepararDatos((array)$datos->detalle);
      $orden = OrdenCompra::find($datos->orden_compra->ID_ORDENC);
      $datosPDF = array(
        'es_oc'    => true,
        'nom_prov' => $orden->empresa->NOMBRE_EMPR,
        'rut_prov' => $orden->empresa->RUT_EMPR,
        'obra_man' => $orden->proyecto->NOMBRE_PROY,
        'dire_des' => $orden->proyecto->DIRECCION,
        'detalles' => $params
      );
      $pdf = PDF::loadView('recepcion-pendiente', $datosPDF);
      return $pdf->setPaper('a4','landscape')->download('Recepcion-OC.pdf');
    }

    public function generarPDFDui($ID_DUI) {
      $data = $this->detalleRececpionDUI($ID_DUI);
      $params = $this->prepararDatsoDUI((array)$data->detalle);
      $dui = DUI::find($data->ID_DUI);
      $datosPDF = array(
        'es_oc'    => false,
        'nom_proy' => $dui->proyectoE->NOMBRE_PROY,
        'nom_rece' => $dui->proyectoR->NOMBRE_PROY,
        'dire_des' => $dui->proyectoR->DIRECCION,
        'detalles' => $params
      );
      $pdf = PDF::loadView('recepcion-pendiente', $datosPDF);
      return $pdf->setPaper('a4','landscape')->download('Recepcion-DUI.pdf');
    }

    public function prepararDatos($datos) {
      $params = [];
      foreach ($datos as $key => $dato) {
        $dato = (object)$dato;
        $parm = (object)array(
          'fecha' => $dato->FECHA_RECEPCION_DETCO,
          'material' => $dato->NOMBRE_MATE,
          'unidad' => $dato->NOMBRE_UNID,
          'cantidad' => $dato->CANTIDAD_ENTREGAR_DETCO
        );
        $params[] = $parm;
      }
      return $params;
    }

    public function prepararDatsoDUI($datos) {
      $params = [];
      foreach ($datos as $key => $dato) {
        $parm = (object)array(
          'fecha' => (new \Datetime($dato[0]['FECHA_ULTIMO_RETIRO']))->format('Y-m-d'),
          'material' => $dato[0]['material']['NOMBRE_MATE'],
          'unidad' => $dato[0]['material']['unidad']['NOMBRE_UNID'],
          'cantidad' => $dato[0]['CANT_SOLICITADA']
        );
        $params[] = $parm;
      }
      return $params;
    }

}
