<?php

namespace App\Http\Controllers;

use App\NotaPedido;
use App\FlujoNotaPedido;
use App\Cotizacion;
use App\OrdenCompra;
use App\DetalleCotizacion;
use App\FlujoOrdenCompra;
use App\RecepcionMaterial;
use App\Area;
use App\Subarea;
use App\Empleado;
use App\Dueno;
use App\Proyecto;
use App\Usuario;
use App\Empresa;
use App\FacturaProveedor;
use App\DetalleFacturaProveedor;
use App\Pago;
use App\Cargo;
use Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use DB;

class ProyectoOrdenController extends Controller
{

    public function cargarNotasSeleccionadas(Request $request){
      $lista = $request->input('lista');
      $nota = NotaPedido::whereIn('ID_NOTA_PED',$lista)
                        ->join('MATERIAL','MATERIAL.ID_MATE','MATERIAL_ID_MATE')
                        ->join('SUBCATEGORIA','SUBCATEGORIA.ID_SUBC','MATERIAL.SUBCATEGORIA_ID_SUBC')
                        ->join('UNIDAD_MEDIDA','UNIDAD_MEDIDA.ID_UNID','MATERIAL.UNIDAD_MEDIDA_ID_UNID')
                        ->selectRaw('NOTA_PEDIDO.*,MATERIAL.NOMBRE_MATE,UNIDAD_MEDIDA.NOMBRE_UNID,SUBCATEGORIA.NOMBRE_SUBC')
                        ->get();
      foreach ($nota as $key => $value) {
        $value->CANTIDAD_ENTREGAR_DETCO=null;
        $value->PRECIO_NETO_UNIT=null;
        $value->PRECIO_NETO_DETCO=0;
        $value->FECHA_RECEPCION_DETCO=null;
        $value->estado = false;
      }
      return $nota;
    }
    // guardar cotizacion con proveedor
    public function guardarCotizacion(Request $request){
      $lista = $request->input('LISTA');
      $ID_EMPR = $request->input('ID_EMPR');
      $arch64 = $request->input('ARCHIVO');
      $arch64_nombre = $request->input('NOMBRE_ARCH');
      $arch64_tipo = $request->input('TIPO_ARCH');
      $estado = $request->input('ESTADO');
      $ID_CONS = $request->input('ID_CONS');
      $ID_PROY = $request->input('ID_PROY');
      $OBS = $request->input('OBS');

      foreach ($lista as $key => $det) {
        if($det['estado'] == false){
          $coti = new Cotizacion();
          $coti->ESTADO_COTI = $estado?'C':'I';
          $coti->EMPRESA_ID_EMPR = $ID_EMPR;
          date_default_timezone_set('America/Santiago');
          $now = new \DateTime();
          $coti->FECHA_CREACION_COTI = $now;
          $coti->PROYECTO_ID_PROY = $ID_PROY;
          $coti->OBSERVACION_COTI = $OBS;
          $coti->save();
          if($arch64 != null){
            $cotizacion = Cotizacion::find($coti->ID_COTI);
                $archivo = base64_decode(preg_replace('#^data:'.$arch64_tipo.';base64,#i', '', $arch64));
                file_put_contents($arch64_nombre , $archivo);
                $file = fopen($arch64_nombre,'r+');
                Storage::disk(app('App\Entorno')->origen_storage)->put('archivos/cons/'.$ID_CONS.'/proyecto/'.$ID_PROY.'/cotizaciones/'.$cotizacion->ID_COTI.'/'.$arch64_nombre,  $file);
                ftruncate($file,0);
                fclose($file);

                // La ruta usa el cargarImgAdi()
                $cotizacion->ADJUNTO_URL= app('App\Entorno')->route_api.'cotizacion/'.$ID_CONS.'/'.$ID_PROY.'/adjunto/'.$cotizacion->ID_COTI.'/'.$arch64_nombre;
                $cotizacion->NOMBRE_ADJUNTO = $arch64_nombre;
                $cotizacion->save();
            }
            foreach ($lista as $key => $value) {
              if($value['estado'] == false){
                $det = new DetalleCotizacion();
                $det->NOTA_PEDIDO_ID_NOTA_PED = $value['ID_NOTA_PED'];
                $det->COTIZACION_ID_COTI = $coti->ID_COTI;
                if($value['CANTIDAD_ENTREGAR_DETCO'] == null){
                  $det->CANTIDAD_ENTREGAR_DETCO = 0;
                }else{
                  $det->CANTIDAD_ENTREGAR_DETCO = $value['CANTIDAD_ENTREGAR_DETCO'];
                }
                $det->PRECIO_NETO_UNIT = $value['PRECIO_NETO_UNIT'];
                if($value['PRECIO_NETO_DETCO'] == null){
                  $det->PRECIO_NETO_DETCO = 0;
                }else{
                  $det->PRECIO_NETO_DETCO = $value['PRECIO_NETO_DETCO'];
                }
                if($value['FECHA_RECEPCION_DETCO'] == null){
                  $det->FECHA_RECEPCION_DETCO = 0;
                }else{
                  $det->FECHA_RECEPCION_DETCO = $value['FECHA_RECEPCION_DETCO'];
                }
                $det->ESTADO_COTIZACION = "N";
                $det->save();
              }
            }
          return response()->json(['code' => 200, 'message' => 'Cotizacion guardado con exito'],200);
        }
      }
    }

    public function cargarAdjunto($ID_CONS,$ID_PROY,$ID_COTI,$NOMBRE_ARCH){
        $NOMBRE= trim($NOMBRE_ARCH);
        $file =storage_path()."/app/archivos/cons/$ID_CONS/proyecto/$ID_PROY/cotizaciones/$ID_COTI/$NOMBRE";
        return response()->download($file,$NOMBRE);
    }
    // anibal
    public function cargarCotizacionesPreOrden($ID_PROY) {
      $notas = NotaPedido::select('NOTA_PEDIDO.*')
                         ->where('NOTA_PEDIDO.PROYECTO_ID_PROY',$ID_PROY)
                         ->join('DETALLE_COTIZACION','DETALLE_COTIZACION.NOTA_PEDIDO_ID_NOTA_PED','=','ID_NOTA_PED')
                         ->join('COTIZACION','COTIZACION.ID_COTI','=','DETALLE_COTIZACION.COTIZACION_ID_COTI')
                         ->whereIn('DETALLE_COTIZACION.ESTADO_COTIZACION',['E','N'])
                         ->where('COTIZACION.ESTADO_COTI','C')
                         ->groupBy('ID_NOTA_PED')
                         ->with(['DetalleCotizacion' => function ($c) {
                           $c->join('COTIZACION','COTIZACION_ID_COTI','ID_COTI')
                             ->where('COTIZACION.ESTADO_COTI','C')
                             ->select('DETALLE_COTIZACION.*');
                         }])
                         ->with(['Material'])
                         ->orderBy('FECHA_CREACION_NOTA_PED','desc')
                         ->get();
      $idsProv[] = 0;
      $selectProveedores[] = (object)array(
        'empresa' => 'Todos',
        'id_prov' => 0,
        'idsnp' => []
      );
      foreach ($notas as $key => $nota) {
        foreach ($nota->DetalleCotizacion as $key => $value) {
          $value->checked = false;
          if(!in_array($value->empresa[0]->ID_EMPR,$idsProv)) {
            $idsProv[] = $value->empresa[0]->ID_EMPR;
            $selectProveedores[] = (object)array(
              'empresa' => $value->empresa[0]->NOMBRE_EMPR,
              'id_prov' => $value->empresa[0]->ID_EMPR,
              'idsnp' => [$nota->ID_NOTA_PED]
            );
          }
          else if(!empty($idsProv)){
            $i = array_search($value->empresa[0]->ID_EMPR,$idsProv);
            if(!in_array($nota->ID_NOTA_PED,$selectProveedores[$i]->idsnp)){
              $selectProveedores[$i]->idsnp[] = $nota->ID_NOTA_PED;
            }
          }
        }
      }
      $asignFact = false;
      $empls = Proyecto::find($ID_PROY)->DetalleEmpleados()->wherePivot('RECIBE_FACTURA',true)->first();
      if($empls) { $asignFact = true; }
      return [$notas,$selectProveedores,$asignFact];
    }
    // historial de cotizacion
    public function historialCotizaciones(Request $request){
      $nomProv = $request->input('nomProv');
      $nomMate = $request->input('nomMate');
      $estCoti = $request->input('estCoti');
      $estDCot = $request->input('estDCot');
      $por_pagina = $request->input('POR_PAGINA');
      $ID_PROY = $request->input('ID_PROY');

      $historial = Cotizacion::join('EMPRESA','EMPRESA.ID_EMPR','EMPRESA_ID_EMPR')
                             ->where('COTIZACION.PROYECTO_ID_PROY',$ID_PROY)
                             ->where(function ($query) use ($nomProv) {
                               if($nomProv){ $query->whereRaw("NOMBRE_EMPR like '%$nomProv%'"); }
                             })
                             ->where(function ($query) use ($nomMate) {
                               if($nomMate){
                                 $query->whereRaw(DB::raw(
                                   "(
                                      select count(ID_DETA_COTI) from DETALLE_COTIZACION
                                      join NOTA_PEDIDO on ID_NOTA_PED = NOTA_PEDIDO_ID_NOTA_PED
                                      join MATERIAL on ID_MATE = MATERIAL_ID_MATE
                                      where COTIZACION_ID_COTI = ID_COTI
                                      and NOMBRE_MATE like '%$nomMate%'
                                     ) > 0"
                                 ));
                               }
                             })->where(function ($query) use ($estCoti) {
                               if($estCoti){ $query->where('ESTADO_COTI',$estCoti); }
                             })->where(function ($query) use ($estDCot) {
                               if($estDCot){
                                 $query->whereRaw(DB::raw("(
                                    select count(ID_DETA_COTI) from DETALLE_COTIZACION
                                    where COTIZACION_ID_COTI = ID_COTI
                                    and ESTADO_COTIZACION like '$estDCot'
                                   ) > 0"));
                               }
                             })
                             ->selectRaw('COTIZACION.*,EMPRESA.NOMBRE_EMPR')
                             ->with(['detalle'])
                             ->orderBy('FECHA_CREACION_COTI','desc')
                             ->paginate($por_pagina);
      return $historial;
    }
    // verifica si en la cotizacion ya hay proveedor asignado
    public function verificarProvedorContizacion(Request $request){
      $lista = $request->input('LISTA');
      $ID_EMPR = $request->input('ID_EMPR');
      $detalle = DetalleCotizacion::join('COTIZACION','COTIZACION.ID_COTI','COTIZACION_ID_COTI')
                                    ->where('COTIZACION.EMPRESA_ID_EMPR','=',$ID_EMPR)
                                    ->get();
      $estado = 'false';
      $notasAsignadas = collect();
      foreach ($lista as $key => $value) {
          foreach ($detalle as $key => $val) {
              if($value['ID_NOTA_PED'] == $val->NOTA_PEDIDO_ID_NOTA_PED){
                $estado = 'true';
                $notasAsignadas->push($value['ID_NOTA_PED']);
              }
          }
        }
      return response()->json(['code' => 200, 'message' => 'Cotizacion guardado con exito','estado'=>$estado,'asignados'=>$notasAsignadas],200);
    }
    // cargar datos de cotizacion
    public function cargarDatosCotizados($ID_COTI){
      $coti = Cotizacion::where('ID_COTI',$ID_COTI)->withCount(['detalle'=>function ($query){
        $query->where('ESTADO_COTIZACION','!=','N');
      }])->first();
      $det = DetalleCotizacion::where('COTIZACION_ID_COTI',$ID_COTI)
                              ->join('COTIZACION','COTIZACION.ID_COTI','COTIZACION_ID_COTI')
                              ->join('NOTA_PEDIDO','NOTA_PEDIDO.ID_NOTA_PED','NOTA_PEDIDO_ID_NOTA_PED')
                              ->join('MATERIAL','MATERIAL.ID_MATE','MATERIAL_ID_MATE')
                              ->join('SUBCATEGORIA','SUBCATEGORIA.ID_SUBC','MATERIAL.SUBCATEGORIA_ID_SUBC')
                              ->join('UNIDAD_MEDIDA','UNIDAD_MEDIDA.ID_UNID','MATERIAL.UNIDAD_MEDIDA_ID_UNID')
                              ->join('EMPRESA','EMPRESA.ID_EMPR','COTIZACION.EMPRESA_ID_EMPR')
                              ->selectRaw('DETALLE_COTIZACION.*,EMPRESA.RAZON_SOCIAL_EMPR,EMPRESA.ID_EMPR,NOTA_PEDIDO.*,MATERIAL.NOMBRE_MATE,UNIDAD_MEDIDA.NOMBRE_UNID,SUBCATEGORIA.NOMBRE_SUBC')
                              ->get();
      foreach ($det as $key => $value) {
        $value->FECHA_RECEPCION_DETCO = $value->FECHA_RECEPCION_DETCO.'T00:00:00';
      }
      $coti->proveedor;
      // $coti->countOrds = $coti->withCount(['detalle'=>function ($query){
      //   $query->where('ESTADO_COTIZACION','!=','N');
      // }])->first()->detalle_count;
      return response()->json(['data_1' => $det, 'data_2' => $coti],200);
    }

    public function editarCotizacion(Request $request){

      $lista = $request->input('LISTA');
      $ID_COTI = $request->input('ID_COTI');
      $OBS = $request->input('OBS');
      $arch64 = $request->input('ARCHIVO');
      $arch64_nombre = $request->input('NOMBRE_ARCH');
      $arch64_tipo = $request->input('TIPO_ARCH');
      $estado = $request->input('ESTADO');
      $cotizacion = Cotizacion::where('ID_COTI',$ID_COTI)
                                ->join('PROYECTO','PROYECTO.ID_PROY','=','PROYECTO_ID_PROY')
                                ->first();

      $cotizacion->ESTADO_COTI = $estado?'C':'I';
      $cotizacion->OBSERVACION_COTI = $OBS;
      if($arch64 != null && $arch64_tipo){
        if(Storage::disk(app('App\Entorno')->origen_storage)->exists('archivos/cons/'.$cotizacion->CONSTRUCTORA_ID_CONS.'/proyecto/'.$cotizacion->PROYECTO_ID_PROY.'/cotizaciones/'.$cotizacion->ID_COTI.'/'.$arch64_nombre)){
             Storage::disk(app('App\Entorno')->origen_storage)->delete('archivos/cons/'.$cotizacion->CONSTRUCTORA_ID_CONS.'/proyecto/'.$cotizacion->PROYECTO_ID_PROY.'/cotizaciones/'.$cotizacion->ID_COTI.'/'.$arch64_nombre);
        }
            $archivo = base64_decode(preg_replace('#^data:'.$arch64_tipo.';base64,#i', '', $arch64));

            file_put_contents($arch64_nombre , $archivo);
            $file = fopen($arch64_nombre,'r+');

            Storage::disk(app('App\Entorno')->origen_storage)->put('archivos/cons/'.$cotizacion->CONSTRUCTORA_ID_CONS.'/proyecto/'.$cotizacion->PROYECTO_ID_PROY.'/cotizaciones/'.$cotizacion->ID_COTI.'/'.$arch64_nombre,  $file);
            // Storage::disk(app('App\Entorno')->origen_storage)->put('archivos/cotizacion/'.$cotizacion->ID_COTI.'/adjunto/'.$arch64_nombre,  $file);
            ftruncate($file,0);
            fclose($file);

            $cotizacion->ADJUNTO_URL= app('App\Entorno')->route_api.'cotizacion/'.$cotizacion->CONSTRUCTORA_ID_CONS.'/'.$cotizacion->PROYECTO_ID_PROY.'/adjunto/'.$cotizacion->ID_COTI.'/'.$arch64_nombre;
            $cotizacion->NOMBRE_ADJUNTO = $arch64_nombre;

        }
        $cotizacion->save();
        foreach ($lista as $key => $value) {
            $det = DetalleCotizacion::where('NOTA_PEDIDO_ID_NOTA_PED',$value['ID_NOTA_PED'])
                                    ->where('COTIZACION_ID_COTI',$ID_COTI)
                                    ->where('ESTADO_COTIZACION','N')->first();
            if($det) {
              $det->delete();
              if($value['CANTIDAD_ENTREGAR_DETCO'] == null){
                $CANTIDAD_ENTREGAR_DETCO = 0;
              }else{
                $CANTIDAD_ENTREGAR_DETCO = $value['CANTIDAD_ENTREGAR_DETCO'];
              }
              if($value['PRECIO_NETO_DETCO'] == null){
                $PRECIO_NETO_DETCO = 0;
              }else{
                $PRECIO_NETO_DETCO = $value['PRECIO_NETO_DETCO'];
              }
              $PRECIO_NETO_UNIT = $value['PRECIO_NETO_UNIT'];
              if($value['FECHA_RECEPCION_DETCO'] == null){
                $FECHA_RECEPCION_DETCO = 0;
              }else{
                $FECHA_RECEPCION_DETCO = $value['FECHA_RECEPCION_DETCO'];
              }
              $ESTADO_COTIZACION = "N";

              $cotizacion->DetalleCotizacion()->attach($value['ID_NOTA_PED'],
                                            ['CANTIDAD_ENTREGAR_DETCO'=> $CANTIDAD_ENTREGAR_DETCO,
                                             'PRECIO_NETO_DETCO'=> $PRECIO_NETO_DETCO,
                                             'FECHA_RECEPCION_DETCO'=> $FECHA_RECEPCION_DETCO,
                                             'ESTADO_COTIZACION'=> $ESTADO_COTIZACION,
                                             'PRECIO_NETO_UNIT' => $PRECIO_NETO_UNIT
                                            ]);
            }
        }
        return response()->json(['code' => 200, 'message' => 'Cotizacion editada con exito'],200);
    }

    public function generarOrdenDeCompra(Request $request) {
      $orden = new OrdenCompra();
      date_default_timezone_set('America/Santiago');
      $now = new \DateTime();
      $orden->ID_PROY = $request->input('ID_PROY');
      $orden->ID_EMPR = $request->input('ID_EMPR');
      $orden->FECHA_EMISION_ORDENC = $now;
      $orden->FECHA_PAGO = $request->input('FECHA_PAGO');
      $orden->MONTO_TOTAL = $request->input('MONTO_TOTAL');
      // $orden->CARGO_APRUEBA = $request->input('cargo_autoriza');
      $orden->save();
      $ids = $request->input('idc');
      $idsD = $request->input('idsDesc');
      $idsNP = $request->input('idsNP');
      $orden->detalle()->sync($ids);
      DetalleCotizacion::whereIn('ID_DETA_COTI',$ids)->update(['ESTADO_COTIZACION'=>'O']);
      DetalleCotizacion::whereIn('ID_DETA_COTI',$idsD)->update(['ESTADO_COTIZACION'=>'D']);
      NotaPedido::whereIn('ID_NOTA_PED',$idsNP)->update(['ESTADO_NOTA_PED'=>'O']);
      $usr = Usuario::find($request->input('id_usu'));
      $flujo = new FlujoOrdenCompra();
      $flujo->TIPO_FLUJO = 'C';
      $flujo->OBSERVACION_FLUJO = $request->input('obs');
      $flujo->FECHA_FLUJO = $now;
      $flujo->ORDEN_COMPRA_ID_ORDENC = $orden->ID_ORDENC;
      $flujo->USUARIO_ID_USU = $request->input('id_usu');
      $flujo->NOMBRE_USU = $usr->dueno?($usr->dueno->NOMBRE_DUE.' '.$usr->dueno->APELLIDO_DUE):($usr->empleado->NOMBRE_EMP.' '.$usr->empleado->APELLIDO_EMP);
      $flujo->CARGO_USU = $usr->dueno?($usr->dueno->cargo->NOMBRE_CARG):($usr->empleado->cargo->NOMBRE_CARG);
      $flujo->save();
      if(Dueno::where('USUARIO_ID_USU',$request->input('id_usu'))->first()){
        $orden->ESTADO_ORDEN = 'A';
        $orden->save();
      }
      foreach ($idsNP as $key => $value) {
        $flujo2 = new FlujoNotaPedido();
        $flujo2->FECHA_FLUJO_NOTA = $now;
        $flujo2->TIPO_FLUJO_NOTA = 'O';
        $flujo2->OBSERVACION_FLUJO_NOTA = 'Nota de pedido asignada a una orden de compra';
        $flujo2->NOTA_PEDIDO_ID_NOTA_PED = $value;
        $flujo2->USUARIO_ID_USU = $request->input('id_usu');
        $flujo2->NOMBRE_USU = $usr->dueno?($usr->dueno->NOMBRE_DUE.' '.$usr->dueno->APELLIDO_DUE):($usr->empleado->NOMBRE_EMP.' '.$usr->empleado->APELLIDO_EMP);
        $flujo2->CARGO_USU = $usr->dueno?($usr->dueno->cargo->NOMBRE_CARG):($usr->empleado->cargo->NOMBRE_CARG);
        $flujo2->save();
      }
      if(filter_var($request->input('aprueba'), FILTER_VALIDATE_BOOLEAN)) {
        $req = new \Illuminate\Http\Request();
        $req->replace([
          'estado' => 'A',
          'obs' => 'OC aprobada automaticamente',
          'id_usu' => $request->input('id_usu'),
          'PROY' => $request->input('ID_PROY')
        ]);
        $this->aprobarRechazarOrdenCompra($req, $orden->ID_ORDENC);
      }
      else {
        $orden->CARGO_APRUEBA = $request->input('cargo_autoriza');
        $orden->save();
        $flujo = new FlujoOrdenCompra();
        $flujo->TIPO_FLUJO = 'SA';
        $flujo->OBSERVACION_FLUJO = 'Solicito aprobacion a cargo superior';
        $flujo->FECHA_FLUJO = $now;
        $flujo->ORDEN_COMPRA_ID_ORDENC = $orden->ID_ORDENC;
        $flujo->USUARIO_ID_USU = $request->input('id_usu');
        $flujo->NOMBRE_USU = $usr->dueno?($usr->dueno->NOMBRE_DUE.' '.$usr->dueno->APELLIDO_DUE):($usr->empleado->NOMBRE_EMP.' '.$usr->empleado->APELLIDO_EMP);
        $flujo->CARGO_USU = $usr->dueno?($usr->dueno->cargo->NOMBRE_CARG):($usr->empleado->cargo->NOMBRE_CARG);
        $flujo->save();
        $flujo = new FlujoOrdenCompra();
        $flujo->TIPO_FLUJO = 'P';
        $flujo->OBSERVACION_FLUJO = '';
        $flujo->FECHA_FLUJO = $now;
        $flujo->ORDEN_COMPRA_ID_ORDENC = $orden->ID_ORDENC;
        // $flujo->USUARIO_ID_USU = null;
        // $flujo->NOMBRE_USU = $usr->dueno?($usr->dueno->NOMBRE_DUE.' '.$usr->dueno->APELLIDO_DUE):($usr->empleado->NOMBRE_EMP.' '.$usr->empleado->APELLIDO_EMP);
        $flujo->CARGO_USU = Cargo::find($orden->CARGO_APRUEBA)->NOMBRE_CARG;
        $flujo->save();
      }
      return response()->json('Orden generada');
    }

    public function historialOrdenCompra($ID_PROY){
      $hist =  OrdenCompra::join('EMPRESA','EMPRESA.ID_EMPR','ORDEN_COMPRA.ID_EMPR')
        ->where('ID_PROY',$ID_PROY)
        ->selectRaw('ORDEN_COMPRA.*, EMPRESA.NOMBRE_EMPR')
        ->with('detalleFecha')
        ->get();

      return $hist;
    }

    public function buscarHistorial(Request $request,$ID_PROY){
      $fechaInicio = null;
      $fechaFin = null;
      $palabra = null;
      $estado = null;
      $nrFact = null;
      $por_pagina = $request->input('POR_PAGINA');
      if($request->input('palabra')){ $palabra = $request->input('palabra'); }
      if($request->input('fechaInicio')){ $fechaInicio = $request->input('fechaInicio'); }
      if($request->input('fechaFin')){ $fechaFin = $request->input('fechaFin'); }
      if($request->input('estado')) { $estado = $request->input('estado'); }
      if($request->input('nrFact')) { $nrFact = $request->input('nrFact'); }
      if($nrFact) {
        $hist =  OrdenCompra::join('EMPRESA','EMPRESA.ID_EMPR','ORDEN_COMPRA.ID_EMPR')
          ->where('ID_PROY',$ID_PROY)->selectRaw('ORDEN_COMPRA.*, EMPRESA.NOMBRE_EMPR, EMPRESA.RUT_EMPR')
          ->where(function ($query) use ($palabra){
            if($palabra != null){ $query->where('EMPRESA.NOMBRE_EMPR','like','%'.$palabra.'%'); }
          })->where(function ($query) use ($fechaInicio,$fechaFin){
            if($fechaInicio != null && $fechaFin != null){ $query->whereBetween('ORDEN_COMPRA.FECHA_EMISION_ORDENC', [$fechaInicio, $fechaFin]); }
          })->where(function ($query) use ($estado){
            if($estado != null){ $query->where('ESTADO_ORDEN',$estado); }
          })->whereHas('cfacturas', function ($q) use ($nrFact) {
            $q->whereHas('factura', function ($h) use ($nrFact) { $h->where('NRO_FACT',$nrFact); });
          })->withCount('cfacturas')->orderBy('FECHA_EMISION_ORDENC','desc')->paginate($por_pagina);
      }
      else {
        $hist =  OrdenCompra::join('EMPRESA','EMPRESA.ID_EMPR','ORDEN_COMPRA.ID_EMPR')
          ->where('ID_PROY',$ID_PROY)->selectRaw('ORDEN_COMPRA.*, EMPRESA.NOMBRE_EMPR, EMPRESA.RUT_EMPR')
          ->where(function ($query) use ($palabra){
            if($palabra != null){ $query->where('EMPRESA.NOMBRE_EMPR','like','%'.$palabra.'%'); }
          })->where(function ($query) use ($fechaInicio,$fechaFin){
            if($fechaInicio != null && $fechaFin != null){ $query->whereBetween('ORDEN_COMPRA.FECHA_EMISION_ORDENC', [$fechaInicio, $fechaFin]); }
          })->where(function ($query) use ($estado){
            if($estado != null){ $query->where('ESTADO_ORDEN',$estado); }
          })->withCount('cfacturas')->orderBy('FECHA_EMISION_ORDENC','desc')->paginate($por_pagina);
      }

      foreach ($hist as $key => $value) {
        $sum1 = $value->detalle()->selectRaw(DB::raw("sum((CANTIDAD_RECEPCION*100)/CANTIDAD_ENTREGAR_DETCO) as suma"))->first()->suma;
        $sum2 = $value->detalle()->count();
        $value->porc_recep = $sum1/$sum2;
        $value->mont_fact = $value->facturas()->sum('MONTO_TOTAL');
      }
      return $hist;

    }

    public function getOrdenesCompraEspera($ID_PROY) {
      return OrdenCompra::where('ID_PROY',$ID_PROY)
        ->where('ESTADO_ORDEN','E')
        ->with(['empresa'])
        ->get();
    }

    public function getDetalleOrdenCompraEspera($ID_ORDEN) {
      $orden = OrdenCompra::find($ID_ORDEN);
      $orden->detalle;
      $orden->empresa;
      $orden->obs = '';
      if($orden->ESTADO_ORDEN == 'E') {
        $orden->ult_fluj = FlujoOrdenCompra::where('ORDEN_COMPRA_ID_ORDENC',$ID_ORDEN)
          ->where('TIPO_FLUJO','P')
          ->orderBy('ID_FLUJO','desc')->first()->ID_FLUJO;
      }
      $orden->puedeAnular = $orden->cfacturas()->first()?false:true;
      $orden->total_fact = $orden->facturas()->sum('MONTO_TOTAL');
      foreach ($orden->detalle as $key => $value) {
        $value->area = Area::join('SUBAREA','SUBAREA.AREA_ID_AREA','ID_AREA')
                    ->join("DETALLE_SAREA_PROY",'DETALLE_SAREA_PROY.SUBAREA_ID_SAREA','SUBAREA.ID_SAREA')
                    ->select('AREA.*')->groupBy('AREA.ID_AREA')
                    ->where("DETALLE_SAREA_PROY.PROYECTO_ID_PROY",$value->NotaPedido->PROYECTO_ID_PROY)->get();
        foreach ($value->area as $key => $ar) {
          $ar->CANTIDAD_TOTAL;
          $ar->subarea = Subarea::where('AREA_ID_AREA','=',$ar->ID_AREA)->get();
          foreach ($ar->subarea as $key => $sub) {
             $CANTIDAD_DNP = $value->NotaPedido->DetalleNotaPedido()->where('SUBAREA_ID_SAREA',$sub->ID_SAREA)->value('CANTIDAD_DNP');
             // return $detalle;
             $sub->cantidad = $CANTIDAD_DNP;
             $ar->CANTIDAD_TOTAL = $ar->CANTIDAD_TOTAL + $sub->cantidad;
          }
        }
      }
      return $orden;
    }

    public function solicitarAprobacionOC(Request $request) {
      $carg_apr = $request->input('cargo_aprueba');
      $id_orden = $request->input('ID_ORDENC');
      $id_usu = $request->input('ID_USU');
      $obs = $request->input('obs');
      $ult_fluj = $request->input('ult_fluj');
      date_default_timezone_set('America/Santiago');
      $now = new \DateTime();
      OrdenCompra::where('ID_ORDENC',$id_orden)->update(['CARGO_APRUEBA'=>$carg_apr]);
      $usr = Usuario::find($id_usu);
      $flujo = FlujoOrdenCompra::find($ult_fluj);
      $flujo->TIPO_FLUJO = 'SA';
      $flujo->OBSERVACION_FLUJO = $obs;
      $flujo->FECHA_FLUJO = $now;
      $flujo->ORDEN_COMPRA_ID_ORDENC = $id_orden;
      $flujo->USUARIO_ID_USU = $id_usu;
      $flujo->NOMBRE_USU = $usr->dueno?($usr->dueno->NOMBRE_DUE.' '.$usr->dueno->APELLIDO_DUE):($usr->empleado->NOMBRE_EMP.' '.$usr->empleado->APELLIDO_EMP);
      $flujo->CARGO_USU = $usr->dueno?($usr->dueno->cargo->NOMBRE_CARG):($usr->empleado->cargo->NOMBRE_CARG);
      $flujo->save();

      $flujo = new FlujoOrdenCompra();
      $flujo->TIPO_FLUJO = 'P';
      $flujo->OBSERVACION_FLUJO = '';
      $flujo->FECHA_FLUJO = $now;
      $flujo->ORDEN_COMPRA_ID_ORDENC = $id_orden;
      // $flujo->USUARIO_ID_USU = null;
      // $flujo->NOMBRE_USU = $usr->dueno?($usr->dueno->NOMBRE_DUE.' '.$usr->dueno->APELLIDO_DUE):($usr->empleado->NOMBRE_EMP.' '.$usr->empleado->APELLIDO_EMP);
      $flujo->CARGO_USU = Cargo::find($carg_apr)->NOMBRE_CARG;
      $flujo->save();
      return response()->json(['msg'=>'Aprobacion solicitada']);
    }

    public function aprobarRechazarOrdenCompra(Request $request, $ID_ORDEN) {

      OrdenCompra::where('ID_ORDENC',$ID_ORDEN)->update(['ESTADO_ORDEN'=>$request->input('estado')]);

      date_default_timezone_set('America/Santiago');
      $now = new \DateTime();
      $usr = Usuario::find($request->input('id_usu'));
      $ult_fluj = $request->input('ult_fluj');
      $flujo = new FlujoOrdenCompra();
      if($ult_fluj) { $flujo = FlujoOrdenCompra::find($ult_fluj); }
      $flujo->TIPO_FLUJO = $request->input('estado');
      $flujo->OBSERVACION_FLUJO = $request->input('obs');
      $flujo->FECHA_FLUJO = $now;
      $flujo->ORDEN_COMPRA_ID_ORDENC = $ID_ORDEN;
      $flujo->USUARIO_ID_USU = $request->input('id_usu');
      $flujo->NOMBRE_USU = $usr->dueno?($usr->dueno->NOMBRE_DUE.' '.$usr->dueno->APELLIDO_DUE):($usr->empleado->NOMBRE_EMP.' '.$usr->empleado->APELLIDO_EMP);
      $flujo->CARGO_USU = $usr->dueno?($usr->dueno->cargo->NOMBRE_CARG):($usr->empleado->cargo->NOMBRE_CARG);
      $flujo->save();
      $userCrea = FlujoOrdenCompra::where('ORDEN_COMPRA_ID_ORDENC',$ID_ORDEN)->where('TIPO_FLUJO','C')->first()->usuario;
      if($request->input('estado')<>'R'){
        $data = DetalleCotizacion::join('DETALLE_ORDEN_COTIZACION','DETALLE_ORDEN_COTIZACION.DETALLE_COTIZACION_ID_DETA_COTI','=','ID_DETA_COTI')
                                  ->join('DETALLE_NOTA_PEDIDO','DETALLE_NOTA_PEDIDO.NOTA_PEDIDO_ID_NOTA_PED','=','DETALLE_COTIZACION.NOTA_PEDIDO_ID_NOTA_PED')
                                  ->join('NOTA_PEDIDO','NOTA_PEDIDO.ID_NOTA_PED','DETALLE_NOTA_PEDIDO.NOTA_PEDIDO_ID_NOTA_PED')
                                  ->join('MATERIAL','MATERIAL.ID_MATE','NOTA_PEDIDO.MATERIAL_ID_MATE')
                                  ->where('DETALLE_ORDEN_COTIZACION.ORDEN_COMPRA_ID_ORDENC',$ID_ORDEN)
                                  ->select('DETALLE_COTIZACION.*','ID_MATE','DETALLE_NOTA_PEDIDO.*')
                                  ->get()->groupBy('ID_MATE');
        $proyecto = Proyecto::find($request->input('PROY'));
        foreach ($data as $key => $detas) {
          foreach ($detas as $key => $deta) {
            $detalle = $proyecto->DetalleSubarea()->wherePivot('SUBAREA_ID_SAREA',$deta->SUBAREA_ID_SAREA)->first();
            $detalle->pivot->MONTO_PEDIDO += $detas->sum('PRECIO_NETO_DETCO');
            $detalle->pivot->save();
          }
        }
        $orden = OrdenCompra::find($ID_ORDEN);
        $nom = $userCrea->dueno?($userCrea->dueno->NOMBRE_DUE):($userCrea->empleado->NOMBRE_EMP);
        $tlf = $userCrea->dueno?($userCrea->dueno->TELEFONO_DUE):($userCrea->empleado->TELEFONO_EMP);
        $reply = $userCrea->dueno?false:true;
        $correos = [];
        if($reply){
          $cargoJefe = $usr->dueno?($usr->dueno->cargo):($usr->empleado->cargo->jefe);
          $correos[] = (object)array('email'=>$userCrea->EMAIL_USU,'name'=>$nom);
          if($cargoJefe->dueno) {
            $correos[] = (object)array('email'=>$cargoJefe->dueno->usuario->EMAIL_USU,'name'=>$cargoJefe->dueno->NOMBRE_DUE);
          }
          else {
            foreach ($cargoJefe->empleados as $key => $value) {
              $correos[] = (object)array('email'=>$value->usuario->EMAIL_USU,'name'=>$value->NOMBRE_EMP);
            }
          }
        }
        $this->enviarCorreoProveedor($orden,$proyecto,$orden->empresa,$nom,$userCrea->EMAIL_USU,$tlf,$correos);
      }
      else {
         $ord = OrdenCompra::find($ID_ORDEN);
         foreach ($ord->detalle as $key => $deta) {
           $deta->ESTADO_COTIZACION = 'E';
           $deta->save();
           NotaPedido::where('ID_NOTA_PED',$deta->NOTA_PEDIDO_ID_NOTA_PED)->update(['ESTADO_NOTA_PED'=>'A']);
         }
      }
    }

    public function anularOC(Request $request) {
      $ord = OrdenCompra::find($request->input('ID_ORDEN'));
      foreach ($ord->detalle as $key => $deta) {
        $deta->ESTADO_COTIZACION = 'E';
        $deta->save();
        NotaPedido::where('ID_NOTA_PED',$deta->NOTA_PEDIDO_ID_NOTA_PED)->update(['ESTADO_NOTA_PED'=>'A']);
      }
      $ord->ESTADO_ORDEN = 'AN';
      $ord->save();
      date_default_timezone_set('America/Santiago');
      $now = new \DateTime();
      $usr = Usuario::find($request->input('id_usu'));
      $flujo = new FlujoOrdenCompra();
      $flujo->TIPO_FLUJO = 'AN';
      $flujo->OBSERVACION_FLUJO = $request->input('obs');
      $flujo->FECHA_FLUJO = $now;
      $flujo->ORDEN_COMPRA_ID_ORDENC = $ord->ID_ORDENC;
      $flujo->USUARIO_ID_USU = $usr->ID_USU;
      $flujo->NOMBRE_USU = $usr->dueno?($usr->dueno->NOMBRE_DUE.' '.$usr->dueno->APELLIDO_DUE):($usr->empleado->NOMBRE_EMP.' '.$usr->empleado->APELLIDO_EMP);
      $flujo->CARGO_USU = $usr->dueno?($usr->dueno->cargo->NOMBRE_CARG):($usr->empleado->cargo->NOMBRE_CARG);
      $flujo->save();
      return response()->json(['message'=>'Orden de compra anulada'],200);
    }

    private function enviarCorreoProveedor($orden,$proyecto,$proveedor,$nombre,$CORR_CREA,$telf,$reply=false) {
      // correo
      date_default_timezone_set('America/Santiago');
      $correo_asignado = $proyecto->DetalleEmpleados()->wherePivot('RECIBE_FACTURA',1)->first()->usuario->EMAIL_USU;
      $constructora = $proyecto->constructora;
      $fact = $constructora->facturacion;
      $dueno = $constructora->dueno;
      $detalle = collect();
      $total = 0;
      foreach ($orden->detalle as $key => $deta) {
        $det = (object)array(
          'NOM_MATE' => $deta->Material()->first()->NOMBRE_MATE,
          'UNI_MATE' => $deta->Material()->first()->Unidad->NOMBRE_UNID,
          'CAN_MATE' => $deta->CANTIDAD_ENTREGAR_DETCO,
          'PRE_UNIT' => number_format($deta->PRECIO_NETO_UNIT,0,',','.'),
          'PRE_TOTL' => number_format($deta->PRECIO_NETO_UNIT * $deta->CANTIDAD_ENTREGAR_DETCO,0,',','.'),
          'FECH_ENT' => date('d-m-Y',strtotime($deta->FECHA_RECEPCION_DETCO))
        );
        $total += $deta->PRECIO_NETO_UNIT * $deta->CANTIDAD_ENTREGAR_DETCO;
        $detalle->push($det);
      }
      $FEC_ENT = date('d-m-Y',strtotime($detalle->max('FECH_ENT')));
      $cargos = collect();
      foreach ($orden->Flujo->unique('USUARIO_ID_USU') as $key => $value) {
        $cargo = count($value->Empleado)>0?$value->Empleado()->first()->cargo->NOMBRE_CARG:$value->Dueno()->first()->cargo->NOMBRE_CARG;
        if(!$cargos->search($cargo, true)) { $cargos->push($cargo); }
      }
      $data = array(
        'NRO_ORDEN' => $orden->ID_ORDENC,
        'NOM_CONS' => $constructora->NOMBRE_CONS,
        'RUT_CONS' => $fact->RUT_FACT,
        'DIR_CONS' => $fact->DIRECCION_FACT,
        'COR_USU' => $CORR_CREA,
        'TEL_USU' => $telf,
        'FEC_EMI' => date('d-m-Y'),
        'RAZ_PRO' => $proveedor->RAZON_SOCIAL_EMPR,
        'RUT_PRO' => $proveedor->RUT_EMPR,
        'FEC_PAG' => date('d-m-Y',strtotime(substr($orden->FECHA_PAGO,0,10))),
        'FEC_ENT' => date('d-m-Y',strtotime($FEC_ENT)),
        'NOM_PRY' => $proyecto->NOMBRE_PROY,
        'DIR_DES' => $proyecto->DIRECCION,
        'RUT_PRY' => $proyecto->RUT,
        'RAZ_PRY' => $proyecto->RAZON,
        'DETALLE' => $detalle,
        'SUB_TOT' => number_format($total,0,',','.'),
        'IVA_INP' => number_format($total*0.19,0,',','.'),
        'TOT_ORD' => number_format($total*1.19,0,',','.'),
        'CAR_AUT' => $cargos,
        'CORR_AS' => $correo_asignado
      );
      Mail::send('orden-compra', $data, function($message) use ($CORR_CREA, $nombre,$reply) {
        foreach ($reply as $key => $value) {
          $message->to($value->email,$value->name)->subject('Orden de compra a proveedor');
        }
        $message->from('no-reply@budgeter.cl','Budgeter');
      });
    }

    public function verDetalleRecepcion($ID_RECE_MAT) {
      $rece = RecepcionMaterial::find($ID_RECE_MAT);
      if($rece->TIPO_RECE == 'O') {
        $rece->OC;
        foreach ($rece->detalle as $key => $value) {
          $areas = Area::join('SUBAREA','SUBAREA.AREA_ID_AREA','ID_AREA')
            ->join("DETALLE_SAREA_PROY",'DETALLE_SAREA_PROY.SUBAREA_ID_SAREA','SUBAREA.ID_SAREA')
            ->where("DETALLE_SAREA_PROY.PROYECTO_ID_PROY",'=',$rece->PROYECTO_ID_PROY)
            ->select('AREA.*')->groupBy('AREA.ID_AREA')->get();

          if($areas)$value->area =   $areas;

          foreach ($value->detalleC->NotaPedido->flujo as $key => $val) {
            if($val->TIPO_USU == 'E'){
              $val->empleado = Empleado::where('USUARIO_ID_USU',$val->USUARIO_ID_USU)
                ->join('CARGO','CARGO.ID_CARG','CARGO_ID_CARG')->first();
            }else{
              $val->dueno = Dueno::where('USUARIO_ID_USU',$val->USUARIO_ID_USU)
                ->join('CARGO','CARGO.ID_CARG','CARGO_ID_CARG')->first();
            }

            $nota = NotaPedido::find($val->NOTA_PEDIDO_ID_NOTA_PED);
            foreach ($value->area as $key => $ar) {
              $ar->CANTIDAD_TOTAL = 0;
              $ar->subarea = Subarea::where('AREA_ID_AREA','=',$ar->ID_AREA)->get();
              foreach ($ar->subarea as $key => $sub) {
                $CANTIDAD_DNP = $nota->DetalleNotaPedido()->where('SUBAREA_ID_SAREA',$sub->ID_SAREA)->value('CANTIDAD_DNP');
                $sub->cantidad = $CANTIDAD_DNP;
                $ar->CANTIDAD_TOTAL = $ar->CANTIDAD_TOTAL + $sub->cantidad;
              }
            }
          }
        }
      }
      else {
        $rece->detalle;
        $rece->DUI;
      }
      return $rece;
    }

    public function cargarDatosOCAprobadas($ID_ORDEN){
      $oc = OrdenCompra::where('ID_ORDENC',$ID_ORDEN)
                       ->leftjoin('EMPRESA','EMPRESA.ID_EMPR','=','ORDEN_COMPRA.ID_EMPR')
                       ->selectRaw('ORDEN_COMPRA.*,EMPRESA.NOMBRE_EMPR')
                       ->first();
      // return $oc;

      $detalle = OrdenCompra::where('ID_ORDENC',$ID_ORDEN)
                              ->join('DETALLE_ORDEN_COTIZACION','DETALLE_ORDEN_COTIZACION.ORDEN_COMPRA_ID_ORDENC','ID_ORDENC')
                              ->join('DETALLE_COTIZACION','ID_DETA_COTI','DETALLE_ORDEN_COTIZACION.DETALLE_COTIZACION_ID_DETA_COTI')
                              ->join('COTIZACION','COTIZACION.ID_COTI','DETALLE_COTIZACION.COTIZACION_ID_COTI')
                              ->join('NOTA_PEDIDO','NOTA_PEDIDO.ID_NOTA_PED','DETALLE_COTIZACION.NOTA_PEDIDO_ID_NOTA_PED')
                              ->join('MATERIAL','MATERIAL.ID_MATE','.MATERIAL_ID_MATE')
                              ->join('SUBCATEGORIA','SUBCATEGORIA.ID_SUBC','MATERIAL.SUBCATEGORIA_ID_SUBC')
                              ->join('UNIDAD_MEDIDA','UNIDAD_MEDIDA.ID_UNID','MATERIAL.UNIDAD_MEDIDA_ID_UNID')
                              ->selectRaw('DETALLE_COTIZACION.ID_DETA_COTI,MATERIAL.NOMBRE_MATE,SUBCATEGORIA.NOMBRE_SUBC,
                                           UNIDAD_MEDIDA.NOMBRE_UNID,DETALLE_COTIZACION.CANTIDAD_ENTREGAR_DETCO,
                                           DETALLE_COTIZACION.FECHA_RECEPCION_DETCO,DETALLE_COTIZACION.CANTIDAD_RECEPCION,
                                           MATERIAL.ID_MATE,DETALLE_COTIZACION.NOTA_PEDIDO_ID_NOTA_PED')
                              ->get();
      foreach ($detalle as $key => $value) {
        $value->CANTIDAD_RECIBIR = 0;
        $value->detalle_flujo = DetalleCotizacion::join('FLUJO_NOTA_PEDIDO','FLUJO_NOTA_PEDIDO.NOTA_PEDIDO_ID_NOTA_PED','DETALLE_COTIZACION.NOTA_PEDIDO_ID_NOTA_PED')
                                                 ->join('USUARIO','USUARIO.ID_USU','FLUJO_NOTA_PEDIDO.USUARIO_ID_USU')
                                                 ->where('FLUJO_NOTA_PEDIDO.NOTA_PEDIDO_ID_NOTA_PED',$value['NOTA_PEDIDO_ID_NOTA_PED'])
                                                 ->selectRaw('FLUJO_NOTA_PEDIDO.*,USUARIO.TIPO_USU')->groupBy('ID_FLUJO_NOTA')->get();
      $areas =  Area::join('SUBAREA','SUBAREA.AREA_ID_AREA','ID_AREA')
                  ->join("DETALLE_SAREA_PROY",'DETALLE_SAREA_PROY.SUBAREA_ID_SAREA','SUBAREA.ID_SAREA')
                  ->where("DETALLE_SAREA_PROY.PROYECTO_ID_PROY",'=',$oc->ID_PROY)
                  ->select('AREA.*')
                  ->groupBy('AREA.ID_AREA')
                  ->get();
      if($areas)$value->area =   $areas;


               foreach ($value->detalle_flujo as $key => $val) {
                 if($val->TIPO_USU == 'E'){
                   $val->empleado = Empleado::where('USUARIO_ID_USU',$val->USUARIO_ID_USU)
                                             ->join('CARGO','CARGO.ID_CARG','CARGO_ID_CARG')->first();
                 }else{
                   $val->dueno = Dueno::where('USUARIO_ID_USU',$val->USUARIO_ID_USU)
                                       ->join('CARGO','CARGO.ID_CARG','CARGO_ID_CARG')->first();
                 }

                 $nota = NotaPedido::find($val->NOTA_PEDIDO_ID_NOTA_PED);
                 foreach ($value->area as $key => $ar) {
                   $ar->CANTIDAD_TOTAL = 0;
                   $ar->subarea = Subarea::where('AREA_ID_AREA','=',$ar->ID_AREA)
                                          ->get();
                              foreach ($ar->subarea as $key => $sub) {
                                $CANTIDAD_DNP = $nota->DetalleNotaPedido()->where('SUBAREA_ID_SAREA',$sub->ID_SAREA)->value('CANTIDAD_DNP');
                                $sub->cantidad = $CANTIDAD_DNP;
                                $ar->CANTIDAD_TOTAL = $ar->CANTIDAD_TOTAL + $sub->cantidad;
                              }
                 }
               }

      }

      // return $detalle_flujo;
      return response()->json(['code' => 200, 'message' => 'Datos obtenido con exito',
                                              'orden_compra'=>$oc,
                                              'detalle'=>$detalle],200);

    }

    public function eliminarCotizacion($ID_COTI) {
      Cotizacion::find($ID_COTI)->delete();
      return response()->json('Se elimino la cotizacion');
    }

    public function cerrarOC($ID_ORDEN) {
      OrdenCompra::where('ID_ORDENC',$ID_ORDEN)->update(['ESTADO_ORDEN' => 'CE']);
      return response()->json(['message'=>'Orden de compra cerrada'],200);
    }

    public function abrirOC($ID_ORDEN) {
      OrdenCompra::where('ID_ORDENC',$ID_ORDEN)->update(['ESTADO_ORDEN' => 'A']);
      return response()->json(['message'=>'Orden de compra abierta'],200);
    }

    public function facturasOC($ID_ORDEN) {
      $ids = OrdenCompra::find($ID_ORDEN)->cfacturas()->pluck('PAGO_ID_PAGO');
      $facturas = Pago::whereIn('ID_PAGO',$ids)->with(['detalles','empresa','notas_credito'])->withCount('ordenDist')->get();
      $totales = [
        'total_neto' => $facturas->sum('MONTO_NETO'),
        'total_iva' => $facturas->sum('MONTO_NETO')*1.19,
      ];
      return response()->json(['message'=>'Facturas asociadas','data'=>$facturas,'totales'=>$totales],200);
    }

}
