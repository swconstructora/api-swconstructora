<?php

namespace App\Http\Controllers;

use App\OrdenCompra;
use App\Pago;
use App\FlujoPago;
use App\FlujoOrdenCompra;
use App\Proyecto;
use App\NotaCredito;
use App\Empresa;
use App\Cartola;
use App\Consolidado;
use App\Dueno;
use App\Empleado;
use App\Area;
use App\Subarea;
use App\DetalleCotizacion;
use App\FacturaProveedor;
use App\Gasto;
use App\DetalleFacturaProveedor;
use App\Usuario;
// use App\Subcontratar;
use App\Exports\PagosExcelExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use DB;
use PDF;

class ProyectoPagoController extends Controller
{
    public function cargarHistorialOrdenesCompraFactura(Request $request, $ID_PROY) {
      $nOrd = $request->input('nOrd');
      $Prov = $request->input('Prov');
      $FecI = $request->input('FecI');
      $FecT = $request->input('FecT');
      $por_pagina = $request->input('POR_PAGINA');

      $ordenes = OrdenCompra::select('ID_ORDENC','FECHA_EMISION_ORDENC','NOMBRE_EMPR','RUT_EMPR','MONTO_TOTAL','FECHA_PAGO','ESTADO_ORDEN')
                            ->join('EMPRESA','EMPRESA.ID_EMPR','ORDEN_COMPRA.ID_EMPR')
                            ->where('ID_PROY',$ID_PROY)
                              ->whereIn('ESTADO_ORDEN',['A','F','RP'])
                            ->with(['pago'])
                            ->where(function ($query) use ($nOrd) {
                                if($nOrd) { $query->where('ID_ORDENC',$nOrd); }
                            })->where(function ($query) use ($Prov) {
                                if($Prov) { $query->whereRaw("NOMBRE_EMPR like '%$Prov%'"); }
                            })->where(function ($query) use ($FecI,$FecT) {
                                if($FecI && $FecT) { $query->whereBetween('FECHA_PAGO',$FecI,(new \DateTime($FecT))->format('Y-m-d 23:59:59')); }
                            })->orderBy('FECHA_EMISION_ORDENC', 'desc')->paginate($por_pagina);
      return $ordenes;
    }

    public function detalleOC($ID_ORDENC) {
      $orden = OrdenCompra::find($ID_ORDENC);
      $orden->detalle;
      $orden->empresa;
      $orden->pago;
      $orden->MONTO_TOTAL_IVA = intval($orden->MONTO_TOTAL) + (intval($orden->MONTO_TOTAL) * 0.19);
      $orden->MONTO_IVA =intval($orden->MONTO_TOTAL) * 0.19;

      foreach ($orden->detalle as $key => $value) {
        $value->area = Area::join('SUBAREA','SUBAREA.AREA_ID_AREA','ID_AREA')
                    ->join("DETALLE_SAREA_PROY",'DETALLE_SAREA_PROY.SUBAREA_ID_SAREA','SUBAREA.ID_SAREA')
                    ->select('AREA.*')->groupBy('AREA.ID_AREA')
                    ->where("DETALLE_SAREA_PROY.PROYECTO_ID_PROY",$orden->ID_PROY)->get();
        foreach ($value->area as $key => $ar) {
          $ar->CANTIDAD_TOTAL;
          $ar->subarea = Subarea::where('AREA_ID_AREA','=',$ar->ID_AREA)->get();
          foreach ($ar->subarea as $key => $sub) {
             $CANTIDAD_DNP = $value->NotaPedido->DetalleNotaPedido()->where('SUBAREA_ID_SAREA',$sub->ID_SAREA)->value('CANTIDAD_DNP');
             // return $detalle;
             $sub->cantidad = $CANTIDAD_DNP;
             $ar->CANTIDAD_TOTAL = $ar->CANTIDAD_TOTAL + $sub->cantidad;
          }
        }
      }

      return $orden;
    }

    public function guardarDocumentoPago(Request $request){

      $verif = Pago::where('EMPRESA_ID_EMPR',$request->input('ID_EMPR'))
        ->where('NUM_FACTURA',$request->input('NUM_FACTURA'))->first();
      if($verif) {
        return response()->json(['code' => 201, 'message' => 'Este número de factura ya fue registrado con este proveedor'],200);
      }

      $pago = new Pago();
      date_default_timezone_set('America/Santiago');
      $now = new \DateTime();
      $pago->FECHA_CREACION_PAGO = $now;
      $pago->ESTADO_PAGO = 'A';
      $pago->MONTO_NETO = $request->input('MONTO_NETO');
      $pago->OBSERVACION = $request->input('OBSERVACION');
      $pago->MONTO_TOTAL = $request->input('MONTO_TOTAL');
      $pago->APLICA_IVA = 'N';
      $APLICA_IVA = $request->input('APLICA_IVA');
      if($APLICA_IVA){
          $pago->APLICA_IVA = 'S';
          $pago->MONTO_IVA = $request->input('MONTO_IVA');
      }

      $pago->EMPRESA_ID_EMPR = $request->input('ID_EMPR');
      $pago->ORDEN_COMPRA_ID_ORDENC = $request->input('ID_ORDENC');
      $pago->NUM_FACTURA  = $request->input('NUM_FACTURA');
      $pago->save();

      $arch64 = $request->input('ARCHIVO');
      $arch64_nombre = $request->input('NOMBRE_ARCH');
      $arch64_tipo = $request->input('TIPO_ARCH');

      // Procesar base 64
      $archivo = base64_decode(preg_replace('#^data:'.$arch64_tipo.';base64,#i', '', $arch64));
      $pagoEdit = Pago::find($pago->ID_PAGO);
      $orden = OrdenCompra::find($pagoEdit->ORDEN_COMPRA_ID_ORDENC);
      $ID_CONS = Proyecto::find($orden->ID_PROY)->value("CONSTRUCTORA_ID_CONS");

      file_put_contents($arch64_nombre , $archivo);
      $file = fopen($arch64_nombre,'r+');
      Storage::disk(app('App\Entorno')->origen_storage)->put('archivos/cons/'.$ID_CONS.'/proyecto/'.$orden->ID_PROY.'/ordenes/'.$orden->ID_ORDENC.'/pagos/'.$pago->ID_PAGO.'/'.$arch64_nombre,  $file);
      ftruncate($file,0);
      fclose($file);

      // La ruta usa el cargarUrlSubcontratoPago
      $pagoEdit->FACTURA_URL= app('App\Entorno')->route_api.'orden/'.$orden->ID_ORDENC.'/pago/'.$pago->ID_PAGO.'/'.$arch64_nombre;
      $pagoEdit->NOMBRE_FACTURA = $arch64_nombre;
      $pagoEdit->save();


      $flujoPago = new FlujoPago();
      $flujoPago->FECHA_FLUJO_PAGO= $now;
      $flujoPago->TIPO_FLUJO_PAGO = 'A';
      $flujoPago->OBSERVACION_FLUJO_PAGO = $request->input('OBSERVACION');
      $flujoPago->PAGO_ID_PAGO = $pago->ID_PAGO;
      $flujoPago->USUARIO_ID_USU = $request->input('ID_EMP');
      $flujoPago->save();
      return response()->json(['code' => 200, 'message' => 'Pago creado con exito'],200);

    }

    public function cargarUrlOrdenPago($ID_ORDENC,$ID_PAGO,$NOMBRE_ARCH){
        $NOMBRE= trim($NOMBRE_ARCH);
        $orden = OrdenCompra::find($ID_ORDENC);
        $ID_CONS = Proyecto::find($orden->ID_PROY)->value("CONSTRUCTORA_ID_CONS");
        $file =storage_path().'/app/archivos/cons/'.$ID_CONS.'/proyecto/'.$orden->ID_PROY.'/ordenes/'.$ID_ORDENC.'/pagos/'.$ID_PAGO.'/'.$NOMBRE;
        return response()->download($file,$NOMBRE);
    }

    public function agregarNotaCreditoOrdenPago(Request $request){

      $nc = new NotaCredito();
      date_default_timezone_set('America/Santiago');
      $now = new \DateTime();
      $nc->FECHA_CREACION_NOTA_CRED = $now;
      $nc->OBSERVACION = $request->input('OBSERVACION');
      $nc->MONTO_NETO = $request->input('MONTO_NETO');
      $nc->MONTO_TOTAL = $request->input('MONTO_TOTAL');
      $nc->APLICA_IVA = 'N';
      $APLICA_IVA = $request->input('APLICA_IVA');
      if($APLICA_IVA){
          $nc->APLICA_IVA = 'S';
          $nc->MONTO_IVA = $request->input('MONTO_IVA');
      }

      $nc->PAGO_ID_PAGO = $request->input('ID_PAGO');
      // $nc->FACTURA_PROVEEDOR_ID_FACT = $request->input('ID_FACT');
      $nc->NUM_DOCUMENTO  = $request->input('NUM_DOCUMENTO');
      $nc->save();

      $arch64 = $request->input('ARCHIVO');
      $arch64_nombre = $request->input('NOMBRE_ARCH');
      $arch64_tipo = $request->input('TIPO_ARCH');

      if($request->input('ORIGEN')!='F') {
        // Procesar base 64
        $archivo = base64_decode(preg_replace('#^data:'.$arch64_tipo.';base64,#i', '', $arch64));
        $ncEdit = NotaCredito::find($nc->ID_NOTA_CRED);
        $pago = Pago::find($ncEdit->PAGO_ID_PAGO);
        $orden = OrdenCompra::find($pago->ORDEN_COMPRA_ID_ORDENC);
        $ID_CONS = Proyecto::find($orden->ID_PROY)->value("CONSTRUCTORA_ID_CONS");

        file_put_contents($arch64_nombre , $archivo);
        $file = fopen($arch64_nombre,'r+');
        Storage::disk(app('App\Entorno')->origen_storage)->put('archivos/cons/'.$ID_CONS.'/proyecto/'.$orden->ID_PROY.'/ordenes/'.$orden->ID_ORDENC.'/pagos/'.$pago->ID_PAGO.'/notas-credito/'.$nc->ID_NOTA_CRED.'/'.$arch64_nombre,  $file);
        ftruncate($file,0);
        fclose($file);

        // La ruta usa el cargarUrlOrdenNotaCredito
        $ncEdit->NOTA_CRED_URL= app('App\Entorno')->route_api.'orden/'.$orden->ID_ORDENC.'/pago/'.$pago->ID_PAGO.'/nota-credito/'.$nc->ID_NOTA_CRED.'/'.$arch64_nombre;
        $ncEdit->NOMBRE_NOTA_CRED = $arch64_nombre;
        $ncEdit->save();
      }
      else {
        $archivo = base64_decode(preg_replace('#^data:'.$arch64_tipo.';base64,#i', '', $arch64));
        file_put_contents($arch64_nombre , $archivo);
        $file = fopen($arch64_nombre,'r+');
        $ID_PROY = $request->input('ID_PROY');
        $id_cons = Proyecto::find($ID_PROY)->CONSTRUCTORA_ID_CONS;
        Storage::disk(app('App\Entorno')->origen_storage)->put('archivos/cons/'.$id_cons.'/proyecto/'.$ID_PROY.'/facturas/notas-credito/'.$arch64_nombre,  $file);
        ftruncate($file,0);
        fclose($file);
        $nc->NOTA_CRED_URL= app('App\Entorno')->route_api.'proyecto/'.$id_cons.'/'.$ID_PROY.'/nota-credito-descargar/'.$arch64_nombre;
        $nc->NOMBRE_NOTA_CRED = $arch64_nombre;
        $nc->save();
        return response()->json(['code' => 200, 'message' => 'Nota de crédito agregada con exito','data'=>$nc],200);
      }

      return response()->json(['code' => 200, 'message' => 'Nota de crédito agregada con exito'],200);

    }

    public function cargarUrlOrdenNotaCredito($ID_ORDENC,$ID_PAGO,$ID_NOTA_CRED,$NOMBRE_ARCH){
        $NOMBRE= trim($NOMBRE_ARCH);
        $orden = OrdenCompra::find($ID_ORDENC);
        $ID_CONS = Proyecto::find($orden->ID_PROY)->value("CONSTRUCTORA_ID_CONS");
        $file =storage_path().'/app/archivos/cons/'.$ID_CONS.'/proyecto/'.$orden->ID_PROY.'/ordenes/'.$ID_ORDENC.'/pagos/'.$ID_PAGO.'/notas-credito/'.$ID_NOTA_CRED.'/'.$NOMBRE;
        return response()->download($file,$NOMBRE);
    }

    public function cargarUrlFacturaNotaCredito($ID_CONS,$ID_PROY,$NOMBRE_ARCH){
        $NOMBRE= trim($NOMBRE_ARCH);
        $file =storage_path().'/app/archivos/cons/'.$ID_CONS.'/proyecto/'.$ID_PROY.'/facturas/notas-credito/'.$NOMBRE;
        return response()->download($file,$NOMBRE);
    }

    public function cargarPagosEmpresa(Request $request,$ID_PROY){
      $RUT = null;
      if($request->input('RUT')){$RUT = $request->input('RUT');}
      $NOMBRE = null;
      if($request->input('NOMBRE')){$NOMBRE = $request->input('NOMBRE');}
      $FECHAINI = null;
      if($request->input('FECHAINI')){$FECHAINI =$request->input('FECHAINI');}
      $FECHAFIN = null;
      if($request->input('FECHAFIN')){$FECHAFIN = $request->input('FECHAFIN');}

      date_default_timezone_set('America/Santiago');

      $actual = date('Y-m-d');
      $mesSiguiente = date('Y-m-d', strtotime('+1 month'));
      $anio = date('Y',strtotime($mesSiguiente));
      $mes =  date('m',strtotime($mesSiguiente));

      $empresas = Empresa::join('CONSTRUCTORA','CONSTRUCTORA.ID_CONS','=','EMPRESA.CONSTRUCTORA_ID_CONS')
                           ->join('PROYECTO','PROYECTO.CONSTRUCTORA_ID_CONS','=','CONSTRUCTORA.ID_CONS')
                          ->where('PROYECTO.ID_PROY',$ID_PROY)
                          ->where(function ($query) use ($RUT) {
                              if($RUT <> null) { $query->whereRaw("EMPRESA.RUT_EMPR like '%$RUT%'"); }
                          })
                          ->where(function ($query) use ($NOMBRE) {
                              if($NOMBRE <> null) { $query->whereRaw("EMPRESA.RAZON_SOCIAL_EMPR like '%$NOMBRE%'"); }
                          })
                          ->selectRaw('EMPRESA.RAZON_SOCIAL_EMPR,EMPRESA.NOMBRE_EMPR,EMPRESA.RUT_EMPR,EMPRESA.ID_EMPR')
                          ->groupBy('EMPRESA.ID_EMPR')
                          ->get();

      $data =  collect();
      // return $empresas;
      foreach ($empresas as $key => $emp) {
        $emp->TOTAL_FACTURADO = 0;
        $emp->TOTAL_SELECCIONADO= 0;
        $emp->FECHA_VENCIMIENTO= null;
        $emp->TOTAL_GLOBAL= 0;
        $emp->TOTAL_VENCIDAS= 0;
        $emp->SELECCIONADO = false;
        // $emp->CHECKED = false;
        $pertenece = DB::table('DETALLE_CARTOLA')->where('ID_EMPR',$emp->ID_EMPR)->first();
        // return $pertenece;
        // if($pertenece!=null){
          $emp->subcontrata = Pago::join('SUBCONTRATAR','SUBCONTRATAR.ID_SUBCON','=','SUBCONTRATAR_ID_SUBCON')
                              ->join('PROYECTO','PROYECTO.ID_PROY','=','SUBCONTRATAR.PROYECTO_ID_PROY')
                              ->join('EMPRESA','EMPRESA.ID_EMPR','=','SUBCONTRATAR.EMPRESA_ID_EMPR')
                              ->where('EMPRESA.ID_EMPR',$emp->ID_EMPR)
                              ->where('PROYECTO.ID_PROY',$ID_PROY)
                              ->where('PAGO.ESTADO_PAGO','A')
                              ->where(function ($query) use ($FECHAINI,$FECHAFIN) {
                                  if($FECHAINI <> null && $FECHAFIN <> null) {
                                    $query->whereBetween('PAGO.FECHA_CREACION_PAGO',[(new \DateTime($FECHAINI))->format('Y-m-d 00:00:00'),(new \DateTime($FECHAFIN))->format('Y-m-d 23:59:59')]);
                                  }
                              })
                              ->leftJoin('NOTA_CREDITO','NOTA_CREDITO.PAGO_ID_PAGO','PAGO.ID_PAGO')
                              ->selectRaw('PAGO.*,IFNULL(SUM(NOTA_CREDITO.MONTO_NETO), 0)as MONTO_NETO_NCRED,
                              (PAGO.MONTO_NETO - IFNULL(SUM(NOTA_CREDITO.MONTO_NETO), 0)) as MONTO_APROBADO_NETO,
                              IF(PAGO.APLICA_IVA = "S", (PAGO.MONTO_TOTAL - IFNULL(SUM(NOTA_CREDITO.MONTO_TOTAL), 0)), "EXENTA") as MONTO_APROBADO_IVA,EMPRESA.ID_EMPR')
                              ->groupBy('PAGO.ID_PAGO')
                              ->get();
            // obtener fecha vencimiento subcontrta
            if(count($emp->subcontrata)>0)$emp->FECHA_VENCIMIENTO = $emp->subcontrata->min('FECHA_CREACION_PAGO');
            // obtener TOTAL_FACTURADO
            foreach ($emp->subcontrata as $key => $val) {
                $val->SELECCIONADO = false;
                if($val->APLICA_IVA == 'S'){
                  $emp->TOTAL_FACTURADO = $emp->TOTAL_FACTURADO  + $val->MONTO_APROBADO_IVA ;
                }else{
                  $emp->TOTAL_FACTURADO = $emp->TOTAL_FACTURADO  + $val->MONTO_APROBADO_NETO ;
                }
            }

            $global_subc = Pago::join('SUBCONTRATAR','SUBCONTRATAR.ID_SUBCON','=','SUBCONTRATAR_ID_SUBCON')
                                ->join('PROYECTO','PROYECTO.ID_PROY','=','SUBCONTRATAR.PROYECTO_ID_PROY')
                                ->join('EMPRESA','EMPRESA.ID_EMPR','=','SUBCONTRATAR.EMPRESA_ID_EMPR')
                                ->where('EMPRESA.ID_EMPR',$emp->ID_EMPR)
                                ->where('PROYECTO.ID_PROY',$ID_PROY)
                                ->where('PAGO.ESTADO_PAGO','A')
                                ->leftJoin('NOTA_CREDITO','NOTA_CREDITO.PAGO_ID_PAGO','PAGO.ID_PAGO')
                                ->selectRaw('PAGO.*,IFNULL(SUM(NOTA_CREDITO.MONTO_NETO), 0)as MONTO_NETO_NCRED,
                                (PAGO.MONTO_NETO - IFNULL(SUM(NOTA_CREDITO.MONTO_NETO), 0)) as MONTO_APROBADO_NETO,
                                IF(PAGO.APLICA_IVA = "S", (PAGO.MONTO_TOTAL - IFNULL(SUM(NOTA_CREDITO.MONTO_TOTAL), 0)), "EXENTA") as MONTO_APROBADO_IVA,EMPRESA.ID_EMPR')
                                ->groupBy('PAGO.ID_PAGO')
                                ->get();

          if(count($global_subc)<>0){
            foreach ($global_subc as $key => $glo) {
                if(date('Y-m-d',strtotime($glo->FECHA_CREACION_PAGO)) < $actual){
                  if($glo->APLICA_IVA == 'S'){
                    $emp->TOTAL_VENCIDAS = $emp->TOTAL_VENCIDAS  + $glo->MONTO_APROBADO_IVA ;
                  }else{
                    $emp->TOTAL_VENCIDAS = $emp->TOTAL_VENCIDAS  + $glo->MONTO_APROBADO_NETO ;
                  }
                }
                if($glo->APLICA_IVA == 'S'){
                  $emp->TOTAL_GLOBAL = $emp->TOTAL_GLOBAL  + $glo->MONTO_APROBADO_IVA ;
                }else{
                  $emp->TOTAL_GLOBAL = $emp->TOTAL_GLOBAL  + $glo->MONTO_APROBADO_NETO ;
                }
            }
          }
          $emp->ordenes =  Pago::join('EMPRESA','EMPRESA.ID_EMPR','=','EMPRESA_ID_EMPR')
                              // ->join('ORDEN_COMPRA','ORDEN_COMPRA.ID_ORDENC','=','PAGO.ORDEN_COMPRA_ID_ORDENC')
                              ->where('EMPRESA.ID_EMPR',$emp->ID_EMPR)
                              ->where('PAGO.ESTADO_PAGO','A')
                              ->where('PAGO.ID_PROY',$ID_PROY)
                              ->where(function ($query) use ($FECHAINI,$FECHAFIN) {
                                  if($FECHAINI <> null && $FECHAFIN <> null) {
                                    $query->whereBetween('PAGO.FECHA_CREACION_PAGO',[(new \DateTime($FECHAINI))->format('Y-m-d 00:00:00'),(new \DateTime($FECHAFIN))->format('Y-m-d 23:59:59')]);
                                  }
                              })
                              ->leftJoin('NOTA_CREDITO','NOTA_CREDITO.PAGO_ID_PAGO','PAGO.ID_PAGO')
                              ->selectRaw('PAGO.*,IFNULL(SUM(NOTA_CREDITO.MONTO_NETO), 0)as MONTO_NETO_NCRED,
                              (PAGO.MONTO_NETO - IFNULL(SUM(NOTA_CREDITO.MONTO_NETO), 0)) as MONTO_APROBADO_NETO,
                              IF(PAGO.APLICA_IVA = "S", (PAGO.MONTO_TOTAL - IFNULL(SUM(NOTA_CREDITO.MONTO_TOTAL), 0)), "EXENTA") as MONTO_APROBADO_IVA')
                              ->groupBy('PAGO.ID_PAGO')
                              ->get();
        // obtener fecha vencimiento ordenes
        if(count($emp->ordenes)>0)$emp->FECHA_VENCIMIENTO = $emp->ordenes->min('FECHA_CREACION_PAGO');
        // obtener datos de nota de credito
        foreach ($emp->ordenes as $key => $val) {
          $val->SELECCIONADO = false;
            if($val->APLICA_IVA == 'S'){
              $val->MONTO_APROBADO_IVA = $val->MONTO_APROBADO_NETO*1.19;
              $emp->TOTAL_FACTURADO = $emp->TOTAL_FACTURADO  + $val->MONTO_APROBADO_IVA ;
            }else{
              $emp->TOTAL_FACTURADO = $emp->TOTAL_FACTURADO  + $val->MONTO_APROBADO_NETO ;
            }
          }
          $emp->notas_credito = NotaCredito::join('PAGO','PAGO.ID_PAGO','NOTA_CREDITO.PAGO_ID_PAGO')
            ->whereIn('PAGO.ESTADO_PAGO',['A','C','CE'])->where('PAGO.EMPRESA_ID_EMPR',$emp->ID_EMPR)
            ->where('PAGO.ID_PROY',$ID_PROY)->where('NOTA_ASIGNADA',false)->select('NOTA_CREDITO.*')->get();

          $global_orden =  Pago::join('EMPRESA','EMPRESA.ID_EMPR','=','EMPRESA_ID_EMPR')
                              // ->join('ORDEN_COMPRA','ORDEN_COMPRA.ID_ORDENC','=','PAGO.ORDEN_COMPRA_ID_ORDENC')
                              ->where('EMPRESA.ID_EMPR',$emp->ID_EMPR)
                              ->where('PAGO.ESTADO_PAGO','A')
                              ->leftJoin('NOTA_CREDITO','NOTA_CREDITO.PAGO_ID_PAGO','PAGO.ID_PAGO')
                              ->selectRaw('PAGO.*,IFNULL(SUM(NOTA_CREDITO.MONTO_NETO), 0)as MONTO_NETO_NCRED,
                              (PAGO.MONTO_NETO - IFNULL(SUM(NOTA_CREDITO.MONTO_NETO), 0)) as MONTO_APROBADO_NETO,
                              IF(PAGO.APLICA_IVA = "S", (PAGO.MONTO_TOTAL - IFNULL(SUM(NOTA_CREDITO.MONTO_TOTAL), 0)), "EXENTA") as MONTO_APROBADO_IVA')
                              ->groupBy('PAGO.ID_PAGO')
                              ->get();
          if(count($global_orden) <> 0){
            foreach ($global_orden as $key => $glo_or) {
                if(date('Y',strtotime($glo_or->FECHA_PAGO))==$anio && date('m',strtotime($glo_or->FECHA_PAGO))==$mes){
                  if($glo_or->APLICA_IVA == 'S'){
                    $emp->TOTAL_GLOBAL = $emp->TOTAL_GLOBAL  + $glo_or->MONTO_APROBADO_IVA ;
                  }else{
                    $emp->TOTAL_GLOBAL = $emp->TOTAL_GLOBAL  + $glo_or->MONTO_APROBADO_NETO ;
                  }
                }
                if(date('Y-m-d',strtotime($glo_or->FECHA_PAGO)) < $actual){
                  if($glo_or->APLICA_IVA == 'S'){
                    $emp->TOTAL_VENCIDAS = $emp->TOTAL_VENCIDAS  + $glo_or->MONTO_APROBADO_IVA ;
                  }else{
                    $emp->TOTAL_VENCIDAS = $emp->TOTAL_VENCIDAS  + $glo_or->MONTO_APROBADO_NETO ;
                  }
                }
                if($glo_or->APLICA_IVA == 'S'){
                  $emp->TOTAL_GLOBAL = $emp->TOTAL_GLOBAL  + $glo_or->MONTO_APROBADO_IVA ;
                }else{
                  $emp->TOTAL_GLOBAL = $emp->TOTAL_GLOBAL  + $glo_or->MONTO_NETO ;
                }
                $emp->TOTAL_GLOBAL = $emp->TOTAL_GLOBAL/2;
            }
          }
        // }
      }


      foreach ($empresas as $key => $va) {
        if(count($va->subcontrata) <> 0 || count($va->ordenes) <> 0){
          $pasa = true;
          if(count($va->notas_credito)>0) {
            if(($va->ordenes->sum('MONTO_TOTAL')-$va->notas_credito->sum('MONTO_TOTAL'))<0)$pasa = false;
          }
          if($pasa) {
            $empresa = Empresa::find($va->ID_EMPR);
            $empresa->CONTACTOS = Empresa::join('CONTACTO_EMPRESA','CONTACTO_EMPRESA.EMPRESA_ID_EMPR','=','ID_EMPR')
                                          ->where('ID_EMPR',$va->ID_EMPR)
                                          ->selectRaw('CONTACTO_EMPRESA.*')->get();
            $va->EMPRESA = $empresa;
            $data->push($va);
          }
        }
      }
      $SUMA_TOTAL_GLOBAL=0;
      $SUMA_TOTAL_VENCIDAS=0;
      foreach ($data as $key => $value) {
        $SUMA_TOTAL_GLOBAL = $SUMA_TOTAL_GLOBAL +$value->TOTAL_GLOBAL;
        $SUMA_TOTAL_VENCIDAS = $SUMA_TOTAL_VENCIDAS + $value->TOTAL_VENCIDAS;
      }
      return response()->json(['code' => 200, 'message' => 'Datos compras empresas obtenidos',
                                      'data'=>$data,
                                      'TOTAL_GLOBAL'=>$SUMA_TOTAL_GLOBAL,
                                      'TOTAL_VENCIDAS'=>$SUMA_TOTAL_VENCIDAS],200);
    }

    public function generarCartola(Request $request,$ID_PROY){
      $empresas = $request->input('EMPRESA');
      date_default_timezone_set('America/Santiago');
      $now = new \DateTime();

        $cartola = new Cartola();
        $cartola->FECHA_CREACION = $now;
        $cartola->ID_PROY = $ID_PROY;
        $cartola->save();
      foreach ($empresas as $key => $val) {
        if(count($val['subcontrata'])<>0){
          foreach ($val['subcontrata'] as $key => $value) {
            if($value['SELECCIONADO']==true){
              if($value['MONTO_APROBADO_IVA']=='EXENTA')$value['MONTO_APROBADO_IVA'] = $value['MONTO_APROBADO_NETO'];
              $cartola->DetalleCartola()->attach($value['ID_PAGO'],
                                                       ['TOTAL_FACTURADO_IVA'=> $value['MONTO_APROBADO_IVA'],
                                                        'FECHA_VENCIMIENTO'=> $value['FECHA_CREACION_PAGO'],
                                                        'ESTADO_CARTO'=> 'P',
                                                        'TOTAL_NETO'=> $value['MONTO_APROBADO_NETO'],
                                                        'ID_EMPR'=>$val['ID_EMPR']]);
              $pago = Pago::find($value['ID_PAGO']);
              $pago->ESTADO_PAGO = 'S';
              $pago->save();

              $flujoPago = new FlujoPago();
              $flujoPago->FECHA_FLUJO_PAGO= $now;
              $flujoPago->TIPO_FLUJO_PAGO = 'S';
              $flujoPago->OBSERVACION_FLUJO_PAGO = 'Pago agregado a una cartola';
              $flujoPago->PAGO_ID_PAGO = $value['ID_PAGO'];
              $flujoPago->USUARIO_ID_USU = $request->input('ID_USU');
              $flujoPago->save();
            }
          }
        }
        else if(count($val['ordenes'])<>0){
          foreach ($val['ordenes'] as $key => $value) {
            if($value['SELECCIONADO']==true){
              if($value['MONTO_APROBADO_IVA']=='EXENTA')$value['MONTO_APROBADO_IVA'] = $value['MONTO_APROBADO_NETO'];
              $cartola->DetalleCartola()->attach($value['ID_PAGO'],
                                                       ['TOTAL_FACTURADO_IVA'=> $value['MONTO_APROBADO_IVA'],
                                                        'FECHA_VENCIMIENTO'=> $value['FECHA_CREACION_PAGO'],
                                                        'ESTADO_CARTO'=> 'P',
                                                        'TOTAL_NETO'=> $value['MONTO_APROBADO_NETO'],
                                                        'ID_EMPR'=>$val['ID_EMPR']]);
              $pago = Pago::find($value['ID_PAGO']);
              $pago->ESTADO_PAGO = 'S';
              $pago->save();

              $flujoPago = new FlujoPago();
              $flujoPago->FECHA_FLUJO_PAGO = $now;
              $flujoPago->TIPO_FLUJO_PAGO = 'S';
              $flujoPago->OBSERVACION_FLUJO_PAGO = 'Factura agregada a una cartola';
              $flujoPago->PAGO_ID_PAGO = $value['ID_PAGO'];
              $flujoPago->USUARIO_ID_USU = $request->input('ID_USU');
              $flujoPago->save();
            }

          }
        }
        else if(count($val['notas_credito'])<>0) {

        }
      }
      return response()->json(['code' => 200, 'message' => 'Datos compras empresas obtenidos',
                                      'cartola'=>$cartola->ID_CARTO],200);
    }

    public function cargarCartola($ID_CARTO){
      $cartola = Cartola::find($ID_CARTO);

      $empresas = DB::table('DETALLE_CARTOLA')
                    ->where('CARTOLA_ID_CARTO',$ID_CARTO)
                    ->join('PAGO','PAGO.ID_PAGO','=','PAGO_ID_PAGO')
                    ->join('EMPRESA','EMPRESA.ID_EMPR','=','DETALLE_CARTOLA.ID_EMPR')
                    ->selectRaw('EMPRESA.RAZON_SOCIAL_EMPR,EMPRESA.RUT_EMPR,EMPRESA.ID_EMPR,EMPRESA.TIPO_EMPR')
                    ->groupBY('ID_EMPR')
                    ->get();
      foreach ($empresas as $key => $value) {
        $value->TOTAL_FACTURADO_IVA = 0;
        $value->FECHA_VENCIMENTO = null;
        $value->ESTADO_CARTO = null;
        $detalle = DB::table('DETALLE_CARTOLA')
                      ->where('CARTOLA_ID_CARTO',$ID_CARTO)
                      ->join('PAGO','PAGO.ID_PAGO','=','PAGO_ID_PAGO')
                      ->join('EMPRESA','EMPRESA.ID_EMPR','=','DETALLE_CARTOLA.ID_EMPR')
                      ->selectRaw('DETALLE_CARTOLA.*,PAGO.ESTADO_PAGO')
                      ->where('DETALLE_CARTOLA.ID_EMPR',$value->ID_EMPR)
                      ->get();
              $value->FECHA_VENCIMENTO  =   $detalle->min('FECHA_VENCIMIENTO');

              foreach ($detalle as $key => $val) {
                $value->TOTAL_FACTURADO_IVA = $value->TOTAL_FACTURADO_IVA + $val->TOTAL_FACTURADO_IVA;
                $value->ESTADO_CARTO =   $val->ESTADO_PAGO;
              }
      }
      $cartola->DETALLE_CARTO = $empresas;
      return $cartola;

    }

    public function cargarDetalleCartolaEmpresa($ID_CARTO, $ID_EMPR){
      $cartola = Cartola::find($ID_CARTO);
      $cartola->consolidado;
      $cartola->MONTO_TOTAL=0;
      $detalle = DB::table('DETALLE_CARTOLA')
                      ->where('CARTOLA_ID_CARTO',$ID_CARTO)
                      ->join('PAGO','PAGO.ID_PAGO','=','PAGO_ID_PAGO')
                      ->join('EMPRESA','EMPRESA.ID_EMPR','=','DETALLE_CARTOLA.ID_EMPR')
                      ->selectRaw('DETALLE_CARTOLA.*,PAGO.*,IFNULL(SUM(NOTA_CREDITO.MONTO_NETO), 0)as MONTO_NETO_NCRED,
                      (PAGO.MONTO_NETO - IFNULL(SUM(NOTA_CREDITO.MONTO_NETO), 0)) as MONTO_APROBADO_NETO,
                      IF(PAGO.APLICA_IVA = "S", (PAGO.MONTO_TOTAL - IFNULL(SUM(NOTA_CREDITO.MONTO_TOTAL), 0)), "EXENTA") as MONTO_APROBADO_IVA')
                      ->leftJoin('NOTA_CREDITO','NOTA_CREDITO.PAGO_ID_PAGO','PAGO.ID_PAGO')
                      ->where('DETALLE_CARTOLA.ID_EMPR',$ID_EMPR)
                      ->groupBY('PAGO.ID_PAGO')
                      ->get();
      $cartola->DETALLE_EMPRESA = $detalle;
      $datosBancarios = Empresa::where('ID_EMPR',$ID_EMPR)
                        ->join('BANCO','BANCO.ID_BANCO','=','EMPRESA.BANCO_ID_BANCO')
                        ->join('TIPO_CUENTA_BANCO','TIPO_CUENTA_BANCO.ID_TIPO_CUENTA','=','EMPRESA.TIPO_CUENTA_BANCO_ID_TIPO_CUENTA')
                        ->selectRaw('NUMERO_CUENTA_EMPR,BANCO.NOMBRE_BANCO,NOMBRE_TIPO_CUENTA')->first();
      $cartola->DETALLE_PAGO=$datosBancarios;
      $empresa = Empresa::where('ID_EMPR',$ID_EMPR)->first();
      $cartola->EMPRESA=$empresa;
      foreach ($detalle as $key => $value) {
        if($value->APLICA_IVA == 'S'){
          $cartola->MONTO_TOTAL = $cartola->MONTO_TOTAL + $value->MONTO_APROBADO_IVA;
        }else{
          $cartola->MONTO_TOTAL = $cartola->MONTO_TOTAL + $value->MONTO_APROBADO_NETO;
        }
      }
     return $cartola ;
    }

    public function consolidarPago(Request $request){
      $FECHA_PAGO = $request->input('FECHA_PAGO');
      $Tipo = $request->input('Tipo');
      $OBSERVACION = $request->input('OBSERVACION');
      $NUMERO_TRANFERENCIA = $request->input('NUMERO_TRANFERENCIA');
      $PAGOS = $request->input('PAGOS');
      $MONTO = $request->input('MONTO');
      $ID_CARTO = $request->input('ID_CARTO');
      $ID_PROY = $request->input('ID_PROY');

      $arch64 = $request->input('ARCHIVO');
      $arch64_nombre = $request->input('NOMBRE_ARCH');
      $arch64_tipo = $request->input('TIPO_ARCH');

      $conso = new Consolidado();
      date_default_timezone_set('America/Santiago');
      $now = new \DateTime();
      $conso->FECHA_CONSO = $now;
      $conso->MONTO_TOTAL = $MONTO;
      $conso->ID_CARTO = $ID_CARTO;
      $conso->save();
      foreach ($PAGOS as $key => $value) {
        $conso->DetalleConsolidado()->attach($value['ID_PAGO']);
        $pago = Pago::find($value['ID_PAGO']);
        $pago->ESTADO_PAGO = 'C';
        $pago->FECHA_PAGADO = $FECHA_PAGO;
        $pago->OBSERVACION_PAGO = $OBSERVACION;
        $pago->NUM_TRANSACCION = $NUMERO_TRANFERENCIA;
        $pago->TIPO_PAGO = $Tipo;
        $pago->save();

        $diff = $pago->MONTO_NETO-($pago->MONTO_TOTAL_MATE+$pago->MONTO_FLETE);
        $this->asignarGasto(458,$ID_PROY,$pago->MONTO_FLETE);
        $this->asignarGasto(459,$ID_PROY,$diff);

        $flujoPago = new FlujoPago();
        $flujoPago->FECHA_FLUJO_PAGO= $now;
        $flujoPago->TIPO_FLUJO_PAGO = 'C';
        $flujoPago->OBSERVACION_FLUJO_PAGO = 'Pago consolidado';
        $flujoPago->PAGO_ID_PAGO = $value['ID_PAGO'];
        $flujoPago->USUARIO_ID_USU = $request->input('ID_USU');
        $flujoPago->save();

        if($arch64 != null){
          $pago = Pago::find($pago->ID_PAGO);
          if($pago->SUBCONTRATAR_ID_SUBCON<>null){
            $data = Pago::join('SUBCONTRATAR','SUBCONTRATAR.ID_SUBCON','=','SUBCONTRATAR_ID_SUBCON')
                            ->join('EMPRESA','EMPRESA.ID_EMPR','=','SUBCONTRATAR.EMPRESA_ID_EMPR')
                            ->join('PROYECTO','PROYECTO.ID_PROY','=','SUBCONTRATAR.PROYECTO_ID_PROY')
                            ->where('ID_PAGO',$pago->ID_PAGO)
                            ->selectRaw('EMPRESA.*,PROYECTO.ID_PROY')
                            ->first();
          }else{
            $data = Pago::join('EMPRESA','EMPRESA.ID_EMPR','=','EMPRESA_ID_EMPR')
                            // ->join('ORDEN_COMPRA','ORDEN_COMPRA.ID_ORDENC','=','PAGO.ORDEN_COMPRA_ID_ORDENC')
                            ->where('ID_PAGO',$pago->ID_PAGO)
                            ->selectRaw('EMPRESA.*')
                            ->first();
          }

              $archivo = base64_decode(preg_replace('#^data:'.$arch64_tipo.';base64,#i', '', $arch64));

              file_put_contents($arch64_nombre , $archivo);
              $file = fopen($arch64_nombre,'r+');
              Storage::disk(app('App\Entorno')->origen_storage)->put('archivos/cons/'.$data->CONSTRUCTORA_ID_CONS.'/proyecto/'.$ID_PROY.'/pagos/'.$pago->ID_PAGO.'/consolidado/'.$arch64_nombre,  $file);
              ftruncate($file,0);
              fclose($file);

              // La ruta usa el cargarImgAdi()
              $pago->URL_ADJUNTO= app('App\Entorno')->route_api.'pago/'.$pago->ID_PAGO.'/adjunto/'.$arch64_nombre;
              $pago->NOMBRE_ADJUNTO = $arch64_nombre;
              $pago->save();
          }
      }
        return response()->json(['code' => 200, 'message' => 'Consolidado pagado con exito'],200);
    }

    public function cargarAdjuntoConsolidado($ID_PAGO,$NOMBRE_ARCH){
        $NOMBRE= trim($NOMBRE_ARCH);
        $pago = Pago::find($ID_PAGO);
        // if($pago->SUBCONTRATAR_ID_SUBCON<>null){
          $data = Pago::join('SUBCONTRATAR','SUBCONTRATAR.ID_SUBCON','=','SUBCONTRATAR_ID_SUBCON')
                          ->join('EMPRESA','EMPRESA.ID_EMPR','=','SUBCONTRATAR.EMPRESA_ID_EMPR')
                          ->join('PROYECTO','PROYECTO.ID_PROY','=','SUBCONTRATAR.PROYECTO_ID_PROY')
                          ->where('ID_PAGO',$ID_PAGO)
                          ->selectRaw('EMPRESA.*,PROYECTO.ID_PROY')
                          ->first();
        // }else{
        //   $data = Pago::join('EMPRESA','EMPRESA.ID_EMPR','=','EMPRESA_ID_EMPR')
        //                   ->join('ORDEN_COMPRA','ORDEN_COMPRA.ID_ORDENC','=','PAGO.ORDEN_COMPRA_ID_ORDENC')
        //                   ->where('ID_PAGO',$ID_PAGO)
        //                   ->selectRaw('EMPRESA.*,ORDEN_COMPRA.ID_PROY')
        //                   ->first();
        // }
        $file =storage_path().'/app/archivos/cons/'.$data->CONSTRUCTORA_ID_CONS.'/proyecto/'.$data->ID_PROY.'/pagos/'.$pago->ID_PAGO.'/consolidado/'.$NOMBRE;
        return response()->download($file,$NOMBRE);
    }

    public function cargarHistorialCartola(Request $request,$ID_PROY){
      $nCartola = null;
      if($request->input('nCartola')){$nCartola = $request->input('nCartola');}
      $fechaIni = null;
      if($request->input('fechaIni')){$fechaIni = $request->input('fechaIni');}
      $fechaFin = null;
      if($request->input('fechaFin')){$fechaFin = $request->input('fechaFin');}

      $por_pagina = $request->input('POR_PAGINA');

      $cartola = Cartola::where('ID_PROY',$ID_PROY)
                          ->where(function ($query) use ($nCartola) {
                              if($nCartola <> null) { $query->where('ID_CARTO',$nCartola);
                              }
                          })
                          ->where(function ($query) use ($fechaIni,$fechaFin) {
                              if($fechaIni <> null && $fechaFin <> null) {
                                $query->whereBetween('FECHA_CREACION',[$fechaIni,$fechaFin]);
                              }
                          })->orderBy('FECHA_CREACION','desc')->paginate($por_pagina);

      foreach ($cartola as $key => $value) {
        $value->MONTO_NETO = 0;
        $value->MONTO_IVA = 0;
        $value->ESTADO = true;
          $detalle = DB::table('DETALLE_CARTOLA')
                          ->where('CARTOLA_ID_CARTO',$value->ID_CARTO)
                          ->selectRaw('SUM(TOTAL_FACTURADO_IVA) AS TOTAL_FACTURADO_IVA,SUM(TOTAL_NETO) AS TOTAL_NETO')
                          ->first();
         $datos = DB::table('DETALLE_CARTOLA')
                      ->where('CARTOLA_ID_CARTO',$value->ID_CARTO)
                      ->join('PAGO','PAGO.ID_PAGO','=','DETALLE_CARTOLA.PAGO_ID_PAGO')
                      ->get();

              foreach ($datos as $key => $val) {
                if($val->ESTADO_PAGO == 'S'){
                  $value->ESTADO = false;
                }
              }

        $value->MONTO_IVA =    $detalle->TOTAL_FACTURADO_IVA;
        $value->MONTO_NETO =    $detalle->TOTAL_NETO;
      }

      return $cartola;

    }

    public function cargarEmpresasCartola(Request $request,$ID_PROY){
      $RUT = null;
      if($request->input('RUT')){$RUT = $request->input('RUT');}
      $NOMBRE = null;
      if($request->input('NOMBRE')){$NOMBRE = $request->input('NOMBRE');}

      $por_pagina = $request->input('POR_PAGINA');

      $empresa = DB::table('DETALLE_CARTOLA')
                    ->join('CARTOLA','CARTOLA.ID_CARTO','=','DETALLE_CARTOLA.CARTOLA_ID_CARTO')
                    ->join('EMPRESA','EMPRESA.ID_EMPR','=','DETALLE_CARTOLA.ID_EMPR')
                    ->where('CARTOLA.ID_PROY',$ID_PROY)
                    ->where(function ($query) use ($RUT) {
                        if($RUT <> null) {
                          $query->whereRaw(" RUT_EMPR like '%$RUT%'");
                        }
                    })
                    ->where(function ($query) use ($NOMBRE) {
                        if($NOMBRE <> null) {
                          $query->whereRaw(" CONCAT(NOMBRE_EMPR,RAZON_SOCIAL_EMPR) like '%$NOMBRE%'");
                        }
                    })
                    ->selectRaw('EMPRESA.*')
                    ->groupBY('EMPRESA.ID_EMPR')
                    ->paginate($por_pagina);
       return  $empresa;
    }

    public function detallePagoEmpresa(Request $request,$ID_EMPR){
      $ID_FAC = null;
      $FECHAINI = null;
      $FECHAFIN = null;

      if($request->input('id_fac')){$ID_FAC = $request->input('id_fac');}
      if($request->input('id_orden')){$ID_ORD = $request->input('id_orden');}
      if($request->input('inicio')){$FECHAINI = $request->input('inicio');}
      if($request->input('fin')){$FECHAFIN = $request->input('fin');}

      $empresa = Empresa::find($ID_EMPR);
      $empresa->CONTACTOS = Empresa::join('CONTACTO_EMPRESA','CONTACTO_EMPRESA.EMPRESA_ID_EMPR','=','ID_EMPR')
                                    ->where('ID_EMPR',$ID_EMPR)->get();
      // busca las cartolas que estan consolidadas
      $cartola_consolidad = Cartola::join('DETALLE_CARTOLA','DETALLE_CARTOLA.CARTOLA_ID_CARTO','=','ID_CARTO')
                          ->join('PAGO','PAGO.ID_PAGO','=','DETALLE_CARTOLA.PAGO_ID_PAGO')
                          ->where('PAGO.ESTADO_PAGO','C')
                          ->where('DETALLE_CARTOLA.ID_EMPR',$ID_EMPR)
                          ->select('CARTOLA.*')
                          ->groupBY('ID_CARTO')
                          ->where(function ($query) use ($ID_FAC) {
                              if($ID_FAC) {
                                $query->where('PAGO.NUM_FACTURA',$ID_FAC);
                              }
                          })
                          ->where(function ($query) use ($FECHAINI,$FECHAFIN) {
                              if($FECHAINI  && $FECHAFIN) {
                                $query->whereBetween('PAGO.FECHA_CREACION_PAGO',[(new \DateTime($FECHAINI))->format('Y-m-d 00:00:00'),(new \DateTime($FECHAFIN))->format('Y-m-d 23:59:59')]);
                              }
                          })
                          ->get();

      foreach ($cartola_consolidad as $key => $value) {
        $value->MONTO_NETO = 0;
        $value->MONTO_PAGADO = 0;
        $value->MONTO_APROBADO_IVA = 0;
        $value->MONTO_NETO_CREDITO = 0;
        $value->MONTO_NETO_APROBADO = 0;

        $detalle = DB::table('DETALLE_CARTOLA')
                     ->join('PAGO','PAGO.ID_PAGO','=','DETALLE_CARTOLA.PAGO_ID_PAGO')
                     ->where('CARTOLA_ID_CARTO',$value->ID_CARTO)
                     ->where('DETALLE_CARTOLA.ID_EMPR',$ID_EMPR)
                     ->where(function ($query) use ($ID_FAC) {
                         if($ID_FAC) {
                           $query->where('PAGO.NUM_FACTURA',$ID_FAC);
                         }
                     })
                     ->where(function ($query) use ($FECHAINI,$FECHAFIN) {
                         if($FECHAINI  && $FECHAFIN) {
                           $query->whereBetween('PAGO.FECHA_CREACION_PAGO',[(new \DateTime($FECHAINI))->format('Y-m-d 00:00:00'),(new \DateTime($FECHAFIN))->format('Y-m-d 23:59:59')]);
                         }
                     })
                     ->leftJoin('NOTA_CREDITO','NOTA_CREDITO.PAGO_ID_PAGO','PAGO.ID_PAGO')
                     ->selectRaw('PAGO.*,IFNULL(SUM(NOTA_CREDITO.MONTO_NETO), 0)as MONTO_NETO_NCRED,
                     (PAGO.MONTO_NETO - IFNULL(SUM(NOTA_CREDITO.MONTO_NETO), 0)) as MONTO_APROBADO_NETO,
                     IF(PAGO.APLICA_IVA = "S", (PAGO.MONTO_TOTAL - IFNULL(SUM(NOTA_CREDITO.MONTO_TOTAL), 0)), "EXENTA") as MONTO_APROBADO_IVA')
                     ->groupBy('PAGO.ID_PAGO')
                     ->get();

         foreach ($detalle as $key => $val) {
           $val->NOTA_CREDITO = NotaCredito::where('PAGO_ID_PAGO',$val->ID_PAGO)->get();
           $val->SEGUIMIENTO = FlujoPago::where('PAGO_ID_PAGO',$val->ID_PAGO)->get();
           $value->MONTO_NETO_CREDITO = $value->MONTO_NETO_CREDITO + $val->MONTO_NETO_NCRED;
           $value->MONTO_NETO_APROBADO = $value->MONTO_NETO_APROBADO + $val->MONTO_APROBADO_NETO;
           if($val->APLICA_IVA == 'S'){
             $value->MONTO_APROBADO_IVA = $value->MONTO_APROBADO_IVA + $val->MONTO_APROBADO_IVA;
             $value->MONTO_PAGADO = $value->MONTO_PAGADO + $val->MONTO_APROBADO_IVA;;
             $value->MONTO_NETO = $value->MONTO_NETO +$val->MONTO_NETO;
           }else{
             $value->MONTO_APROBADO_IVA = $value->MONTO_APROBADO_IVA + $val->MONTO_APROBADO_NETO;
             $value->MONTO_PAGADO = $value->MONTO_PAGADO + $val->MONTO_APROBADO_NETO;

             $value->MONTO_NETO = $value->MONTO_NETO +$val->MONTO_NETO;
           }
         }
         $value->DETALLE_PAGO=$detalle;
        }

        // busca las cartolas que estan sin consolidar
        $cartola_sin_conso = Cartola::join('DETALLE_CARTOLA','DETALLE_CARTOLA.CARTOLA_ID_CARTO','=','ID_CARTO')
                            ->join('PAGO','PAGO.ID_PAGO','=','DETALLE_CARTOLA.PAGO_ID_PAGO')
                            ->where('PAGO.ESTADO_PAGO','S')
                            ->where('DETALLE_CARTOLA.ID_EMPR',$ID_EMPR)
                            ->selectRaw('CARTOLA.*')
                            ->groupBY('ID_CARTO')
                            ->where(function ($query) use ($ID_FAC) {
                                if($ID_FAC) {
                                  $query->where('PAGO.NUM_FACTURA',$ID_FAC);
                                }
                            })
                            ->where(function ($query) use ($FECHAINI,$FECHAFIN) {
                                if($FECHAINI  && $FECHAFIN) {
                                  $query->whereBetween('PAGO.FECHA_CREACION_PAGO',[(new \DateTime($FECHAINI))->format('Y-m-d 00:00:00'),(new \DateTime($FECHAFIN))->format('Y-m-d 23:59:59')]);
                                }
                            })
                            ->get();

        foreach ($cartola_sin_conso as $key => $value) {
          $value->MONTO_NETO = 0;

          $value->MONTO_APROBADO_IVA = 0;
          $value->MONTO_NETO_CREDITO = 0;
          $value->MONTO_NETO_APROBADO = 0;

          $detalle = DB::table('DETALLE_CARTOLA')
                       ->join('PAGO','PAGO.ID_PAGO','=','DETALLE_CARTOLA.PAGO_ID_PAGO')
                       ->where('CARTOLA_ID_CARTO',$value->ID_CARTO)
                       ->where('DETALLE_CARTOLA.ID_EMPR',$ID_EMPR)
                       ->where(function ($query) use ($ID_FAC) {
                           if($ID_FAC) {
                             $query->where('PAGO.NUM_FACTURA',$ID_FAC);

                           }
                       })
                       ->where(function ($query) use ($FECHAINI,$FECHAFIN) {
                           if($FECHAINI  && $FECHAFIN) {
                             $query->whereBetween('PAGO.FECHA_CREACION_PAGO',[(new \DateTime($FECHAINI))->format('Y-m-d 00:00:00'),(new \DateTime($FECHAFIN))->format('Y-m-d 23:59:59')]);
                           }
                       })
                       ->leftJoin('NOTA_CREDITO','NOTA_CREDITO.PAGO_ID_PAGO','PAGO.ID_PAGO')
                       ->selectRaw('PAGO.*,IFNULL(SUM(NOTA_CREDITO.MONTO_NETO), 0)as MONTO_NETO_NCRED,
                       (PAGO.MONTO_NETO - IFNULL(SUM(NOTA_CREDITO.MONTO_NETO), 0)) as MONTO_APROBADO_NETO,
                       IF(PAGO.APLICA_IVA = "S", (PAGO.MONTO_TOTAL - IFNULL(SUM(NOTA_CREDITO.MONTO_TOTAL), 0)), "EXENTA") as MONTO_APROBADO_IVA')
                       ->groupBy('PAGO.ID_PAGO')
                       ->get();

           foreach ($detalle as $key => $val) {
             $val->NOTA_CREDITO = NotaCredito::where('PAGO_ID_PAGO',$val->ID_PAGO)->get();
             $val->SEGUIMIENTO = FlujoPago::where('PAGO_ID_PAGO',$val->ID_PAGO)->get();

             $value->MONTO_NETO_CREDITO = $value->MONTO_NETO_CREDITO + $val->MONTO_NETO_NCRED;
             $value->MONTO_NETO_APROBADO = $value->MONTO_NETO_APROBADO + $val->MONTO_APROBADO_NETO;

             if($val->APLICA_IVA == 'S'){
               $value->MONTO_APROBADO_IVA = $value->MONTO_APROBADO_IVA + $val->MONTO_APROBADO_IVA;

               $value->MONTO_NETO = $value->MONTO_NETO +$val->MONTO_NETO;

             }else{
               $value->MONTO_APROBADO_IVA = $value->MONTO_APROBADO_IVA + $val->MONTO_APROBADO_NETO;

               $value->MONTO_NETO = $value->MONTO_NETO +$val->MONTO_NETO;
             }
           }
           $value->DETALLE_PAGO=$detalle;
          }

      $detalle_pen = Pago::join('EMPRESA','EMPRESA.ID_EMPR','=','EMPRESA_ID_EMPR')
                                  ->where('EMPRESA.ID_EMPR',$ID_EMPR)
                                  ->where('PAGO.ESTADO_PAGO','A')
                                  ->where(function ($query) use ($ID_FAC) {
                                      if($ID_FAC) {
                                        $query->where('PAGO.NUM_FACTURA',$ID_FAC);
                                      }
                                  })
                                  ->where(function ($query) use ($FECHAINI,$FECHAFIN) {
                                      if($FECHAINI  && $FECHAFIN) {
                                        $query->whereBetween('PAGO.FECHA_CREACION_PAGO',[(new \DateTime($FECHAINI))->format('Y-m-d 00:00:00'),(new \DateTime($FECHAFIN))->format('Y-m-d 23:59:59')]);
                                      }
                                  })
                                  ->leftJoin('NOTA_CREDITO','NOTA_CREDITO.PAGO_ID_PAGO','PAGO.ID_PAGO')
                                  ->selectRaw('PAGO.*,IFNULL(SUM(NOTA_CREDITO.MONTO_NETO), 0)as MONTO_NETO_NCRED,
                                  (PAGO.MONTO_NETO - IFNULL(SUM(NOTA_CREDITO.MONTO_NETO), 0)) as MONTO_APROBADO_NETO,
                                  IF(PAGO.APLICA_IVA = "S", (PAGO.MONTO_TOTAL - IFNULL(SUM(NOTA_CREDITO.MONTO_TOTAL), 0)), "EXENTA") as MONTO_APROBADO_IVA')
                                  ->groupBy('PAGO.ID_PAGO')
                                  ->get();

      foreach ($detalle_pen as $key => $val) {
        $val->NOTA_CREDITO = NotaCredito::where('PAGO_ID_PAGO',$val->ID_PAGO)->get();
        $val->SEGUIMIENTO = FlujoPago::where('PAGO_ID_PAGO',$val->ID_PAGO)->get();
      }

      $empresa->PENDIENTES = $detalle_pen;
      $empresa->CARTOLA_SIN_CONSO =  $cartola_sin_conso;
      $empresa->CARTOLA_CONSOLIDAD =  $cartola_consolidad;

      $empresa->TOTAL_FAC=0;
      $empresa->TOTAL_CRED=0;
      $empresa->TOTAL_PAG=0;
      $empresa->TOTAL_PEN=0;

        if ($empresa['PENDIENTES']) {
          foreach ($empresa['PENDIENTES'] as $key => $pend) {
            $empresa->TOTAL_PEN = $empresa->TOTAL_PEN + $pend['MONTO_TOTAL'];
            $empresa->TOTAL_CRED=$empresa->TOTAL_CRED +$pend['MONTO_NETO_NCRED'];
            $empresa->TOTAL_FAC = $empresa->TOTAL_FAC + $pend['MONTO_TOTAL'];
          }
        }

        if ($empresa['CARTOLA_CONSOLIDAD']) {
          foreach ($empresa['CARTOLA_CONSOLIDAD'] as $key => $cons) {
            $empresa->TOTAL_PAG = $empresa->TOTAL_PAG + $cons['MONTO_PAGADO'];
            $empresa->TOTAL_CRED = $empresa->TOTAL_CRED + $cons['MONTO_NETO_CREDITO'];
            $empresa->TOTAL_FAC = $empresa->TOTAL_FAC + $cons['MONTO_APROBADO_IVA'];
          }
        }

        if ($empresa['CARTOLA_SIN_CONSO']) {
          foreach ($empresa['CARTOLA_SIN_CONSO'] as $key => $sin_cons) {
            $empresa->TOTAL_PEN = $empresa->TOTAL_PEN + $sin_cons['MONTO_APROBADO_IVA'];
            $empresa->TOTAL_CRED = $empresa->TOTAL_CRED + $sin_cons['MONTO_NETO_CREDITO'];
            $empresa->TOTAL_FAC = $empresa->TOTAL_FAC + $sin_cons['MONTO_APROBADO_IVA'];
          }
        }



      return $empresa;
    }

    public function obtenerFlujoOrden($ID_ORDEN){
      $flujoOrden = FlujoOrdenCompra::where('ORDEN_COMPRA_ID_ORDENC',$ID_ORDEN)
                                  ->leftjoin('USUARIO','USUARIO.ID_USU','=','USUARIO_ID_USU')
                                  ->get();
      foreach ($flujoOrden as $key => $value) {
        $user = null;
        if($value->TIPO_USU == 'D'){
          $user = Dueno::where('USUARIO_ID_USU',$value->USUARIO_ID_USU)
                            ->join('CARGO','CARGO.ID_CARG','=','CARGO.ID_CARG')->first();
        }
        else{
          $user = Empleado::where('USUARIO_ID_USU',$value->USUARIO_ID_USU)
                            ->join('CARGO','CARGO.ID_CARG','=','CARGO.ID_CARG')->first();
        }
        $value->usuario =  $user;
      }


      return $flujoOrden;
    }

    public function prepararDatos($ID_CARTO, $ID_EMPR) {
      $cartola = $this->cargarDetalleCartolaEmpresa($ID_CARTO, $ID_EMPR);
      $datos = array(
        'nro_cart' => $ID_CARTO,
        'razon_so' => $cartola->EMPRESA->RAZON_SOCIAL_EMPR,
        'tipo_emp' => $cartola->EMPRESA == 'P'?'Proveedor':'Subcontratista',
        'rut_empr' => $cartola->EMPRESA->RUT_EMPR,
        'tot_ne_f' => 0,
        'tota_iva' => 0,
        'tot_fina' => $cartola->MONTO_TOTAL,
        'detalles' => []
      );
      foreach ($cartola->DETALLE_EMPRESA as $key => $value) {
        $deta = (object)array(
          'nro_fact' => $value->NUM_FACTURA,
          'net_fact' => $value->MONTO_NETO,
          'net_no_c' => $value->MONTO_NETO_NCRED,
          'net_apro' => $value->MONTO_APROBADO_NETO,
          'iva_deta' => $value->MONTO_IVA,
          'tota_iva' => $value->MONTO_TOTAL,
          'vencimie' => date('d-m-Y',strtotime($value->FECHA_VENCIMIENTO)),
        );
        $datos['detalles'][] = $deta;
        $datos['tot_ne_f'] += $value->MONTO_NETO;
        $datos['tota_iva'] += $value->MONTO_APROBADO_IVA == 'EXENTA'?0:$value->MONTO_IVA;
      }
      return $datos;
    }

    public function prepararDatos2($ID_PROY) {
      $pagos = (array)$this->cargarPagosEmpresa(new Request(), $ID_PROY)->getData()->data;
      $datos = [];
      foreach ($pagos as $key => $data) {
        $dato = (object)array(
          'razon_so' => $data->RAZON_SOCIAL_EMPR,
          'tipo_emp' => $data->EMPRESA->TIPO_EMPR == 'P'?'Proveedor':'Subcontratista',
          'rut_empr' => $data->RUT_EMPR,
          'tot_ne_f' => 0,
          'tota_iva' => 0,
          'tot_fina' => $data->TOTAL_FACTURADO,
          'detalles' => []
        );
        foreach ($data->ordenes as $key => $value) {
          $deta = (object)array(
            // 'nro_ordc' => $value->ID_ORDENC,
            'nro_fact' => $value->NUM_FACTURA,
            'net_fact' => $value->MONTO_NETO,
            'net_no_c' => $value->MONTO_NETO_NCRED,
            'net_apro' => $value->MONTO_APROBADO_NETO,
            'iva_deta' => $value->MONTO_IVA,
            'tota_iva' => $value->MONTO_TOTAL,
            'vencimie' => date('d-m-Y',strtotime($value->FECHA_CREACION_PAGO)),
          );
          $dato->detalles[] = $deta;
          $dato->tot_ne_f += $value->MONTO_NETO;
          $dato->tota_iva += $value->MONTO_APROBADO_IVA == 'EXENTA'?0:$value->MONTO_IVA;
        }
        $datos[] = $dato;
      }
      return $datos;
    }

    public function convertirExcel($ID_PROY, $ID_CARTO, $ID_EMPR, $tipo) {
      date_default_timezone_set('America/Santiago');
      $datos = [];
      $view = 'historial-cartola-excel';
      $nomPro = Proyecto::find($ID_PROY)->NOMBRE_PROY;
      if($tipo == 1) {
        $datos = (array)$this->prepararDatos($ID_CARTO, $ID_EMPR);
        $nomPro = $nomPro . ' - Cartola N' . $ID_CARTO . '.xlsx';
      }
      else if($tipo == 2) {
        $view = 'excel-pago';
        $datos = array('datos' => (array)$this->prepararDatos2($ID_PROY));
        $nomPro = $nomPro . ' - Pagos pendientes - ' . date('d-m-Y') . '.xlsx';
      }
      return Excel::download(new PagosExcelExport($view, $datos), $nomPro);
    }

    public function generarPDF($ID_PROY, $ID_CARTO, $ID_EMPR, $tipo) {
      date_default_timezone_set('America/Santiago');
      $datos = [];
      $view = 'historial-cartola-excel';
      $nomPro = Proyecto::find($ID_PROY)->NOMBRE_PROY;
      $page_footer_html = view('footer')->render();
      $options = [
        'encoding'      => 'UTF-8',
        'footer-html'   => $page_footer_html,
        'margin-bottom' => '30mm'
      ];
      if($tipo == 1) {
        $datos = (array)$this->prepararDatos($ID_CARTO, $ID_EMPR);
        $nomPro = $nomPro . ' - Cartola N' . $ID_CARTO . '.pdf';
      }
      else if($tipo == 2) {
        $view = 'excel-pago';
        $datos = array('datos' => (array)$this->prepararDatos2($ID_PROY));
        $nomPro = $nomPro . ' - Pagos pendientes - ' . date('d-m-Y') . '.pdf';
      }
      $pdf = PDF::loadView($view, $datos)->setOptions($options);
      return $pdf->setPaper('a3','landscape')->download($nomPro);
    }

    public function proveedoresPendientesPago(Request $request) {
      $rut = $request->input('rut');
      $nom = $request->input('nom');
      $ppg = $request->input('ppg');
      $ID_PROY = $request->input('ID_PROY_ACTUAL');
      $prov = Empresa::whereHas('ordenesPendientes', function($q) use ($ID_PROY) {
          $q->where('ID_PROY',$ID_PROY)->whereHas('detalle', function ($h) {
            $h->orWhereRaw(DB::raw("CANTIDAD_ENTREGAR_DETCO > CANTIDAD_FACTURA_PROV"))
              ->orWhereNull('CANTIDAD_FACTURA_PROV');
          });
        })->where(function ($query) use ($rut,$nom) {
          if($rut) $query->where('RUT_EMPR','like',"%$rut%");
          if($nom) $query->orWhere('NOMBRE_EMPR','like',"%$nom%")->orWhere('RAZON_SOCIAL_EMPR','like',"%$nom%");
        })->paginate($ppg);
      return response()->json(['message'=>'Proveedores con ordenes pendientes','data'=>$prov],200);
    }

    public function ordenesCompraPorProveedor($ID_PROY,$ID_EMPR) {
      $detalles = DetalleCotizacion::join('DETALLE_ORDEN_COTIZACION','DETALLE_ORDEN_COTIZACION.DETALLE_COTIZACION_ID_DETA_COTI','DETALLE_COTIZACION.ID_DETA_COTI')
        ->join('ORDEN_COMPRA','DETALLE_ORDEN_COTIZACION.ORDEN_COMPRA_ID_ORDENC','ORDEN_COMPRA.ID_ORDENC')
        // ->join('NOTA_PEDIDO','NOTA_PEDIDO_ID_NOTA_PED','ID_NOTA_PED')
        ->orWhereRaw(DB::raw("CANTIDAD_ENTREGAR_DETCO > CANTIDAD_FACTURA_PROV"))->orWhereNull('CANTIDAD_FACTURA_PROV')
        ->where('ORDEN_COMPRA.ID_EMPR',$ID_EMPR)->where('ID_PROY',$ID_PROY)->whereIn('ORDEN_COMPRA.ESTADO_ORDEN',['A','RP'])
        ->with(['Material','NotaPedido','Cotizacion'])->select('ID_ORDENC','DETALLE_COTIZACION.*')->get();
      return response()->json(['message'=>'Detalles','data'=>$detalles,'empresa'=>Empresa::find($ID_EMPR)],200);
    }

    public function validarFactura(Request $request) {
      $existe = Pago::where('EMPRESA_ID_EMPR',$request->input('ID_EMPR'))
        ->where('NUM_FACTURA',$request->input('NRO_FACT'))->first();
      if($existe) { return response()->json(['message'=>'Factura existente','valid'=>false]); }
      return response()->json(['message'=>'Factura Valida','valid'=>true]);
    }

    public function guardarFactura(Request $request) {
      date_default_timezone_set('America/Santiago');
      // $factura = new FacturaProveedor();
      $ID_USU = $request->input('ID_USU');
      $factura = new Pago();
      $factura->FECHA_CREACION_PAGO = new \DateTime();
      $factura->EMPRESA_ID_EMPR = $request->input('ID_EMPR');
      $factura->NUM_FACTURA = $request->input('NRO_FACT');
      $factura->OBSERVACION = $request->input('OBS_FACT');
      $factura->MONTO_NETO = $request->input('MONTO_NETO');
      $factura->MONTO_TOTAL = $request->input('MONTO_NETO')*1.19;
      $factura->MONTO_TOTAL_MATE = $request->input('MONTO_TOTAL_MATE');
      $factura->MONTO_FLETE = $request->input('MONTO_FLETE');
      $factura->APLICA_IVA = $request->input('APLICA_IVA');
      if($factura->APLICA_IVA == 'S') { $factura->MONTO_IVA = $request->input('MONTO_NETO')*0.19; }
      $factura->ESTADO_PAGO = 'A';
      $fact = (object)$request->input('archivo');
      $factura->ID_PROY = $fact->ID_PROY;
      $id_cons = Proyecto::find($fact->ID_PROY)->CONSTRUCTORA_ID_CONS;
      $archivo = base64_decode(preg_replace('#^data:'.$fact->tipo.';base64,#i', '', $fact->base64));
      file_put_contents($fact->nombre , $archivo);
      $file = fopen($fact->nombre,'r+');
      Storage::disk(app('App\Entorno')->origen_storage)->put('archivos/cons/'.$id_cons.'/proyecto/'.$fact->ID_PROY.'/facturas/'.$fact->nombre,  $file);
      ftruncate($file,0);
      fclose($file);
      $factura->FACTURA_URL= app('App\Entorno')->route_api.'proyecto/'.$fact->ID_PROY.'/documento-tributario/'.$id_cons.'/'.$fact->nombre;
      $factura->NOMBRE_FACTURA = $fact->nombre;
      $factura->save();
      $materiales = (array)$request->input('materiales');
      foreach ($materiales as $key => $material) {
        $material = (object)$material;
        $detalle = new DetalleFacturaProveedor();
        $detalle->PAGO_ID_PAGO = $factura->ID_PAGO;
        $detalle->ID_ORDENC = $material->ID_ORDENC;
        $detalle->ID_MATE = $material->material[0]['ID_MATE'];
        $detalle->ID_DETA_COTI = $material->ID_DETA_COTI;
        $detalle->CANT_FACT = $material->cantidad_facturar;
        $detalle->MONTO_TOTAL = $material->PRECIO_NETO_UNIT*$material->cantidad_facturar;
        $detalle->save();
        DetalleCotizacion::where('ID_DETA_COTI',$material->ID_DETA_COTI)
          ->increment('CANTIDAD_FACTURA_PROV',$material->cantidad_facturar);
        $this->cuadrarOrdenCompra($detalle->ID_ORDENC);
        $this->flujoOrdenFact($detalle->ID_ORDENC,$factura->OBSERVACION,$ID_USU);
      }

      return response()->json(['message'=>'Factura proveedor guardada'],200);
    }

    private function flujoOrdenFact($ID_ORDENC,$obs,$ID_USU) {
      $usr = Usuario::find($ID_USU);
      $flujo = new FlujoOrdenCompra();
      $flujo->TIPO_FLUJO = 'F';
      $flujo->OBSERVACION_FLUJO = $obs;
      $flujo->FECHA_FLUJO = new \DateTime();
      $flujo->ORDEN_COMPRA_ID_ORDENC = $ID_ORDENC;
      $flujo->USUARIO_ID_USU = $ID_USU;
      $flujo->NOMBRE_USU = $usr->dueno?($usr->dueno->NOMBRE_DUE.' '.$usr->dueno->APELLIDO_DUE):($usr->empleado->NOMBRE_EMP.' '.$usr->empleado->APELLIDO_EMP);
      $flujo->CARGO_USU = $usr->dueno?($usr->dueno->cargo->NOMBRE_CARG):($usr->empleado->cargo->NOMBRE_CARG);
      $flujo->save();
    }

    private function asignarGasto($ID_SUBC,$ID_PROY,$monto) {
      $gasto = Gasto::whereDate('FECHA_MODIFICACION',new \DateTime())->where('PROYECTO_ID_PROY',$ID_PROY)
        ->where('SUBCATEGORIA_ID_SUBC',$ID_SUBC)->first();
        if($gasto) {
          $gasto->MONTO_GASTO += $monto;
          $gasto->save();
        }
        else {
          $gasto = new Gasto();
          $gasto->FECHA_MODIFICACION = new \DateTime();
          $gasto->SUBCATEGORIA_ID_SUBC = $ID_SUBC;
          $gasto->MONTO_GASTO = $monto;
          $gasto->MES_ANO_ASIGNADO = new \DateTime();
          $gasto->PROYECTO_ID_PROY = $ID_PROY;
          $gasto->save();
        }
    }

    public function cuadrarOrdenCompra($ID_ORDENC) {
      $orden = OrdenCompra::find($ID_ORDENC);
      if(!$orden->detalle()->orWhereRaw(DB::raw("CANTIDAD_ENTREGAR_DETCO > CANTIDAD_FACTURA_PROV"))->orWhereNull('CANTIDAD_FACTURA_PROV')->first()) {
        $orden->ESTADO_ORDEN = 'CU';
        $orden->save();
      }
    }

    public function descargarFactura($ID_PROY,$ID_CONS,$FILENAME) {
      $NOMBRE= trim($FILENAME);
      $file =storage_path().'/app/archivos/cons/'.$ID_CONS.'/proyecto/'.$ID_PROY.'/facturas/'.$NOMBRE;
      return response()->download($file,$NOMBRE);
    }
}
