<?php

namespace App\Http\Controllers;

use App\PagoSoft;
use App\FacturacionCons;
use App\ModeloNegocio;
use App\ActivoProyecto;
use Mail;
use Freshwork\Transbank\CertificationBagFactory;
use Freshwork\Transbank\TransbankServiceFactory;
use Freshwork\Transbank\RedirectorHelper;
use Illuminate\Http\Request;

class PagoSoftController extends Controller
{

  public function getCobroUF() {
    $apiUrl = 'https://mindicador.cl/api';
    $json = null;
    if ( ini_get('allow_url_fopen') ) {
      $json = file_get_contents($apiUrl);
    }
    else {
      //De otra forma utilizamos cURL
      $curl = curl_init($apiUrl);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
      $json = curl_exec($curl);
      curl_close($curl);
    }
    $dailyIndicators = json_decode($json);
    $modelo = ModeloNegocio::where('NOMBRE_MODELO','COBRO_UF')->orderBy('ID_MODE_NEGO','DESC')->first();
    $iva = ModeloNegocio::where('NOMBRE_MODELO','IVA')->orderBy('ID_MODE_NEGO','DESC')->first();
    return [$dailyIndicators->uf->valor * $modelo->VALOR,$modelo->VALOR,($dailyIndicators->uf->valor * $modelo->VALOR)*($iva->VALOR/100+1),$dailyIndicators->uf->valor];
  }
  public function getNumUF() {
    $modelo = ModeloNegocio::where('NOMBRE_MODELO','COBRO_UF')->orderBy('ID_MODE_NEGO','DESC')->first();
    return $modelo->VALOR;
  }
  public function obtenerPendientes(Request $request) {
      $ID_CONS = $request->input('ID_CONS');
      $ID_USU = $request->input('ID_USU'); //Puede ser dueno o empleado con el modulo, quitar where ID_USU
      if($ID_CONS != null && $ID_USU != null){
        $pendientes = PagoSoft::where('ESTADO_PAGO','P')
                      ->where('CONSTRUCTORA_ID_CONS',$ID_CONS)->with(['detalle_pago'])->get();

        date_default_timezone_set('America/Santiago');
        $facturacion_cons = FacturacionCons::where('CONSTRUCTORA_ID_CONS', $ID_CONS)->first();
        $dia_pago = date("d", strtotime($facturacion_cons->FECHA_FACT_PAGO));

        return response()->json(['code' => 200, 'message' => 'Pendientes obtenidos','pendientes'=>$pendientes,
                            'DIA_PAGO'=>$dia_pago,'facturacion_cons'=>$facturacion_cons],200);
      }
  }

  public function obtenerHistorial(Request $request) {
      $ID_CONS = $request->input('ID_CONS');
      $ID_USU = $request->input('ID_USU'); //Puede ser dueno o empleado con el modulo, quitar where ID_USU
      if($ID_CONS != null && $ID_USU != null){

        $historial = PagoSoft::where('ESTADO_PAGO','F')
                      ->where('CONSTRUCTORA_ID_CONS',$ID_CONS)->with(['detalle_pago'])->get();

        return response()->json(['code' => 200, 'message' => 'Historial obtenido','historial'=>$historial],200);
      }
  }

  public function actualizarFacturacion(Request $request){
    $datosFact = (object)$request->input('datosFact');
    $ID_CONS = $request->input('ID_CONS');

      $fact = FacturacionCons::where('CONSTRUCTORA_ID_CONS',$ID_CONS)->first();
      $fact->RAZON_SOCIAL_FACT = $datosFact->RAZON_SOCIAL_FACT;
      $fact->RUT_FACT =$datosFact->RUT_FACT;
      $fact->TELEFONO_FACT = $datosFact->TELEFONO_FACT;
      $fact->DIRECCION_FACT = $datosFact->DIRECCION_FACT;
      $fact->EMAIL_FACT = $datosFact->EMAIL_FACT;
      $fact->COMUNA_FACT = $datosFact->COMUNA_FACT;
      $fact->GIRO_COMERCIAL_FACT = $datosFact->GIRO_COMERCIAL_FACT;
      $fact->save();
      return response()->json(['code' => 200, 'message' => 'Datos de facturación actualizados','facturacion'=>$fact],200);
  }

    public function comenzarWebpay(Request $request) {
      $ID_PAGO = $request->input('ID_PAGO_SOFT');
      if($ID_PAGO){
          return response()->json(['data'=>app('App\Entorno')->route_api.'pago-soft/generar-pago-webpay/'.$ID_PAGO],200);
      }
      return response()->json(['data'=>$ID_PAGO],200);
    }
  public function pagarPorWebpay($id_pago) {
    // Obtenemos los certificados y llaves para utilizar el ambiente de integración de Webpay Normal.
    $keycom = resource_path().'/transbank/597034716897.key';
    $crtcom = resource_path().'/transbank/597034716897.crt';
    $crttbk = resource_path().'/transbank/serverTBK.crt';

    // $bag = CertificationBagFactory::integrationWebpayNormal();
    $bag = CertificationBagFactory::production($keycom, $crtcom, $crttbk);
    $plus = TransbankServiceFactory::normal($bag);
    $pago = PagoSoft::find($id_pago);

    // Para transacciones normales, solo puedes añadir una linea de detalle de transacción.
    $plus->addTransactionDetail($pago->MONTO_PAGADO, $id_pago); // Monto e identificador de la orden

    // Debes además, registrar las URLs a las cuales volverá el cliente durante y después del flujo de Webpay
    $response = $plus->initTransaction(app('App\Entorno')->route_api.'pago-soft/respuesta-webpay/'.$id_pago,
    app('App\Entorno')->route_api.'pago-soft/fin-pago-webpay/'.$id_pago);
    // Utilidad para generar formulario y realizar redirección POST
    return RedirectorHelper::redirectHTML($response->url, $response->token);
  }

  public function respuestaWebpay($idorden) {
    $keycom = resource_path().'/transbank/597034716897.key';
    $crtcom = resource_path().'/transbank/597034716897.crt';
    $crttbk = resource_path().'/transbank/serverTBK.crt';
    // $bag = CertificationBagFactory::integrationWebpayNormal();
    $bag = CertificationBagFactory::production($keycom, $crtcom, $crttbk);
    $plus = TransbankServiceFactory::normal($bag);
    $response = $plus->getTransactionResult();
    $pago = PagoSoft::find($idorden);
    // $pago->NUM_PAGO = 1;
    // Comprobar si el pago esta incorrecto en webpay, y enviar a vista rechazo
    if($response->detailOutput->responseCode < 0 || !$response->buyOrder ||
      ! $response->detailOutput || !$response->detailOutput->buyOrder ||
      !$response->detailOutput->amount || intval($response->detailOutput->amount) < 0){
        // Para pruebas de integracion piden orden y fecha webpay
        $pago->FECHA_WEBPAY = $response->transactionDate;
        $pago->ORDEN_WEBPAY = $response->buyOrder;
        $pago->CODRESP_WEBPAY = $response->detailOutput->responseCode;
        $pago->save();
        // Ruta web
        return response()->redirectTo(app('App\Entorno')->route_web.'app/cuenta/facturacion?rechazado=1');
        // return response()->json($response);
      // return response()->redirectTo(app('App\Entorno')->route_web.'/mis-reservas/pago/rechazado?pago='.$ID_TEMP);
    }
    // Si tiene todos los datos guardar valores de webpay en pago (se obtienen del response)
    $pago->FECHA_WEBPAY = $response->transactionDate;
    $pago->ORDEN_WEBPAY = $response->buyOrder;
    $pago->TIPOPAGO_WEBPAY = $response->detailOutput->paymentTypeCode;
    $pago->CODRESP_WEBPAY = $response->detailOutput->responseCode;
    $pago->VCI_WEBPAY = $response->VCI;
    if($response->detailOutput->paymentTypeCode != 'VD' &&
      $response->detailOutput->paymentTypeCode != 'VN'){
        $pago->NCUOTA_WEBPAY = $response->detailOutput->sharesNumber;
        $pago->MONTOCUOTA_WEBPAY = $response->detailOutput->sharesAmount;
    }
    $pago->save();

    //Si todo está bien, puedes llamar a acknowledgeTransaction. Si no se llama a este método, la transaccion se reversará en 30 segundos.
    $plus->acknowledgeTransaction();
    $iva = ModeloNegocio::where('NOMBRE_MODELO','IVA')->orderBy('ID_MODE_NEGO','DESC')->first();
    if($pago->TIPOPAGO_WEBPAY == 'VD'){
      $mod = ModeloNegocio::where('NOMBRE_MODELO','COMISION_VD_WP')->orderBy('ID_MODE_NEGO','DESC')->first();
      $pago->MEDIO_PAGO = 'WEBPAY_DEBITO';
      $pago->MONTO_MEDIOPAGO = ($pago->MONTO_PAGADO * ($mod->VALOR/100));
      $pago->MONTO_RECIBIDO = $pago->MONTO_PAGADO - $pago->MONTO_MEDIOPAGO - ($pago->MONTO_PAGADO * ($iva->VALOR/100));
    }
    if($pago->TIPOPAGO_WEBPAY == 'VN' || $pago->TIPOPAGO_WEBPAY == 'VC' || $pago->TIPOPAGO_WEBPAY == 'SI'
          || $pago->TIPOPAGO_WEBPAY == 'S2'|| $pago->TIPOPAGO_WEBPAY == 'NC'){
      $mod = ModeloNegocio::where('NOMBRE_MODELO','COMISION_VC_WP')->orderBy('ID_MODE_NEGO','DESC')->first();
      $pago->MEDIO_PAGO = 'WEBPAY_CREDITO';
      $pago->MONTO_MEDIOPAGO = ($pago->MONTO_PAGADO * ($mod->VALOR/100));
      $pago->MONTO_RECIBIDO = $pago->MONTO_PAGADO - $pago->MONTO_MEDIOPAGO - ($pago->MONTO_PAGADO * ($iva->VALOR/100));
    }
    $pago->save();
    return RedirectorHelper::redirectBackNormal($response->urlRedirection);
  }

  public function finPagoWebpay($idorden) {
    date_default_timezone_set('America/Santiago');
    $pago = PagoSoft::find($idorden);
    //pago anulado
    if(array_key_exists('TBK_TOKEN',$_POST)){
      return response()->redirectTo(app('App\Entorno')->route_web.'app/cuenta/facturacion?anulado=1;date='.date('Y-m-d H:i:s'));
      // return response()->redirectTo(app('App\Entorno')->route_web.'suscripcion/'.$pago->POTENCIAL_ID_POTN));
      // return [$_POST['TBK_TOKEN'],'TBK_TOKEN'];
    }
    // if(array_key_exists('TBK_ORDEN_COMPRA',$_POST)) {
    //   return [$_POST['TBK_ORDEN_COMPRA'],'TBK_ORDEN_COMPRA'];
    // }
    //pago exitoso normal y credito c/s cuotas
    if(array_key_exists('token_ws',$_POST)) {
      $pago->FECHA_PAGADO = date('Y-m-d H:i:s');
      $pago->ESTADO_PAGO = 'F';
      $pago->save();
      // $datos  = [
      //   'nombre' => $potencial->NOMBRE_POTN.' '.$potencial->APELLIDO_POTN,
      //   'link' => app('App\Entorno')->route_web.'confirmar-acceso/'.$potencial->ID_POTN
      // ]
      // Mail::send('confirmar-acceso', $data, function($message) use ($potencial) {
      //    $message->to($potencial->EMAIL_POTN, ($potencial->NOMBRE_POTN.' '.$potencial->APELLIDO_POTN))->subject('Pago confirmado');
      //    $message->from('no-reply@budgeter.cl','Equipo Budgeter');
      // });

      return response()->redirectTo(app('App\Entorno')->route_web.'app/cuenta/facturacion?aceptado=1');
      // return response()->redirectTo(app('App\Entorno')->route_web.'confirmar-acceso/'.$pago->POTENCIAL_ID_POTN));
      // return [$_POST['token_ws'],'token_ws'];
    }
  }
}
