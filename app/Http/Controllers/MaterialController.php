<?php

namespace App\Http\Controllers;


use App\UnidadMedida;
use App\Material;
use App\NotaPedido;
use App\InventarioProyecto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MaterialController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     //crear materiales para la constructora
    public function store(Request $request){}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){}

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $ID_USU){}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){}
    // crear material
    public function crearMaterial(Request $request){
      $NOMBRE = $request->input('NOMBRE');
      $MATERIAL = $request->input('MATERIAL');
      $UNIDADM = $request->input('UNIDADM');
      $CONSTRUCTORA = $request->input('CONSTRUCTORA');
      $mate = new Material();
      $mate->NOMBRE_MATE = $NOMBRE;
      $mate->UNIDAD_MEDIDA_ID_UNID = $UNIDADM;
      $mate->SUBCATEGORIA_ID_SUBC = $MATERIAL;
      $mate->CONSTRUCTORA_ID_CONS = $CONSTRUCTORA;
      $mate->save();
      return response()->json(['code' => 200, 'message' => 'Material guardado correctamente'],200);
    }
    // cargar unidades de medida
    public function cargarUnidadMedida(Request $request){
      $filtro = $request->input('FILTRO');
      $unidad = UnidadMedida::where('TIPO_UNID',$filtro)->get();
      return $unidad;
    }
    //carfar materiales de la constructora con paginacion
    public function cargarMateriales($ID_CONS,Request $request){
      $por_pagina = $request->input('POR_PAGINA');
      $mate = Material::join('SUBCATEGORIA','SUBCATEGORIA.ID_SUBC','SUBCATEGORIA_ID_SUBC')
                      ->join('CATEGORIA','CATEGORIA.ID_CATE','CATEGORIA_ID_CATE')
                      ->join('UNIDAD_MEDIDA','UNIDAD_MEDIDA.ID_UNID','UNIDAD_MEDIDA_ID_UNID')
                      ->where('CONSTRUCTORA_ID_CONS',$ID_CONS)->paginate($por_pagina);
      return $mate;
    }

    //carfar materiales de la constructora sin paginacion
    public function getMateriales($ID_CONS){
      $mate = Material::join('SUBCATEGORIA','SUBCATEGORIA.ID_SUBC','SUBCATEGORIA_ID_SUBC')
                      ->join('CATEGORIA','CATEGORIA.ID_CATE','CATEGORIA_ID_CATE')
                      ->join('UNIDAD_MEDIDA','UNIDAD_MEDIDA.ID_UNID','UNIDAD_MEDIDA_ID_UNID')
                      ->where('CONSTRUCTORA_ID_CONS',$ID_CONS)->get();
      return $mate;
    }

    public function buscarMateriales(Request $request,$ID_CONS){
      $palabra = $request->input('palabra');
      $por_pagina = $request->input('POR_PAGINA');
      $mate = Material::join('SUBCATEGORIA','SUBCATEGORIA.ID_SUBC','SUBCATEGORIA_ID_SUBC')
                      ->join('CATEGORIA','CATEGORIA.ID_CATE','CATEGORIA_ID_CATE')
                      ->join('UNIDAD_MEDIDA','UNIDAD_MEDIDA.ID_UNID','UNIDAD_MEDIDA_ID_UNID')
                      ->where('CONSTRUCTORA_ID_CONS',$ID_CONS)
                      ->where('NOMBRE_MATE','like','%'.$palabra.'%')->paginate($por_pagina);
      return $mate;
    }

    public function eliminarMaterial($ID_MATE){
      $material = Material::where('ID_MATE',$ID_MATE)
                            ->first();
      $estado = false;
      $nota = NotaPedido::where('MATERIAL_ID_MATE',$ID_MATE)->first();
      if($nota <> null){
        $estado = true;
      }
      $invetario = InventarioProyecto::where('MATERIAL_ID_MATE',$ID_MATE)->first();
      if($invetario <> null){
        $estado = true;
      }
      if($estado == false){
        $material->delete();
        return response()->json(['code' => 200, 'message' => 'Material eliminado con exito'],200);
      }else{
        return response()->json(['code' => 200, 'message' => 'Material está asociado a un proyecto'],200);
      }
    }


}
