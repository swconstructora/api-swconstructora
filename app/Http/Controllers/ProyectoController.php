<?php

namespace App\Http\Controllers;

use App\Proyecto;
use App\Empleado;
use App\Dueno;
use App\NotaPedido;
use App\Presupuesto;
use App\Area;
use App\Gasto;
use App\Pago;
use App\OrdenCompra;
use App\InventarioProyecto;
use App\Cotizacion;
use App\DUI;
use App\DevolucionProveedor;
use App\Merma;
use App\GuiaDespacho;
use App\Cargo;
use App\ActivoProyecto;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class ProyectoController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $proyecto = Proyecto::all();
        return $proyecto;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($ID_PROY){
        $proyecto = Proyecto::where('ID_PROY',$ID_PROY)->with(['DetalleEmpleados'])->first();
        return $proyecto;
    }

//PROYECTOS//
    // crear nuevo proyecto
    public function crearProyecto(Request $request){
      $NOMBRE_PROY = $request->input('NOMBRE_PROY');
      $PRESUPUESTO_TOTAL_PROY = $request->input('PRESUPUESTO_TOTAL_PROY');
      $MONTO_MAX_PROY = $request->input('MONTO_MAX_PROY');
      $ID_CONS = $request->input('ID_CONS');
      $ID_PROY = $request->input('ID_PROY');
      $RAZON = $request->input('RAZON');
      $RUT = $request->input('RUT');
      $DIRECCION = $request->input('DIRECCION');
      if($ID_PROY == 0){
        $proyecto = new Proyecto();
        $proyecto->NOMBRE_PROY = $NOMBRE_PROY;
        $proyecto->PRESUPUESTO_TOTAL_PROY = $PRESUPUESTO_TOTAL_PROY;
        $proyecto->CONSTRUCTORA_ID_CONS = $ID_CONS;
        $proyecto->RAZON = $RAZON;
        $proyecto->RUT = $RUT;
        $proyecto->DIRECCION = $DIRECCION;
        $proyecto->save();

        $activo = new ActivoProyecto();
        date_default_timezone_set('America/Santiago');
        $activo->FECHA_ACTIVO = date('Y-m-d H:i:s');
        $activo->ESTADO_ACTIVO = 'A';
        $activo->PROYECTO_ID_PROY = $proyecto->ID_PROY;
        $activo->save();
        return response()->json(['code' => 200, 'message' => 'Proyecto registrado correctamente','data'=>$proyecto->ID_PROY],200);
      }else{
        $proyecto = Proyecto::find($ID_PROY);
        $proyecto->PRESUPUESTO_TOTAL_PROY = $PRESUPUESTO_TOTAL_PROY;
        $proyecto->RAZON = $RAZON;
        $proyecto->RUT = $RUT;
        $proyecto->DIRECCION = $DIRECCION;
        $proyecto->save();
        return response()->json(['code' => 200, 'message' => 'Proyecto editado correctamente'],200);
      }
    }
    // carcar proyectos de una constructora
    public function cargarProyectos(Request $request, $ID_CONS){
      $proyecto = Proyecto::where('PROYECTO.CONSTRUCTORA_ID_CONS','=',$ID_CONS)->get();
      $proycs = [];
      foreach ($proyecto as $key => $value) {
        if($request->input('id_usu')){
          $value->puedeAcceder = false;
          if($value->DetalleEmpleados()->where('USUARIO_ID_USU',$request->input('id_usu'))->first()){
            $value->puedeAcceder = true;
            $proycs[] = $value;
          }
        }
        else { $value->puedeAcceder = true; }
      }
      if($request->input('id_usu')) { return $proycs; }
      return $proyecto;
    }
    // eliminar proyecto
    public function eliminarProyecto($ID_PROY){
      $proyecto = Proyecto::find($ID_PROY);
      $proyecto->DetalleEmpleados()->detach();
      ActivoProyecto::where('PROYECTO_ID_PROY', $ID_PROY)->delete();
      $proyecto->delete();
      return response()->json(['code' => 200, 'message' => 'Proyecto eliminado correctamente'],200);
    }


    public function cargarEmpleados($ID_CONS,$ID_CARG){
      $idsCarg = app('App\Http\Controllers\CargoController')->revisarSubordinados($ID_CARG);
      $empleado = Empleado::where('EMPLEADO.CONSTRUCTORA_ID_CONS','=',$ID_CONS)
                          ->join('CARGO','CARGO.ID_CARG','CARGO_ID_CARG')
                          ->join('USUARIO','USUARIO.ID_USU','USUARIO_ID_USU')
                          ->whereIn('CARGO.ID_CARG',$idsCarg)
                          ->selectRaw('EMPLEADO.* , USUARIO.EMAIL_USU, CARGO.*')->get();
      foreach ($empleado as $key => $value) {
        $value->monto = 0;
        $value->horas = null;
        $value->montoIlimitado = 'N';
        $value->horaIlimitada = 'F';
      }
      return $empleado;
    }

    public function guardarEmpleadosAdmin(Request $request){
      $ID_PROY = $request->input('ID_PROY');
      $empleados = $request->input('empleados');
      $quitados = $request->input('quitados');
      $asignadoFact = $request->input('ID_RECIVE_FACT');
      $asignadoAnteFact = $request->input('ID_EMP_ANTERIO');

      $proyecto = Proyecto::find($ID_PROY);



      if(count($quitados)<>0){
        foreach ($quitados as $key => $val) {
          $proyecto->DetalleEmpleados()->detach($val['ID_EMP']);
        }
      }

      foreach ($empleados as $key => $value) {
        $proyecto->DetalleEmpleados()->detach($value['ID_EMP']);

        if($value['SIGLA_CARG'] == 'F' || $value['SIGLA_CARG'] == 'B'){
          $proyecto->DetalleEmpleados()->attach($value['ID_EMP'],['ID_ASIGNADOR'=> $request->input('ID_ASIGNADOR')]);
        }else{
          $proyecto->DetalleEmpleados()->attach($value['ID_EMP'],['ID_ASIGNADOR'=> $request->input('ID_ASIGNADOR'),'MONTO_LIMITE'=> $value['monto'],'HORAS_LIMITE'=> $value['horas'],
          'MONTO_ILIMITADO'=> $value['montoIlimitado'],'HORA_ILIMITADA'=> $value['horaIlimitada']]);
        }
      }

      $proyecto->DetalleEmpleados()->where('ID_EMP',$asignadoAnteFact)->update(['RECIBE_FACTURA'=>false]);
      $proyecto->DetalleEmpleados()->where('ID_EMP',$asignadoFact)->update(['RECIBE_FACTURA'=>true]);

      return response()->json(['code' => 200, 'message' => 'Empleados registrados correctamente'],200);

    }

    public function cargarEmpleadosProyecto(Request $request){
      $ID_PROY = $request->input('ID_PROY');
      $tipo = $request->input('TIPO');
      $id_usu = $request->input('id_usu');
      $id_carg = $request->input('id_carg');
      $idsCarg = app('App\Http\Controllers\CargoController')->revisarSubordinados($id_carg);
      $proyecto = Proyecto::find($ID_PROY);
      $admin = $proyecto->DetalleEmpleados()
                        ->join('CARGO','CARGO.ID_CARG','CARGO_ID_CARG')
                        ->where('USUARIO_ID_USU','!=',$id_usu)
                        ->whereIn('CARGO_ID_CARG',$idsCarg)
                        ->selectRaw('EMPLEADO.*, CARGO.*')->get();
      foreach ($admin as $key => $value) {
        $value->monto = $value->pivot->MONTO_LIMITE;
        $value->horas = $value->pivot->HORAS_LIMITE;
        $value->montoIlimitado = $value->pivot->MONTO_ILIMITADO;
        $value->horaIlimitada = $value->pivot->HORA_ILIMITADA;
      }
      return $admin;
    }



    //obtiene datos del proyecto dependiendo del tipo de empleado
    public function detalleProyecto(Request $request){
      $ID_PROY = $request->input('ID_PROY');
      $ID_EMP = $request->input('ID_EMP');
      $proyecto = Proyecto::find($ID_PROY);
      $proyecto->DetalleEmpleados;
      $detalle = $proyecto->DetalleEmpleados()->where('ID_EMP',$ID_EMP)->first();
      return $detalle;
    }

    public function cargarTareasPendientes(Request $request) {
      $ID_CARGO = $request->input('ID_CARG');
      $modulos = Cargo::find($ID_CARGO)->modulosR;
      $ID_PROY = $request->input('ID_PROY');
      $tareas = [];
      foreach ($modulos as $key => $modulo) {
        if($modulo->pivot->PUEDE_EDITAR) {
          switch ($modulo->ID_MOD) {
            case 23: // Tareas pendientes del modulo pedidos
              foreach ($modulo->hijosC()->wherePivot('CARGO_ID_CARG',$ID_CARGO)->get() as $key => $hijo) {
                if($hijo->pivot->PUEDE_EDITAR){
                  switch ($hijo->ID_MOD) {
                    case 25:
                      if(NotaPedido::where('PROYECTO_ID_PROY',$ID_PROY)->where('ESTADO_NOTA_PED','E')->first())
                        $tareas[] = (object)array(
                          'titulo' => 'Pedidos',
                          'msg' => 'Tienes notas de pedido pendientes de aprobar',
                          'url' => $hijo->ROUTE_MOD
                        );
                      break;
                    case 24:
                      if(!NotaPedido::where('PROYECTO_ID_PROY',$ID_PROY)->first())
                        $tareas[] = (object)array(
                          'titulo' => 'Pedidos',
                          'msg' => 'Crea tu primera nota de pedido',
                          'url' => $hijo->ROUTE_MOD
                        );
                      break;
                  }
                }
              }
              break;
            case 27: // Tareas pendientes del modulo trabajadores
              foreach ($modulo->hijosC()->wherePivot('CARGO_ID_CARG',$ID_CARGO)->get() as $key => $hijo) {
                if($hijo->pivot->PUEDE_EDITAR) {
                  switch ($hijo->ID_MOD) {
                    case 28:
                      if(Proyecto::find($ID_PROY)->DetalleEmpleados()->count() <= 0)
                        $tareas[] = (object)array(
                          'titulo' => 'Trabajadores',
                          'msg' => 'Asignar trabajadores al proyecto y su monto máximo de cotización',
                          'url' => $hijo->ROUTE_MOD
                        );
                      break;
                  }
                }
              }
              break;
            case  7: // Tareas pendientes del modulo presupuesto
              foreach ($modulo->hijosC()->wherePivot('CARGO_ID_CARG',$ID_CARGO)->get() as $key => $hijo) {
                if($hijo->pivot->PUEDE_EDITAR) {
                  switch ($hijo->ID_MOD) {
                    case 8:
                      if(!Presupuesto::where('PROYECTO_ID_PROY',$ID_PROY)->first())
                        $tareas[] = (object)array(
                          'titulo' => 'Presupuesto',
                          'msg' => 'Crear presupuesto',
                          'url' => $hijo->ROUTE_MOD
                        );
                      break;
                    case 9:
                    $area = Area::join('SUBAREA','SUBAREA.AREA_ID_AREA','ID_AREA')
                                ->join("DETALLE_SAREA_PROY",'DETALLE_SAREA_PROY.SUBAREA_ID_SAREA','SUBAREA.ID_SAREA')
                                ->select('AREA.*')->groupBy('AREA.ID_AREA')
                                ->where("DETALLE_SAREA_PROY.PROYECTO_ID_PROY",$ID_PROY)->first();
                      if(!$area) $tareas[] = (object)array(
                        'titulo' => 'Presupuesto',
                        'msg' => 'Crear Áreas del proyecto',
                        'url' => $hijo->ROUTE_MOD
                      );
                      break;
                    case 10:
                      if(!Gasto::where('PROYECTO_ID_PROY',$ID_PROY)->first())
                        $tareas[] = (object)array(
                          'titulo' => 'Presupuesto',
                          'msg' => 'Asignar otros costos de este mes',
                          'url' => $hijo->ROUTE_MOD
                        );
                      break;
                  }
                }
              }
              break;
            case 39: // Tareas pendientes del modulo pagos
              foreach ($modulo->hijosC()->wherePivot('CARGO_ID_CARG',$ID_CARGO)->get() as $key => $hijo) {
                if($hijo->pivot->PUEDE_EDITAR) {
                  switch ($hijo->ID_MOD) {
                    case 40:
                      $p1 = Pago::join('SUBCONTRATAR','SUBCONTRATAR.ID_SUBCON','PAGO.SUBCONTRATAR_ID_SUBCON')
                        ->where('SUBCONTRATAR.PROYECTO_ID_PROY',$ID_PROY)->where('ESTADO_PAGO','A')->first();
                      $p2 = Pago::join('ORDEN_COMPRA','ORDEN_COMPRA.ID_ORDENC','PAGO.ORDEN_COMPRA_ID_ORDENC')
                        ->where('ORDEN_COMPRA.ID_PROY',$ID_PROY)->where('ESTADO_PAGO','A')->first();
                      if($p1 || $p2) $tareas[] = (object)array(
                        'titulo' => 'Pagos',
                        'msg' => 'Pagos sin asignar a cartolas',
                        'url' => $hijo->ROUTE_MOD
                      );
                      break;
                    case 42:
                      $p1 = Pago::join('SUBCONTRATAR','SUBCONTRATAR.ID_SUBCON','PAGO.SUBCONTRATAR_ID_SUBCON')
                        ->where('SUBCONTRATAR.PROYECTO_ID_PROY',$ID_PROY)->where('ESTADO_PAGO','S')->first();
                      $p2 = Pago::join('ORDEN_COMPRA','ORDEN_COMPRA.ID_ORDENC','PAGO.ORDEN_COMPRA_ID_ORDENC')
                        ->where('ORDEN_COMPRA.ID_PROY',$ID_PROY)->where('ESTADO_PAGO','S')->first();
                      if($p1 || $p2) $tareas[] = (object)array(
                        'titulo' => 'Pagos',
                        'msg' => 'Pagos en cartolas sin consolidar',
                        'url' => $hijo->ROUTE_MOD
                      );
                      break;
                    case 41:
                      if(OrdenCompra::where('ID_PROY',$ID_PROY)->whereIn('ESTADO_ORDEN',['A','RP'])->doesntHave('pago')->first())
                        $tareas[] = (object)array(
                          'titulo' => 'Pagos',
                          'msg' => 'Hay órdenes de compra sin documento tributario',
                          'url' => $hijo->ROUTE_MOD
                        );
                      break;
                  }
                }
              }
              break;
            case 44: // Tareas pendientes del modulo bodega
              foreach ($modulo->hijosC()->wherePivot('CARGO_ID_CARG',$ID_CARGO)->get() as $key => $hijo) {
                if($hijo->pivot->PUEDE_EDITAR) {
                  switch ($hijo->ID_MOD) {
                    case 45:
                      if(InventarioProyecto::where('PROYECTO_ID_PROY',$ID_PROY)->whereNull('LIMITE_CRITICO')->first())
                        $tareas[] = (object)array(
                          'titulo' => 'Bodega',
                          'msg' => 'Definir stock crítico de materiales (Opcional)',
                          'url' => $hijo->ROUTE_MOD
                        );
                      break;
                    case 47:
                      if(DUI::where('PROYECTO_ID_PROY_R',$ID_PROY)->where('ESTADO_DUI','ES')->first())
                        $tareas[] = (object)array(
                          'titulo' => 'Bodega',
                          'msg' => 'Tienes DUI por recibir',
                          'url' => $hijo->ROUTE_MOD
                        );
                      break;
                    case 49:
                      if(Merma::where('PROYECTO_ID_PROY',$ID_PROY)->where('ESTADO_MERMA','P')->first())
                        $tareas[] = (object)array(
                          'titulo' => 'Bodega',
                          'msg' => 'Hay mermas pendientes de aprobar',
                          'url' => $hijo->ROUTE_MOD
                        );
                      break;
                    case 50:
                      if(GuiaDespacho::where('PROYECTO_ID_PROY',$ID_PROY)->where('ESTADO_GUIA','P')->first())
                        $tareas[] = (object)array(
                          'titulo' => 'Bodega',
                          'msg' => 'Hay guías de despacho sin documento',
                          'url' => $hijo->ROUTE_MOD
                        );
                      break;
                  }
                }
              }
              break;
            case 13: // Tareas pendientes del modulo OC
              foreach ($modulo->hijosC()->wherePivot('CARGO_ID_CARG',$ID_CARGO)->get() as $key => $hijo) {
                if($hijo->pivot->PUEDE_EDITAR){
                  switch ($hijo->ID_MOD) {
                    case 19:
                      if(OrdenCompra::where('ID_PROY',$ID_PROY)->where('ESTADO_ORDEN','E')->first())
                        $tareas[] = (object)array(
                          'titulo' => 'Órdenes de compra',
                          'msg' => 'Tiene órdenes pendientes de aprobar',
                          'url' => $hijo->ROUTE_MOD
                        );
                      break;
                    case 18:
                      if(Cotizacion::where('PROYECTO_ID_PROY',$ID_PROY)->where('ESTADO_COTI','C')
                        ->whereHas('detalle',function ($q) { $q->where('ESTADO_COTIZACION','N'); })->first())
                        $tareas[] = (object)array(
                          'titulo' => 'Órdenes de compra',
                          'msg' => 'Hay cotizaciones listas para comprar',
                          'url' => $hijo->ROUTE_MOD
                        );
                      break;
                    case 17:
                      if(Cotizacion::where('PROYECTO_ID_PROY',$ID_PROY)->where('ESTADO_COTI','I')->first())
                        $tareas[] = (object)array(
                          'titulo' => 'Órdenes de compra',
                          'msg' => 'Hay cotizaciones incompletas',
                          'url' => $hijo->ROUTE_MOD
                        );

                      if(NotaPedido::where('PROYECTO_ID_PROY',$ID_PROY)->where('ESTADO_NOTA_PED','A')
                        ->withCount('DetalleCotizacionModal')->having('detalle_cotizacion_modal_count','<',3)->first())
                        $tareas[] = (object)array(
                          'titulo' => 'Órdenes de compra',
                          'msg' => 'Tienes notas de pedido por cotizar',
                          'url' => $hijo->ROUTE_MOD
                        );
                      break;
                  }
                }
              }
              break;
            case 29: // Tareas pendientes del modulo recepciones
              foreach ($modulo->hijosC()->wherePivot('CARGO_ID_CARG',$ID_CARGO)->get() as $key => $hijo) {
                if($hijo->pivot->PUEDE_EDITAR) {
                  switch ($hijo->ID_MOD) {
                    case 30:
                      if(OrdenCompra::where('ID_PROY',$ID_PROY)->where('ESTADO_ORDEN','A')->first())
                        $tareas[] = (object)array(
                          'titulo' => 'Recepciones',
                          'msg' => 'Hay órdenes de compra pospuestas o por recepcionar',
                          'url' => $hijo->ROUTE_MOD
                        );
                      break;
                    case 31:
                      if(DUI::where('PROYECTO_ID_PROY_R',$ID_PROY)->where('ESTADO_DUI','EN')->first())
                        $tareas[] = (object)array(
                          'titulo' => 'Recepciones',
                          'msg' => 'Tienes DUI por recepcionar',
                          'url' => $hijo->ROUTE_MOD
                        );
                      break;
                  }
                }
              }
              break;
            case 33: // Tareas pendientes del modulo entregas
              foreach ($modulo->hijosC()->wherePivot('CARGO_ID_CARG',$ID_CARGO)->get() as $key => $hijo) {
                if($hijo->pivot->PUEDE_EDITAR) {
                  switch ($hijo->ID_MOD) {
                    case 35:
                      if(DUI::where('PROYECTO_ID_PROY_E',$ID_PROY)->where('ESTADO_DUI','AC')->first())
                        $tareas[] = (object)array(
                          'titulo' => 'Entregas',
                          'msg' => 'Tienes DUI por entregar',
                          'url' => $hijo->ROUTE_MOD
                        );
                      break;
                    case 36:
                      if(DevolucionProveedor::where('ID_PROY',$ID_PROY)->where('ESTADO','A')->first())
                        $tareas[] = (object)array(
                          'titulo' => 'Entregas',
                          'msg' => 'Hay devoluciones pendientes de entregar',
                          'url' => $hijo->ROUTE_MOD
                        );
                      break;
                  }
                }
              }
              break;
          }
        }
      }
      return $tareas;
    }

    public function cambiarEstadoProyecto(Request $request, $ID_PROY) {
        $estado = $request->input('estado');
        $proy = Proyecto::where('ID_PROY',$ID_PROY)->update(['ESTADO_PROY'=>$estado]);
        date_default_timezone_set('America/Santiago');
        if($estado == 'C'){
              $activo = ActivoProyecto::where('PROYECTO_ID_PROY', $ID_PROY)->where('ESTADO_ACTIVO', 'A')
                                        ->where('FECHA_CERRADO', null)->first();

              if($activo){
                $fecha_cierre = date('Y-m-d H:i:s');
                $interval = date_diff(date_create($activo->FECHA_ACTIVO), date_create($fecha_cierre));
                $activo->FECHA_CERRADO = $fecha_cierre;
                $activo->DIAS_ACTIVO = $interval->days;
                $activo->ESTADO_ACTIVO ='C';
                $activo->save();

              }


        } else{
          $activo = new ActivoProyecto();
          $activo->FECHA_ACTIVO = date('Y-m-d H:i:s');
          $activo->ESTADO_ACTIVO = 'A';
          $activo->PROYECTO_ID_PROY = $ID_PROY;
          $activo->save();
        }


      return response()->json('Estado del proyecto actualizado');
    }

}
