<?php

namespace App\Http\Controllers;
use App\Categoria;
use App\Subcategoria;
use Illuminate\Http\Request;
use DB;
class CategoriaController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $proyecto = Proyecto::all();
        return $proyecto;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){}

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $ID_USU){}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){}

    // Para buscador con autocompletado
    public function cargarSubcategorias(Request $request){
      $tipo = $request->input('TIPO');

      $subcate = Subcategoria::join('CATEGORIA','CATEGORIA.ID_CATE','CATEGORIA_ID_CATE')
                              ->whereIn('CATEGORIA.TIPO_CATE',$tipo)
                              ->with(['categoria'])
                              ->get();
      // $subcate->categoria;
      return $subcate;
    }

    // Para listas de categorias en tablas
    public function cargarPorTipo(Request $request){
      $tipo = $request->input('TIPO');
      $categorias = Categoria::where('TIPO_CATE',$tipo)->with(['subcategorias'])->get();

      return $categorias;
    }

    public function buscarCategorias(Request $request){
      $palabra = $request->input('palabra');
      $tipo = $request->input('TIPO');
      $subcate = Categoria::where('CATEGORIA.TIPO_CATE','=',$tipo)
                              ->with(['subcategorias'])
                              ->where(function($query) use ($palabra){
                                $query->orWhere('CATEGORIA.NOMBRE_CATE','like','%'.$palabra.'%')
                                ->orWhereRaw("(".DB::raw("SELECT COUNT(*) FROM SUBCATEGORIA as s
                                WHERE s.CATEGORIA_ID_CATE = CATEGORIA.ID_CATE AND s.NOMBRE_SUBC like '%$palabra%'").") > 0");
                              })
                              ->get();
      return $subcate;

    }

}
