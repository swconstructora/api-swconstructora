<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FacturaProveedor extends Model
{
  //referenciar tabla con la clase
  protected $table = 'FACTURA_PROVEEDOR';
  protected $primaryKey = 'ID_FACT';
  // desactiva el metodo de la fecha de creacion de laravel
  public $timestamps = false;

  public $casts = ['APLICA_IVA' => 'boolean'];

  public function detalles() {
    return $this->hasMany(DetalleFacturaProveedor::class, 'ID_FACT_PROV', 'ID_FACT')->with(['detalle_cotizacion','material']);
  }

  public function ordenDist() {
    return $this->hasMany(DetalleFacturaProveedor::class, 'ID_FACT_PROV', 'ID_FACT')->distinct('ID_ORDENC');
  }

  public function empresa() {
    return $this->hasOne(Empresa::class, 'ID_EMPR', 'ID_EMPR');
  }

  public function notas_credito() {
    return $this->hasMany(NotaCredito::class, 'FACTURA_PROVEEDOR_ID_FACT', 'ID_FACT');
  }
}
