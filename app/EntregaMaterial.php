<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EntregaMaterial extends Model
{
  //referenciar tabla con la clase
  protected $table = 'ENTREGA_MATERIAL';
  protected $primaryKey = 'ID_ENTRE';
  // desactiva el metodo de la fecha de creacion de laravel
  public $timestamps = false;

  public function detalle() {
    return $this->belongsToMany(InventarioProyecto::class, 'DETALLE_INVENTARIO_ENTREGA', 'ENTREGA_MATERIAL_ID_ENTRE','INVENTARIO_PROY_ID_INVENT_PROY')
      ->withPivot('CANT_ENTREGADA');
  }
}
