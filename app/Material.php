<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Material extends Model
{
    //referenciar tabla con la clase
    protected $table = 'MATERIAL';
    protected $primaryKey = 'ID_MATE';
    // desactiva el metodo de la fecha de creacion de laravel
    public $timestamps = false;

    public function Unidad() {
      return $this->hasOne(UnidadMedida::class, 'ID_UNID', 'UNIDAD_MEDIDA_ID_UNID');
    }

    public function Subcategoria() {
      return $this->hasOne(Subcategoria::class, 'ID_SUBC', 'SUBCATEGORIA_ID_SUBC');
    }
}
