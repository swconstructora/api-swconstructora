<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Arriendo extends Model
{
    //referenciar tabla con la clase
    protected $table = 'ARRIENDO';
    protected $primaryKey = 'ID_ARRIEN';
    // desactiva el metodo de la fecha de creacion de laravel
    public $timestamps = false;

    public function Subcategoria() {
      return $this->hasOne(Subcategoria::class, 'ID_SUBC', 'SUBCATEGORIA_ID_SUBC');
    }
}
