<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class DUI extends Model
{
  //referenciar tabla con la clase
  protected $table = 'DUI';
  protected $primaryKey = 'ID_DUI';
  // desactiva el metodo de la fecha de creacion de laravel
  public $timestamps = false;

  public function detalle() {
    return $this->belongsToMany(InventarioProyecto::class, 'DETALLE_DUI', 'DUI_ID_DUI', 'INVENTARIO_PROY_ID_INVENT_PROY')
      ->select(DB::raw('*, null as cantidad_entregar,  null as cantidad_recibir'))->withPivot('CANT_SOLICITADA','PREC_PROM_COTI','CANT_RECEPCIONADA')->with('material');
  }

  public function proyectoE() {
    return $this->hasOne(Proyecto::class, 'ID_PROY', 'PROYECTO_ID_PROY_E');
  }

  public function proyectoR() {
    return $this->hasOne(Proyecto::class, 'ID_PROY', 'PROYECTO_ID_PROY_R');
  }

  public function guiaDespacho() {
    return $this->hasOne(GuiaDespacho::class, 'DUI_ID_DUI', 'ID_DUI');
  }

  public function entrega() {
    return $this->hasOne(EntregaMaterial::class, 'DUI_ID_DUI', 'ID_DUI')
      ->with('detalle');
  }

  public function recepcion() {
    return $this->hasOne(RecepcionMaterial::class, 'NRO_ID', 'ID_DUI');
  }

  public function flujo() {
    return $this->hasMany(FlujoDUI::class, 'DUI_ID_DUI', 'ID_DUI')->with(['Empleado','Dueno']);
  }
}
