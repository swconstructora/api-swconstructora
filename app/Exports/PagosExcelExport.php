<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class PagosExcelExport implements FromView
{
    public $view;
    public $data;

    public function __construct($view, $data) {
      $this->view = $view;
      $this->data = $data;
    }
    /**
    * @return \Illuminate\Support\View
    */
    public function view(): View
    {
        return view($this->view, (array)$this->data);
    }
}
