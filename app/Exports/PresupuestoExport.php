<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;

class PresupuestoExport implements FromView, WithTitle
{
    protected $nombre;
    protected $datos;
    protected $view;

    public function __construct($nombre, $datos, $view)
    {
      $this->nombre = $nombre;
      $this->datos = $datos;
      $this->view = $view;
    }

    /**
    * @return \Illuminate\Support\View
    */
    public function view(): View
    {
      return view($this->view, (array)$this->datos);
    }

    public function title(): string
    {
      return $this->nombre;
    }
}
