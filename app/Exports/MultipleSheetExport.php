<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class MultipleSheetExport implements WithMultipleSheets
{
  use Exportable;

  protected $datos;

  public function __construct(array $datos)
  {
    $this->datos = $datos;
  }
  public function sheets(): array
  {
    $sheets = [];

    foreach ($this->datos as $key => $dato) {
      $sheets[] = new PresupuestoExport($dato->nombre, $dato->datos, $dato->vista);
    }

    return $sheets;
  }
}
