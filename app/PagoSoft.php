<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PagoSoft extends Model
{
  //referenciar tabla con la clase
  protected $table = 'PAGO_SOFT';
  protected $primaryKey = 'ID_PAGO_SOFT';
  // desactiva el metodo de la fecha de creacion de laravel
  public $timestamps = false;

  public function detalle_pago() {
    return $this->hasMany(DetallePagoSoft::class ,'PAGO_SOFT_ID_PAGO_SOFT', 'ID_PAGO_SOFT')->with('Proyecto');
  }
}
