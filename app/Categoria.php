<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    //referenciar tabla con la clase
    protected $table = 'CATEGORIA';
    protected $primaryKey = 'ID_CATE';
    // desactiva el metodo de la fecha de creacion de laravel
    public $timestamps = false;

    public function subcategorias(){
        return $this->hasMany(Subcategoria::class,'CATEGORIA_ID_CATE','ID_CATE')->orderBy('NOMBRE_SUBC');
    }
}
