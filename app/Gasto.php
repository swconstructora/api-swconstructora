<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gasto extends Model
{
    //referenciar tabla con la clase
    protected $table = 'GASTO';
    protected $primaryKey = 'ID_GASTO';
    // desactiva el metodo de la fecha de creacion de laravel
    public $timestamps = false;


}
