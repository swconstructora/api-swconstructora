<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FlujoOrdenCompra extends Model
{
  //referenciar tabla con la clase
  protected $table = 'FLUJO_ORDEN';
  protected $primaryKey = 'ID_FLUJO';
  // desactiva el metodo de la fecha de creacion de laravel
  public $timestamps = false;

  public function Empleado() {
    return $this->belongsToMany(Empleado::class, 'USUARIO','ID_USU','ID_USU','USUARIO_ID_USU','USUARIO_ID_USU')
      ->with('cargo');
  }

  public function Dueno() {
    return $this->belongsToMany(Dueno::class, 'USUARIO','ID_USU','ID_USU','USUARIO_ID_USU','USUARIO_ID_USU')
      ->with('cargo');
  }

  public function usuario() {
    return $this->hasOne(Usuario::class, 'ID_USU', 'USUARIO_ID_USU');
  }
}
