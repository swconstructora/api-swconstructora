<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subcontratar extends Model
{
    //referenciar tabla con la clase
    protected $table = 'SUBCONTRATAR';
    protected $primaryKey = 'ID_SUBCON';
    // desactiva el metodo de la fecha de creacion de laravel
    public $timestamps = false;

    public function pagos(){
        return $this->hasMany(Pago::class,'SUBCONTRATAR_ID_SUBCON')->with('flujos')->with(['notas_credito','detalle_pago_arriendo']);
    }

}
