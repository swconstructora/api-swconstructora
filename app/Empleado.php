<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empleado extends Model
{
    //referenciar tabla con la clase
    protected $table = 'EMPLEADO';
    protected $primaryKey = 'ID_EMP';
    // desactiva el metodo de la fecha de creacion de laravel
    public $timestamps = false;


    public function DetalleProyecto(){
      return $this->belongsToMany(Proyecto::class,'DETALLE_EMP_PROY','EMPLEADO_ID_EMP','PROYECTO_ID_PROY')
        ->withPivot('ID_ASIGNADOR','MONTO_LIMITE','HORAS_LIMITE','RECIBE_FACTURA','MONTO_ILIMITADO','HORA_ILIMITADA');
    }

    public function cargo(){
      return $this->hasOne(Cargo::class,'ID_CARG','CARGO_ID_CARG');
    }

    public function constructora(){
      return $this->hasOne(Constructora::class,'ID_CONS','CONSTRUCTORA_ID_CONS');
    }

    public function usuario() {
      return $this->hasOne(Usuario::class, 'ID_USU', 'USUARIO_ID_USU');
    }
}
