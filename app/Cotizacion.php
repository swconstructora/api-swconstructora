<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cotizacion extends Model
{
    //referenciar tabla con la clase
    protected $table = 'COTIZACION';
    protected $primaryKey = 'ID_COTI';
    // desactiva el metodo de la fecha de creacion de laravel
    public $timestamps = false;

    public function DetalleCotizacion(){
        return $this->belongsToMany(NotaPedido::class,'DETALLE_COTIZACION','COTIZACION_ID_COTI','NOTA_PEDIDO_ID_NOTA_PED')
                                    ->withPivot('CANTIDAD_ENTREGAR_DETCO')->withPivot('PRECIO_NETO_DETCO')
                                    ->withPivot('FECHA_RECEPCION_DETCO')->withPivot('ESTADO_COTIZACION');
    }

    public function detalle() {
      return $this->hasMany(DetalleCotizacion::class,'COTIZACION_ID_COTI', 'ID_COTI')
        ->with(['Material','NotaPedido']);
    }

    public function proveedor() {
      return $this->hasOne(Empresa::class, 'ID_EMPR', 'EMPRESA_ID_EMPR');
    }

}
