<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetallePagoArriendo extends Model
{
    //referenciar tabla con la clase
    protected $table = 'DETALLE_PAGO_ARRIENDO';
    protected $primaryKey = 'ID_DET_PAGO_ARRIEN';
    // desactiva el metodo de la fecha de creacion de laravel
    public $timestamps = false;
}
