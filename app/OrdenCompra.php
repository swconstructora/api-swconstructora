<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class OrdenCompra extends Model
{
    protected $table = 'ORDEN_COMPRA';
    protected $primaryKey = 'ID_ORDENC';

    public $timestamps = false;

    public function detalle() {
      return $this->belongsToMany(DetalleCotizacion::class, 'DETALLE_ORDEN_COTIZACION','ORDEN_COMPRA_ID_ORDENC','DETALLE_COTIZACION_ID_DETA_COTI')
        ->with(['Material','NotaPedido','Empresa','Cotizacion']);
    }

    public function detalle_valor() {
      return $this->belongsToMany(DetalleCotizacion::class, 'DETALLE_ORDEN_COTIZACION','ORDEN_COMPRA_ID_ORDENC','DETALLE_COTIZACION_ID_DETA_COTI');
    }

    public function detalle_devolucion() {
      return $this->belongsToMany(DetalleCotizacion::class, 'DETALLE_ORDEN_COTIZACION','ORDEN_COMPRA_ID_ORDENC','DETALLE_COTIZACION_ID_DETA_COTI')
        ->select(DB::raw('*, null as cantidad_devolver'))->with(['Material']);
    }

    public function detalleFecha() {
      return $this->belongsToMany(DetalleCotizacion::class, 'DETALLE_ORDEN_COTIZACION','ORDEN_COMPRA_ID_ORDENC','DETALLE_COTIZACION_ID_DETA_COTI')
        ->selectRaw('MIN(DETALLE_COTIZACION.FECHA_RECEPCION_DETCO) as FECHA_RECEPCION_DETCO')
        ->groupBy('DETALLE_ORDEN_COTIZACION.ORDEN_COMPRA_ID_ORDENC');
    }

    public function empresa() {
      return $this->hasOne(Empresa::class, 'ID_EMPR', 'ID_EMPR');
    }
    public function pago() {
      return $this->hasOne(Pago::class, 'ORDEN_COMPRA_ID_ORDENC', 'ID_ORDENC')->with('flujos')->with('notas_credito');
    }

    public function proyecto() {
      return $this->hasOne(Proyecto::class, 'ID_PROY' ,'ID_PROY');
    }

    public function Flujo() {
      return $this->hasMany(FlujoOrdenCompra::class, 'ORDEN_COMPRA_ID_ORDENC', 'ID_ORDENC')
        ->orderBy('ID_FLUJO','ASC')->with(['Empleado','Dueno']);
    }

    public function cfacturas() {
      return $this->hasMany(DetalleFacturaProveedor::class, 'ID_ORDENC', 'ID_ORDENC')
        ->distinct('PAGO_ID_PAGO');
    }

    public function facturas() {
      return $this->hasMany(DetalleFacturaProveedor::class, 'ID_ORDENC', 'ID_ORDENC');
    }
}
