<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subarea extends Model
{
    //referenciar tabla con la clase
    protected $table = 'SUBAREA';
    protected $primaryKey = 'ID_SAREA';
    // desactiva el metodo de la fecha de creacion de laravel
    public $timestamps = false;

    public function area(){
      return $this->hasOne(Area::class,'ID_AREA','AREA_ID_AREA');
    }

    public function countEntregas() {
      return $this->hasMany(EntregaMaterial::class, 'SUBAREA_ID_SAREA', 'ID_SAREA')->count();
    }

    public function countNotas() {
      return $this->belongsToMany(NotaPedido::class, 'DETALLE_NOTA_PEDIDO', 'SUBAREA_ID_SAREA', 'NOTA_PEDIDO_ID_NOTA_PED')->count();
    }
}
