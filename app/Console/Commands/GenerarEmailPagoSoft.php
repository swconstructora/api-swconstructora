<?php

namespace App\Console\Commands;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\PagoSoft;
use App\Constructora;
use App\DetallePagoSoft;
use Mail;
class GenerarEmailPagoSoft extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generar:emailpagosoft';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Email que avisa al cliente que tiene un pago pendiente';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    date_default_timezone_set('America/Santiago');
      Log::info('1. Envio de email: Ya puedes pagar Budgeter'. date('Y-m-d H:i:s'));
      $pagos_soft = PagoSoft::join('CONSTRUCTORA','CONSTRUCTORA.ID_CONS', 'PAGO_SOFT.CONSTRUCTORA_ID_CONS')
                              ->join('DUENO','DUENO.ID_DUE', 'CONSTRUCTORA.DUENO_ID_DUE')
                              ->join('USUARIO','USUARIO.ID_USU', 'DUENO.USUARIO_ID_USU')
                              ->where('ESTADO_PAGO','P')->get();
      Log::info('2. Pagos obtenidos:'.$pagos_soft);
      $monto_uf_aplicado = 0;
      $link = app('App\Entorno')->route_web.'app/cuenta/facturacion';

      foreach ($pagos_soft as $key => $pago) {
        // Obtener dia de pago, y validar que sea mismo dia y mes
        $dia_actual = date('d-m');
        $fecha_fp = $pago->FECHA_EMITIDO;
        $dia_pago = date("d-m", strtotime($fecha_fp));
        if($dia_actual == $dia_pago){

          $pago->detalle =  DetallePagoSoft::where('PAGO_SOFT_ID_PAGO_SOFT', $pago->ID_PAGO_SOFT)->orderBy('ID_DETA_PAGO_SOFT', 'DESC')->first();
          $monto_uf_aplicado = $pago->detalle->MONTO_UF_APLICADO;
          $email = $pago->EMAIL_USU;
          $nombre = $pago->NOMBRE_DUE;
          $fecha_vencimiento = $pago->FECHA_VENCIMIENTO;
          $num_activos = $pago->PROYECTOS_ACTIVOS;
          $monto_pagar = $pago->MONTO_PAGADO;
          $interval_dias = date_diff(date_create($pago->FECHA_EMITIDO), date_create($pago->FECHA_VENCIMIENTO));
          $dias_vencimiento = $interval_dias->days;

          $data = array('link'=>$link,'nombre'=>$nombre,'monto_uf'=>number_format($monto_uf_aplicado, 2, ',', '.'), 'monto_pagar'=>number_format($monto_pagar, 0, '', '.'),
          'fecha_vencimiento'=> date("d-m-Y", strtotime($fecha_vencimiento)), 'num_activos'=>$num_activos, 'dias_vencimiento'=>$dias_vencimiento);

            // return response()->json(['code' => 200, 'message' => 'Datos email', 'data'=>$data],200);

            Mail::send('pago-pendiente-generado', $data, function($message) use ($email, $nombre) {
               $message->to($email, $nombre)->subject('Ya puedes pagar Budgeter');
               $message->from('no-reply@budgeter.cl','Equipo Budgeter');
            });

        }

      }
        Log::info('3. Ciclo de envios finalizado con éxito');
    }
}
