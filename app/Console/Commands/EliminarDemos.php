<?php

namespace App\Console\Commands;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\FacturacionCons;
use App\ActivoProyecto;
use App\Proyecto;
use App\Constructora;
use App\Dueno;
use App\Usuario;
use App\Cargo;
class EliminarDemos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'eliminar:demos';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Elimina los usuarios demos y sus registros';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      date_default_timezone_set('America/Santiago');
      Log::info('1. Eliminación de demos '. date('Y-m-d H:i:s'));
      // Eliminar ACTIVO_PROYECTO; PROYECTO, FACTURACION_CONS, CONSTRUCTORA, DUEÑO, USUARIO, CARGO y MODULOS CARGO según ES_DEMO ='S'
      $usuarios_demo = Usuario::where('ES_DEMO_USU', 'S')->get();
      foreach ($usuarios_demo as $key => $usuario) {

          // Eliminar dueno
        $dueno = Dueno::where('USUARIO_ID_USU', $usuario->ID_USU)->first();
        $constructoras = Constructora::where('DUENO_ID_DUE', $dueno->ID_DUE)->get();
            foreach ($constructoras as $key => $cons) {

              $proyectos = Proyecto::where('CONSTRUCTORA_ID_CONS', $cons->ID_CONS)->get();
              foreach ($proyectos as $key => $proy) {
                $activo_proyecto = ActivoProyecto::where('PROYECTO_ID_PROY', $proy->ID_PROY)->delete();
              }
              $facturacion_cons = FacturacionCons::where('CONSTRUCTORA_ID_CONS', $cons->ID_CONS)->delete();
              $cargos = Cargo::where('CONSTRUCTORA_ID_CONS', $cons->ID_CONS)->delete();
              $proyectos = Proyecto::where('CONSTRUCTORA_ID_CONS', $cons->ID_CONS)->delete();
          }

        $constructoras = Constructora::where('DUENO_ID_DUE', $dueno->ID_DUE)->delete();
        $dueno = Dueno::where('USUARIO_ID_USU', $usuario->ID_USU)->delete();
      }

      $usuarios_demo = Usuario::where('ES_DEMO_USU', 'S')->delete();
      Log::info('2. Demos eliminados con éxito '. date('Y-m-d H:i:s').'Objeto:'.$usuarios_demo);


    }
}
