<?php

namespace App\Console\Commands;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\PagoSoft;
use App\FacturacionCons;
use App\ActivoProyecto;
use App\Proyecto;
use App\Constructora;
use App\DetallePagoSoft;
class GenerarPagoSoft extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generar:pagosoft';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Genera el pago al cliente según su fecha de facturación';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    date_default_timezone_set('America/Santiago');
      Log::info('1. Ejecución de Pago_Soft Diario '. date('Y-m-d H:i:s'));
      $valor_2uf_iva = app('App\Http\Controllers\PagoSoftController')->getCobroUF();
    if($valor_2uf_iva[2]){
      Log::info('2. Valor UF:'.$valor_2uf_iva[3]);

       $constructoras = Constructora::join('FACTURACION_CONS','FACTURACION_CONS.CONSTRUCTORA_ID_CONS', 'CONSTRUCTORA.ID_CONS')->get();
      foreach ($constructoras as $key => $cons) {
              $ID_CONS = $cons->ID_CONS;
              // Obtener dia de pago
              $dia_actual = date('d');
              $fecha_fp = $cons->FECHA_FACT_PAGO;
              $dia_pago = date("d", strtotime($fecha_fp));
              if($dia_actual == $dia_pago){
                  Log::info('Constructura:'.$cons->ID_CONS.'-'.$cons->NOMBRE_CONS);
                  // Generar pago pendiente
                  $pago = new PagoSoft();
                  $pago->NUM_PAGO = (PagoSoft::where('CONSTRUCTORA_ID_CONS', $ID_CONS)->orderBy('NUM_PAGO', 'desc')->value("NUM_PAGO")?:0)+1;
                  $pago->FECHA_EMITIDO = date('Y-m-d H:i:s');
                  $pago->FECHA_VENCIMIENTO = date('Y-m-d H:i:s',strtotime('+1 months'));
                  $pago->CONSTRUCTORA_ID_CONS = $ID_CONS;
                  $pago->ESTADO_PAGO = 'P';
                  $pago->save(); //Se guarda antes para tener ID

                  $rango_inicio = date('Y-m-d 23:59:59', strtotime('-1 months'));
                  $rango_fin = date("Y-m-d 00:00:00");
                  $interval_dias = date_diff(date_create($rango_inicio), date_create($rango_fin));

                    // $num_dias_mes = date('t'); Valor uf + IVA por 2
                    $num_dias_rango = $interval_dias->days;
                    $valor_dia_rango = $valor_2uf_iva[2]/$num_dias_rango;

                    $TOTAL_PAGAR_CONS = 0;
                    $TOTAL_ACTIVOS_CONS = 0; //Total activos almenos un dia
                    // Obtener dias que estuvieron activos los proyectos por constructora
                    $proyectos = Proyecto::where('CONSTRUCTORA_ID_CONS',$ID_CONS)->get();
                    Log::info($cons->ID_CONS.', num_dias_rango:'.$num_dias_rango.', valor_dia:'.$valor_dia_rango);
                    foreach ($proyectos as $key => $proy) {
                      Log::info($cons->ID_CONS.', Proyecto:'.$proy->ID_PROY.'-'.$proy->NOMBRE_PROY);

                      $proy->DIAS_ACTIVO = ActivoProyecto::where('PROYECTO_ID_PROY',$proy->ID_PROY)
                                ->whereBetween('FECHA_ACTIVO',[$rango_inicio,$rango_fin])
                                ->sum('DIAS_ACTIVO');

                      // Cuando se mantiene activo sin cerrarlo en el periodo
                      $proy->DIAS_SIN_CERRAR = 0;
                      // Caso 1 dentro del periodo
                      $ultimo = ActivoProyecto::where('PROYECTO_ID_PROY',$proy->ID_PROY)->whereBetween('FECHA_ACTIVO',[$rango_inicio,$rango_fin])
                                  ->orderBy('FECHA_ACTIVO', 'desc')->first();
                      Log::info($cons->ID_CONS.','.$proy->ID_PROY.', objeto_ultimo:'.$ultimo);
                      if($ultimo){
                        if($ultimo->FECHA_CERRADO == null){
                          $cerrar_hoy = date('Y-m-d H:i:s');
                          $interval = date_diff(date_create($ultimo->FECHA_ACTIVO), date_create($cerrar_hoy));
                          $proy->DIAS_SIN_CERRAR = $interval->days;
                        }
                      } else{
                          // Caso 2 no ha cerrado el proyecto en el periodo
                          $preguntar = ActivoProyecto::where('PROYECTO_ID_PROY',$proy->ID_PROY)->orderBy('FECHA_ACTIVO', 'desc')->first();
                          Log::info($cons->ID_CONS.','.$proy->ID_PROY.', objeto_preguntar:'.$preguntar);
                          // Validar si lo cerro anteriormente o no
                          if($preguntar){
                            if($preguntar->FECHA_CERRADO == null){
                              // No esta cerrado
                                $proy->DIAS_SIN_CERRAR = $num_dias_rango;
                            } else{
                              // Esta cerrado
                                $proy->DIAS_SIN_CERRAR = 0;
                            }

                          }

                      }
                      Log::info($cons->ID_CONS.','.$proy->ID_PROY.', dias_activo:'.$proy->DIAS_ACTIVO);
                      Log::info($cons->ID_CONS.','.$proy->ID_PROY.', dias_sin_cerrar:'.$proy->DIAS_SIN_CERRAR);
                      // Total pagar
                      $proy->TOTAL_DIAS = $proy->DIAS_ACTIVO+$proy->DIAS_SIN_CERRAR;
                      $proy->TOTAL_VALOR_ACTIVO = $valor_dia_rango * $proy->TOTAL_DIAS;
                      $TOTAL_PAGAR_CONS = $TOTAL_PAGAR_CONS + $proy->TOTAL_VALOR_ACTIVO;
                      Log::info($cons->ID_CONS.','.$proy->ID_PROY.', total_proy_pagar:'.$proy->TOTAL_VALOR_ACTIVO);
                      //Total activos almenos un dia
                      if($proy->TOTAL_DIAS > 0){
                        $TOTAL_ACTIVOS_CONS = $TOTAL_ACTIVOS_CONS+1;
                        // Guardar detalles por proyecto activo
                        $deta_pago_soft = new DetallePagoSoft();
                        $deta_pago_soft->PAGO_SOFT_ID_PAGO_SOFT = $pago->ID_PAGO_SOFT;
                        $deta_pago_soft->PROYECTO_ID_PROY = $proy->ID_PROY;
                        $deta_pago_soft->MONTO_TOTAL_APLICADO = $valor_2uf_iva[2];
                        $deta_pago_soft->MONTO_UF_APLICADO = $valor_2uf_iva[3];
                        $deta_pago_soft->MONTO_COBRADO = $proy->TOTAL_VALOR_ACTIVO;
                        $deta_pago_soft->UF_COBRADO = $valor_2uf_iva[1];
                        $deta_pago_soft->DIAS_ACTIVO = $proy->TOTAL_DIAS;
                        $deta_pago_soft->save();
                      }
                    }
                    $pago->MONTO_PAGADO = $TOTAL_PAGAR_CONS;
                    Log::info($cons->ID_CONS.', total_pagar:'.$TOTAL_PAGAR_CONS);

                    $pago->PROYECTOS_ACTIVOS = $TOTAL_ACTIVOS_CONS;
                    if($TOTAL_ACTIVOS_CONS > 0){
                          $pago->save();
                    } else{
                        $pago->delete();
                    }


                    // return response()->json(['code' => 200, 'message' => 'Pendientes obtenidos', 'dias_rango'=>$num_dias_rango,'pago'=>$pago,
                    // 'valor_2uf_iva'=>$valor_2uf_iva[2],'valor_dia_mes'=>$valor_dia_rango,
                    // 'proyectos'=>$proyectos,'TOTAL_PAGAR_CONS'=>$TOTAL_PAGAR_CONS,'TOTAL_ACTIVOS_CONS'=>$TOTAL_ACTIVOS_CONS],200);



                }
              // return response()->json(['code' => 200, 'message' => 'No se pudo obtener el valor UF'],200);

              }

        }
        // return response()->json(['code' => 200, 'message' => 'Pagos de software generados'],200);


    }
}
