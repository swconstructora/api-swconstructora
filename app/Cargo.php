<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cargo extends Model
{
    //referenciar tabla con la clase
    protected $table = 'CARGO';
    protected $primaryKey = 'ID_CARG';
    // desactiva el metodo de la fecha de creacion de laravel
    public $timestamps = false;
    protected $hidden = ['pivot'];

    protected $casts = [
      'ES_DUENO' => 'boolean',
      'NIVEL_CARG' =>'integer',
      'CARGO_AUTORIZA_ID_CARG' => 'integer'
    ];

    public function jefe() {
      return $this->hasOne(Cargo::class, 'ID_CARG', 'CARGO_AUTORIZA_ID_CARG');
    }

    public function subordinados() {
      return $this->hasMany(Cargo::class, 'CARGO_AUTORIZA_ID_CARG', 'ID_CARG');
    }

    public function modulos() {
      return $this->belongsToMany(Modulo::class, 'DETALLE_MODULO', 'CARGO_ID_CARG', 'MODULO_ID_MOD');
    }

    public function modulosC() {
      return $this->belongsToMany(Modulo::class, 'DETALLE_MODULO', 'CARGO_ID_CARG', 'MODULO_ID_MOD')
        ->using(CustomPivotModulo::class)->where('TIPO_MOD','C')->withPivot('PUEDE_EDITAR','PUEDE_VER');
    }

    public function modulosR() {
      return $this->belongsToMany(Modulo::class, 'DETALLE_MODULO', 'CARGO_ID_CARG', 'MODULO_ID_MOD')
        ->using(CustomPivotModulo::class)->where('TIPO_MOD','P')->withPivot('PUEDE_EDITAR','PUEDE_VER')->with('hijosC');
    }

    public function modulosP() {
      return $this->belongsToMany(Modulo::class, 'DETALLE_MODULO', 'CARGO_ID_CARG', 'MODULO_ID_MOD')
        ->using(CustomPivotModulo::class)->where('TIPO_MOD','P')->withPivot('PUEDE_EDITAR','PUEDE_VER');
    }

    public function empleados() {
      return $this->hasMany(Empleado::class, 'CARGO_ID_CARG', 'ID_CARG');
    }

    public function dueno() {
      return $this->hasOne(Dueno::class, 'CARGO_ID_CARG', 'ID_CARG');
    }

}
