<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Constructora extends Model
{
    //referenciar tabla con la clase
    protected $table = 'CONSTRUCTORA';
    protected $primaryKey = 'ID_CONS';
    // desactiva el metodo de la fecha de creacion de laravel
    public $timestamps = false;

    public function facturacion() {
      return $this->hasOne(FacturacionCons::class, 'CONSTRUCTORA_ID_CONS', 'ID_CONS');
    }

    public function dueno() {
      return $this->hasOne(Dueno::class, 'ID_DUE', 'DUENO_ID_DUE');
    }

}
