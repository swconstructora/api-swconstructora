<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FacturacionCons extends Model
{
  //referenciar tabla con la clase
  protected $table = 'FACTURACION_CONS';
  protected $primaryKey = 'ID_FACT_CONS';
  // desactiva el metodo de la fecha de creacion de laravel
  public $timestamps = false;
}
