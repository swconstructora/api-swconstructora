<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Merma extends Model
{
  //referenciar tabla con la clase
  protected $table = 'MERMA';
  protected $primaryKey = 'ID_MERMA';
  // desactiva el metodo de la fecha de creacion de laravel
  public $timestamps = false;

  public function articulo() {
    return $this->hasOne(InventarioProyecto::class, 'ID_INVENT_PROY', 'INVENTARIO_PROY_ID_INVENT_PROY')
      ->with('material');
  }
}
