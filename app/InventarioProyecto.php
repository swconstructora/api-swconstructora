<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InventarioProyecto extends Model
{
  //referenciar tabla con la clase
  protected $table = 'INVENTARIO_PROY';
  protected $primaryKey = 'ID_INVENT_PROY';
  // desactiva el metodo de la fecha de creacion de laravel
  public $timestamps = false;

  public function detalleOC() {
    return $this->belongsToMany(DetalleCotizacion::class, 'DETALLE_INVENTARIO_OC', 'INVENTARIO_PROY_ID_INVENT_PROY', 'DETALLE_COTIZACION_ID_DETA_COTI')
      ->withPivot('COTIZACION_USADA','CANTIDAD_USADA');
  }

  public function detalleOCEntrega() {
    return $this->belongsToMany(DetalleCotizacion::class, 'DETALLE_INVENTARIO_OC', 'INVENTARIO_PROY_ID_INVENT_PROY', 'DETALLE_COTIZACION_ID_DETA_COTI')
      ->withPivot('COTIZACION_USADA','CANTIDAD_USADA')->wherePivot('COTIZACION_USADA',0);
  }

  public function detalleDUI() {
    return $this->belongsToMany(DetalleDUI::class, 'DETALLE_INVENTARIO_DUI', 'INVENTARIO_ID_INVENT_PROY', 'DETALLE_DUI_ID_DETA_DUI')
      ->withPivot('DUI_UTILIZADA','CANTIDAD_USADA');
  }

  public function material() {
    return $this->hasOne(Material::class, 'ID_MATE', 'MATERIAL_ID_MATE')->with(['Unidad','Subcategoria']);
  }

  public function detalleInvExt() {
    return $this->belongsToMany(InventarioExtra::class, 'DETALLE_INVENTARIO_EXTRA', 'INVENTARIO_PROY_ID_INVENT_PROY', 'INVENTARIO_EXTRA_ID_INVENT_EXT')
      ->withPivot('INVENT_EXT_USADO','CANTIDAD_USADA')->orderBy('FECHA_CREACION','asc');
  }
}
