<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pago extends Model
{
    //referenciar tabla con la clase
    protected $table = 'PAGO';
    protected $primaryKey = 'ID_PAGO';
    // desactiva el metodo de la fecha de creacion de laravel
    public $timestamps = false;

    public function flujos(){
        return $this->hasMany(FlujoPago::class,'PAGO_ID_PAGO')->with(['Empleado','Dueno']);
    }
    public function notas_credito(){
        return $this->hasMany(NotaCredito::class,'PAGO_ID_PAGO');
    }
    public function detalles() {
      return $this->hasMany(DetalleFacturaProveedor::class,'PAGO_ID_PAGO')->with(['detalle_cotizacion','material']);
    }
    public function ordenDist() {
      return $this->hasMany(DetalleFacturaProveedor::class, 'PAGO_ID_PAGO', 'ID_PAGO')->distinct('ID_ORDENC');
    }

    public function empresa() {
      return $this->hasOne(Empresa::class, 'ID_EMPR', 'EMPRESA_ID_EMPR');
    }
    public function detalle_pago_arriendo() {
      return $this->belongsToMany(DetalleArriendo::class, 'DETALLE_PAGO_ARRIENDO', 'PAGO_ID_PAGO', 'DETALLE_ARRIENDO_ID_DET_ARRIEN')
        ->withPivot('MONTO_PAGADO')->with('arriendo');
    }
}
