<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modulo extends Model
{
  //referenciar tabla con la clase
  protected $table = 'MODULO';
  protected $primaryKey = 'ID_MOD';
  // desactiva el metodo de la fecha de creacion de laravel
  public $timestamps = false;

  protected $casts = [
    'EDITAR_MOD' => 'boolean',
    'VER_MOD' => 'boolean'
  ];

  public function hijosC() {
    return $this->belongsToMany(Modulo::class, 'DETALLE_MODULO_PADRE', 'MODULO_PADRE_ID', 'MODULO_HIJO_ID')
      ->using(CustomPivotModulo::class)->withPivot('PUEDE_EDITAR','PUEDE_VER','CARGO_ID_CARG');
  }

  public function hijosR() {
    return $this->hasMany(Modulo::class, 'MODULO_ID_MOD_PADRE', 'ID_MOD');
  }
}
