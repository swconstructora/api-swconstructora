<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleCotizacion extends Model
{
    protected $table = 'DETALLE_COTIZACION';
    protected $primaryKey = 'ID_DETA_COTI';
    public $timestamps = false;

    public function Empresa() {
      return $this->belongsToMany(Empresa::class, 'COTIZACION', 'ID_COTI', 'EMPRESA_ID_EMPR', 'COTIZACION_ID_COTI')
        ->withPivot('ADJUNTO_URL','NOMBRE_ADJUNTO');
    }

    public function Material() {
      return $this->belongsToMany(Material::class, 'NOTA_PEDIDO', 'ID_NOTA_PED', 'MATERIAL_ID_MATE', 'NOTA_PEDIDO_ID_NOTA_PED')
        ->with(['Unidad','Subcategoria']);
    }

    public function NotaPedido() {
      return $this->hasOne(NotaPedido::class, 'ID_NOTA_PED', 'NOTA_PEDIDO_ID_NOTA_PED')
        ->with(['Flujo','DetalleCotizacionModal']);
    }

    public function Cotizacion() {
      return $this->hasOne(Cotizacion::class, 'ID_COTI', 'COTIZACION_ID_COTI')->with('proveedor');
    }
}
