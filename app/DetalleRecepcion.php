<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleRecepcion extends Model
{
    //referenciar tabla con la clase
    protected $table = 'DETALLE_RECEPCION';
    protected $primaryKey = 'ID_DET_RECE_OC';
    // desactiva el metodo de la fecha de creacion de laravel
    public $timestamps = false;

    public function detalleC() {
      return $this->hasOne(DetalleCotizacion::class, 'ID_DETA_COTI', 'ID_DETA_COTI')
        ->with(['Material','NotaPedido']);
    }

    public function detalleD() {
      return $this->hasOne(DetalleDUI::class, 'ID_DETA_DUI', 'DETALLE_DUI_ID_DETA_DUI')
        ->with(['Material']);
    }
}
