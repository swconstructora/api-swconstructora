<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleDUI extends Model
{
  //referenciar tabla con la clase
  protected $table = 'DETALLE_DUI';
  protected $primaryKey = 'ID_DETA_DUI';
  // desactiva el metodo de la fecha de creacion de laravel
  public $timestamps = false;

  public function Material() {
    return $this->belongsToMany(Material::class, 'INVENTARIO_PROY', 'ID_INVENT_PROY', 'MATERIAL_ID_MATE', 'INVENTARIO_PROY_ID_INVENT_PROY')
      ->with(['Unidad','Subcategoria']);
  }
}
