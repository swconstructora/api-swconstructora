<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banco extends Model
{
    //referenciar tabla con la clase
    protected $table = 'BANCO';
    protected $primaryKey = 'ID_BANCO';
    // desactiva el metodo de la fecha de creacion de laravel
    public $timestamps = false;

}
